<!doctype html>
<!--
    Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation

    See the NOTICE file(s) distributed with this work for additional
    information regarding copyright ownership.

    This program and the accompanying materials are made available under the terms
    of the MIT License which is available at https://opensource.org/licenses/MIT

    SPDX-License-Identifier: MIT
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module 1.2 | SBE Course</title>
        <link rel="icon" type="image/png" href="../sbe-course-favicon.png"/>

        <!-- Bootstrap. -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.0/font/bootstrap-icons.css" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

        <!-- Website styling. -->
        <link rel="stylesheet" href="../../assets/eclipse-escet.css">
        <link rel="stylesheet" href="../../assets/eclipse-escet-sbe-course.css">

        <!-- Version selector. -->
        <script src="../../assets/eclipse-escet.js"></script>

        <!-- Code block syntax highlighting. -->
        <link rel="stylesheet" href="../../assets/styles/asciidoctor-highlightjs-escet.min.css">
        <script src="../../assets/highlight.min.js"></script>
        <script src="../../assets/languages/cif.min.js"></script>
        <script src="../../assets/languages/tooldef.min.js"></script>

        <!-- Quiz. -->
        <link rel="stylesheet" href="../assets/quiz.css">
        <script src="../assets/quiz.js"></script>

        <!-- SBE course. -->
        <link rel="stylesheet" href="../assets/course.css">
        <script src="../assets/course.js"></script>
    </head>

    <body class="escet-dark">

        <!-- Top navigation bar -->
        <nav class="navbar navbar-expand-lg navbar-dark escet-bg">
            <div class="container-fluid">
                <a class="navbar-brand d-lg-none">SBE Course</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-0 mb-lg-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle escet-brand px-0" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Eclipse ESCET™ / SBE Course</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="../.."><img src="../../eclipse-escet-favicon.png" class="pe-2"> Eclipse ESCET / Project</a></li>
                                <li><a class="dropdown-item" href=".."><img src="../sbe-course-favicon.png" class="pe-2"> Eclipse ESCET / SBE Course</a></li>
                                <li><a class="dropdown-item" href="../../cif"><img src="../../cif/cif-favicon.png" class="pe-2"> Eclipse ESCET / CIF</a></li>
                                <li><a class="dropdown-item" href="../../chi"><img src="../../chi/chi-favicon.png" class="pe-2"> Eclipse ESCET / Chi</a></li>
                                <li><a class="dropdown-item" href="../../tooldef"><img src="../../tooldef/tooldef-favicon.png" class="pe-2"> Eclipse ESCET / ToolDef</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../" aria-expanded="false">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle active" aria-current="page" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 1</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
                                <li><a class="dropdown-item" href="./">Module 1: Supervisory control of discrete event systems</a></li>
                                <li><a class="dropdown-item" href="supervisory-control.html">Module 1.1: Supervisory control</a></li>
                                <li><a class="dropdown-item active" aria-current="page" href="discrete-event-systems.html">Module 1.2: Discrete event systems</a></li>
                                <li><a class="dropdown-item" href="modeling-discrete-event-systems.html">Module 1.3: Modeling discrete event systems</a></li>
                                <li><a class="dropdown-item" href="get-started-with-cif.html">Module 1.4: Get started with CIF</a></li>
                                <li><a class="dropdown-item" href="exercises.html">Module 1.5: Exercises</a></li>
                                <li><a class="dropdown-item" href="introduction-to-the-water-lock-case.html">Module 1.6: Introduction to the water lock case</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 2</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                <li><a class="dropdown-item" href="../module2">Module 2: Working with automata</a></li>
                                <li><a class="dropdown-item" href="../module2/reachability-deadlock-blocking-livelock.html">Module 2.1: Reachability, deadlock, blocking and livelock</a></li>
                                <li><a class="dropdown-item" href="../module2/non-deterministic-automata.html">Module 2.2: Non-deterministic automata</a></li>
                                <li><a class="dropdown-item" href="../module2/synchronizing-automata.html">Module 2.3: Synchronizing automata</a></li>
                                <li><a class="dropdown-item" href="../module2/shared-variables.html">Module 2.4: Shared variables</a></li>
                                <li><a class="dropdown-item" href="../module2/computing-state-spaces.html">Module 2.5: Computing state spaces</a></li>
                                <li><a class="dropdown-item" href="../module2/cif-tools-view-analyze-models.html">Module 2.6: CIF tools to view and analyze models</a></li>
                                <li><a class="dropdown-item" href="../module2/constants-and-algebraic-variables.html">Module 2.7: Constants and algebraic variables</a></li>
                                <li><a class="dropdown-item" href="../module2/exercises.html">Module 2.8: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module2/more-detailed-models-water-lock-case.html">Module 2.9: More detailed models for the water lock case</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 3</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown3">
                                <li><a class="dropdown-item" href="../module3">Module 3: Supervisory control and plant models</a></li>
                                <li><a class="dropdown-item" href="../module3/supervisory-control-loop.html">Module 3.1: Supervisory control loop</a></li>
                                <li><a class="dropdown-item" href="../module3/synthesis-guarantees.html">Module 3.2: Synthesis guarantees</a></li>
                                <li><a class="dropdown-item" href="../module3/supervisory-controller-synthesis.html">Module 3.3: Supervisory controller synthesis</a></li>
                                <li><a class="dropdown-item" href="../module3/modeling-larger-systems.html">Module 3.4: Modeling larger systems</a></li>
                                <li><a class="dropdown-item" href="../module3/exercises.html">Module 3.5: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module3/water-lock-case-plant-models.html">Module 3.6: Water lock case plant models</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 4</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown4">
                                <li><a class="dropdown-item" href="../module4">Module 4: Modeling requirements and supervisor synthesis</a></li>
                                <li><a class="dropdown-item" href="../module4/user-specified-requirements.html">Module 4.1: User-specified requirements</a></li>
                                <li><a class="dropdown-item" href="../module4/requirement-automata-and-invariants.html">Module 4.2: Requirements automata and invariants</a></li>
                                <li><a class="dropdown-item" href="../module4/synthesis-with-requirements.html">Module 4.3: Synthesis with requirements</a></li>
                                <li><a class="dropdown-item" href="../module4/modeling-for-synthesis.html">Module 4.4: Modeling for synthesis</a></li>
                                <li><a class="dropdown-item" href="../module4/performing-synthesis-with-cif.html">Module 4.5: Performing synthesis with CIF</a></li>
                                <li><a class="dropdown-item" href="../module4/introduction-to-svg-visualization.html">Module 4.6: Introduction to SVG visualization</a></li>
                                <li><a class="dropdown-item" href="../module4/exercises.html">Module 4.7: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module4/water-lock-case-requirements-and-supervisor.html">Module 4.8: Water lock case requirements and supervisor</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown5" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 5</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown5">
                                <li><a class="dropdown-item" href="../module5">Module 5: Hybrid simulation models</a></li>
                                <li><a class="dropdown-item" href="../module5/timed-and-hybrid-models.html">Module 5.1: Timed and hybrid models</a></li>
                                <li><a class="dropdown-item" href="../module5/variable-time.html">Module 5.2: Variable 'time'</a></li>
                                <li><a class="dropdown-item" href="../module5/continuous-variables.html">Module 5.3: Continuous variables</a></li>
                                <li><a class="dropdown-item" href="../module5/urgency.html">Module 5.4: Urgency</a></li>
                                <li><a class="dropdown-item" href="../module5/tau-event.html">Module 5.5: The 'tau' event</a></li>
                                <li><a class="dropdown-item" href="../module5/exercises.html">Module 5.6: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module5/water-lock-case-hybrid-model.html">Module 5.7: Water lock case hybrid model</a></li>
                                <li><a class="dropdown-item" href="../module5/course-conclusion.html">Module 5.8: Course conclusion</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Intro -->
        <div id="intro" class="position-relative p-3 text-center escet-bg escet-bg-opacity-125">
            <div class="clearfix mb-4 mb-md-0">
                <span class="float-end">
                    Version:
                    <div class="dropdown d-inline-block">
                        <a class="dropdown-toggle" href="#" role="button" id="versionDropdown" data-bs-toggle="dropdown" aria-expanded="false">@@ESCET-VERSION-ENDUSER@@</a>
                        <ul id="versions-dropdown" class="dropdown-menu dropdown-menu-end" aria-labelledby="versionDropdown" data-this-version="/@@ESCET-DEPLOY-FOLDER-NAME@@">
                            <li><h6 id="versions-previews" class="dropdown-header">Previews</h6></li>
                            <li><h6 id="versions-latest-releases" class="dropdown-header">Latest releases</h6></li>
                            <li><h6 class="dropdown-header">Other</h6></li>
                            <li><a class="dropdown-item" href="../../versions.html">All versions...</a></li>
                        </ul>
                    </div>
                    <a class="text-nowrap ms-3" href="../../release-notes.html">[release notes]</a>
                </span>
            </div>
        </div>

        <!-- Content -->
        <div class="px-4 pt-5">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-xl-8 col-xxl-6" id="content">

                    <h1>Module 1.2: Discrete event systems</h1>
                    <p>
                        An essential ingredient of synthesis-based engineering is the synthesis of a supervisory controller model.
                        The synthesis procedure allows a computer to automatically compute a supervisory controller model, from a model of the plant (uncontrolled system) and a model of the requirements.
                        The synthesis procedure requires that the cyber-physical system that is to be controlled can be seen as a <em>discrete event system</em> (DES).
                        The plant model can then be modeled as a discrete event model.
                    </p>

                    <h2>Discrete event system</h2>
                    <p>
                        A discrete event system is a system that can be defined by a discrete set of all its possible <em>states</em>.
                        The kind of cyber-physical systems that we consider in this course, such as water locks, bridges and X-ray machines, can all be characterized as discrete event systems.
                        For instance, a bridge may have a bridge deck, which may be in various discrete states, as it is either fully opened, partially opened/closed, or fully closed.
                        And opening and closing the bridge likely requires a motor, which may be either on or off.
                        The states are usually named.
                        For instance, we may name the states of the bridge deck as <code>deck_is_open</code>, <code>deck_is_partially_open</code> and <code>deck_is_closed</code>, and the states of the motor as <code>motor_is_on</code> and <code>motor_is_off</code>.
                    </p>
                    <p>
                        A discrete event system can <em>transition</em> from one state to another.
                        For instance, a bridge deck that is opened, may be closed to allow cars to pass over the bridge.
                        The motor is turned on first, making the motor transition from its off to its on state.
                        The bridge deck then transitions from its fully opened state to its partially opened/closed state, and ultimately to its fully closed state.
                        Then, the motor is turned off again, making it transition from its on state back to its off state.
                        In discrete event systems, these transitions from one state to another occur at discrete points in time.
                    </p>
                    <p>
                        Each transition has an associated <em>event</em> that indicates a discrete change in the system.
                        Events should be thought of as instantaneous changes.
                        An event can represent different things.
                        It may for instance represent a specific action like pressing a button, or a spontaneous occurrence dictated by mere chance like the breaking down of a machine.
                        It may also represent conditions that are suddenly met, like temperature reaching a certain threshold.
                        Events are the driving factors that can lead to changes in state.
                        For instance, the motor has two events, since it can be turned on and off.
                        And the bridge deck has three events, as it can become fully opened, partially opened/closed, and fully closed.
                        A discrete event system may transition between its states many times, as for instance a motor is turned on, off, on, off, and so on.
                        The events are usually named as well.
                        They are ideally named after what happens in the system, rather than the effect of that change on the state of the system.
                        For instance, the events to turn the motor on and off may be named <code>turn_motor_on</code> and <code>turn_motor_off</code>.
                        And the events that indicate that the bridge deck has reached a different position, could be named <code>deck_has_opened</code>, <code>deck_has_become_partially_open</code> and <code>deck_has_closed</code>.
                    </p>

                    <h2 id="system-behavior">System behavior</h2>
                    <p>
                        A system consists of multiple parts, called <em>components</em>, like a bridge deck and a motor.
                        A state of the entire system consists of a state for each of its components.
                        That is, a single system state is a combination of states of the components of the system.
                        For instance, the bridge as a whole may be in six different states, which cover all the combinations of the states of the deck and the motor: <code>(deck_is_open, motor_is_on)</code>, <code>(deck_is_open, motor_is_off)</code>, <code>(deck_is_partially_open, motor_is_on)</code>, <code>(deck_is_partially_open, motor_is_off)</code>, <code>(deck_is_closed, motor_is_on)</code> and <code>(deck_is_closed, motor_is_off)</code>.
                        <code>(deck_is_open, motor_is_on)</code> then indicates the state where the deck is fully open and the motor is turned on.
                        The collection of all the possible states of a system is called the <em>state space</em> of the system.
                    </p>
                    <p>
                        Note that not all system states are always desired states, as some may be undesired or even unsafe.
                        For instance, consider that there would also be a traffic light that indicates to road traffic when it is safe to cross the bridge.
                        The state where the bridge deck opens, while the traffic light is green, is then clearly unsafe, as cars could drive over the opening bridge deck and fall into the water under the bridge.
                        A supervisory controller may then be added to the system, to prevent the system for getting into such an unsafe state.
                    </p>
                    <p>
                        A system may transition from one state to another, when certain events occur.
                        For instance, with event <code>turn_motor_off</code> the bridge may transition from state <code>(deck_is_open, motor_is_on)</code> to <code>(deck_is_open, motor_is_off)</code>, from <code>(deck_is_partially_open, motor_is_on)</code> to <code>(deck_is_partially_open, motor_is_off)</code>, or from <code>(deck_is_closed, motor_is_on)</code> to <code>(deck_is_closed, motor_is_off)</code>.
                    </p>
                    <p>
                        As different events occur over time, a system may transition from state to state.
                        In different situations, the system may behave differently, leading to different events to occur in different orders.
                        In the theory behind discrete event systems, all the events of the system together are called the <em>alphabet</em> of the system.
                        Each possible sequence of events that may occur in the system is called a <em>word</em>.
                        And all the possible sequences of events that may occur in a system are together called the <em>language</em> of the system.
                        This terminology comes from the fact that each event can be viewed as a unique letter, and together all the events (letters) form an alphabet.
                        Using the letters of the alphabet, words can be formed (like sequences of events), and all the words together form a language.
                        The language of an uncontrolled system represents the system's possible behaviors, all the different orders in which the events can occur.
                        With a supervisory controller added to the system, undesired and unsafe situations are prevent, and thus certain orders of events are prevented.
                        The language of a controlled system thus represents the system's desired behaviors, all the different orders in which the events are allowed to occur.
                    </p>
                    <p>
                        The concept of state space, and specifically how to compute state spaces, is later revisited in Module 2.
                    </p>

                    <h2>Time-driven and event-driven systems</h2>
                    <p>
                        Discrete event systems differ from continuous-time systems.
                        For a continuous-time systems, such as a speedometer of a car, the system's state may change continuously as time progresses.
                        In the figure below, the speed of a car is shown as a smooth curve, with time progressing from left to right, and the speed of the car increasing from bottom to top.
                        In continuous-time systems, the state of the system depends on the time that has passed, and time thus 'drives' such systems.
                        Such systems are therefore also referred to as time-driven systems.
                    </p>
                    <img src="images/continuous-time-model.png">
                    <p>
                        Discrete event systems, as we discussed earlier, are defined by discrete states, and transitions between the states when certain events occur.
                        In this course, we primarily consider discrete event systems where the events may occur at any moment in time, rather than at fixed moments in time.
                        It is then the events, not time, that drive the system.
                        Such systems are therefore also referred to as event-driven systems.
                    </p>
                    <p>
                        Since we need to model the plant as a discrete event system to be able to apply the synthesis procedure, the system should be event-driven.
                        If a supervisory controller needs to be developed to control a time-driven system, that system needs to first be abstracted to an event-driven system.
                        Rather than relying directly on continuous values, such as time, temperature and speed, relevant discrete events should be identified that signify relevant changes to these values.
                        For instance, events could indicate when a certain amount of time has passed, when a temperature becomes too high, or when a car slows down so much that it stands still.
                        How the events are chosen, depends on the system itself and the requirements to be imposed on the system.
                        For instance, if a controller needs to ensure that the temperature of a system does not rise above a certain limit, it needs to be able to detect temperature changes around that limit.
                    </p>
                    <p>
                        Later in this course, in Module 5, we will also consider time-driven systems directly, for validation of synthesized controllers.
                        The continuous-time part of the system is then no longer abstracted, but modeled as a time-driven system.
                        And it is combined with a model of the discrete event controller.
                        This combination of time-driven model and event-driven model of these different parts of the system is a so-called <em>hybrid</em> model.
                    </p>

                    <h2 id="quiz">Quiz</h2>
                    <div class="quiz">
                        [
                            {
                                type: 'single-choice',
                                question: "What is a discrete event system?",
                                answers: [
                                    "A system with continuous-time behavior, in which events happen at any time, in a time-driven manner.",
                                    "A system that can be viewed as a set of discrete states, with transitions between the states, associated with events.",
                                    "A system where events that occur at fixed times drive the system, leading to transitions between states."
                                ],
                                correctAnswer: '2'
                            },
                            {
                                type: 'single-choice',
                                question: "Can events be thought of as occurring instantaneously?",
                                answers: [
                                    "Yes.",
                                    "No."
                                ],
                                correctAnswer: '1'
                            },
                            {
                                type: 'single-choice',
                                question: "What could be states and events of a lamp that can turn on and off?",
                                answers: [
                                    "States: <code>turn_on</code>, <code>turn_off</code>. Events: <code>On</code>, <code>Off</code>.",
                                    "States: <code>On</code>, <code>Off</code>. Events: <code>turn_on</code>, <code>turn_off</code>.",
                                    "It is not possible to define states and events, because this is not a discrete event system."
                                ],
                                correctAnswer: '2'
                            },
                            {
                                type: 'single-choice',
                                question: `
                                    A coffee machine can transition from state <code>Waiting</code> to state <code>Preparing</code> when a user enters their choice of coffee.
                                    Once the machine has finished preparing the coffee, it transitions to state <code>MakingCoffee</code>.
                                    Once it is finished (state <code>Finished</code>) the removal of the cup by the user brings the state of the machine back to <code>Waiting</code>.
                                    What would be the most appropriate events for this system?`,
                                answers: [
                                    "<code>Waiting</code>, <code>Preparing</code>, <code>MakingCoffee</code>, <code>Finished</code>.",
                                    "<code>make_choice</code>, <code>enter_choice</code>, <code>coffee_finished</code>.",
                                    "<code>make_choice</code>, <code>finish_prepare</code>, <code>finish_coffee</code>, <code>remove_cup</code>.",
                                    "<code>make_choice</code>, <code>finish_prepare</code>, <code>finish_coffee</code>, <code>back_to_waiting</code>.",
                                    "<code>make_choice</code>, <code>finish_prepare</code>, <code>remove_cup</code>."
                                ],
                                correctAnswer: '3'
                            },
                            {
                                type: 'multiple-choice',
                                question: "Which of the following statements is true?",
                                answers: [
                                    "The alphabet of a system consists of all its events.",
                                    "The language of a system consists of all its possible or desired sequences of events.",
                                    "The state space of a controlled system consists of all its possible states, but not only the safe states."
                                ],
                                correctAnswer: '1, 2'
                            },
                            {
                                type: 'single-choice',
                                question: "Which of the following need to be abstracted, to turn a time-driven system into an event-driven system?",
                                answers: [
                                    "An elevator needs to arrive at the right floor, even after repeated elevator button presses.",
                                    "A bridge should not be opened when cars drive on it, even if the operator repeatedly presses a button in quick succession.",
                                    "A maintenance light needs to turn on after a car travels a certain distance.",
                                    "Commands given by an external system at fixed time intervals should be processed in the correct order."
                                ],
                                correctAnswer: '3'
                            }
                        ];
                    </div>

                    <div class="course-navigation-footer">
                        <div class="course-navigation-prev"><a href="supervisory-control.html">Go back to Module 1.1</a></div>
                        <div class="course-navigation-separator">|</div>
                        <div class="course-navigation-next"><a href="modeling-discrete-event-systems.html">Proceed to Module 1.3</a></div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Footer -->
        <div class="container" id="footer">
            <footer class="pt-3 mt-4">
                <ul class="nav justify-content-center border-top pb-1 mb-1">
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org">Eclipse Home</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/privacy.php">Eclipse Privacy Policy</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/termsofuse.php">Eclipse Terms of Use</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/copyright.php">Eclipse Copyright Agent</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal">Eclipse Legal</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.dev/escet/legal.html">Eclipse ESCET Legal</a></li>
                </ul>
                <p class="text-center text-muted">Copyright © 2010, 2024 Contributors to the Eclipse Foundation</p>
            </footer>
        </div>

    </body>
</html>
