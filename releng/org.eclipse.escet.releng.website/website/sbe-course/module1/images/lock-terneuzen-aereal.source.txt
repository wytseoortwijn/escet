//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

Source: Beeldarchief Rijkswaterstaat, photo number 427950
Source: https://proxy.archieven.nl/0/3640BF9CA1E14103AF83A17197335D8B
Copyright: Rijkswaterstaat
