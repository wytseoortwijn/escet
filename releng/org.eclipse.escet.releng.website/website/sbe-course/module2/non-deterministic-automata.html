<!doctype html>
<!--
    Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation

    See the NOTICE file(s) distributed with this work for additional
    information regarding copyright ownership.

    This program and the accompanying materials are made available under the terms
    of the MIT License which is available at https://opensource.org/licenses/MIT

    SPDX-License-Identifier: MIT
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module 2.2 | SBE Course</title>
        <link rel="icon" type="image/png" href="../sbe-course-favicon.png"/>

        <!-- Bootstrap. -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.0/font/bootstrap-icons.css" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

        <!-- Website styling. -->
        <link rel="stylesheet" href="../../assets/eclipse-escet.css">
        <link rel="stylesheet" href="../../assets/eclipse-escet-sbe-course.css">

        <!-- Version selector. -->
        <script src="../../assets/eclipse-escet.js"></script>

        <!-- Code block syntax highlighting. -->
        <link rel="stylesheet" href="../../assets/styles/asciidoctor-highlightjs-escet.min.css">
        <script src="../../assets/highlight.min.js"></script>
        <script src="../../assets/languages/cif.min.js"></script>
        <script src="../../assets/languages/tooldef.min.js"></script>

        <!-- Quiz. -->
        <link rel="stylesheet" href="../assets/quiz.css">
        <script src="../assets/quiz.js"></script>

        <!-- SBE course. -->
        <link rel="stylesheet" href="../assets/course.css">
        <script src="../assets/course.js"></script>
    </head>

    <body class="escet-dark">

        <!-- Top navigation bar -->
        <nav class="navbar navbar-expand-lg navbar-dark escet-bg">
            <div class="container-fluid">
                <a class="navbar-brand d-lg-none">SBE Course</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-0 mb-lg-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle escet-brand px-0" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Eclipse ESCET™ / SBE Course</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="../.."><img src="../../eclipse-escet-favicon.png" class="pe-2"> Eclipse ESCET / Project</a></li>
                                <li><a class="dropdown-item" href=".."><img src="../sbe-course-favicon.png" class="pe-2"> Eclipse ESCET / SBE Course</a></li>
                                <li><a class="dropdown-item" href="../../cif"><img src="../../cif/cif-favicon.png" class="pe-2"> Eclipse ESCET / CIF</a></li>
                                <li><a class="dropdown-item" href="../../chi"><img src="../../chi/chi-favicon.png" class="pe-2"> Eclipse ESCET / Chi</a></li>
                                <li><a class="dropdown-item" href="../../tooldef"><img src="../../tooldef/tooldef-favicon.png" class="pe-2"> Eclipse ESCET / ToolDef</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../" aria-expanded="false">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 1</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
                                <li><a class="dropdown-item" href="../module1">Module 1: Supervisory control of discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/supervisory-control.html">Module 1.1: Supervisory control</a></li>
                                <li><a class="dropdown-item" href="../module1/discrete-event-systems.html">Module 1.2: Discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/modeling-discrete-event-systems.html">Module 1.3: Modeling discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/get-started-with-cif.html">Module 1.4: Get started with CIF</a></li>
                                <li><a class="dropdown-item" href="../module1/exercises.html">Module 1.5: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module1/introduction-to-the-water-lock-case.html">Module 1.6: Introduction to the water lock case</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle active" aria-current="page" href="#" id="navbarDropdown2" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 2</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                <li><a class="dropdown-item" href="./">Module 2: Working with automata</a></li>
                                <li><a class="dropdown-item" href="reachability-deadlock-blocking-livelock.html">Module 2.1: Reachability, deadlock, blocking and livelock</a></li>
                                <li><a class="dropdown-item active" aria-current="page" href="non-deterministic-automata.html">Module 2.2: Non-deterministic automata</a></li>
                                <li><a class="dropdown-item" href="synchronizing-automata.html">Module 2.3: Synchronizing automata</a></li>
                                <li><a class="dropdown-item" href="shared-variables.html">Module 2.4: Shared variables</a></li>
                                <li><a class="dropdown-item" href="computing-state-spaces.html">Module 2.5: Computing state spaces</a></li>
                                <li><a class="dropdown-item" href="cif-tools-view-analyze-models.html">Module 2.6: CIF tools to view and analyze models</a></li>
                                <li><a class="dropdown-item" href="constants-and-algebraic-variables.html">Module 2.7: Constants and algebraic variables</a></li>
                                <li><a class="dropdown-item" href="exercises.html">Module 2.8: Exercises</a></li>
                                <li><a class="dropdown-item" href="more-detailed-models-water-lock-case.html">Module 2.9: More detailed models for the water lock case</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 3</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown3">
                                <li><a class="dropdown-item" href="../module3">Module 3: Supervisory control and plant models</a></li>
                                <li><a class="dropdown-item" href="../module3/supervisory-control-loop.html">Module 3.1: Supervisory control loop</a></li>
                                <li><a class="dropdown-item" href="../module3/synthesis-guarantees.html">Module 3.2: Synthesis guarantees</a></li>
                                <li><a class="dropdown-item" href="../module3/supervisory-controller-synthesis.html">Module 3.3: Supervisory controller synthesis</a></li>
                                <li><a class="dropdown-item" href="../module3/modeling-larger-systems.html">Module 3.4: Modeling larger systems</a></li>
                                <li><a class="dropdown-item" href="../module3/exercises.html">Module 3.5: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module3/water-lock-case-plant-models.html">Module 3.6: Water lock case plant models</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 4</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown4">
                                <li><a class="dropdown-item" href="../module4">Module 4: Modeling requirements and supervisor synthesis</a></li>
                                <li><a class="dropdown-item" href="../module4/user-specified-requirements.html">Module 4.1: User-specified requirements</a></li>
                                <li><a class="dropdown-item" href="../module4/requirement-automata-and-invariants.html">Module 4.2: Requirements automata and invariants</a></li>
                                <li><a class="dropdown-item" href="../module4/synthesis-with-requirements.html">Module 4.3: Synthesis with requirements</a></li>
                                <li><a class="dropdown-item" href="../module4/modeling-for-synthesis.html">Module 4.4: Modeling for synthesis</a></li>
                                <li><a class="dropdown-item" href="../module4/performing-synthesis-with-cif.html">Module 4.5: Performing synthesis with CIF</a></li>
                                <li><a class="dropdown-item" href="../module4/introduction-to-svg-visualization.html">Module 4.6: Introduction to SVG visualization</a></li>
                                <li><a class="dropdown-item" href="../module4/exercises.html">Module 4.7: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module4/water-lock-case-requirements-and-supervisor.html">Module 4.8: Water lock case requirements and supervisor</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown5" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 5</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown5">
                                <li><a class="dropdown-item" href="../module5">Module 5: Hybrid simulation models</a></li>
                                <li><a class="dropdown-item" href="../module5/timed-and-hybrid-models.html">Module 5.1: Timed and hybrid models</a></li>
                                <li><a class="dropdown-item" href="../module5/variable-time.html">Module 5.2: Variable 'time'</a></li>
                                <li><a class="dropdown-item" href="../module5/continuous-variables.html">Module 5.3: Continuous variables</a></li>
                                <li><a class="dropdown-item" href="../module5/urgency.html">Module 5.4: Urgency</a></li>
                                <li><a class="dropdown-item" href="../module5/tau-event.html">Module 5.5: The 'tau' event</a></li>
                                <li><a class="dropdown-item" href="../module5/exercises.html">Module 5.6: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module5/water-lock-case-hybrid-model.html">Module 5.7: Water lock case hybrid model</a></li>
                                <li><a class="dropdown-item" href="../module5/course-conclusion.html">Module 5.8: Course conclusion</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Intro -->
        <div id="intro" class="position-relative p-3 text-center escet-bg escet-bg-opacity-125">
            <div class="clearfix mb-4 mb-md-0">
                <span class="float-end">
                    Version:
                    <div class="dropdown d-inline-block">
                        <a class="dropdown-toggle" href="#" role="button" id="versionDropdown" data-bs-toggle="dropdown" aria-expanded="false">@@ESCET-VERSION-ENDUSER@@</a>
                        <ul id="versions-dropdown" class="dropdown-menu dropdown-menu-end" aria-labelledby="versionDropdown" data-this-version="/@@ESCET-DEPLOY-FOLDER-NAME@@">
                            <li><h6 id="versions-previews" class="dropdown-header">Previews</h6></li>
                            <li><h6 id="versions-latest-releases" class="dropdown-header">Latest releases</h6></li>
                            <li><h6 class="dropdown-header">Other</h6></li>
                            <li><a class="dropdown-item" href="../../versions.html">All versions...</a></li>
                        </ul>
                    </div>
                    <a class="text-nowrap ms-3" href="../../release-notes.html">[release notes]</a>
                </span>
            </div>
        </div>

        <!-- Content -->
        <div class="px-4 pt-5">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-xl-8 col-xxl-6" id="content">

                    <h1>Module 2.2: Non-deterministic automata</h1>
                    <p>
                        Depending on the context in which it is used, <em>non-determinism</em> can mean different things.
                        Different communities also use different definitions, and some communities only use one of the definitions, and use a different name to refer to the other concept.
                    </p>
                    <p>
                        One definition of non-determinism is that multiple transitions are possible, for different events.
                        In other words, there are multiple possible traces through the state space.
                        In this course, as well as generally in CIF, this is not considered non-determism as it involves different events.
                    </p>
                    <p>
                        Another definition of non-determinism, and the one used in this course and also generally in CIF, is the presence of multiple possible transitions for the same event from a single state.
                        For instance, consider the following CIF model:
                    </p>
                    <pre class="highlightjs highlight">
                        <code class="language-cif hljs" data-lang="cif">
                            automaton coin_toss:
                                event toss, pick_up;

                                location coin_in_hand:
                                    initial;
                                    marked;
                                    edge toss goto heads;
                                    edge toss goto tails;

                                location heads:
                                    edge pick_up goto coin_in_hand;

                                location tails:
                                    edge pick_up goto coin_in_hand;
                            end
                        </code>
                    </pre>
                    <p>
                        Initially, a coin is in your hand.
                        You can <code>toss</code> the coin, and it can land in two different ways, with either heads or tails on top.
                        In either case, you can pick up the coin and toss it again.
                        The outcome of the coin toss is non-deterministic, as it is either heads or tails, and this is not known in advance.
                        That is, in location <code>coin_in_hand</code>, two transitions are possible for the same event, with different next states (going to location <code>heads</code> or <code>tails</code>), and this choice is random.
                    </p>
                    <p>
                        In the coin toss example, the presence of non-determinism is easy to spot, as there are two edges for event <code>toss</code> in location <code>coin_in_hand</code>, and they lead to different states (different target locations).
                        But, multiple edges for the same event in the same location does not always lead to non-determinism.
                        If the edges lead to the same state, they are essentially the same edge, and thus there is no non-determinism.
                        And if the edges have guards that ensure they are not enabled at the same time, then there is also no non-determinism.
                        For instance, consider the following model:
                    </p>
                    <pre class="highlightjs highlight">
                        <code class="language-cif hljs" data-lang="cif">
                            automaton a:
                                event e;
                                disc bool v;

                                location x:
                                    initial;
                                    marked;
                                    edge e when     v goto y;
                                    edge e when not v goto z;

                                location y;

                                location z;
                            end
                        </code>
                    </pre>
                    <p>
                        There are two edges for event <code>e</code> from location <code>x</code>, but they are never enabled at the same time.
                        If <code>v</code> is <code>true</code> then the first edge is enabled, but the second is not.
                        And if <code>v</code> is <code>false</code> then the second edge is enabled, while the first is not.
                        Thus always only one of the edges is enabled, and there is no non-determinism.
                    </p>
                    <p>
                        Non-determinism can be useful, like when modeling the coin toss, but for most models it is not needed.
                        And some CIF tools do not support non-determinism in certain cases.
                    </p>

                    <h2>Quiz</h2>
                    <div class="quiz">
                        [
                            {
                                type: 'multiple-choice',
                                question: `
                                    Which of the statements is true about non-determinism in the following CIF model?
                                    <pre class="highlightjs highlight">
                                        <code class="language-cif hljs" data-lang="cif">
                                            automaton a:
                                                event e, f, g, h, i;
                                                disc int v;

                                                location x:
                                                    initial;
                                                    marked;
                                                    edge e when v &lt;  4           goto y;
                                                    edge e when v &gt;  4           goto z;
                                                    edge f when v &lt;= 4           goto y;
                                                    edge f when v &gt;  4           goto z;
                                                    edge g when v &lt;= 4           goto y;
                                                    edge g when v &gt;= 4           goto z;
                                                    edge h when v &lt;  4 and 4 &lt; v goto y;
                                                    edge h when v  = 4           goto z;
                                                    edge i when v  = 4           goto y;

                                                location y;
                                                    edge i when v  = 4           goto z;

                                                location z;
                                            end
                                        </code>
                                    </pre>`,
                                answers: [
                                    "None of the events has non-determinism.",
                                    "Event <code>e</code> has non-determinism.",
                                    "Event <code>f</code> has non-determinism.",
                                    "Event <code>g</code> has non-determinism.",
                                    "Event <code>h</code> has non-determinism.",
                                    "Event <code>i</code> has non-determinism."
                                ],
                                correctAnswer: '4'
                            }
                        ]
                    </div>

                    <div class="course-navigation-footer">
                        <div class="course-navigation-prev"><a href="reachability-deadlock-blocking-livelock.html">Go back to Module 2.1</a></div>
                        <div class="course-navigation-separator">|</div>
                        <div class="course-navigation-next"><a href="synchronizing-automata.html">Proceed to Module 2.3</a></div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Footer -->
        <div class="container" id="footer">
            <footer class="pt-3 mt-4">
                <ul class="nav justify-content-center border-top pb-1 mb-1">
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org">Eclipse Home</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/privacy.php">Eclipse Privacy Policy</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/termsofuse.php">Eclipse Terms of Use</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/copyright.php">Eclipse Copyright Agent</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal">Eclipse Legal</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.dev/escet/legal.html">Eclipse ESCET Legal</a></li>
                </ul>
                <p class="text-center text-muted">Copyright © 2010, 2024 Contributors to the Eclipse Foundation</p>
            </footer>
        </div>

    </body>
</html>
