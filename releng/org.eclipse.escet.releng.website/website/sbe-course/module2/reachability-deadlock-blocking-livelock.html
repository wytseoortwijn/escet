<!doctype html>
<!--
    Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation

    See the NOTICE file(s) distributed with this work for additional
    information regarding copyright ownership.

    This program and the accompanying materials are made available under the terms
    of the MIT License which is available at https://opensource.org/licenses/MIT

    SPDX-License-Identifier: MIT
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module 2.1 | SBE Course</title>
        <link rel="icon" type="image/png" href="../sbe-course-favicon.png"/>

        <!-- Bootstrap. -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.0/font/bootstrap-icons.css" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

        <!-- Website styling. -->
        <link rel="stylesheet" href="../../assets/eclipse-escet.css">
        <link rel="stylesheet" href="../../assets/eclipse-escet-sbe-course.css">

        <!-- Version selector. -->
        <script src="../../assets/eclipse-escet.js"></script>

        <!-- Code block syntax highlighting. -->
        <link rel="stylesheet" href="../../assets/styles/asciidoctor-highlightjs-escet.min.css">
        <script src="../../assets/highlight.min.js"></script>
        <script src="../../assets/languages/cif.min.js"></script>
        <script src="../../assets/languages/tooldef.min.js"></script>

        <!-- Quiz. -->
        <link rel="stylesheet" href="../assets/quiz.css">
        <script src="../assets/quiz.js"></script>

        <!-- SBE course. -->
        <link rel="stylesheet" href="../assets/course.css">
        <script src="../assets/course.js"></script>
    </head>

    <body class="escet-dark">

        <!-- Top navigation bar -->
        <nav class="navbar navbar-expand-lg navbar-dark escet-bg">
            <div class="container-fluid">
                <a class="navbar-brand d-lg-none">SBE Course</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-0 mb-lg-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle escet-brand px-0" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Eclipse ESCET™ / SBE Course</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="../.."><img src="../../eclipse-escet-favicon.png" class="pe-2"> Eclipse ESCET / Project</a></li>
                                <li><a class="dropdown-item" href=".."><img src="../sbe-course-favicon.png" class="pe-2"> Eclipse ESCET / SBE Course</a></li>
                                <li><a class="dropdown-item" href="../../cif"><img src="../../cif/cif-favicon.png" class="pe-2"> Eclipse ESCET / CIF</a></li>
                                <li><a class="dropdown-item" href="../../chi"><img src="../../chi/chi-favicon.png" class="pe-2"> Eclipse ESCET / Chi</a></li>
                                <li><a class="dropdown-item" href="../../tooldef"><img src="../../tooldef/tooldef-favicon.png" class="pe-2"> Eclipse ESCET / ToolDef</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../" aria-expanded="false">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 1</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
                                <li><a class="dropdown-item" href="../module1">Module 1: Supervisory control of discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/supervisory-control.html">Module 1.1: Supervisory control</a></li>
                                <li><a class="dropdown-item" href="../module1/discrete-event-systems.html">Module 1.2: Discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/modeling-discrete-event-systems.html">Module 1.3: Modeling discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/get-started-with-cif.html">Module 1.4: Get started with CIF</a></li>
                                <li><a class="dropdown-item" href="../module1/exercises.html">Module 1.5: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module1/introduction-to-the-water-lock-case.html">Module 1.6: Introduction to the water lock case</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle active" aria-current="page" href="#" id="navbarDropdown2" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 2</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                <li><a class="dropdown-item" href="./">Module 2: Working with automata</a></li>
                                <li><a class="dropdown-item active" aria-current="page" href="reachability-deadlock-blocking-livelock.html">Module 2.1: Reachability, deadlock, blocking and livelock</a></li>
                                <li><a class="dropdown-item" href="non-deterministic-automata.html">Module 2.2: Non-deterministic automata</a></li>
                                <li><a class="dropdown-item" href="synchronizing-automata.html">Module 2.3: Synchronizing automata</a></li>
                                <li><a class="dropdown-item" href="shared-variables.html">Module 2.4: Shared variables</a></li>
                                <li><a class="dropdown-item" href="computing-state-spaces.html">Module 2.5: Computing state spaces</a></li>
                                <li><a class="dropdown-item" href="cif-tools-view-analyze-models.html">Module 2.6: CIF tools to view and analyze models</a></li>
                                <li><a class="dropdown-item" href="constants-and-algebraic-variables.html">Module 2.7: Constants and algebraic variables</a></li>
                                <li><a class="dropdown-item" href="exercises.html">Module 2.8: Exercises</a></li>
                                <li><a class="dropdown-item" href="more-detailed-models-water-lock-case.html">Module 2.9: More detailed models for the water lock case</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 3</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown3">
                                <li><a class="dropdown-item" href="../module3">Module 3: Supervisory control and plant models</a></li>
                                <li><a class="dropdown-item" href="../module3/supervisory-control-loop.html">Module 3.1: Supervisory control loop</a></li>
                                <li><a class="dropdown-item" href="../module3/synthesis-guarantees.html">Module 3.2: Synthesis guarantees</a></li>
                                <li><a class="dropdown-item" href="../module3/supervisory-controller-synthesis.html">Module 3.3: Supervisory controller synthesis</a></li>
                                <li><a class="dropdown-item" href="../module3/modeling-larger-systems.html">Module 3.4: Modeling larger systems</a></li>
                                <li><a class="dropdown-item" href="../module3/exercises.html">Module 3.5: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module3/water-lock-case-plant-models.html">Module 3.6: Water lock case plant models</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 4</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown4">
                                <li><a class="dropdown-item" href="../module4">Module 4: Modeling requirements and supervisor synthesis</a></li>
                                <li><a class="dropdown-item" href="../module4/user-specified-requirements.html">Module 4.1: User-specified requirements</a></li>
                                <li><a class="dropdown-item" href="../module4/requirement-automata-and-invariants.html">Module 4.2: Requirements automata and invariants</a></li>
                                <li><a class="dropdown-item" href="../module4/synthesis-with-requirements.html">Module 4.3: Synthesis with requirements</a></li>
                                <li><a class="dropdown-item" href="../module4/modeling-for-synthesis.html">Module 4.4: Modeling for synthesis</a></li>
                                <li><a class="dropdown-item" href="../module4/performing-synthesis-with-cif.html">Module 4.5: Performing synthesis with CIF</a></li>
                                <li><a class="dropdown-item" href="../module4/introduction-to-svg-visualization.html">Module 4.6: Introduction to SVG visualization</a></li>
                                <li><a class="dropdown-item" href="../module4/exercises.html">Module 4.7: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module4/water-lock-case-requirements-and-supervisor.html">Module 4.8: Water lock case requirements and supervisor</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown5" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 5</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown5">
                                <li><a class="dropdown-item" href="../module5">Module 5: Hybrid simulation models</a></li>
                                <li><a class="dropdown-item" href="../module5/timed-and-hybrid-models.html">Module 5.1: Timed and hybrid models</a></li>
                                <li><a class="dropdown-item" href="../module5/variable-time.html">Module 5.2: Variable 'time'</a></li>
                                <li><a class="dropdown-item" href="../module5/continuous-variables.html">Module 5.3: Continuous variables</a></li>
                                <li><a class="dropdown-item" href="../module5/urgency.html">Module 5.4: Urgency</a></li>
                                <li><a class="dropdown-item" href="../module5/tau-event.html">Module 5.5: The 'tau' event</a></li>
                                <li><a class="dropdown-item" href="../module5/exercises.html">Module 5.6: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module5/water-lock-case-hybrid-model.html">Module 5.7: Water lock case hybrid model</a></li>
                                <li><a class="dropdown-item" href="../module5/course-conclusion.html">Module 5.8: Course conclusion</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Intro -->
        <div id="intro" class="position-relative p-3 text-center escet-bg escet-bg-opacity-125">
            <div class="clearfix mb-4 mb-md-0">
                <span class="float-end">
                    Version:
                    <div class="dropdown d-inline-block">
                        <a class="dropdown-toggle" href="#" role="button" id="versionDropdown" data-bs-toggle="dropdown" aria-expanded="false">@@ESCET-VERSION-ENDUSER@@</a>
                        <ul id="versions-dropdown" class="dropdown-menu dropdown-menu-end" aria-labelledby="versionDropdown" data-this-version="/@@ESCET-DEPLOY-FOLDER-NAME@@">
                            <li><h6 id="versions-previews" class="dropdown-header">Previews</h6></li>
                            <li><h6 id="versions-latest-releases" class="dropdown-header">Latest releases</h6></li>
                            <li><h6 class="dropdown-header">Other</h6></li>
                            <li><a class="dropdown-item" href="../../versions.html">All versions...</a></li>
                        </ul>
                    </div>
                    <a class="text-nowrap ms-3" href="../../release-notes.html">[release notes]</a>
                </span>
            </div>
        </div>

        <!-- Content -->
        <div class="px-4 pt-5">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-xl-8 col-xxl-6" id="content">

                    <h1>Module 2.1: Reachability, deadlock, blocking and livelock</h1>
                    <p>
                        Reachability, deadlock, blocking and livelock are important concepts to understand automata, and are essential to later understand how synthesizing a supervisory controller works.
                        We first discuss reachability, then deadlock, and then the related concepts of blocking and livelock.
                    </p>

                    <h2>Reachability</h2>
                    <p>
                        States are <em>reachable</em> if there is a way to get there.
                        Initial state are per definition reachable.
                        Any other state is only reachable if there is a sequence of transitions from the initial state to that state.
                    </p>
                    <p>
                        A state of a discrete event system consists of a current locations for each automaton and a current value for each discrete variable.
                        An initial state thus consists of an initial location for each automaton, and an initial value for each discrete variable.
                    </p>
                    <p>
                        A location of an automaton is reachable if there is a way to get to the location from the initial location of that automaton.
                        Initial locations of automata are per definition reachable.
                        Other locations are only reachable if there are edges that can be taken to get those locations, starting from the initial location.
                        A value of a variable is reachable if there is a way to assign that value to the variable, by starting from the initial state and taking edges.
                    </p>
                    <p>
                        In the figure below, an example automaton is shown.
                        Location <code>1</code> is its initial location.
                        It is per definition reachable, and can be reached without taking any edges.
                        Location <code>2</code> is reachable, because there is an edge from reachable location <code>1</code> to location <code>2</code>.
                        We can reach location <code>2</code> by a transition for event <code>a</code>.
                        Location <code>1</code> is also reachable by a sequence of transitions for events <code>a</code> and <code>b</code>.
                        And location <code>2</code> is also from the initial state by transitions for events <code>a</code>, <code>b</code> and <code>a</code>.
                        However, to show that a state is reachable, it is sufficient to have one sequence to reach it, and the shortest sequence is then sufficient.
                        Location <code>3</code> is not reachable, because there is no such sequence of edges to get to it from location <code>1</code>.
                    </p>
                    <img src="images/automaton-property-reachability.png">
                    <p>
                        Unreachable states can't be reached from initial states.
                        They are thus not very useful when modeling a system.
                        Therefore, unreachable locations of automata are typically omitted from models.
                    </p>

                    <h2 id="deadlock">Deadlock</h2>
                    <p>
                        A <em>deadlock</em> state is a state where no transitions are possible, and thus no further behavior is possible.
                        Since a discrete event system can transition between states by taking edges, if no more transitions are possible, this means no edges can be taken.
                    </p>
                    <p>
                        A location is a deadlock location if it has no outgoing edges.
                        Then, per definition, if such a location is the current location of its automaton, the automaton deadlocks.
                        But, an automaton can also deadlock in a location that does have outgoing edges, but where none of the guards of these edges is satisfied.
                        If for instance a location has one edge, with a guard <code>x &gt; 3</code>, but while being in that location <code>x</code> has value <code>2</code>, then the edge can't be taken, and the automaton is stuck in the location.
                    </p>
                    <p>
                        The figure below shows an example of an automaton with a deadlock location.
                        All three locations are reachable from the initial location.
                        Location <code>3</code> for instance is reachable by taking transitions for events <code>a</code> and <code>c</code>.
                        But once the automaton transitions to location <code>3</code>, there is no way to leave that location, and thus the automaton is in a deadlock.
                    </p>
                    <img src="images/automaton-property-deadlock.png">
                    <p>
                        Since in a deadlock state no further behavior is possible, such states are of course undesired.
                        A supervisory controller should prevent the system from ending up in such a state, as we will explore further in Module 3.
                    </p>

                    <h2 id="blocking-and-livelock">Blocking and livelock</h2>
                    <p>
                        In Module 1, you learned about marked states.
                        Marked states are stable states, or safe havens.
                        The system should always be able to reach a marked state.
                        For instance, a machine should always be able to reach its idle state.
                        For safety reasons, it is important that a system can always be turned off.
                        And for the correct functioning of a production system, it is important that once a product enters the system, it can also always leave the system again after having been processed.
                    </p>
                    <p>
                        A system is <em>non-blocking</em> if from every reachable state, it is possible to reach a marked state.
                        All marked states are thus per definition non-blocking.
                        For other states, there must be a sequence of transitions to a marked state.
                        Any state that is reachable, but does not have a sequence of transitions to a marked state, is called a <em>blocking</em> state.
                    </p>
                    <p>
                        A state is a marked state if all current locations of its automata are marked locations, and all current values of its variables are marked values.
                        Within an automaton, a marked location can be reached if from the current location there are edges that can be taken to get to a location that is marked.
                        A marked value of a variable can be reached by taking an edge that assigns a marked value to the variable.
                        An automaton is blocking if it has at least one blocking location, and a system is blocking if it has at least one blocking automaton.
                    </p>
                    <p>
                        Below, you see two automata, that only differ by which locations are marked.
                        The first automaton has location <code>1</code> as marked location, and thus location <code>1</code> is a non-blocking location.
                        Since there is no way to get to location <code>1</code> from locations <code>2</code> and <code>3</code>, both these locations are blocking locations.
                        Hence, the automaton and the system are blocking.
                    </p>
                    <p>
                        The second automaton has location <code>2</code> as its marked location, and thus this location is a non-blocking location.
                        From location <code>1</code> it is possible to reach location <code>2</code> by taking the edge for event <code>c</code>.
                        From location <code>3</code> it is also possible to reach location <code>2</code> by taking the edge for event <code>b</code>.
                        All the locations, the automaton, and the system are thus non-blocking.
                    </p>
                    <img src="images/automaton-property-blocking.png">
                    <img src="images/automaton-property-nonblocking.png">
                    <p>
                        Now look back at the earlier example of the automaton with a deadlock location (location <code>3</code>).
                        It is in not only a deadlock location, but also a blocking location, as from location <code>3</code> it is not possible to reach marked location <code>1</code>.
                        Often deadlock locations are also blocking locations, but this does not have to be the case.
                        When the deadlock location is also marked, the location is not blocking.
                    </p>
                    <p>
                        Now look back at the blocking automaton just above (with marked location <code>1</code> and blocking locations <code>2</code> and <code>3</code>).
                        Locations <code>2</code> and <code>3</code> form a loop, and thus always another transition can be taken.
                        The automaton does not deadlock, but while taking further transitions never a marked state can be reached.
                        If progress is still possible by taking transitions, but no marked state can be reached, this is called a <em>livelock</em>.
                    </p>
                    <p>
                        As an example of a blocking state in a model with variables, consider again the example from <a href="../module1/get-started-with-cif.html#modeling-with-variables">Module 1.4</a>, of a lamp and a counter that counts the number of times that the lamp is turned off.
                        Through simulation, you observed the behavior of the model.
                        After turning off the lamp five times, and turning on the lamp a final time, no further behavior is possible.
                        The system is then in a deadlock state.
                        Below, the graphical representation of the model is shown.
                        If the <code>On</code> location is the current location, and <code>count</code> has reached value <code>5</code>, then the guard of the edge with event <code>turn_on</code> is not satisfied.
                        The edge is thus not enabled, and the automaton deadlocks in that state.
                        Location <code>On</code> is marked, but value <code>5</code> of variable <code>count</code> is not.
                        Thus, the state is not marked, and since no transitions are possible, it is blocking.
                    </p>
                    <img src="images/automaton-property-blocking-with-variable.png">

                    <h2>Quiz</h2>
                    <div class="quiz">
                        [
                            {
                                type: 'single-choice',
                                question: "When is a location reachable?",
                                answers: [
                                    "When it has an event that can be executed.",
                                    "When there exists a sequence of events leading to that location from the initial location.",
                                    "When from the location no possible events can occur."
                                ],
                                correctAnswer: '2'
                            },
                            {
                                type: 'single-choice',
                                question: "When does a system have a deadlock?",
                                answers: [
                                    "When there exists a state from where no behavior is possible.",
                                    "When there is a state that does not have a sequence of events leading to a marked state.",
                                    "When there is a state that represents some dead behavior of the system, like being off or broken down."
                                ],
                                correctAnswer: '1'
                            },
                            {
                                type: 'single-choice',
                                question: "When is a system non-blocking?",
                                answers: [
                                    "When not all states have a sequence of transitions leading to a marked state.",
                                    "When all states have a sequence of transitions leading to a marked state.",
                                    "When there are no states that block the system's behavior."
                                ],
                                correctAnswer: '2'
                            },
                            {
                                type: 'single-choice',
                                question: "Is a deadlock state always a blocking state?",
                                answers: [
                                    "Yes, all deadlock states are per definition also blocking states.",
                                    "No, when the deadlock state is a marked state it is non-blocking."
                                ],
                                correctAnswer: '2'
                            },
                            {
                                type: 'single-choice',
                                question: "Does a blocking state that has a deadlock also have a livelock?",
                                answers: [
                                    "Always.",
                                    "Sometimes.",
                                    "Never."
                                ],
                                correctAnswer: '3'
                            },
                            {
                                type: 'multiple-choice',
                                question: `
                                    Which of the statements is true about reachability in the following CIF model?
                                    <pre class="highlightjs highlight">
                                        <code class="language-cif hljs" data-lang="cif">
                                            automaton a:
                                                event e1, e2, e3, e4, e5;
                                                disc int v = 4;

                                                location a:
                                                    initial;
                                                    marked;
                                                    edge e1 do v := v + 3 goto b;

                                                location b:
                                                    edge e2 do v := v - 1 goto a;
                                                    edge e3 when v = 8 goto c;
                                                    edge e3 when v = 14 goto c;
                                                    edge e3 when v = 15 goto d;
                                                    edge e4 when v = 15 goto e;
                                                    edge e5 when v = 17 goto f;

                                                location c:
                                                    edge e3;

                                                location d:
                                                    edge e3 when v = 15;

                                                location e:
                                                    edge e3 when v mod 2 = 0;

                                                location f:
                                                    marked;
                                                    edge e3 when v = 15;
                                            end
                                        </code>
                                    </pre>`,
                                answers: [
                                    "All locations are reachable.",
                                    "Location <code>a</code> is not reachable.",
                                    "Location <code>b</code> is not reachable.",
                                    "Location <code>c</code> is not reachable.",
                                    "Location <code>d</code> is not reachable.",
                                    "Location <code>e</code> is not reachable.",
                                    "Location <code>f</code> is not reachable."
                                ],
                                correctAnswer: '4'
                            },
                            {
                                type: 'multiple-choice',
                                question: "Which of the statements is true about deadlocks in that same model?",
                                answers: [
                                    "The system can not deadlock.",
                                    "The system can deadlock in location <code>a</code>.",
                                    "The system can deadlock in location <code>b</code>.",
                                    "The system can deadlock in location <code>c</code>.",
                                    "The system can deadlock in location <code>d</code>.",
                                    "The system can deadlock in location <code>e</code>.",
                                    "The system can deadlock in location <code>f</code>."
                                ],
                                correctAnswer: '6, 7'
                            },
                            {
                                type: 'multiple-choice',
                                question: "Which of the statements is true about blocking in that same model?",
                                answers: [
                                    "The system is non-blocking.",
                                    "Location <code>a</code> is blocking.",
                                    "Location <code>b</code> is blocking.",
                                    "Location <code>c</code> is blocking.",
                                    "Location <code>d</code> is blocking.",
                                    "Location <code>e</code> is blocking.",
                                    "Location <code>f</code> is blocking."
                                ],
                                correctAnswer: '5, 6'
                            },
                            {
                                type: 'multiple-choice',
                                question: "Which of the statements is true about livelock in that same model?",
                                answers: [
                                    "The system has no livelock.",
                                    "Location <code>a</code> part of a livelock.",
                                    "Location <code>b</code> part of a livelock.",
                                    "Location <code>c</code> part of a livelock.",
                                    "Location <code>d</code> part of a livelock.",
                                    "Location <code>e</code> part of a livelock.",
                                    "Location <code>f</code> part of a livelock."
                                ],
                                correctAnswer: '4, 5'
                            }
                        ]
                    </div>

                    <div class="course-navigation-footer">
                        <div class="course-navigation-prev"><a href="./">Go back to Module 2</a></div>
                        <div class="course-navigation-separator">|</div>
                        <div class="course-navigation-next"><a href="non-deterministic-automata.html">Proceed to Module 2.2</a></div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Footer -->
        <div class="container" id="footer">
            <footer class="pt-3 mt-4">
                <ul class="nav justify-content-center border-top pb-1 mb-1">
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org">Eclipse Home</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/privacy.php">Eclipse Privacy Policy</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/termsofuse.php">Eclipse Terms of Use</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/copyright.php">Eclipse Copyright Agent</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal">Eclipse Legal</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.dev/escet/legal.html">Eclipse ESCET Legal</a></li>
                </ul>
                <p class="text-center text-muted">Copyright © 2010, 2024 Contributors to the Eclipse Foundation</p>
            </footer>
        </div>

    </body>
</html>
