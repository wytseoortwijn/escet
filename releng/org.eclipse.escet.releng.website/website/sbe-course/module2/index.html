<!doctype html>
<!--
    Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation

    See the NOTICE file(s) distributed with this work for additional
    information regarding copyright ownership.

    This program and the accompanying materials are made available under the terms
    of the MIT License which is available at https://opensource.org/licenses/MIT

    SPDX-License-Identifier: MIT
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module 2 | SBE Course</title>
        <link rel="icon" type="image/png" href="../sbe-course-favicon.png"/>

        <!-- Bootstrap. -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.0/font/bootstrap-icons.css" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

        <!-- Website styling. -->
        <link rel="stylesheet" href="../../assets/eclipse-escet.css">
        <link rel="stylesheet" href="../../assets/eclipse-escet-sbe-course.css">

        <!-- Version selector. -->
        <script src="../../assets/eclipse-escet.js"></script>

        <!-- Code block syntax highlighting. -->
        <link rel="stylesheet" href="../../assets/styles/asciidoctor-highlightjs-escet.min.css">
        <script src="../../assets/highlight.min.js"></script>
        <script src="../../assets/languages/cif.min.js"></script>
        <script src="../../assets/languages/tooldef.min.js"></script>

        <!-- Quiz. -->
        <link rel="stylesheet" href="../assets/quiz.css">
        <script src="../assets/quiz.js"></script>

        <!-- SBE course. -->
        <link rel="stylesheet" href="../assets/course.css">
        <script src="../assets/course.js"></script>
    </head>

    <body class="escet-dark">

        <!-- Top navigation bar -->
        <nav class="navbar navbar-expand-lg navbar-dark escet-bg">
            <div class="container-fluid">
                <a class="navbar-brand d-lg-none">SBE Course</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-0 mb-lg-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle escet-brand px-0" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Eclipse ESCET™ / SBE Course</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="../.."><img src="../../eclipse-escet-favicon.png" class="pe-2"> Eclipse ESCET / Project</a></li>
                                <li><a class="dropdown-item" href=".."><img src="../sbe-course-favicon.png" class="pe-2"> Eclipse ESCET / SBE Course</a></li>
                                <li><a class="dropdown-item" href="../../cif"><img src="../../cif/cif-favicon.png" class="pe-2"> Eclipse ESCET / CIF</a></li>
                                <li><a class="dropdown-item" href="../../chi"><img src="../../chi/chi-favicon.png" class="pe-2"> Eclipse ESCET / Chi</a></li>
                                <li><a class="dropdown-item" href="../../tooldef"><img src="../../tooldef/tooldef-favicon.png" class="pe-2"> Eclipse ESCET / ToolDef</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../" aria-expanded="false">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 1</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
                                <li><a class="dropdown-item" href="../module1">Module 1: Supervisory control of discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/supervisory-control.html">Module 1.1: Supervisory control</a></li>
                                <li><a class="dropdown-item" href="../module1/discrete-event-systems.html">Module 1.2: Discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/modeling-discrete-event-systems.html">Module 1.3: Modeling discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/get-started-with-cif.html">Module 1.4: Get started with CIF</a></li>
                                <li><a class="dropdown-item" href="../module1/exercises.html">Module 1.5: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module1/introduction-to-the-water-lock-case.html">Module 1.6: Introduction to the water lock case</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle active" aria-current="page" href="#" id="navbarDropdown2" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 2</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                <li><a class="dropdown-item active" aria-current="page" href="./">Module 2: Working with automata</a></li>
                                <li><a class="dropdown-item" href="reachability-deadlock-blocking-livelock.html">Module 2.1: Reachability, deadlock, blocking and livelock</a></li>
                                <li><a class="dropdown-item" href="non-deterministic-automata.html">Module 2.2: Non-deterministic automata</a></li>
                                <li><a class="dropdown-item" href="synchronizing-automata.html">Module 2.3: Synchronizing automata</a></li>
                                <li><a class="dropdown-item" href="shared-variables.html">Module 2.4: Shared variables</a></li>
                                <li><a class="dropdown-item" href="computing-state-spaces.html">Module 2.5: Computing state spaces</a></li>
                                <li><a class="dropdown-item" href="cif-tools-view-analyze-models.html">Module 2.6: CIF tools to view and analyze models</a></li>
                                <li><a class="dropdown-item" href="constants-and-algebraic-variables.html">Module 2.7: Constants and algebraic variables</a></li>
                                <li><a class="dropdown-item" href="exercises.html">Module 2.8: Exercises</a></li>
                                <li><a class="dropdown-item" href="more-detailed-models-water-lock-case.html">Module 2.9: More detailed models for the water lock case</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 3</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown3">
                                <li><a class="dropdown-item" href="../module3">Module 3: Supervisory control and plant models</a></li>
                                <li><a class="dropdown-item" href="../module3/supervisory-control-loop.html">Module 3.1: Supervisory control loop</a></li>
                                <li><a class="dropdown-item" href="../module3/synthesis-guarantees.html">Module 3.2: Synthesis guarantees</a></li>
                                <li><a class="dropdown-item" href="../module3/supervisory-controller-synthesis.html">Module 3.3: Supervisory controller synthesis</a></li>
                                <li><a class="dropdown-item" href="../module3/modeling-larger-systems.html">Module 3.4: Modeling larger systems</a></li>
                                <li><a class="dropdown-item" href="../module3/exercises.html">Module 3.5: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module3/water-lock-case-plant-models.html">Module 3.6: Water lock case plant models</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 4</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown4">
                                <li><a class="dropdown-item" href="../module4">Module 4: Modeling requirements and supervisor synthesis</a></li>
                                <li><a class="dropdown-item" href="../module4/user-specified-requirements.html">Module 4.1: User-specified requirements</a></li>
                                <li><a class="dropdown-item" href="../module4/requirement-automata-and-invariants.html">Module 4.2: Requirements automata and invariants</a></li>
                                <li><a class="dropdown-item" href="../module4/synthesis-with-requirements.html">Module 4.3: Synthesis with requirements</a></li>
                                <li><a class="dropdown-item" href="../module4/modeling-for-synthesis.html">Module 4.4: Modeling for synthesis</a></li>
                                <li><a class="dropdown-item" href="../module4/performing-synthesis-with-cif.html">Module 4.5: Performing synthesis with CIF</a></li>
                                <li><a class="dropdown-item" href="../module4/introduction-to-svg-visualization.html">Module 4.6: Introduction to SVG visualization</a></li>
                                <li><a class="dropdown-item" href="../module4/exercises.html">Module 4.7: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module4/water-lock-case-requirements-and-supervisor.html">Module 4.8: Water lock case requirements and supervisor</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown5" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 5</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown5">
                                <li><a class="dropdown-item" href="../module5">Module 5: Hybrid simulation models</a></li>
                                <li><a class="dropdown-item" href="../module5/timed-and-hybrid-models.html">Module 5.1: Timed and hybrid models</a></li>
                                <li><a class="dropdown-item" href="../module5/variable-time.html">Module 5.2: Variable 'time'</a></li>
                                <li><a class="dropdown-item" href="../module5/continuous-variables.html">Module 5.3: Continuous variables</a></li>
                                <li><a class="dropdown-item" href="../module5/urgency.html">Module 5.4: Urgency</a></li>
                                <li><a class="dropdown-item" href="../module5/tau-event.html">Module 5.5: The 'tau' event</a></li>
                                <li><a class="dropdown-item" href="../module5/exercises.html">Module 5.6: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module5/water-lock-case-hybrid-model.html">Module 5.7: Water lock case hybrid model</a></li>
                                <li><a class="dropdown-item" href="../module5/course-conclusion.html">Module 5.8: Course conclusion</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Intro -->
        <div id="intro" class="position-relative p-3 text-center escet-bg escet-bg-opacity-125">
            <div class="clearfix mb-4 mb-md-0">
                <span class="float-end">
                    Version:
                    <div class="dropdown d-inline-block">
                        <a class="dropdown-toggle" href="#" role="button" id="versionDropdown" data-bs-toggle="dropdown" aria-expanded="false">@@ESCET-VERSION-ENDUSER@@</a>
                        <ul id="versions-dropdown" class="dropdown-menu dropdown-menu-end" aria-labelledby="versionDropdown" data-this-version="/@@ESCET-DEPLOY-FOLDER-NAME@@">
                            <li><h6 id="versions-previews" class="dropdown-header">Previews</h6></li>
                            <li><h6 id="versions-latest-releases" class="dropdown-header">Latest releases</h6></li>
                            <li><h6 class="dropdown-header">Other</h6></li>
                            <li><a class="dropdown-item" href="../../versions.html">All versions...</a></li>
                        </ul>
                    </div>
                    <a class="text-nowrap ms-3" href="../../release-notes.html">[release notes]</a>
                </span>
            </div>
        </div>

        <!-- Content -->
        <div class="px-4 pt-5">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-xl-8 col-xxl-6" id="content">

                    <h1>Module 2: Working with automata</h1>
                    <p>
                        In Module 1, you learned that:
                    </p>
                    <ul>
                        <li>Supervisory controllers play an important role in cyber-physical systems, as they ensure that the system operates correctly and safely.</li>
                        <li>Synthesis-based engineering of supervisory controllers combines model-based engineering with computer-aided design, allowing engineers to focus on <em>what</em> the controller should do (the requirements) rather than on <em>how</em> it should do it (the design and implementation).</li>
                        <li>Synthesis-based engineering can help to reduce the effort required to develop and evolve supervisory controllers, and improve the quality of the resulting controllers.</li>
                        <li>Supervisory controller synthesis can automatically generate correct-by-construction supervisory controller models, from models of the uncontrolled system (the plant) and the requirements.</li>
                        <li>For synthesis, the system should be seen as a discrete event system, and the plants and requirements should be modeled using discrete event models.</li>
                        <li>Important concepts of discrete event systems are components, states, state spaces, events, alphabets, transitions and languages.</li>
                        <li>Discrete event systems can be modeled using automata, which primarily consist of locations that model states, and edges that model transitions.</li>
                        <li>Besides using locations to model states, using discrete variables to model states can be convenient to reduce the size of the model.</li>
                        <li>The state of a discrete event system modeled using automata with variables consists of the current locations of the automata, and the current values of the discrete variables.</li>
                        <li>The value of a variable can only be updated using assignments on edges within the same automaton as where the variable is declared.</li>
                        <li>An edge can have a guard that limits when a transition can be performed for the edge, while the updates and target location of the edge then determine the effect on the automaton's state once a transition is performed.</li>
                        <li>The initial state of a system is defined by the initial locations of its automata and the initial values of its variables.</li>
                        <li>Marked locations and variable values indicate states where the system is stable, and are safe havens that the system should be able to return back to.</li>
                        <li>The CIF simulator can be used to simulate CIF models, and analyze their behavior.</li>
                        <li>A water lock allows water vessels to bridge height differences in rivers and canals, consists of various components, and if not controlled properly is prone to various dangerous situations.</li>
                    </ul>
                    <p>
                        In this module, you dive deeper into the concepts related to automata, and you will learn more about using multiple automata that interact.
                        You learn about terms like reachability, deadlock, non-determinism and synchronization.
                        You extend your knowledge of modeling automata with CIF, and learn new ways of using variables.
                        And you will work with more CIF tools.
                    </p>
                    <p>
                        This module is divided into the following sub-modules:
                    </p>
                    <ul>
                        <li>Module 2.1: <a href="reachability-deadlock-blocking-livelock.html">Reachability, deadlock, blocking and livelock</a></li>
                        <li>Module 2.2: <a href="non-deterministic-automata.html">Non-deterministic automata</a></li>
                        <li>Module 2.3: <a href="synchronizing-automata.html">Synchronizing automata</a></li>
                        <li>Module 2.4: <a href="shared-variables.html">Shared variables</a></li>
                        <li>Module 2.5: <a href="computing-state-spaces.html">Computing state spaces</a></li>
                        <li>Module 2.6: <a href="cif-tools-view-analyze-models.html">CIF tools to view and analyze models</a></li>
                        <li>Module 2.7: <a href="constants-and-algebraic-variables.html">Constants and algebraic variables</a></li>
                        <li>Module 2.8: <a href="exercises.html">Exercises</a></li>
                        <li>Module 2.9: <a href="more-detailed-models-water-lock-case.html">More detailed models for the water lock case</a></li>
                    </ul>

                </div>
            </div>
        </div>

        <!-- Footer -->
        <div class="container" id="footer">
            <footer class="pt-3 mt-4">
                <ul class="nav justify-content-center border-top pb-1 mb-1">
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org">Eclipse Home</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/privacy.php">Eclipse Privacy Policy</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/termsofuse.php">Eclipse Terms of Use</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/copyright.php">Eclipse Copyright Agent</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal">Eclipse Legal</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.dev/escet/legal.html">Eclipse ESCET Legal</a></li>
                </ul>
                <p class="text-center text-muted">Copyright © 2010, 2024 Contributors to the Eclipse Foundation</p>
            </footer>
        </div>

    </body>
</html>
