<!doctype html>
<!--
    Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation

    See the NOTICE file(s) distributed with this work for additional
    information regarding copyright ownership.

    This program and the accompanying materials are made available under the terms
    of the MIT License which is available at https://opensource.org/licenses/MIT

    SPDX-License-Identifier: MIT
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Module 2.4 | SBE Course</title>
        <link rel="icon" type="image/png" href="../sbe-course-favicon.png"/>

        <!-- Bootstrap. -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.0/font/bootstrap-icons.css" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>

        <!-- Website styling. -->
        <link rel="stylesheet" href="../../assets/eclipse-escet.css">
        <link rel="stylesheet" href="../../assets/eclipse-escet-sbe-course.css">

        <!-- Version selector. -->
        <script src="../../assets/eclipse-escet.js"></script>

        <!-- Code block syntax highlighting. -->
        <link rel="stylesheet" href="../../assets/styles/asciidoctor-highlightjs-escet.min.css">
        <script src="../../assets/highlight.min.js"></script>
        <script src="../../assets/languages/cif.min.js"></script>
        <script src="../../assets/languages/tooldef.min.js"></script>

        <!-- Quiz. -->
        <link rel="stylesheet" href="../assets/quiz.css">
        <script src="../assets/quiz.js"></script>

        <!-- SBE course. -->
        <link rel="stylesheet" href="../assets/course.css">
        <script src="../assets/course.js"></script>
    </head>

    <body class="escet-dark">

        <!-- Top navigation bar -->
        <nav class="navbar navbar-expand-lg navbar-dark escet-bg">
            <div class="container-fluid">
                <a class="navbar-brand d-lg-none">SBE Course</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-0 mb-lg-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle escet-brand px-0" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Eclipse ESCET™ / SBE Course</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="../.."><img src="../../eclipse-escet-favicon.png" class="pe-2"> Eclipse ESCET / Project</a></li>
                                <li><a class="dropdown-item" href=".."><img src="../sbe-course-favicon.png" class="pe-2"> Eclipse ESCET / SBE Course</a></li>
                                <li><a class="dropdown-item" href="../../cif"><img src="../../cif/cif-favicon.png" class="pe-2"> Eclipse ESCET / CIF</a></li>
                                <li><a class="dropdown-item" href="../../chi"><img src="../../chi/chi-favicon.png" class="pe-2"> Eclipse ESCET / Chi</a></li>
                                <li><a class="dropdown-item" href="../../tooldef"><img src="../../tooldef/tooldef-favicon.png" class="pe-2"> Eclipse ESCET / ToolDef</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../" aria-expanded="false">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 1</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown1">
                                <li><a class="dropdown-item" href="../module1">Module 1: Supervisory control of discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/supervisory-control.html">Module 1.1: Supervisory control</a></li>
                                <li><a class="dropdown-item" href="../module1/discrete-event-systems.html">Module 1.2: Discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/modeling-discrete-event-systems.html">Module 1.3: Modeling discrete event systems</a></li>
                                <li><a class="dropdown-item" href="../module1/get-started-with-cif.html">Module 1.4: Get started with CIF</a></li>
                                <li><a class="dropdown-item" href="../module1/exercises.html">Module 1.5: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module1/introduction-to-the-water-lock-case.html">Module 1.6: Introduction to the water lock case</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle active" aria-current="page" href="#" id="navbarDropdown2" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 2</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown2">
                                <li><a class="dropdown-item" href="./">Module 2: Working with automata</a></li>
                                <li><a class="dropdown-item" href="reachability-deadlock-blocking-livelock.html">Module 2.1: Reachability, deadlock, blocking and livelock</a></li>
                                <li><a class="dropdown-item" href="non-deterministic-automata.html">Module 2.2: Non-deterministic automata</a></li>
                                <li><a class="dropdown-item" href="synchronizing-automata.html">Module 2.3: Synchronizing automata</a></li>
                                <li><a class="dropdown-item active" aria-current="page" href="shared-variables.html">Module 2.4: Shared variables</a></li>
                                <li><a class="dropdown-item" href="computing-state-spaces.html">Module 2.5: Computing state spaces</a></li>
                                <li><a class="dropdown-item" href="cif-tools-view-analyze-models.html">Module 2.6: CIF tools to view and analyze models</a></li>
                                <li><a class="dropdown-item" href="constants-and-algebraic-variables.html">Module 2.7: Constants and algebraic variables</a></li>
                                <li><a class="dropdown-item" href="exercises.html">Module 2.8: Exercises</a></li>
                                <li><a class="dropdown-item" href="more-detailed-models-water-lock-case.html">Module 2.9: More detailed models for the water lock case</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown3" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 3</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown3">
                                <li><a class="dropdown-item" href="../module3">Module 3: Supervisory control and plant models</a></li>
                                <li><a class="dropdown-item" href="../module3/supervisory-control-loop.html">Module 3.1: Supervisory control loop</a></li>
                                <li><a class="dropdown-item" href="../module3/synthesis-guarantees.html">Module 3.2: Synthesis guarantees</a></li>
                                <li><a class="dropdown-item" href="../module3/supervisory-controller-synthesis.html">Module 3.3: Supervisory controller synthesis</a></li>
                                <li><a class="dropdown-item" href="../module3/modeling-larger-systems.html">Module 3.4: Modeling larger systems</a></li>
                                <li><a class="dropdown-item" href="../module3/exercises.html">Module 3.5: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module3/water-lock-case-plant-models.html">Module 3.6: Water lock case plant models</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown4" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 4</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown4">
                                <li><a class="dropdown-item" href="../module4">Module 4: Modeling requirements and supervisor synthesis</a></li>
                                <li><a class="dropdown-item" href="../module4/user-specified-requirements.html">Module 4.1: User-specified requirements</a></li>
                                <li><a class="dropdown-item" href="../module4/requirement-automata-and-invariants.html">Module 4.2: Requirements automata and invariants</a></li>
                                <li><a class="dropdown-item" href="../module4/synthesis-with-requirements.html">Module 4.3: Synthesis with requirements</a></li>
                                <li><a class="dropdown-item" href="../module4/modeling-for-synthesis.html">Module 4.4: Modeling for synthesis</a></li>
                                <li><a class="dropdown-item" href="../module4/performing-synthesis-with-cif.html">Module 4.5: Performing synthesis with CIF</a></li>
                                <li><a class="dropdown-item" href="../module4/introduction-to-svg-visualization.html">Module 4.6: Introduction to SVG visualization</a></li>
                                <li><a class="dropdown-item" href="../module4/exercises.html">Module 4.7: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module4/water-lock-case-requirements-and-supervisor.html">Module 4.8: Water lock case requirements and supervisor</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown5" role="button" data-bs-toggle="dropdown" aria-expanded="false">Module 5</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown5">
                                <li><a class="dropdown-item" href="../module5">Module 5: Hybrid simulation models</a></li>
                                <li><a class="dropdown-item" href="../module5/timed-and-hybrid-models.html">Module 5.1: Timed and hybrid models</a></li>
                                <li><a class="dropdown-item" href="../module5/variable-time.html">Module 5.2: Variable 'time'</a></li>
                                <li><a class="dropdown-item" href="../module5/continuous-variables.html">Module 5.3: Continuous variables</a></li>
                                <li><a class="dropdown-item" href="../module5/urgency.html">Module 5.4: Urgency</a></li>
                                <li><a class="dropdown-item" href="../module5/tau-event.html">Module 5.5: The 'tau' event</a></li>
                                <li><a class="dropdown-item" href="../module5/exercises.html">Module 5.6: Exercises</a></li>
                                <li><a class="dropdown-item" href="../module5/water-lock-case-hybrid-model.html">Module 5.7: Water lock case hybrid model</a></li>
                                <li><a class="dropdown-item" href="../module5/course-conclusion.html">Module 5.8: Course conclusion</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Intro -->
        <div id="intro" class="position-relative p-3 text-center escet-bg escet-bg-opacity-125">
            <div class="clearfix mb-4 mb-md-0">
                <span class="float-end">
                    Version:
                    <div class="dropdown d-inline-block">
                        <a class="dropdown-toggle" href="#" role="button" id="versionDropdown" data-bs-toggle="dropdown" aria-expanded="false">@@ESCET-VERSION-ENDUSER@@</a>
                        <ul id="versions-dropdown" class="dropdown-menu dropdown-menu-end" aria-labelledby="versionDropdown" data-this-version="/@@ESCET-DEPLOY-FOLDER-NAME@@">
                            <li><h6 id="versions-previews" class="dropdown-header">Previews</h6></li>
                            <li><h6 id="versions-latest-releases" class="dropdown-header">Latest releases</h6></li>
                            <li><h6 class="dropdown-header">Other</h6></li>
                            <li><a class="dropdown-item" href="../../versions.html">All versions...</a></li>
                        </ul>
                    </div>
                    <a class="text-nowrap ms-3" href="../../release-notes.html">[release notes]</a>
                </span>
            </div>
        </div>

        <!-- Content -->
        <div class="px-4 pt-5">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-xl-8 col-xxl-6" id="content">

                    <h1>Module 2.4: Shared variables</h1>
                    <p>
                        Besides interaction through synchronization on events, automata can also interact by sharing discrete variables.
                        Discrete variables must be declared in automata, and only the automaton that declares a variable can change its value through updates on edges.
                        Variables can thus only be 'written' locally, by the automata that declare them.
                        Other automata can however access the values of the variables, and use them in for instance guards or assign them to their own variables.
                        Variables can thus be 'read' globally, by all automata.
                        This principle is called 'global read, local write'.
                        This prevents that multiple automata write different, conflicting values, when they synchronize on a shared event.
                    </p>

                    <h2 id="example">Example</h2>
                    <p>
                        Consider for instance the following example, with four automata.
                        The first automaton is the <code>buffer</code>:
                    </p>
                    <img src="images/shared-variables-example-buffer.png">
                    <p>
                        The second one is a <code>decision_unit</code>:
                    </p>
                    <img src="images/shared-variables-example-decision-unit.png">
                    <p>
                        The third and fourth are the <code>even_packer</code> and <code>odd_packer</code>, respectively:
                    </p>
                    <img src="images/shared-variables-example-even-packer.png">
                    <img src="images/shared-variables-example-odd-packer.png">
                    <p>
                        The four automata together model a production line.
                        The <code>buffer</code> automaton represents a buffer that can take products (event <code>take_product</code>) from a production process.
                        The variable <code>products</code> represents the number of products in the buffer.
                        At most 10 products can be stored in the buffer and the products are sold in batches of 5 to 10 products.
                        The operator executes the <code>send_batch</code> event when the next batch has reached the desired size.
                    </p>
                    <p>
                        There are two types of packages in which the batches are sold, even and odd ones, each packaged by a different packaging machine.
                        The <code>decision_unit</code> automaton represents a decision unit that decides whether a batch is to be sent to the even or odd packaging machine, once the batch is made available by the operator.
                        The <code>even_packer</code> automaton represents the packer for even batches.
                        Similarly, the <code>odd_packer</code> automaton represents the packer for odd batches.
                    </p>
                    <p>
                        The <code>products</code> variable is declared in the <code>buffer</code> automaton, and all updates to that variable are therefore done in that same automaton.
                        Due to the 'global read, local write' principle, other automata can not update the value of this variable, but they can read it.
                        The guards on edges of the <code>decision_unit</code> automaton use the value of the <code>products</code> variable declared in the <code>buffer</code> automaton.
                        To access the variable of another automaton, the variable name is prefixed with the name of that other automaton and a period, to form <code>buffer.products</code>.
                        This is similar to how in the previous sub-module an event declared in another automaton was used.
                    </p>
                    <p>
                        Remember that it is possible to label one edge with multiple events, as is used here in the <code>buffer</code> automaton.
                        The automaton can go from location <code>Sending</code> to location <code>Receiving</code> by either the <code>batch_to_even</code> event or the <code>batch_to_odd</code> event, but not both for the same batch.
                        Such an edge is essentially an abbreviation for multiple separate edges, each labeled with one of the events, but with the same source location, target location, guards and updates.
                        This improves the readability of the model, by not having to include many very similar edges.
                    </p>

                    <h2>Quiz</h2>
                    <div class="quiz">
                        [
                            {
                                type: 'single-choice',
                                question: "Can the <code>even_packer</code> automaton change the value of the <code>products</code> variable?",
                                answers: [
                                    "No, because the variable is declared in the <code>buffer</code> automaton and the 'local write' concept states that it can only be changed in the automaton where it is declared.",
                                    "Yes, an update can assign new values to variables. It does not matter in which automaton this update is located, because of the 'global read' concept."
                                ],
                                correctAnswer: '1'
                            },
                            {
                                type: 'single-choice',
                                question: "If the <code>odd_packer</code> automaton would have declared the variable <code>packages</code> and the <code>decision_unit</code> would like to use this variable in one of its guards, would this be possible, and if so, how should the <code>decision_unit</code> refer to this variable in its guard?",
                                answers: [
                                    "Yes, this is possible by referring to the variable with <code>packages.odd_packer</code>.",
                                    "Yes, this is possible by referring to the variable with <code>odd_packer.packages</code>.",
                                    "No, this is not possible because of the 'local read' concept."
                                ],
                                correctAnswer: '2'
                            },
                            {
                                type: 'single-choice',
                                question: "What are the shared events in the production line model?",
                                answers: [
                                    "Events <code>batch_to_even</code> and <code>batch_to_odd</code>.",
                                    "Events <code>batch_to_even</code>, <code>batch_to_odd</code> and <code>send_batch</code>.",
                                    "Events <code>batch_to_odd</code>, <code>finish_even</code> and <code>finish_odd</code>."
                                ],
                                correctAnswer: '2'
                            },
                            {
                                type: 'single-choice',
                                question: `
                                    When the <code>send_batch</code> event occurs, is then the batch also physically sent?
                                    What do you think?`,
                                answers: [
                                    `Yes, the <code>send_batch</code> event takes the <code>buffer</code> to its <code>Sending</code> location and the <code>decision_unit</code> to its <code>Deciding</code> location.
                                     The <code>send_batch</code> event is executed when the batch has the desired size according to the operator.
                                     So, this event must start the sending process.`,
                                    `This depends on the situation.
                                     If the decision unit is very fast, the actual sending that happens when the <code>batch_to_even</code> or <code>batch_to_odd</code> event is executed will almost coincide with the <code>send_batch</code> event.
                                     When the decision unit is slow, this will not coincide, and the <code>send_batch</code> event does not physically send the batch.`,
                                    `No, the <code>send_batch</code> event synchronizes the <code>buffer</code> and <code>decision_unit</code>.
                                     The <code>send_batch</code> event makes sure the decision unit only evaluates the batch size (variable <code>buffer.products</code>) on whether it is even or odd when the batch is ready to be sent.
                                     The physical sending happens with the <code>batch_to_even</code> or <code>batch_to_odd</code> event.
                                     These events cause the <code>products</code> variable to be reset to <code>0</code> in the <code>buffer</code> automaton, and change the locations of the packaging machines using synchronization.`
                                ],
                                correctAnswer: '3'
                            },
                            {
                                type: 'single-choice',
                                question: "Would it be possible to add the update <code>buffer.products := 0</code> to the edges with the <code>batch_to_even</code> and <code>batch_to_odd</code> events in the <code>decision_unit</code> automaton, instead of the update <code>products := 0</code> of the edge labeled with both the <code>batch_to_even</code> and <code>batch_to_odd</code> events in the <code>buffer</code> automaton?",
                                answers: [
                                    "No, because then you try to change a variable in a different automaton than in which it was declared. This is not possible according to the 'local write' concept.",
                                    "No, because then you try to change a variable in a different automaton than in which it was declared. This is not possible according to the 'global read' concept.",
                                    "Yes, an update can assign variables a new value. It does not matter in which automaton this update is declared because of the 'global write' concept."
                                ],
                                correctAnswer: '1'
                            },
                            {
                                type: 'single-choice',
                                question: `
                                    The packing machines can only pack one batch at a time.
                                    Is this restriction properly modeled?
                                    In other words, is it never the case that the packing machine receives another batch while it is still packing?`,
                                answers: [
                                    `This restriction is properly modeled.
                                     A machine can only receive a batch when the buffer, decision unit and packaging machine are in location <code>Sending</code>, <code>Deciding</code> and <code>Idle</code>, respectively, because in these three locations, an edge with event <code>batch_to_even</code> or <code>batch_to_odd</code> is present.
                                     This means that a machine first needs to finish a batch (event <code>finish_even</code> or <code>finish_odd</code>) to go back to its <code>Idle</code> location, before the three automata can synchronize on <code>batch_to_even</code> or <code>batch_to_odd</code> again, for a new batch.`,
                                    `This is not properly modeled.
                                     When the decision unit executes the <code>batch_to_even</code> or <code>batch_to_odd</code> event, the corresponding packaging machine will be sent a new batch without looking whether it is already busy.`
                                ],
                                correctAnswer: '1'
                            },
                            {
                                type: 'single-choice',
                                question: "If the even packer would like to know how many products it received from the buffer, how could that be achieved?",
                                answers: [
                                    "This is not possible, due to the 'local write' principle.",
                                    "This is not possible, due to the 'global read' principle.",
                                    "It could perform the <code>products := buffer.products</code> assignment, to assign its own local variable named <code>products</code>."
                                ],
                                correctAnswer: '3'
                            }
                        ]
                    </div>

                    <div class="course-navigation-footer">
                        <div class="course-navigation-prev"><a href="synchronizing-automata.html">Go back to Module 2.3</a></div>
                        <div class="course-navigation-separator">|</div>
                        <div class="course-navigation-next"><a href="computing-state-spaces.html">Proceed to Module 2.5</a></div>
                    </div>

                </div>
            </div>
        </div>

        <!-- Footer -->
        <div class="container" id="footer">
            <footer class="pt-3 mt-4">
                <ul class="nav justify-content-center border-top pb-1 mb-1">
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org">Eclipse Home</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/privacy.php">Eclipse Privacy Policy</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/termsofuse.php">Eclipse Terms of Use</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal/copyright.php">Eclipse Copyright Agent</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.org/legal">Eclipse Legal</a></li>
                    <li class="nav-link disabled text-muted">|</li>
                    <li class="nav-item"><a class="nav-link px-2 text-muted" href="https://eclipse.dev/escet/legal.html">Eclipse ESCET Legal</a></li>
                </ul>
                <p class="text-center text-muted">Copyright © 2010, 2024 Contributors to the Eclipse Foundation</p>
            </footer>
        </div>

    </body>
</html>
