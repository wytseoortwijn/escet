//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.codegen.options;

import static org.eclipse.escet.common.java.Strings.fmt;

import org.eclipse.escet.cif.common.CifValidationUtils;
import org.eclipse.escet.common.app.framework.Paths;
import org.eclipse.escet.common.app.framework.options.InputFileOption;
import org.eclipse.escet.common.app.framework.options.Options;
import org.eclipse.escet.common.app.framework.options.StringOption;
import org.eclipse.escet.common.java.Assert;
import org.eclipse.escet.common.java.exceptions.InvalidOptionException;

/** Code prefix option. */
public class CodePrefixOption extends StringOption {
    /** Constructor for the {@link CodePrefixOption} class. */
    public CodePrefixOption() {
        super(
                // name
                "Code prefix",

                // description
                "The code prefix, used to prefix file names, identifiers in the code, etc. "
                        + "If no prefix is given, the prefix is derived from the input filename, "
                        + "by removing the \".cif\" file extension, if present. This is also the default. "
                        + "The prefix must be a valid CIF identifier.",

                // cmdShort
                'p',

                // cmdLong
                "code-prefix",

                // cmdValue
                "PREFIX",

                // defaultValue
                null,

                // emptyAsNull
                true,

                // showInDialog
                true,

                // optDialogDescr
                "The code prefix, used to prefix file names, identifiers in the code, etc. "
                        + "The prefix must be a valid CIF identifier."
                        + "If no prefix is given, the prefix is derived from the input filename, "
                        + "by removing the \".cif\" file extension (if present), and adapting the "
                        + "result to be a valid CIF identifier.",

                // optDialogLabelText
                "Prefix:");
    }

    /**
     * Returns the code prefix.
     *
     * @return The code prefix.
     */
    public static String getPrefix() {
        String prefix = Options.get(CodePrefixOption.class);
        if (prefix == null) {
            // Derive prefix from input file name.
            prefix = InputFileOption.getPath();
            prefix = Paths.getFileName(prefix);
            prefix = Paths.pathChangeExtension(prefix, "cif", null);
            if (!CifValidationUtils.isValidIdentifier(prefix)) {
                prefix = prefix.replaceAll("[^A-Za-z0-9_]", "_");
                if (prefix.matches("[0-9].*")) {
                    prefix = "_" + prefix;
                }
                Assert.check(CifValidationUtils.isValidIdentifier(prefix));
            }
            return prefix;
        }

        if (!CifValidationUtils.isValidIdentifier(prefix)) {
            String msg = fmt("Code prefix \"%s\" specified using the code prefix option of the CIF code generator "
                    + "is not a valid CIF identifier.", prefix);
            throw new InvalidOptionException(msg);
        }
        return prefix;
    }
}
