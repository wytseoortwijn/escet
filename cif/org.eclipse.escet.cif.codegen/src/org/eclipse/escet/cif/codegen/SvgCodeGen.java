//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2023, 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.codegen;

import static org.eclipse.escet.common.java.Lists.list;
import static org.eclipse.escet.common.java.Maps.map;
import static org.eclipse.escet.common.java.Maps.mapc;
import static org.eclipse.escet.common.java.Sets.set;
import static org.eclipse.escet.common.java.Strings.fmt;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.escet.cif.common.CifEvalException;
import org.eclipse.escet.cif.common.CifEvalUtils;
import org.eclipse.escet.cif.metamodel.cif.IoDecl;
import org.eclipse.escet.cif.metamodel.cif.Specification;
import org.eclipse.escet.cif.metamodel.cif.cifsvg.SvgCopy;
import org.eclipse.escet.cif.metamodel.cif.cifsvg.SvgIn;
import org.eclipse.escet.cif.metamodel.cif.cifsvg.SvgInEvent;
import org.eclipse.escet.cif.metamodel.cif.cifsvg.SvgInEventIf;
import org.eclipse.escet.cif.metamodel.cif.cifsvg.SvgInEventIfEntry;
import org.eclipse.escet.cif.metamodel.cif.cifsvg.SvgInEventSingle;
import org.eclipse.escet.cif.metamodel.cif.cifsvg.SvgMove;
import org.eclipse.escet.cif.metamodel.cif.cifsvg.SvgOut;
import org.eclipse.escet.cif.metamodel.cif.declarations.Event;
import org.eclipse.escet.cif.metamodel.cif.expressions.EventExpression;
import org.eclipse.escet.cif.metamodel.cif.expressions.Expression;
import org.eclipse.escet.cif.metamodel.java.CifConstructors;
import org.eclipse.escet.common.app.framework.Paths;
import org.eclipse.escet.common.java.Assert;
import org.eclipse.escet.common.java.Strings;

/** SVG code generator for the target language. */
public abstract class SvgCodeGen {
    /**
     * Create environment events for the CIF/SVG input mappings with updates.
     *
     * @param svgIns The CIF/SVG input mappings.
     * @param cifSpecFileDir The absolute local file system path of the directory that contains the CIF specification.
     * @return Per SVG input mapping with updates, its environment event, sorted on event names.
     */
    public static Map<SvgIn, Event> createEnvironmentEvents(List<SvgIn> svgIns, String cifSpecFileDir) {
        Specification environmentSpec = CifConstructors.newSpecification(); // Container for the new events.
        Map<SvgIn, Event> environmentEvents = map();

        for (SvgIn svgIn: svgIns) {
            // Skip SVG input mappings with events.
            Assert.check((svgIn.getEvent() == null) != (svgIn.getUpdates().isEmpty()));
            if (svgIn.getEvent() != null) {
                continue;
            }

            // SVG input mapping with updates. Create an environment event for it.
            Assert.check(!svgIn.getUpdates().isEmpty());

            // Get event name.
            String svgInId = evalSvgStringExpr(svgIn.getId());
            String svgFileRelPath = svgIn.getSvgFile().getPath();
            String svgFileAbsPath = Paths.resolve(svgFileRelPath, cifSpecFileDir);
            String svgFileNormRelPath = Paths.getRelativePath(svgFileAbsPath, cifSpecFileDir);
            String eventName = fmt("<svgin id \"%s\" file \"%s\">", svgInId, svgFileNormRelPath);

            // Create metamodel event.
            Event event = CifConstructors.newEvent();
            event.setName(eventName);

            // Add event to the environment specification, such that it is contained and we can get its absolute name.
            environmentSpec.getDeclarations().add(event);

            // Add environment event to the mapping.
            environmentEvents.put(svgIn, event);
        }

        // Sort the environment events by their event names.
        Map<SvgIn, Event> sortedEnvironmentEvents = mapc(environmentEvents.size());
        Comparator<Entry<SvgIn, Event>> comparator = Comparator.comparing(e -> e.getValue().getName(), Strings.SORTER);
        environmentEvents.entrySet().stream().sorted(comparator).forEach(e -> {
            sortedEnvironmentEvents.put(e.getKey(), e.getValue());
        });

        // Return the environment events.
        return environmentEvents;
    }

    /**
     * Gets the interactive events, the events coupled to SVG input mappings that map to an event. This method ignores
     * SVG input mappings with updates.
     *
     * @param svgIns The SVG input mappings.
     * @return The interactive events.
     */
    public static Set<Event> getSvgInEvents(List<SvgIn> svgIns) {
        Set<Event> svgInEvents = set();
        for (SvgIn svgIn: svgIns) {
            // Skip SVG input mappings with updates.
            Assert.check((svgIn.getEvent() == null) != (svgIn.getUpdates().isEmpty()));
            if (!svgIn.getUpdates().isEmpty()) {
                continue;
            }

            // SVG input mapping that maps to an event.
            SvgInEvent svgInEvent = svgIn.getEvent();
            if (svgInEvent instanceof SvgInEventSingle singleEvt) {
                // Single event.
                Event event = ((EventExpression)singleEvt.getEvent()).getEvent();
                svgInEvents.add(event);
            } else if (svgInEvent instanceof SvgInEventIf ifEvent) {
                // 'if/then/else' event mapping.
                for (SvgInEventIfEntry entry: ifEvent.getEntries()) {
                    Event event = ((EventExpression)entry.getEvent()).getEvent();
                    svgInEvents.add(event);
                }
            } else {
                throw new RuntimeException("Unknown SVG input mapping event: " + svgInEvent);
            }
        }

        return svgInEvents;
    }

    /**
     * Filters the given CIF/SVG declarations to those for a certain SVG file, and group them per type.
     *
     * @param svgDecls The relevant CIF/SVG declarations of the specification to consider.
     * @param svgAbsPath The absolute local file system path of the SVG file to consider.
     * @param svgPathsRelToAbs Mapping from relative SVG file paths as used in SVG file declarations of the CIF/SVG
     *     declarations to their absolute paths.
     * @return The CIF/SVG declarations for the SVG file to consider, grouped per type.
     */
    public static CifSvgDecls filterAndGroup(List<IoDecl> svgDecls, String svgAbsPath,
            Map<String, String> svgPathsRelToAbs)
    {
        CifSvgDecls cifSvgDecls = new CifSvgDecls();
        for (IoDecl ioDecl: svgDecls) {
            if (ioDecl instanceof SvgCopy svgCopy) {
                if (svgAbsPath.equals(svgPathsRelToAbs.get(svgCopy.getSvgFile().getPath()))) {
                    cifSvgDecls.svgCopies.add(svgCopy);
                }
            } else if (ioDecl instanceof SvgMove svgMove) {
                if (svgAbsPath.equals(svgPathsRelToAbs.get(svgMove.getSvgFile().getPath()))) {
                    cifSvgDecls.svgMoves.add(svgMove);
                }
            } else if (ioDecl instanceof SvgOut svgOut) {
                if (svgAbsPath.equals(svgPathsRelToAbs.get(svgOut.getSvgFile().getPath()))) {
                    cifSvgDecls.svgOuts.add(svgOut);
                }
            } else if (ioDecl instanceof SvgIn svgIn) {
                if (svgAbsPath.equals(svgPathsRelToAbs.get(svgIn.getSvgFile().getPath()))) {
                    cifSvgDecls.svgIns.add(svgIn);
                }
            } else {
                throw new RuntimeException("Unexpected CIF/SVG declaration: " + ioDecl);
            }
        }
        return cifSvgDecls;
    }

    /**
     * Evaluates a CIF expression that can be statically evaluated for use by a CIF/SVG declaration.
     *
     * @param expr The expression to evaluate. The expression must have a string type.
     * @return The text resulting from evaluation of the expression.
     */
    public static String evalSvgStringExpr(Expression expr) {
        try {
            return (String)CifEvalUtils.eval(expr, false);
        } catch (CifEvalException e) {
            // Shouldn't happen, as type checker already evaluated it.
            throw new RuntimeException(e);
        }
    }

    /**
     * Evaluates a CIF expression that can be statically evaluated for use by a CIF/SVG declaration.
     *
     * @param expr The expression to evaluate. The expression must have an integer or real type.
     * @return The number resulting from evaluation of the expression.
     */
    protected static double evalSvgNumberExpr(Expression expr) {
        try {
            Object rslt = CifEvalUtils.eval(expr, false);
            if (rslt instanceof Integer) {
                return (int)rslt;
            }
            if (rslt instanceof Double) {
                return (double)rslt;
            }
            throw new RuntimeException("Number expected: " + rslt);
        } catch (CifEvalException e) {
            // Shouldn't happen, as type checker already evaluated it.
            throw new RuntimeException(e);
        }
    }

    /** CIF/SVG declarations for a single SVG file, grouped per type. */
    public static class CifSvgDecls {
        /** SVG copy declarations. */
        public final List<SvgCopy> svgCopies = list();

        /** SVG move declarations. */
        public final List<SvgMove> svgMoves = list();

        /** SVG output mappings. */
        public final List<SvgOut> svgOuts = list();

        /** SVG input mappings. */
        public final List<SvgIn> svgIns = list();

        /**
         * Returns the number of declarations for this SVG file.
         *
         * @return The number of declarations.
         */
        public int size() {
            return svgCopies.size() + svgMoves.size() + svgOuts.size() + svgIns.size();
        }
    }
}
