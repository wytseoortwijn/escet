//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.bdd.utils;

import static org.eclipse.escet.common.java.Lists.list;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertLinesMatch;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.eclipse.escet.cif.bdd.settings.CifBddSettings;
import org.eclipse.escet.cif.bdd.spec.CifBddInputVariable;
import org.eclipse.escet.cif.bdd.spec.CifBddLocPtrVariable;
import org.eclipse.escet.cif.bdd.spec.CifBddSpec;
import org.eclipse.escet.cif.bdd.spec.CifBddVariable;
import org.eclipse.escet.cif.metamodel.cif.Specification;
import org.eclipse.escet.cif.metamodel.cif.automata.Automaton;
import org.eclipse.escet.cif.metamodel.cif.automata.Location;
import org.eclipse.escet.cif.metamodel.cif.declarations.DiscVariable;
import org.eclipse.escet.cif.metamodel.cif.declarations.EnumDecl;
import org.eclipse.escet.cif.metamodel.cif.declarations.EnumLiteral;
import org.eclipse.escet.cif.metamodel.cif.declarations.InputVariable;
import org.eclipse.escet.cif.metamodel.cif.types.CifType;
import org.eclipse.escet.cif.metamodel.java.CifConstructors;
import org.junit.jupiter.api.Test;

import com.github.javabdd.BDD;
import com.github.javabdd.BDDDomain;
import com.github.javabdd.BDDFactory;
import com.github.javabdd.JFactory;

/** Unit tests for {@link BddUtils}. */
@SuppressWarnings("javadoc")
public class BddUtilsTest {
    // CIF specification.
    final EnumLiteral lit1;

    final EnumLiteral lit2;

    final EnumLiteral lit3;

    final Location loc1;

    final Location loc2;

    // BDD factory.
    final BDDFactory factory;

    // CIF/BDD variables.
    final CifBddVariable lpCifBddVar;

    final CifBddVariable boolCifBddVar;

    final CifBddVariable intCifBddVar;

    final CifBddVariable enumCifBddVar;

    // CIF/BDD specification.
    final CifBddSpec cifBddSpec;

    // States.
    final Map<CifBddVariable, Object> state1;

    final Map<CifBddVariable, Object> state2;

    final Map<CifBddVariable, Object> state3;

    final Map<CifBddVariable, Object> state4;

    List<Map<CifBddVariable, Object>> states;

    public BddUtilsTest() {
        // CIF specification.
        Specification spec = CifConstructors.newSpecification();

        lit1 = CifConstructors.newEnumLiteral(null, "L1", null);
        lit2 = CifConstructors.newEnumLiteral(null, "L2", null);
        lit3 = CifConstructors.newEnumLiteral(null, "L3", null);
        EnumDecl enumDecl = CifConstructors.newEnumDecl(null, list(lit1, lit2, lit3), "E", null);

        Automaton aut = CifConstructors.newAutomaton();
        aut.setName("A");
        loc1 = CifConstructors.newLocation();
        loc2 = CifConstructors.newLocation();
        loc1.setName("loc1");
        loc2.setName("loc2");
        aut.getLocations().add(loc1);
        aut.getLocations().add(loc2);
        spec.getComponents().add(aut);

        CifType boolType = CifConstructors.newBoolType(null);
        CifType intType = CifConstructors.newIntType(1, null, 5);
        CifType enumType = CifConstructors.newEnumType(enumDecl, null);

        DiscVariable lpVar = CifConstructors.newDiscVariable(null, "A", null, null, null);
        InputVariable boolVar = CifConstructors.newInputVariable(null, "b", null, boolType);
        InputVariable intVar = CifConstructors.newInputVariable(null, "i", null, intType);
        InputVariable enumVar = CifConstructors.newInputVariable(null, "e", null, enumType);
        spec.getDeclarations().add(lpVar);
        spec.getDeclarations().add(boolVar);
        spec.getDeclarations().add(intVar);
        spec.getDeclarations().add(enumVar);

        // BDD factory.
        factory = JFactory.init(5, 5);

        // CIF/BDD variables.
        lpCifBddVar = new CifBddLocPtrVariable(aut, lpVar);
        boolCifBddVar = new CifBddInputVariable(boolVar, boolType, 2, 0, 1);
        intCifBddVar = new CifBddInputVariable(intVar, intType, 5, 1, 5);
        enumCifBddVar = new CifBddInputVariable(enumVar, enumType, 3, 0, 2);

        lpCifBddVar.domain = factory.extDomain(new int[] {2, 2})[0]; // A#0, A#0+
        boolCifBddVar.domain = factory.extDomain(new int[] {2, 2})[0]; // b#0, b#0+
        intCifBddVar.domain = factory.extDomain(new int[] {6, 6})[0]; // i#0, i#0+, i#1, i#1+, i#2, i#2+
        enumCifBddVar.domain = factory.extDomain(new int[] {3, 3})[0]; // e#0, e#0+, e#1, e#1+

        // CIF/BDD specification.
        cifBddSpec = new CifBddSpec(new CifBddSettings());

        cifBddSpec.factory = factory;
        cifBddSpec.variables = new CifBddVariable[] {lpCifBddVar, boolCifBddVar, intCifBddVar, enumCifBddVar};
        cifBddSpec.varSetOld = factory.makeSet(
                new BDDDomain[] {lpCifBddVar.domain, boolCifBddVar.domain, intCifBddVar.domain, enumCifBddVar.domain});

        // States.
        state1 = new TreeMap<>(Comparator.comparing(v -> v.name));
        state2 = new TreeMap<>(Comparator.comparing(v -> v.name));
        state3 = new TreeMap<>(Comparator.comparing(v -> v.name));
        state4 = new TreeMap<>(Comparator.comparing(v -> v.name));
    }

    @Test
    public void testBddToStates() {
        // Declare some variables.
        List<Map<CifBddVariable, Object>> actuals;
        List<String> actualsTxts;
        List<String> expectedsTxts;

        // Test "b and i#0 and not i#2" = "b and (i = 1 or i = 3). Ignore BDD leaks.
        BDD b = factory.ithVar(2); // b
        BDD i0 = factory.ithVar(4); // i#0
        BDD ni2 = factory.ithVar(8).not(); // not i#2
        BDD bdd = b.and(i0).and(ni2); // b and i#0 and not i#2
        actuals = BddUtils.bddToStates(bdd, cifBddSpec, false, Integer.MAX_VALUE);
        actualsTxts = actuals.stream().map(s -> BddUtils.stateToStr(s, "")).toList();
        expectedsTxts = list("b=true, i=1", "b=true, i=3");
        assertLinesMatch(expectedsTxts, actualsTxts);

        // Test "i#0 or not i#2" = "(i = 1 or i = 3 or i = 5) or (i = 1 or i = 2 or i = 3)". Ignore BDD leaks.
        bdd = i0.or(ni2); // i#0 or not i#2
        actuals = BddUtils.bddToStates(bdd, cifBddSpec, false, Integer.MAX_VALUE);
        actualsTxts = actuals.stream().map(s -> BddUtils.stateToStr(s, "")).toList();
        expectedsTxts = list("i=1", "i=2", "i=3", "i=5");
        assertLinesMatch(expectedsTxts, actualsTxts);

        // Test previous test again, but with max state count.
        for (int maxStateCount = 0; maxStateCount < 7; maxStateCount++) {
            actuals = BddUtils.bddToStates(bdd, cifBddSpec, false, maxStateCount);
            assertTrue(actuals.size() <= maxStateCount, Integer.toString(maxStateCount));
            assertTrue(actuals.size() <= expectedsTxts.size(), Integer.toString(maxStateCount));
        }
    }

    @Test
    public void testBddToStatesEmpty() {
        CifBddSpec cifBddSpec = new CifBddSpec(new CifBddSettings());
        cifBddSpec.variables = new CifBddVariable[0];
        cifBddSpec.varSetOld = factory.makeSet(new BDDDomain[0]);

        List<Map<CifBddVariable, Object>> actuals;
        List<Map<CifBddVariable, Object>> expecteds;

        for (boolean inclDontCares: new boolean[] {false, true}) {
            // One state for 'true'.
            actuals = BddUtils.bddToStates(factory.one(), cifBddSpec, inclDontCares, Integer.MAX_VALUE);
            expecteds = List.of(new TreeMap<>());
            assertEquals(expecteds, actuals, String.valueOf(inclDontCares));

            // Zero states for 'true', if limited to zero.
            actuals = BddUtils.bddToStates(factory.one(), cifBddSpec, inclDontCares, 0);
            expecteds = List.of();
            assertEquals(expecteds, actuals, String.valueOf(inclDontCares));

            // Zero states for 'false'.
            actuals = BddUtils.bddToStates(factory.zero(), cifBddSpec, inclDontCares, Integer.MAX_VALUE);
            expecteds = List.of();
            assertEquals(expecteds, actuals, String.valueOf(inclDontCares));
        }
    }

    @Test
    public void testValuationToStates() {
        // Declare some variables.
        byte[] valuation;
        List<Map<CifBddVariable, Object>> expecteds, actuals;

        // Test valid concrete valuation, and sorting of variables within a state.
        valuation = new byte[] {/* A */ 0, -1, /* b */ 0, -1, /* i */ 1, -1, 0, -1, 0, -1, /* e */ 0, -1, 0, -1};
        actuals = BddUtils.valuationToStates(valuation, cifBddSpec, true, Integer.MAX_VALUE);
        state1.clear();
        state1.put(lpCifBddVar, loc1);
        state1.put(boolCifBddVar, false);
        state1.put(enumCifBddVar, lit1);
        state1.put(intCifBddVar, 1);
        expecteds = List.of(state1);
        assertEquals(expecteds, actuals);

        // Test invalid concrete valuation.
        valuation = new byte[] {/* A */ 0, -1, /* b */ 0, -1, /* i */ 0, -1, 0, -1, 0, -1, /* e */ 0, -1, 0, -1};
        actuals = BddUtils.valuationToStates(valuation, cifBddSpec, true, Integer.MAX_VALUE);
        expecteds = List.of();
        assertEquals(expecteds, actuals);

        // Test don't cares valuation for 'b'.
        valuation = new byte[] {/* A */ 0, -1, /* b */ -1, -1, /* i */ 1, -1, 0, -1, 0, -1, /* e */ 0, -1, 0, -1};
        actuals = BddUtils.valuationToStates(valuation, cifBddSpec, true, Integer.MAX_VALUE);
        state1.clear();
        state1.put(lpCifBddVar, loc1);
        state1.put(boolCifBddVar, false);
        state1.put(enumCifBddVar, lit1);
        state1.put(intCifBddVar, 1);
        state2.clear();
        state2.put(lpCifBddVar, loc1);
        state2.put(boolCifBddVar, true);
        state2.put(enumCifBddVar, lit1);
        state2.put(intCifBddVar, 1);
        expecteds = List.of(state1, state2);
        assertEquals(expecteds, actuals);

        // Test partial don't cares valuation for 'i', including one invalid value, and sorting of values.
        valuation = new byte[] {/* A */ 0, -1, /* b */ 0, -1, /* i */ -1, -1, -1, -1, 0, -1, /* e */ 0, -1, 0, -1};
        actuals = BddUtils.valuationToStates(valuation, cifBddSpec, true, Integer.MAX_VALUE);
        state1.clear();
        state1.put(lpCifBddVar, loc1);
        state1.put(boolCifBddVar, false);
        state1.put(enumCifBddVar, lit1);
        state1.put(intCifBddVar, 1);
        state2.clear();
        state2.put(lpCifBddVar, loc1);
        state2.put(boolCifBddVar, false);
        state2.put(enumCifBddVar, lit1);
        state2.put(intCifBddVar, 2);
        state3.clear();
        state3.put(lpCifBddVar, loc1);
        state3.put(boolCifBddVar, false);
        state3.put(enumCifBddVar, lit1);
        state3.put(intCifBddVar, 3);
        expecteds = List.of(state1, state2, state3);
        assertEquals(expecteds, actuals);

        // Test complete don't cares valuation for 'i', with 5 valid values.
        valuation = new byte[] {/* A */ 0, -1, /* b */ 0, -1, /* i */ -1, -1, -1, -1, -1, -1, /* e */ 0, -1, 0, -1};
        actuals = BddUtils.valuationToStates(valuation, cifBddSpec, true, Integer.MAX_VALUE);
        assertEquals(5, actuals.size());

        // Test don't cares for both 'A' and 'b'.
        valuation = new byte[] {/* A */ -1, -1, /* b */ -1, -1, /* i */ 1, -1, 0, -1, 0, -1, /* e */ 0, -1, 0, -1};
        actuals = BddUtils.valuationToStates(valuation, cifBddSpec, true, Integer.MAX_VALUE);
        state1.clear();
        state1.put(lpCifBddVar, loc1);
        state1.put(boolCifBddVar, false);
        state1.put(enumCifBddVar, lit1);
        state1.put(intCifBddVar, 1);
        state2.clear();
        state2.put(lpCifBddVar, loc1);
        state2.put(boolCifBddVar, true);
        state2.put(enumCifBddVar, lit1);
        state2.put(intCifBddVar, 1);
        state3.clear();
        state3.put(lpCifBddVar, loc2);
        state3.put(boolCifBddVar, false);
        state3.put(enumCifBddVar, lit1);
        state3.put(intCifBddVar, 1);
        state4.clear();
        state4.put(lpCifBddVar, loc2);
        state4.put(boolCifBddVar, true);
        state4.put(enumCifBddVar, lit1);
        state4.put(intCifBddVar, 1);
        expecteds = List.of(state1, state2, state3, state4);
        assertEquals(expecteds, actuals);

        // Test skip don't cares.
        valuation = new byte[] {/* A */ -1, -1, /* b */ -1, -1, /* i */ -1, -1, -1, -1, 0, -1, /* e */ 0, -1, 0, -1};
        actuals = BddUtils.valuationToStates(valuation, cifBddSpec, false, Integer.MAX_VALUE);
        state1.clear();
        state1.put(enumCifBddVar, lit1);
        state1.put(intCifBddVar, 1);
        state2.clear();
        state2.put(enumCifBddVar, lit1);
        state2.put(intCifBddVar, 2);
        state3.clear();
        state3.put(enumCifBddVar, lit1);
        state3.put(intCifBddVar, 3);
        expecteds = List.of(state1, state2, state3);
        assertEquals(expecteds, actuals);

        // Test max state count (valuation has 3 valid states in total).
        valuation = new byte[] {/* A */ -1, -1, /* b */ -1, -1, /* i */ -1, -1, -1, -1, 0, -1, /* e */ 0, -1, 0, -1};
        for (int maxStateCount = 0; maxStateCount < 5; maxStateCount++) {
            actuals = BddUtils.valuationToStates(valuation, cifBddSpec, false, maxStateCount);
            assertTrue(actuals.size() <= maxStateCount, Integer.toString(maxStateCount));
            assertTrue(actuals.size() <= 3, Integer.toString(maxStateCount));
        }
    }

    @Test
    public void testValuationToStatesEmpty() {
        CifBddSpec cifBddSpec = new CifBddSpec(new CifBddSettings());
        cifBddSpec.variables = new CifBddVariable[0];

        List<Map<CifBddVariable, Object>> actuals;
        List<Map<CifBddVariable, Object>> expecteds;

        // One empty state for an empty valuation.
        actuals = BddUtils.valuationToStates(new byte[0], cifBddSpec, true, Integer.MAX_VALUE);
        expecteds = List.of(new TreeMap<>());
        assertEquals(expecteds, actuals);

        // No empty state for an empty valuation if limit is zero.
        actuals = BddUtils.valuationToStates(new byte[0], cifBddSpec, true, 0);
        expecteds = List.of();
        assertEquals(expecteds, actuals);
    }

    @Test
    public void testValuationToValue() {
        // Test location values.
        assertEquals(loc1, BddUtils.valuationToValue(lpCifBddVar, new byte[] {0}, factory));
        assertEquals(loc2, BddUtils.valuationToValue(lpCifBddVar, new byte[] {1}, factory));

        // Test boolean values.
        assertEquals(false, BddUtils.valuationToValue(boolCifBddVar, new byte[] {0}, factory));
        assertEquals(true, BddUtils.valuationToValue(boolCifBddVar, new byte[] {1}, factory));

        // Test integer values.
        assertEquals(null, BddUtils.valuationToValue(intCifBddVar, new byte[] {0, 0, 0}, factory));
        assertEquals(1, BddUtils.valuationToValue(intCifBddVar, new byte[] {1, 0, 0}, factory));
        assertEquals(2, BddUtils.valuationToValue(intCifBddVar, new byte[] {0, 1, 0}, factory));
        assertEquals(3, BddUtils.valuationToValue(intCifBddVar, new byte[] {1, 1, 0}, factory));
        assertEquals(4, BddUtils.valuationToValue(intCifBddVar, new byte[] {0, 0, 1}, factory));
        assertEquals(5, BddUtils.valuationToValue(intCifBddVar, new byte[] {1, 0, 1}, factory));
        assertEquals(null, BddUtils.valuationToValue(intCifBddVar, new byte[] {0, 1, 1}, factory));
        assertEquals(null, BddUtils.valuationToValue(intCifBddVar, new byte[] {1, 1, 1}, factory));

        // Test enum literal values.
        assertEquals(lit1, BddUtils.valuationToValue(enumCifBddVar, new byte[] {0, 0}, factory));
        assertEquals(lit2, BddUtils.valuationToValue(enumCifBddVar, new byte[] {1, 0}, factory));
        assertEquals(lit3, BddUtils.valuationToValue(enumCifBddVar, new byte[] {0, 1}, factory));
        assertEquals(null, BddUtils.valuationToValue(enumCifBddVar, new byte[] {1, 1}, factory));
    }

    @Test
    public void testSortStates() {
        // Test two same states: "b=false, i=2".
        state1.clear();
        state2.clear();
        state1.put(boolCifBddVar, false);
        state1.put(intCifBddVar, 2);
        state2.put(boolCifBddVar, false);
        state2.put(intCifBddVar, 2);
        states = list(state1, state2);
        BddUtils.sortStates(states);
        assertEquals(list(state1, state2), states);

        // Test different (first) variables: "b=true, i=2", "i=2", "E=L2".
        state1.clear();
        state2.clear();
        state3.clear();
        state1.put(boolCifBddVar, true);
        state1.put(intCifBddVar, 2);
        state2.put(intCifBddVar, 2);
        state3.put(enumCifBddVar, lit2);
        states = list(state1, state2, state3);
        BddUtils.sortStates(states);
        assertEquals(list(state1, state3, state2), states);

        // Test same/different location values: "A=loc2", "A=loc1", "A=loc1".
        state1.clear();
        state2.clear();
        state3.clear();
        state1.put(lpCifBddVar, loc2);
        state2.put(lpCifBddVar, loc1);
        state3.put(lpCifBddVar, loc1);
        states = list(state1, state2, state3);
        BddUtils.sortStates(states);
        assertEquals(list(state2, state3, state1), states);

        // Test same/different boolean values: "i=false", "i=true", "i=false".
        state1.clear();
        state2.clear();
        state3.clear();
        state1.put(boolCifBddVar, false);
        state2.put(boolCifBddVar, true);
        state3.put(boolCifBddVar, false);
        states = list(state1, state2, state3);
        BddUtils.sortStates(states);
        assertEquals(list(state1, state3, state2), states);

        // Test same/different integer values: "i=1", "i=0", "i=1".
        state1.clear();
        state2.clear();
        state3.clear();
        state1.put(intCifBddVar, 1);
        state2.put(intCifBddVar, 0);
        state3.put(intCifBddVar, 1);
        states = list(state1, state2, state3);
        BddUtils.sortStates(states);
        assertEquals(list(state2, state1, state3), states);

        // Test same/different enum literal values: "e=lit1", "e=lit2", "e=lit1".
        state1.clear();
        state2.clear();
        state3.clear();
        state1.put(enumCifBddVar, lit1);
        state2.put(enumCifBddVar, lit2);
        state3.put(enumCifBddVar, lit1);
        states = list(state1, state2, state3);
        BddUtils.sortStates(states);
        assertEquals(list(state1, state3, state2), states);

        // Test combinations: "b=false, i=2", "A=loc1, b=true", "b=false, i=1".
        state1.clear();
        state2.clear();
        state3.clear();
        state1.put(boolCifBddVar, false);
        state1.put(intCifBddVar, 2);
        state2.put(lpCifBddVar, loc1);
        state2.put(boolCifBddVar, true);
        state3.put(boolCifBddVar, false);
        state3.put(intCifBddVar, 1);
        states = list(state1, state2, state3);
        BddUtils.sortStates(states);
        assertEquals(list(state2, state3, state1), states);
    }

    @Test
    public void testStateToStr() {
        // Test state with variables.
        state1.clear();
        state1.put(lpCifBddVar, loc2);
        state1.put(boolCifBddVar, true);
        state1.put(enumCifBddVar, lit2);
        state1.put(intCifBddVar, 5);
        assertEquals("A=loc2, b=true, e=L2, i=5", BddUtils.stateToStr(state1, ""));

        // Test state without variables.
        state1.clear();
        assertEquals("<txt>", BddUtils.stateToStr(state1, "<txt>"));
    }

    @Test
    public void testStateValueToStr() {
        // Test locations.
        Location locL = CifConstructors.newLocation();
        Location locConst = CifConstructors.newLocation();
        locL.setName("L");
        locConst.setName("const");
        assertEquals("L", BddUtils.stateValueToStr(locL));
        assertEquals("const", BddUtils.stateValueToStr(locConst));

        // Test booleans.
        assertEquals("false", BddUtils.stateValueToStr(false));
        assertEquals("true", BddUtils.stateValueToStr(true));

        // Test integers.
        assertEquals("0", BddUtils.stateValueToStr(0));
        assertEquals("1", BddUtils.stateValueToStr(1));
        assertEquals("23", BddUtils.stateValueToStr(23));

        // Test enumeration literals.
        EnumLiteral litA = CifConstructors.newEnumLiteral();
        EnumLiteral litConst = CifConstructors.newEnumLiteral();
        litA.setName("A");
        litConst.setName("const");
        assertEquals("A", BddUtils.stateValueToStr(litA));
        assertEquals("const", BddUtils.stateValueToStr(litConst));
    }
}
