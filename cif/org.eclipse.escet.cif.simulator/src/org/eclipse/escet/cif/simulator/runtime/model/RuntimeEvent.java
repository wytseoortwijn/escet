//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.simulator.runtime.model;

import org.eclipse.escet.cif.common.CifTextUtils;
import org.eclipse.escet.cif.common.CifValidationUtils;
import org.eclipse.escet.common.java.Assert;

/**
 * Runtime event representation.
 *
 * @param <S> The type of state objects to use.
 */
public abstract class RuntimeEvent<S extends RuntimeState> {
    /**
     * The name of the event. For {@link RuntimeEventKind#REGULAR regular events}, it is the absolute name of the event
     * in the CIF model, with keyword escaping, obtained via {@link CifTextUtils#getAbsName}. For
     * {@link RuntimeEventKind#TAU event 'tau'}, it is {@code "tau"}. For {@link RuntimeEventKind#ENVIRONMENT
     * environment events}, it is decided by the corresponding input component, but to avoid confusion it doesn't
     * overlap with the syntax for possibly-escaped absolute CIF names.
     */
    public final String name;

    /** The unique 0-based index of the event. */
    public final int idx;

    /** The kind of runtime event. */
    public final RuntimeEventKind kind;

    /** Is the event controllable? {@code null} for neither controllable nor uncontrollable. */
    public final Boolean controllable;

    /**
     * Constructor for the {@link RuntimeEvent} class.
     *
     * @param name The name of the event. For {@link RuntimeEventKind#REGULAR regular events}, it must be the absolute
     *     name of the event in the CIF model, with keyword escaping, obtained via {@link CifTextUtils#getAbsName}. For
     *     {@link RuntimeEventKind#TAU event 'tau'}, it must be {@code "tau"}. For {@link RuntimeEventKind#ENVIRONMENT
     *     environment events}, it may be decided by the corresponding input component, but to avoid confusion it must
     *     not overlap with the syntax for possibly-escaped absolute CIF names.
     * @param idx The unique 0-based index of the event.
     * @param kind The kind of runtime event.
     * @param controllable Is the event controllable? {@code null} for neither controllable nor uncontrollable.
     */
    public RuntimeEvent(String name, int idx, RuntimeEventKind kind, Boolean controllable) {
        this.name = name;
        this.idx = idx;
        this.kind = kind;
        this.controllable = controllable;

        Assert.areEqual(kind == RuntimeEventKind.TAU, name.equals("tau"));
        Assert.implies(kind == RuntimeEventKind.TAU, controllable == null);

        Assert.areEqual(kind == RuntimeEventKind.ENVIRONMENT, !CifValidationUtils.isValidName(name.replace("$", "")));
        Assert.implies(kind == RuntimeEventKind.ENVIRONMENT, controllable == null);
    }
}
