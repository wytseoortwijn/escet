//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.simulator.compiler;

import static org.apache.commons.text.StringEscapeUtils.escapeJava;
import static org.eclipse.escet.cif.common.CifTextUtils.exprsToStr;
import static org.eclipse.escet.cif.simulator.compiler.AssignmentCodeGenerator.gencodeAssignment;
import static org.eclipse.escet.cif.simulator.compiler.ExprCodeGenerator.gencodePreds;
import static org.eclipse.escet.common.java.Lists.list;
import static org.eclipse.escet.common.java.Strings.truncate;

import java.util.List;

import org.eclipse.escet.cif.metamodel.cif.automata.Assignment;
import org.eclipse.escet.cif.metamodel.cif.automata.Automaton;
import org.eclipse.escet.cif.metamodel.cif.automata.ElifUpdate;
import org.eclipse.escet.cif.metamodel.cif.automata.IfUpdate;
import org.eclipse.escet.cif.metamodel.cif.automata.Update;
import org.eclipse.escet.common.box.CodeBox;

/** Updates code generator. */
public class UpdatesCodeGenerator {
    /** Constructor for the {@link UpdatesCodeGenerator} class. */
    private UpdatesCodeGenerator() {
        // Static class.
    }

    /**
     * Generate Java code for the given updates, of edges or SVG input mappings.
     *
     * @param c The code box to which to write the code.
     * @param aut The automaton, or {@code null} for SVG input mappings.
     * @param ctxt The compiler context to use.
     * @param updates The updates.
     * @param state The name of the source state variable in the context where the generated code is used.
     * @return The {@code ExprCodeGeneratorResult}s for the generated Java code.
     */
    public static List<ExprCodeGeneratorResult> gencodeUpdates(CodeBox c, Automaton aut, CifCompilerContext ctxt,
            List<Update> updates, String state)
    {
        List<ExprCodeGeneratorResult> exprResults = list();
        for (Update update: updates) {
            if (update instanceof Assignment) {
                Assignment asgn = (Assignment)update;
                exprResults.addAll(gencodeAssignment(asgn.getAddressable(), asgn.getValue(), aut, c, ctxt, state));
            } else {
                exprResults.addAll(gencodeIfUpdate(c, aut, ctxt, (IfUpdate)update, state));
            }
        }
        return exprResults;
    }

    /**
     * Generate Java code for the given 'if' update.
     *
     * @param c The code box to which to write the code.
     * @param aut The automaton, or {@code null} for SVG input mappings.
     * @param ctxt The compiler context to use.
     * @param update The 'if' update.
     * @param state The name of the source state variable in the context where the generated code is used.
     * @return The {@code ExprCodeGeneratorResult}s for the generated Java code.
     */
    private static List<ExprCodeGeneratorResult> gencodeIfUpdate(CodeBox c, Automaton aut, CifCompilerContext ctxt,
            IfUpdate update, String state)
    {
        List<ExprCodeGeneratorResult> exprResults = list();
        // Start of 'try'.
        c.add("try {");
        c.indent();

        // If guards.
        ExprCodeGeneratorResult updateResult = gencodePreds(update.getGuards(), ctxt, state);
        c.add("b = %s;", updateResult);
        exprResults.add(updateResult);

        // End of 'try'.
        c.dedent();
        c.add("} catch (CifSimulatorException e) {");
        c.indent();
        c.add("throw new CifSimulatorException(\"Evaluation of \\\"if\\\" update guard(s) \\\"%s\\\" failed.\", e, "
                + "source);", escapeJava(truncate(exprsToStr(update.getGuards()), 1000)));
        c.dedent();
        c.add("}");

        // If updates.
        c.add("if (b) {");
        c.indent();
        exprResults.addAll(gencodeUpdates(c, aut, ctxt, update.getThens(), state));
        c.dedent();

        // Elifs.
        for (ElifUpdate elifUpd: update.getElifs()) {
            c.add("} else {");
            c.indent();

            // Start of 'try'.
            c.add("try {");
            c.indent();

            // Elif guards.
            ExprCodeGeneratorResult elifUpdResult = gencodePreds(elifUpd.getGuards(), ctxt, state);
            c.add("b = %s;", elifUpdResult);
            exprResults.add(updateResult);

            // End of 'try'.
            c.dedent();
            c.add("} catch (CifSimulatorException e) {");
            c.indent();
            c.add("throw new CifSimulatorException(\"Evaluation of \\\"elif\\\" update guard(s) \\\"%s\\\" failed.\", "
                    + "e, source);", escapeJava(truncate(exprsToStr(elifUpd.getGuards()), 1000)));
            c.dedent();
            c.add("}");

            // Elif updates.
            c.add("if (b) {");
            c.indent();
            exprResults.addAll(gencodeUpdates(c, aut, ctxt, elifUpd.getThens(), state));
            c.dedent();
        }

        // Else.
        if (!update.getElses().isEmpty()) {
            c.add("} else {");
            c.indent();
            exprResults.addAll(gencodeUpdates(c, aut, ctxt, update.getElses(), state));
            c.dedent();
        }

        // Close elifs.
        for (int i = 0; i < update.getElifs().size(); i++) {
            c.add("}");
            c.dedent();
        }

        // Close if.
        c.add("}");

        return exprResults;
    }
}
