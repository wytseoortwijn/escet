//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.simulator.options;

import org.eclipse.escet.common.app.framework.options.StringOption;
import org.eclipse.escet.common.java.exceptions.InvalidOptionException;

/** Environment events option. */
public class EnvironmentEventsOption extends StringOption {
    /** Message to indicate the option is unsupported. */
    private static final String UNSUPPORTED_MESSAGE = "This option is no longer supported. "
            + "It will be removed in a future version of the tool. "
            + "Use the 'Non-urgent events' option instead.";

    /** Constructor for the {@link EnvironmentEventsOption} class. */
    public EnvironmentEventsOption() {
        super("Environment events", "Option to specify the environment events. " + UNSUPPORTED_MESSAGE, null,
                "env-events", "EVTS", "", false, false, null, null);
    }

    @Override
    public String parseValue(String optName, String value) {
        throw new InvalidOptionException("The environment events option is used. " + UNSUPPORTED_MESSAGE);
    }
}
