//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.simulator.input;

import org.eclipse.escet.cif.simulator.runtime.model.RuntimeEnvironmentEvent;
import org.eclipse.escet.cif.simulator.runtime.model.RuntimeState;

/**
 * SVG input component runtime environment event.
 *
 * @param <S> The type of state objects to use.
 */
public abstract class SvgInputRuntimeEnvironmentEvent<S extends RuntimeState> extends RuntimeEnvironmentEvent<S> {
    /**
     * Constructor for the {@link RuntimeEnvironmentEvent} class.
     *
     * @param name The name of the environment event. To avoid confusion it must not overlap with the syntax for
     *     possibly-escaped absolute CIF names.
     * @param idx The unique 0-based index of the event.
     */
    public SvgInputRuntimeEnvironmentEvent(String name, int idx) {
        super(name, idx);
    }

    /**
     * Applies the updates of the SVG input mapping to the target location.
     *
     * <p>
     * The provided {@code target} state is a shallow copy of the {@code source} state. Implementations of this method
     * should copy the input variables sub-state and modify it.
     * </p>
     *
     * @param source The source state to use for evaluations.
     * @param target The target state to update.
     */
    public abstract void update(S source, S target);
}
