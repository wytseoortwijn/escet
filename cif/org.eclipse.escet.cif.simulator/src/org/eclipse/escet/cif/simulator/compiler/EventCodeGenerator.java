//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.simulator.compiler;

import static org.apache.commons.text.StringEscapeUtils.escapeJava;
import static org.eclipse.escet.cif.common.CifTextUtils.getAbsName;
import static org.eclipse.escet.cif.common.CifTextUtils.invToStr;
import static org.eclipse.escet.cif.simulator.compiler.CifCompilerContext.INPUT_SUB_STATE_FIELD_NAME;
import static org.eclipse.escet.cif.simulator.compiler.CifCompilerContext.REGULAR_TAU_EVENT_CLS_PREFIX;
import static org.eclipse.escet.cif.simulator.compiler.ExprCodeGenerator.gencodeExpr;
import static org.eclipse.escet.common.app.framework.output.OutputProvider.warn;
import static org.eclipse.escet.common.java.Lists.list;
import static org.eclipse.escet.common.java.Strings.fmt;
import static org.eclipse.escet.common.java.Strings.truncate;

import java.util.List;
import java.util.Map.Entry;

import org.eclipse.escet.cif.common.CifTextUtils;
import org.eclipse.escet.cif.metamodel.cif.ComplexComponent;
import org.eclipse.escet.cif.metamodel.cif.Component;
import org.eclipse.escet.cif.metamodel.cif.Group;
import org.eclipse.escet.cif.metamodel.cif.InvKind;
import org.eclipse.escet.cif.metamodel.cif.Invariant;
import org.eclipse.escet.cif.metamodel.cif.Specification;
import org.eclipse.escet.cif.metamodel.cif.SupKind;
import org.eclipse.escet.cif.metamodel.cif.automata.Automaton;
import org.eclipse.escet.cif.metamodel.cif.automata.Location;
import org.eclipse.escet.cif.metamodel.cif.cifsvg.SvgIn;
import org.eclipse.escet.cif.metamodel.cif.declarations.Event;
import org.eclipse.escet.cif.metamodel.cif.expressions.EventExpression;
import org.eclipse.escet.cif.metamodel.cif.expressions.Expression;
import org.eclipse.escet.cif.simulator.compiler.CifSvgCodeGenerator.CifSvgDecls;
import org.eclipse.escet.cif.simulator.runtime.model.RuntimeEventKind;
import org.eclipse.escet.common.app.framework.Paths;
import org.eclipse.escet.common.box.CodeBox;
import org.eclipse.escet.common.java.Assert;

/** Event code generator. */
public class EventCodeGenerator {
    /** Constructor for the {@link EventCodeGenerator} class. */
    private EventCodeGenerator() {
        // Static class.
    }

    /**
     * Generate Java code for the events of a specification.
     *
     * @param spec The CIF specification.
     * @param ctxt The compiler context to use.
     */
    public static void gencodeEvents(Specification spec, CifCompilerContext ctxt) {
        // Get all events from the specification, including 'tau'.
        List<Event> events = ctxt.getEvents();

        // Generate a unique name for 'tau' first, to avoid renaming of 'tau'.
        Event tau = ctxt.tauEvent;
        String tauName = ctxt.getEventClassName(tau);
        Assert.check(tauName.equals(REGULAR_TAU_EVENT_CLS_PREFIX + "_tau"));

        // Generate code for environment events of input components.
        gencodeEnvironmentEvents(spec, ctxt);

        // Generate code for regular and 'tau' events.
        gencodeRegularAndTauEvents(spec, events, ctxt);
    }

    /**
     * Generate Java code for the environment events of the input components.
     *
     * <p>
     * Note that we generate code for the environment events of all input components that need them, but only the input
     * component that is used will add its environment events to the runtime specification. We may however save the
     * generated code and perform multiple simulations with it, so we do need to generate all the events.
     * </p>
     *
     * @param spec The CIF specification.
     * @param ctxt The compiler context to use.
     */
    private static void gencodeEnvironmentEvents(Specification spec, CifCompilerContext ctxt) {
        // Get environment event infos.
        List<EnvironmentEventInfo> infos = getEnvironmentEventInfos(spec, ctxt);

        // Register environment infos with the CIF compiler context, ensuring they get their runtime event indices.
        ctxt.setEnvironmentEvents(infos);

        // Generate code for the environment events.
        for (EnvironmentEventInfo info: infos) {
            if (info instanceof SvgInputEnvironmentEventInfo svgInfo) {
                gencodeSvgInputEnvironmentEvent(svgInfo, ctxt);
            } else {
                throw new AssertionError("Unknown info: " + info);
            }
        }
    }

    /**
     * Get info about the environment events of the input components that are to be created.
     *
     * @param spec The CIF specification.
     * @param ctxt The compiler context to use.
     * @return The environment event infos.
     */
    private static List<EnvironmentEventInfo> getEnvironmentEventInfos(Specification spec, CifCompilerContext ctxt) {
        // Initialize the infos.
        List<EnvironmentEventInfo> infos = list();

        // Add environment event infos for the SVG input component, for SVG input mappings with updates.
        int svgFileIdx = -1;
        for (Entry<String, CifSvgDecls> entry: ctxt.getSvgFileToDecls().entrySet()) {
            svgFileIdx++;
            for (SvgIn svgIn: entry.getValue().svgIns) {
                if (!svgIn.getUpdates().isEmpty()) {
                    String svgInId = CifSvgCodeGenerator.evalSvgStringExpr(svgIn.getId());
                    String svgFileRelPath = Paths.getRelativePath(entry.getKey(), ctxt.getSpecFileDir());
                    String eventName = fmt("<svgin id \"%s\" file \"%s\">", svgInId, svgFileRelPath);
                    infos.add(new SvgInputEnvironmentEventInfo(eventName, svgFileIdx, svgFileRelPath, svgIn));
                }
            }
        }

        // Return the infos.
        return infos;
    }

    /**
     * Generate Java code for an SVG input environment event.
     *
     * @param info The SVG input environment event info.
     * @param ctxt The compiler context to use.
     */
    private static void gencodeSvgInputEnvironmentEvent(SvgInputEnvironmentEventInfo info, CifCompilerContext ctxt) {
        // Add new code file.
        String className = ctxt.getEventClassName(info);
        JavaCodeFile file = ctxt.addCodeFile(className);

        // Add header.
        CodeBox h = file.header;
        h.add("/** Event \"%s\". */", info.eventName);
        h.add("public final class %s extends SvgInputRuntimeEnvironmentEvent<State> {", className);

        // Add body.
        CodeBox c = file.body;

        // Add constructor.
        c.add();
        c.add("public %s() {", className);
        c.indent();
        c.add("super(\"%s\", %d);", escapeJava(info.eventName), ctxt.getRuntimeEventIdx(info));
        c.dedent();
        c.add("}");

        // Add 'update' method.
        c.add();
        c.add("@Override");
        c.add("public void update(State source, State target) {");
        c.indent();

        // Generate variable 'b', for evaluation of predicates.
        // Reused for guards of if/elif/etc.
        c.add("boolean b; // temp var for pred eval rslts");

        // Copy input variables sub-state.
        c.add("target.%s = target.%s.copy();", INPUT_SUB_STATE_FIELD_NAME, INPUT_SUB_STATE_FIELD_NAME);

        // Start of 'try'.
        c.add("try {");
        c.indent();

        // Apply actual updates.
        List<ExprCodeGeneratorResult> updateResults = UpdatesCodeGenerator.gencodeUpdates(c, null, ctxt,
                info.svgIn.getUpdates(), "source");

        // End of 'try'.
        String svgInId = CifSvgCodeGenerator.evalSvgStringExpr(info.svgIn.getId());
        c.dedent();
        c.add("} catch (CifSimulatorException e) {");
        c.indent();
        c.add("throw new CifSimulatorException(\"Execution of the updates of the SVG input mapping (\\\"%s\\\") "
                + "for the SVG element with id \\\"%s\\\" failed.\", e, source);",
                escapeJava(info.svgFileRelPath), escapeJava(svgInId));
        c.dedent();
        c.add("}");

        c.dedent();
        c.add("}");

        // Add potential extra update expression evaluation methods.
        for (ExprCodeGeneratorResult updateResult: updateResults) {
            updateResult.addExtraMethods(c);
        }
    }

    /**
     * Generate Java code for the regular and 'tau' events of a specification.
     *
     * @param spec The CIF specification.
     * @param events The events of the CIF specification, including event 'tau' as last event.
     * @param ctxt The compiler context to use.
     */
    public static void gencodeRegularAndTauEvents(Specification spec, List<Event> events, CifCompilerContext ctxt) {
        // Generate code for each event.
        for (Event event: events) {
            // Get relevant information about the event.
            boolean isTau = event == ctxt.tauEvent;
            boolean isComm = event.getType() != null;
            String absName = isTau ? "tau" : getAbsName(event);
            String ctrlTxt = (event.getControllable() == null) ? "null" : event.getControllable() ? "true" : "false";
            int runtimeEventIdx = ctxt.getRuntimeEventIdx(event);
            RuntimeEventKind kind = ctxt.getRuntimeEventKind(runtimeEventIdx);

            // Add new code file.
            String className = ctxt.getEventClassName(event);
            JavaCodeFile file = ctxt.addCodeFile(className);

            // Add header.
            CodeBox h = file.header;
            h.add("/** Event \"%s\". */", absName);
            h.add("public final class %s extends RuntimeCifEvent<State> {", className);

            // Add body.
            CodeBox c = file.body;

            // Add constructor.
            c.add("public %s() {", className);
            c.indent();
            c.add("super(\"%s\", %d, RuntimeEventKind.%s, %s);", absName, runtimeEventIdx, kind.name(), ctrlTxt);
            c.dedent();
            c.add("}");

            // Add 'fillData' method.
            c.add();
            c.add("@Override");
            c.add("public boolean fillData(State state) {");
            c.indent();

            if (isComm) {
                c.add("// Send.");
                c.add("SPEC.sendData.get(%d).clear();", runtimeEventIdx);
                genFillCallsSend(event, c, ctxt);
                c.add("if (SPEC.sendData.get(%d).isEmpty()) return false;", runtimeEventIdx);
                c.add();

                c.add("// Receive.");
                c.add("SPEC.recvData.get(%d).clear();", runtimeEventIdx);
                genFillCallsRecv(event, c, ctxt);
                c.add("if (SPEC.recvData.get(%d).isEmpty()) return false;", runtimeEventIdx);
                c.add();
            }

            if (isTau) {
                c.add("SPEC.tauData.clear();");
                genFillCallsTau(c, ctxt);
                c.add("return !SPEC.tauData.isEmpty();");
            } else {
                c.add("// Sync.");
                c.add("boolean proceed;");
                genFillCallsSync(event, c, ctxt);
                c.add();
                c.add("// All done, possible so far.");
                if (!isComm && ctxt.getSyncAuts(event).isEmpty()) {
                    c.add("return false; // No aut has event in alphabet.");
                } else {
                    c.add("return true;");
                }
            }

            c.dedent();
            c.add("}");

            // Warn about channels that are used, but have no senders or no
            // receivers.
            if (isComm) {
                // Get automata.
                List<Automaton> sendAuts = ctxt.getSendAuts(event);
                List<Automaton> recvAuts = ctxt.getRecvAuts(event);
                List<Automaton> syncAuts = ctxt.getSyncAuts(event);

                // Check if event is used.
                int cnt = 0;
                cnt += sendAuts.size();
                cnt += recvAuts.size();
                cnt += syncAuts.size();

                // Warn if applicable.
                if (cnt > 0 && sendAuts.isEmpty()) {
                    warn("No senders found for channel \"%s\".", absName);
                }
                if (cnt > 0 && recvAuts.isEmpty()) {
                    warn("No receivers found for channel \"%s\".", absName);
                }
            }

            // Add 'allowedByInvs' method.
            c.add();
            c.add("@Override");
            c.add("public boolean allowedByInvs(State state) {");
            c.indent();

            List<ExprCodeGeneratorResult> exprResults = list();
            exprResults.addAll(genAllowedByInvsComp(spec, event, c, ctxt));
            for (Automaton aut: ctxt.getAutomata()) {
                exprResults.addAll(genAllowedByInvsAutLocs(aut, event, c, ctxt));
            }
            c.add("// Event allowed by invariants.");
            c.add("return true;");

            c.dedent();
            c.add("}");

            // Add potential extra expression evaluation methods.
            for (ExprCodeGeneratorResult exprResult: exprResults) {
                exprResult.addExtraMethods(c);
            }
        }
    }

    /**
     * Generates 'fillTauData' calls for the 'tau' event, for all automata.
     *
     * @param c The code box in which to generate the code.
     * @param ctxt The compiler context to use.
     */
    private static void genFillCallsTau(CodeBox c, CifCompilerContext ctxt) {
        // We could generate calls only for the automata that actually can do
        // 'tau' events, but that means we have to walk over all edges, which
        // is expensive. So, we generate calls for all automata, regardless of
        // whether they can do a 'tau'.
        List<Automaton> auts = ctxt.getAutomata();
        for (Automaton aut: auts) {
            c.add("%s.fillTauData(state);", ctxt.getAutClassName(aut));
        }
    }

    /**
     * Generates 'fillSendData_*' calls for the given event, for all automata that have the event in their send
     * alphabet.
     *
     * @param event The event. Must be a channel.
     * @param c The code box in which to generate the code.
     * @param ctxt The compiler context to use.
     */
    private static void genFillCallsSend(Event event, CodeBox c, CifCompilerContext ctxt) {
        List<Automaton> auts = ctxt.getSendAuts(event);
        for (Automaton aut: auts) {
            c.add("%s.fillSendData_%d(state);", ctxt.getAutClassName(aut), ctxt.getRuntimeEventIdx(event));
        }
    }

    /**
     * Generates 'fillRecvData_*' calls for the given event, for all automata that have the event in their receive
     * alphabet.
     *
     * @param event The event. Must be a channel.
     * @param c The code box in which to generate the code.
     * @param ctxt The compiler context to use.
     */
    private static void genFillCallsRecv(Event event, CodeBox c, CifCompilerContext ctxt) {
        List<Automaton> auts = ctxt.getRecvAuts(event);
        for (Automaton aut: auts) {
            c.add("%s.fillRecvData_%d(state);", ctxt.getAutClassName(aut), ctxt.getRuntimeEventIdx(event));
        }
    }

    /**
     * Generates 'fillSyncData_*' calls for the given event, for all automata that have the event in their alphabet.
     *
     * @param event The event. Must not be the 'tau' event.
     * @param c The code box in which to generate the code.
     * @param ctxt The compiler context to use.
     */
    private static void genFillCallsSync(Event event, CodeBox c, CifCompilerContext ctxt) {
        List<Automaton> auts = ctxt.getSyncAuts(event);
        for (Automaton aut: auts) {
            c.add();
            c.add("// Check automaton \"%s\".", getAbsName(aut));
            c.add("proceed = %s.fillSyncData_%d(state);", ctxt.getAutClassName(aut), ctxt.getRuntimeEventIdx(event));
            c.add("if (!proceed) return false;");
        }
    }

    /**
     * Generates state/event exclusion invariant code for the invariants of the component (recursively). This does not
     * include the invariants of the locations.
     *
     * @param comp The component.
     * @param event The event.
     * @param c The code box to which to add the code.
     * @param ctxt The compiler context to use.
     * @return The {@code ExprCodeGeneratorResult}s for the generated Java code.
     */
    private static List<ExprCodeGeneratorResult> genAllowedByInvsComp(ComplexComponent comp, Event event, CodeBox c,
            CifCompilerContext ctxt)
    {
        // Generate for component.
        List<Invariant> stateEvtExclInvs = list();
        for (Invariant inv: comp.getInvariants()) {
            if (inv.getInvKind() == InvKind.STATE) {
                continue;
            }
            Event invEvent = ((EventExpression)inv.getEvent()).getEvent();
            if (invEvent != event) {
                continue;
            }
            stateEvtExclInvs.add(inv);
        }

        String absName = getAbsName(comp);
        if (!stateEvtExclInvs.isEmpty()) {
            c.add("// Invariants for \"%s\".", absName);
        }

        String compTxt = CifTextUtils.getComponentText2(comp);

        List<ExprCodeGeneratorResult> exprResults = list();
        for (Invariant inv: stateEvtExclInvs) {
            Expression pred = inv.getPredicate();

            // Start of 'try'.
            c.add("try {");
            c.indent();

            // Actual invariant predicate evaluation.
            ExprCodeGeneratorResult predResult = gencodeExpr(pred, ctxt, "state");
            exprResults.add(predResult);
            InvKind invKind = inv.getInvKind();
            switch (invKind) {
                case EVENT_DISABLES:
                    c.add("if (%s) return false;", predResult);
                    break;

                case EVENT_NEEDS:
                    c.add("if (!(%s)) return false;", predResult);
                    break;

                default:
                    String msg = "Unknown state/evt excl inv: " + invKind;
                    throw new RuntimeException(msg);
            }

            // End of 'try'.
            String invTxt = invToStr(inv, false);
            c.dedent();
            c.add("} catch (CifSimulatorException e) {");
            c.indent();
            c.add("throw new CifSimulatorException(\"Evaluation of invariant \\\"%s\\\" of %s failed.\", e, state);",
                    escapeJava(truncate(invTxt, 1000)), escapeJava(compTxt));
            c.dedent();
            c.add("}");
            c.add();

            // Warn about requirement invariants.
            if (inv.getSupKind() == SupKind.REQUIREMENT) {
                warn("Invariant \"%s\" of %s is a requirement, but will be simulated as a plant.", invTxt, compTxt);
            }
        }

        // Generate recursively.
        if (comp instanceof Group) {
            for (Component child: ((Group)comp).getComponents()) {
                exprResults.addAll(genAllowedByInvsComp((ComplexComponent)child, event, c, ctxt));
            }
        }

        return exprResults;
    }

    /**
     * Generates state/event exclusion invariant code for the invariants of the locations of the given automaton.
     *
     * @param aut The automaton.
     * @param event The event.
     * @param c The code box to which to add the code.
     * @param ctxt The compiler context to use.
     * @return The {@code ExprCodeGeneratorResult}s for the generated Java code.
     */
    private static List<ExprCodeGeneratorResult> genAllowedByInvsAutLocs(Automaton aut, Event event, CodeBox c,
            CifCompilerContext ctxt)
    {
        c.add("// Invariants for current location.");
        c.add("switch (state.%s.%s) {", ctxt.getAutSubStateFieldName(aut), ctxt.getLocationPointerFieldName(aut));
        c.indent();
        List<Location> locs = aut.getLocations();
        List<ExprCodeGeneratorResult> exprResults = list();
        for (int locIdx = 0; locIdx < locs.size(); locIdx++) {
            // Get location.
            Location loc = locs.get(locIdx);

            // If no invariants, then no 'case'.
            List<Invariant> locEvtExclInvs = list();
            for (Invariant inv: loc.getInvariants()) {
                if (inv.getInvKind() == InvKind.STATE) {
                    continue;
                }
                Event invEvent = ((EventExpression)inv.getEvent()).getEvent();
                if (invEvent != event) {
                    continue;
                }
                locEvtExclInvs.add(inv);
            }
            if (locEvtExclInvs.isEmpty()) {
                continue;
            }

            // Add 'case'.
            c.add("case %s:", ctxt.getLocationValueText(loc, locIdx));
            c.indent();

            String locTxt = CifTextUtils.getLocationText2(loc);

            for (Invariant inv: locEvtExclInvs) {
                Expression pred = inv.getPredicate();

                // Start of 'try'.
                c.add("try {");
                c.indent();

                // Actual invariant predicate evaluation.
                ExprCodeGeneratorResult predResult = gencodeExpr(pred, ctxt, "state");
                exprResults.add(predResult);
                InvKind invKind = inv.getInvKind();
                switch (invKind) {
                    case EVENT_DISABLES:
                        c.add("if (%s) return false;", predResult);
                        break;

                    case EVENT_NEEDS:
                        c.add("if (!(%s)) return false;", predResult);
                        break;

                    default:
                        String msg = "Unknown state/evt excl inv: " + invKind;
                        throw new RuntimeException(msg);
                }

                // End of 'try'.
                String invTxt = invToStr(inv, false);
                c.dedent();
                c.add("} catch (CifSimulatorException e) {");
                c.indent();
                c.add("throw new CifSimulatorException(\"Evaluation of invariant \\\"%s\\\" of %s failed.\", e, "
                        + "state);", escapeJava(truncate(invTxt, 1000)), escapeJava(locTxt));
                c.dedent();
                c.add("}");

                // Warn about requirement invariants.
                if (inv.getSupKind() == SupKind.REQUIREMENT) {
                    warn("Invariant \"%s\" of %s is a requirement, but will be simulated as a plant.", invTxt, locTxt);
                }
            }
            c.add("break;");
            c.dedent();
        }
        c.dedent();
        c.add("}");
        c.add();

        return exprResults;
    }

    /** Information about an environment event. */
    abstract static class EnvironmentEventInfo {
        /**
         * The 0-based index of the environment event. Is {@code -1} until it is set by
         * {@link CifCompilerContext#setEnvironmentEvents}.
         */
        int environmentEventIdx = -1;

        /** The name of the environment event. */
        final String eventName;

        /**
         * Constructor for the {@link EnvironmentEventInfo} class.
         *
         * @param eventName The name of the environment event.
         */
        EnvironmentEventInfo(String eventName) {
            this.eventName = eventName;
        }
    }

    /** Information about an SVG input component environment event. */
    static class SvgInputEnvironmentEventInfo extends EnvironmentEventInfo {
        /** The 0-based index of the SVG file. */
        final int svgFileIdx;

        /** The relative path to the SVG file. */
        final String svgFileRelPath;

        /** The SVG input mapping. */
        final SvgIn svgIn;

        /**
         * Constructor for the {@link SvgInputEnvironmentEventInfo} class.
         *
         * @param eventName The name of the environment event.
         * @param svgFileIdx The 0-based index of the SVG file.
         * @param svgFileRelPath The relative path to the SVG file.
         * @param svgIn The SVG input mapping.
         */
        SvgInputEnvironmentEventInfo(String eventName, int svgFileIdx, String svgFileRelPath, SvgIn svgIn) {
            super(eventName);
            this.svgFileIdx = svgFileIdx;
            this.svgFileRelPath = svgFileRelPath;
            this.svgIn = svgIn;
        }
    }
}
