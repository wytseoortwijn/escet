//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.simulator.runtime.model;

/**
 * Runtime {@link RuntimeEventKind#ENVIRONMENT environment} event.
 *
 * @param <S> The type of state objects to use.
 */
public abstract class RuntimeEnvironmentEvent<S extends RuntimeState> extends RuntimeEvent<S> {
    /**
     * Constructor for the {@link RuntimeEnvironmentEvent} class.
     *
     * @param name The name of the environment event. To avoid confusion it must not overlap with the syntax for
     *     possibly-escaped absolute CIF names.
     * @param idx The unique 0-based index of the event.
     */
    public RuntimeEnvironmentEvent(String name, int idx) {
        super(name, idx, RuntimeEventKind.ENVIRONMENT, null);
    }
}
