//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.simulator.options;

import org.eclipse.escet.common.app.framework.options.BooleanOption;
import org.eclipse.escet.common.java.exceptions.InvalidOptionException;

/** External user-defined functions asynchronous execution option. */
public class ExtFuncAsyncOption extends BooleanOption {
    /** Message to indicate the option is unsupported. */
    private static final String UNSUPPORTED_MESSAGE = "This option is no longer supported. "
            + "It will be removed in a future version of the tool. "
            + "External user-defined functions are now always executed synchronously.";

    /** Constructor for the {@link ExtFuncAsyncOption} class. */
    public ExtFuncAsyncOption() {
        super(
                // name
                "External functions asynchronous execution",

                // description
                "Whether to execute external user-defined functions asynchronously. " + UNSUPPORTED_MESSAGE,

                // cmdShort
                null,

                // cmdLong
                "extfunc-async",

                // cmdValue
                "BOOL",

                // defaultValue
                false,

                // showInDialog
                false,

                // optDialogDescr
                null,

                // optDialogCheckboxText
                null);
    }

    @Override
    public Boolean parseValue(String optName, String value) {
        throw new InvalidOptionException(
                "The external functions asynchronous execution option is used. " + UNSUPPORTED_MESSAGE);
    }
}
