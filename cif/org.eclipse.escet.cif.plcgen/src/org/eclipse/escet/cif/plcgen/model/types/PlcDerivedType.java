//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.plcgen.model.types;

/** PLC derived type (reference to a user-defined data type or POU). */
public abstract class PlcDerivedType extends PlcType {
    /** The name of the derived type. */
    protected final String typeName;

    /**
     * Constructor for the {@link PlcDerivedType} class.
     *
     * @param typeName The name of the derived type.
     */
    public PlcDerivedType(String typeName) {
        this.typeName = typeName;
    }

    /**
     * Get the name of the type.
     *
     * @return The name of the type.
     */
    public String getName() {
        return typeName;
    }
}
