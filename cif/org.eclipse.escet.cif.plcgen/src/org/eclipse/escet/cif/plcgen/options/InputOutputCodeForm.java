//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.plcgen.options;

/** Available forms of input and output code. */
public enum InputOutputCodeForm {
    /** Both code reading input and code writing output are in the main program. */
    INPUT_MAIN_AND_OUTPUT_MAIN(true, true),

    /**
     * Code reading input is in the main program, while code writing output has its own POU. The POU that writes the
     * output is called from the main program.
     */
    INPUT_MAIN_AND_OUTPUT_FUNC(true, false),

    /**
     * Code reading input has its own POU, while code writing output is in the main program. The POU that reads the
     * input is called from the main program.
     */
    INPUT_FUNC_AND_OUTPUT_MAIN(false, true),

    /**
     * Both code reading input and code writing output each have their own POU. Both POUs are called from the main
     * program.
     */
    INPUT_FUNC_AND_OUTPUT_FUNC(false, false);

    /**
     * Whether the code to perform input should be in the main program.
     *
     * <p>
     * Value {@code true} means that the code to read input is generated in the main program. Value {@code false} means
     * that a separate POU is generated to read input, and the POU is called from the main program.
     * </p>
     */
    public final boolean inputCodeInMain;

    /**
     * Whether the code to perform output should be in the main program.
     *
     * <p>
     * Value {@code true} means that the code to write output is generated in the main program. Value {@code false}
     * means that a separate POU is generated to write output, and the POU is called from the main program.
     * </p>
     */
    public final boolean outputCodeInMain;

    /**
     * Constructor of the {@link InputOutputCodeForm} enumeration.
     *
     * @param inputCodeInMain Whether the code to perform input should be in the main program.
     * @param outputCodeInMain Whether the code to perform output should be in the main program.
     */
    private InputOutputCodeForm(boolean inputCodeInMain, boolean outputCodeInMain) {
        this.inputCodeInMain = inputCodeInMain;
        this.outputCodeInMain = outputCodeInMain;
    }
}
