//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.plcgen.targets;

import static org.eclipse.escet.cif.plcgen.model.functions.PlcFuncOperation.COMPLEMENT_OP;
import static org.eclipse.escet.cif.plcgen.model.functions.PlcFuncOperation.SEL_OP;
import static org.eclipse.escet.cif.plcgen.model.functions.PlcFuncOperation.STDLIB_ABS;
import static org.eclipse.escet.cif.plcgen.model.functions.PlcFuncOperation.STDLIB_ACOS;
import static org.eclipse.escet.cif.plcgen.model.functions.PlcFuncOperation.STDLIB_ASIN;
import static org.eclipse.escet.cif.plcgen.model.functions.PlcFuncOperation.STDLIB_ATAN;
import static org.eclipse.escet.cif.plcgen.model.functions.PlcFuncOperation.STDLIB_COS;
import static org.eclipse.escet.cif.plcgen.model.functions.PlcFuncOperation.STDLIB_EXP;
import static org.eclipse.escet.cif.plcgen.model.functions.PlcFuncOperation.STDLIB_LN;
import static org.eclipse.escet.cif.plcgen.model.functions.PlcFuncOperation.STDLIB_LOG;
import static org.eclipse.escet.cif.plcgen.model.functions.PlcFuncOperation.STDLIB_MAX;
import static org.eclipse.escet.cif.plcgen.model.functions.PlcFuncOperation.STDLIB_MIN;
import static org.eclipse.escet.cif.plcgen.model.functions.PlcFuncOperation.STDLIB_SIN;
import static org.eclipse.escet.cif.plcgen.model.functions.PlcFuncOperation.STDLIB_SQRT;
import static org.eclipse.escet.cif.plcgen.model.functions.PlcFuncOperation.STDLIB_TAN;
import static org.eclipse.escet.cif.plcgen.model.types.PlcElementaryType.BIT_STRING_TYPES_32;
import static org.eclipse.escet.cif.plcgen.model.types.PlcElementaryType.BIT_STRING_TYPES_64;
import static org.eclipse.escet.cif.plcgen.model.types.PlcElementaryType.INTEGER_TYPES_32;
import static org.eclipse.escet.cif.plcgen.model.types.PlcElementaryType.INTEGER_TYPES_64;
import static org.eclipse.escet.cif.plcgen.model.types.PlcElementaryType.REAL_TYPES_32;
import static org.eclipse.escet.cif.plcgen.model.types.PlcElementaryType.REAL_TYPES_64;

import java.util.BitSet;
import java.util.EnumSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.escet.cif.metamodel.cif.declarations.Constant;
import org.eclipse.escet.cif.plcgen.generators.PlcVariablePurpose;
import org.eclipse.escet.cif.plcgen.model.functions.PlcBasicFuncDescription.PlcFuncNotation;
import org.eclipse.escet.cif.plcgen.model.functions.PlcBasicFuncDescription.PlcFuncTypeExtension;
import org.eclipse.escet.cif.plcgen.model.functions.PlcFuncOperation;
import org.eclipse.escet.cif.plcgen.model.types.PlcElementaryType;
import org.eclipse.escet.cif.plcgen.options.ConvertEnums;
import org.eclipse.escet.cif.plcgen.writers.S7Writer;
import org.eclipse.escet.cif.plcgen.writers.Writer;
import org.eclipse.escet.common.java.Assert;

/** Code generator for Siemens S7-300, S7-400, S7-1200, and S7-1500 PLC types. */
public class SiemensS7Target extends PlcBaseTarget {
    /** S7-300/400 block keywords regular expression pattern. */
    private static final Pattern BLOCK_KEYWORDS_PATTERN_300_400 = Pattern
            .compile("(c|db|fb|fc|ob|sdb|sfc|sfb|t|udt|z)(?<nr>[0-9]+)");

    /** S7-300/400 address identifiers regular expression pattern. */
    private static final Pattern ADDRESS_IDENTIFIERS_PATTERN_300_400 = Pattern
            .compile("(((a|d|e|i|m|q)(b|d|w|x)?)|((pa|pe|pi|pq)(b|d|w)))(?<nr>[0-9]+)");

    /** Replacement strings for the extension in the CIF input file name to construct an output path for each target. */
    private static final Map<PlcTargetType, String> OUT_SUFFIX_REPLACEMENTS;

    /** Supported integer types for each target, ordered in increasing size. */
    private static final Map<PlcTargetType, List<PlcElementaryType>> INTEGER_TYPES;

    /** Supported real types for each target, ordered in increasing size. */
    private static final Map<PlcTargetType, List<PlcElementaryType>> REAL_TYPES;

    /** Supported bit string types for each target, ordered in increasing size. */
    private static final Map<PlcTargetType, List<PlcElementaryType>> BIT_STRING_TYPES;

    /** Special characters in an identifier. */
    private static final BitSet SPECIAL_CHARS;

    /** Normal characters in an identifier. */
    private static final BitSet NORMAL_CHARS;

    static {
        OUT_SUFFIX_REPLACEMENTS = Map.of(
                PlcTargetType.S7_300, "_s7_300", PlcTargetType.S7_400, "_s7_400",
                PlcTargetType.S7_1200, "_s7_1200", PlcTargetType.S7_1500, "_s7_1500");

        INTEGER_TYPES = Map.of(
                PlcTargetType.S7_300, INTEGER_TYPES_32, PlcTargetType.S7_400, INTEGER_TYPES_32,
                PlcTargetType.S7_1200, INTEGER_TYPES_32, PlcTargetType.S7_1500, INTEGER_TYPES_64);

        REAL_TYPES = Map.of(
                PlcTargetType.S7_300, REAL_TYPES_32, PlcTargetType.S7_400, REAL_TYPES_32,
                PlcTargetType.S7_1200, REAL_TYPES_64, PlcTargetType.S7_1500, REAL_TYPES_64);

        BIT_STRING_TYPES = Map.of(
                PlcTargetType.S7_300, BIT_STRING_TYPES_32, PlcTargetType.S7_400, BIT_STRING_TYPES_32,
                PlcTargetType.S7_1200, BIT_STRING_TYPES_32, PlcTargetType.S7_1500, BIT_STRING_TYPES_64);

        // Source: https://cache.industry.siemens.com/dl/files/857/109477857/att_865202/v1/109477857_Bezeichner_Anfuehrungszeichen_en.pdf
        // Consulted: Jul 10, 2024. Valid for TIA portal 10 and higher.
        //
        // ASCII characters neither in NORMAL_CHARS nor in SPECIAL_CHARS are 0..31, 127, '"', '+' and '~'.
        SPECIAL_CHARS = new BitSet(128);
        char[] specials = new char[] {' ', '!', '#', '$', '%', '&', '\'', '(', ')', '*', ',', '-', '.', '/', ':', ';',
                '<', '=', '>', '?', '@', '[', '\\', ']', '^', '`', '{', '|', '}'};
        for (char c: specials) {
            SPECIAL_CHARS.set(c);
        }

        // Source: https://cache.industry.siemens.com/dl/files/857/109477857/att_865202/v1/109477857_Bezeichner_Anfuehrungszeichen_en.pdf
        // Consulted: Jul 10, 2024. Valid for TIA portal 10 and higher.
        //
        // ASCII characters in regular identifiers (letters, digits, and '_').
        NORMAL_CHARS = new BitSet(128);
        NORMAL_CHARS.set('_');
        for (int i = 0; i < 26; i++) {
            NORMAL_CHARS.set('A' + i);
            NORMAL_CHARS.set('a' + i);
        }
        for (int i = 0; i < 10; i++) {
            NORMAL_CHARS.set('0' + i);
        }
    }

    /**
     * Constructor of the {@link SiemensS7Target} class.
     *
     * @param targetType A Siemens S7 target type.
     */
    public SiemensS7Target(PlcTargetType targetType) {
        super(targetType, ConvertEnums.CONSTS, "TON");
        // TODO Verify settings of the Siemens target.

        Assert.check(OUT_SUFFIX_REPLACEMENTS.containsKey(targetType)); // Java can't check existence before super().
    }

    @Override
    protected Set<String> getDisallowedNames() {
        Set<String> names = super.getDisallowedNames();

        // From "Structured Control Language (SCL) for S7-300/S7-400 Programming Manual", order number
        // 6ES7811-1CA02-8BA0, at https://cache.industry.siemens.com/dl/files/188/1137188/att_27471/v1/SCLV4_e.pdf.
        // Accessed on 2024-09-10.
        //
        // Documentation on disallowed names for S7-1200 and S7-1500 seems not to exist. Therefore, we apply the same
        // rules to all S7 targets.

        names.addAll(List.of(
                // 'Keywords' and 'Other reserved words' (from Section 7.3).
                // 'Block Keywords' (from Section 7.5), the ones without a numeric postfix.
                // 'Keywords and Predefined Identifiers' (from Section A.5).
                // These lists are combined here, as they have significant overlap.
                "and", "any", "array",
                "begin", "block_db", "block_fb", "block_fc", "block_sdb", "block_sfb", "block_sfc", "bool", "by",
                "byte",
                "case", "char", "const", "continue", "counter",
                "data_block", "date", "date_and_time", "dbo", "dint", "div", "do", "dt", "dword",
                "else", "elsif", "en", "end_case", "end_const", "end_data_block", "end_for", "end_function",
                "end_function_block", "end_if", "end_label", "end_organization_block", "end_repeat", "end_struct",
                "end_type", "end_var", "end_while", "eno", "exit",
                "false", "for", "function", "function_block",
                "goto",
                "if", "int",
                "label",
                "mod",
                "nil", "not",
                "of", "ok", "or", "organization_block",
                "pointer",
                "real", "repeat", "return",
                "s5time", "string", "struct",
                "then", "time", "time_of_day", "timer", "to", "tod", "true", "type",
                "until",
                "var", "var_in_out", "var_input", "var_output", "var_temp", "void",
                "while", "word",
                "xor",

                // 'List of Conversion Functions (Class A)' (from Section 18.2).
                "bool_to_byte", "bool_to_dword", "bool_to_word", "byte_to_dword", "byte_to_word",
                "char_to_string",
                "dint_to_real",
                "int_to_dint", "int_to_real",
                "word_to_dword",

                // 'List of Conversion Functions (Class B)' (from Section 18.2).
                "block_db_to_word", "byte_to_bool", "byte_to_char",
                "char_to_byte", "char_to_int",
                "date_to_dint", "dint_to_date", "dint_to_dword", "dint_to_int", "dint_to_time", "dint_to_tod",
                "dword_to_bool", "dword_to_byte", "dword_to_dint", "dword_to_real", "dword_to_word", "int_to_char",
                "int_to_word",
                "real_to_dint", "real_to_dword", "real_to_int",
                "string_to_char",
                "time_to_dint", "tod_to_dint",
                "word_to_bool", "word_to_byte", "word_to_int", "word_to_block_db",

                // 'Functions for Rounding and Truncating' (from Section 18.2).
                "round", "trunc",

                // 'List of General Functions' (from Section 18.3).
                "abs", "sqr", "sqrt",

                // 'List of Logarithmic Functions' (from Section 18.3).
                "exp", "expd", "ln", "log",

                // 'List of Trigonometrical Functions' (from Section 18.3).
                "acos", "asin", "atan", "cos", "sin", "tan",

                // 'List of Functions' (from Section 18.4).
                "rol", "ror", "shl", "shr"));

        // Other.
        names.addAll(List.of(
                "iec_timer", "timer", // Timers S7.
                "dummyVar1", "dummyVar2", "dummyVar3", "dummyVar4", "dummyVar5" // Dummy variables S7.
        ));

        // Return all disallowed names.
        return names;
    }

    @Override
    public Writer getPlcCodeWriter() {
        return new S7Writer(this);
    }

    @Override
    public StateVariableStorage getStateVariableStorage() {
        return StateVariableStorage.STATE_VARS_IN_MAIN;
    }

    @Override
    public boolean supportsArrays() {
        // S7 transformation doesn't support list types. That is because S7 doesn't support functions
        // returning arrays and doesn't support arrays of arrays.
        return false;
    }

    @Override
    public boolean supportsConstant(Constant constant) {
        return commonSupportedConstants(constant);
    }

    @Override
    public boolean isAllowedName(String name) {
        // Disallow language names.
        if (!super.isAllowedName(name)) {
            return false;
        }

        // From "Structured Control Language (SCL) for S7-300/S7-400 Programming Manual", order number
        // 6ES7811-1CA02-8BA0, at https://cache.industry.siemens.com/dl/files/188/1137188/att_27471/v1/SCLV4_e.pdf.
        // Accessed on 2024-09-10.
        //
        // Documentation on disallowed names for S7-1200 and S7-1500 seems not to exist. Therefore, we apply the same
        // rules to all S7 targets.

        // Normalize the case of the word.
        name = name.toLowerCase(Locale.US);

        // 'Block Keywords' (from Section 7.5).
        Matcher matcher = BLOCK_KEYWORDS_PATTERN_300_400.matcher(name);
        if (matcher.matches()) {
            try {
                int nr = Integer.valueOf(matcher.group("nr"));
                if (nr >= 0 && nr <= 65535) { // Use more safe/logical limit than 65533 from the documentation.
                    return false;
                }
            } catch (NumberFormatException e) {
                // Not a number that can be parsed to an integer. This rule allows such numbers.
            }
        }

        // 'Address Identifiers' (from Section 7.5).
        matcher = ADDRESS_IDENTIFIERS_PATTERN_300_400.matcher(name);
        if (matcher.matches()) {
            try {
                int nr = Integer.valueOf(matcher.group("nr"));
                if (nr >= 0 && nr <= 65535) { // Use more safe/logical limit than 64535 from the documentation.
                    return false;
                }
            } catch (NumberFormatException e) {
                // Not a number that can be parsed to an integer. This rule allows such numbers.
            }
        }

        // Allowed name.
        return true;
    }

    @Override
    public EnumSet<PlcFuncNotation> getSupportedFuncNotations(PlcFuncOperation funcOper, int numArgs) {
        // S7 doesn't have a function for log10.
        if (funcOper == STDLIB_LOG) {
            return PlcFuncNotation.UNSUPPORTED;
        }

        EnumSet<PlcFuncNotation> funcSupport = super.getSupportedFuncNotations(funcOper, numArgs);

        // Functions that should always be formal.
        EnumSet<PlcFuncOperation> formalFuncs = EnumSet.of(SEL_OP, STDLIB_MAX, STDLIB_MIN);
        if (formalFuncs.contains(funcOper)) {
            funcSupport = EnumSet.copyOf(funcSupport);
            funcSupport.retainAll(PlcFuncNotation.FORMAL_ONLY);
            return funcSupport;
        }

        // Support short circuit AND / OR.
        if (funcOper == PlcFuncOperation.AND_SHORT_CIRCUIT_OP || funcOper == PlcFuncOperation.OR_SHORT_CIRCUIT_OP) {
            // S7 does not allow formal notation for functions with >= 2 arguments. See also below.
            return PlcFuncNotation.NOT_FORMAL;
        }

        // S7 does not allow formal function call syntax for the following functions and not for functions with two or
        // more parameters (except for MIN / MAX above).
        EnumSet<PlcFuncOperation> notFormalFuncs = EnumSet.of(COMPLEMENT_OP, STDLIB_ABS, STDLIB_ACOS, STDLIB_ASIN,
                STDLIB_ATAN, STDLIB_COS, STDLIB_EXP, STDLIB_LN, STDLIB_LOG, STDLIB_SIN, STDLIB_SQRT, STDLIB_TAN);
        if (notFormalFuncs.contains(funcOper) || numArgs >= 2) {
            funcSupport = EnumSet.copyOf(funcSupport);
            funcSupport.remove(PlcFuncNotation.FORMAL);
            return funcSupport;
        }

        return funcSupport;
    }

    @Override
    public PlcFuncTypeExtension getTypeExtension(PlcFuncOperation funcOper) {
        return switch (funcOper) {
            case SEL_OP -> PlcFuncTypeExtension.ELEMENTARY_NOT_BOOL;
            default -> PlcFuncTypeExtension.NEVER;
        };
    }

    @Override
    public List<PlcElementaryType> getSupportedIntegerTypes() {
        return INTEGER_TYPES.get(targetType);
    }

    @Override
    public List<PlcElementaryType> getSupportedRealTypes() {
        return REAL_TYPES.get(targetType);
    }

    @Override
    public List<PlcElementaryType> getSupportedBitStringTypes() {
        return BIT_STRING_TYPES.get(targetType);
    }

    @Override
    public String getUsageVariableText(PlcVariablePurpose purpose, String varName) {
        if (purpose == PlcVariablePurpose.STATE_VAR) {
            return "\"DB\"." + varName;
        } else if (purpose == PlcVariablePurpose.INPUT_VAR || purpose == PlcVariablePurpose.OUTPUT_VAR) {
            String encodedName = encodeTagName(varName, false);
            Assert.notNull(encodedName);
            return encodedName;
        }
        return super.getUsageVariableText(purpose, varName);
    }

    @Override
    public String getPathSuffixReplacement() {
        return OUT_SUFFIX_REPLACEMENTS.get(targetType);
    }

    @Override
    public boolean checkIoVariableName(String name) {
        return encodeTagName(name, false) != null;
    }

    /**
     * Test whether a tag name is acceptable to an S7 system, and if so, how to write it.
     *
     * @param name The name to test.
     * @param isLocal Whether the name is defined locally (as in, not in a global table).
     * @return If {@code null}, the name is not acceptable. Otherwise, the name is converted and returned in the form
     *     needed for using it.
     */
    public static String encodeTagName(String name, boolean isLocal) {
        // Source: https://cache.industry.siemens.com/dl/files/857/109477857/att_865202/v1/109477857_Bezeichner_Anfuehrungszeichen_en.pdf
        // Consulted: Jul 10, 2024. Valid for TIA portal 10 and higher.

        // Decide whether to add double quotes around the name or to reject it.

        // Name cannot be empty.
        if (name.isEmpty()) {
            return null;
        }

        boolean mustBeQuoted = false;
        for (int i = 0; i < name.length(); i++) {
            // S7 allows more than just ASCII, but that gives problems in CIF and CSV files.
            char c = name.charAt(i);
            if (c < 32 || c >= 127) {
                return null; // Control character or not ASCII.
            }
            if (NORMAL_CHARS.get(c)) {
                continue;
            }
            if (SPECIAL_CHARS.get(c)) {
                mustBeQuoted = true;
                continue;
            }
            return null; // Neither a control character, nor a normal or special character.
        }

        // Names starting with a decimal digit must be double-quoted. Obviously this includes names consisting of only
        // digits.
        char first = name.charAt(0);
        mustBeQuoted |= (first >= '0' && first <= '9'); // This leaves [A-Za-z_] as normal first characters.

        // Names with "__" or ending with an underscore must be quoted.
        mustBeQuoted |= (name.endsWith("_") || name.contains("__"));

        if (!mustBeQuoted) {
            return name;
        } else if (isLocal) {
            return "#\"" + name + "\"";
        } else {
            return "\"" + name + "\"";
        }
    }
}
