//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.plcgen.conversion;

import static org.eclipse.escet.common.java.Lists.list;

import java.util.List;

import org.eclipse.escet.cif.plcgen.conversion.expressions.CifDataProvider;
import org.eclipse.escet.cif.plcgen.conversion.expressions.ExprGenerator;
import org.eclipse.escet.cif.plcgen.generators.NameGenerator;
import org.eclipse.escet.cif.plcgen.generators.TypeGenerator;
import org.eclipse.escet.cif.plcgen.model.declarations.PlcBasicVariable;
import org.eclipse.escet.cif.plcgen.model.declarations.PlcDataVariable;
import org.eclipse.escet.cif.plcgen.model.declarations.PlcPou;
import org.eclipse.escet.cif.plcgen.model.declarations.PlcPouType;
import org.eclipse.escet.cif.plcgen.model.expressions.PlcExpression;
import org.eclipse.escet.cif.plcgen.model.statements.PlcCommentLine;
import org.eclipse.escet.cif.plcgen.model.statements.PlcReturnStatement;
import org.eclipse.escet.cif.plcgen.model.statements.PlcStatement;
import org.eclipse.escet.cif.plcgen.model.types.PlcType;
import org.eclipse.escet.cif.plcgen.targets.PlcTarget;
import org.eclipse.escet.common.java.Assert;

/** Helper class to construct a POU. */
public class PouBuilder {
    /** POU being constructed. */
    private PlcPou pou;

    /** The PLC target intended to accept and use the new POU. */
    private final PlcTarget target;

    /** Expression generator for the POU. */
    public final ExprGenerator exprGen;

    /** Storage of added PLC model statements. */
    public final List<PlcStatement> modelStatements = list();

    /**
     * Constructor of the {@link PouBuilder} class.
     *
     * @param target PLC target intended to accept and use the new POU.
     * @param nameGen Name generator.
     * @param typeGen Type generator.
     * @param cifDataProvider PLC access to CIF state variables.
     */
    public PouBuilder(PlcTarget target, NameGenerator nameGen, TypeGenerator typeGen, CifDataProvider cifDataProvider) {
        this.target = target;
        exprGen = new ExprGenerator(target, nameGen, typeGen, cifDataProvider);
    }

    /**
     * Construct a new POU in the builder, and prepare for adding code to the new POU.
     *
     * @param name Name of the new POU.
     * @param pouType Type of the new POU.
     * @param retType Return type of the new POU, must be {@code null} for programs.
     */
    public void createPou(String name, PlcPouType pouType, PlcType retType) {
        // Make a new POU.
        Assert.check(pou == null);
        pou = new PlcPou(name, pouType, retType);

        // Clean out any remaining statements from previous usage.
        modelStatements.clear();
    }

    /**
     * Add an input parameter to the POU. May only be used after creating a POU using {@link #createPou}.
     *
     * @param param Parameter to add.
     */
    public void addInputParameter(PlcDataVariable param) {
        pou.inputVars.add(param);
    }

    /**
     * Obtain a local scratch variable. Its name starts with the provided prefix, and it will have the provided type.
     *
     * <p>
     * In the current implementation, this method delegates variable creation and availability management to the
     * expression generator.
     * </p>
     * <p>
     * When obtaining the finished POU, variables obtained from this method are automatically included in the returned
     * POU.
     * </p>
     *
     * @param prefix Initial part of the name of the variable.
     * @param plcType Type of the returned variable.
     * @return The created variable.
     * @see #finishPou
     */
    public PlcBasicVariable getScratchVariable(String prefix, PlcType plcType) {
        return exprGen.getScratchVariable(prefix, plcType);
    }

    /**
     * Give a variable back to the POU builder for future re-use in the same POU. Returning a non-scratch variable is
     * allowed but it is ignored.
     *
     * <p>
     * In the current implementation, this method delegates variable creation and availability management to the
     * expression generator.
     * </p>
     *
     * @param variable Variable being returned.
     */
    public void releaseScratchVariable(PlcBasicVariable variable) {
        exprGen.releaseScratchVariable(variable);
    }

    /**
     * Construct a variable to use in the generated code.
     *
     * <p>
     * In the current implementation, this method delegates variable creation to the expression generator.
     * </p>
     *
     * @param prefix Initial part of the name of the variable.
     * @param plcType Type of the returned variable.
     * @return The created variable.
     */
    public PlcDataVariable makeVariable(String prefix, PlcType plcType) {
        return exprGen.makeLocalVariable(prefix, plcType);
    }

    /**
     * Add a temporary variable to the POU.
     *
     * <p>
     * This method is only useful for variables that are not created through {@link #getScratchVariable} or similar
     * methods in the expression generator or the POU builder.
     * </p>
     *
     * @param tempVar Temporary variable to add to the POU.
     * @see #addTempVariables
     */
    public void addTempVariable(PlcDataVariable tempVar) {
        pou.tempVars.add(tempVar);
    }

    /**
     * Add temporary variables to the POU.
     *
     * <p>
     * This method is only useful for variables that are not created through {@link #getScratchVariable} or similar
     * methods in the expression generator or the POU builder.
     * </p>
     *
     * @param tempVars Temporary variables to add to the POU.
     * @see #addTempVariable
     */
    public void addTempVariables(List<PlcDataVariable> tempVars) {
        pou.tempVars.addAll(tempVars);
    }

    /**
     * Add an empty line and a {@code RETURN} statement to the model statements.
     *
     * @param value Value to return to the caller of the POU.
     */
    public void addReturnStatement(PlcExpression value) {
        modelStatements.add(new PlcCommentLine(null));
        modelStatements.add(new PlcReturnStatement(value));
    }

    /**
     * Add an empty line, a comment line, and a {@code RETURN} statement to the model statements.
     *
     * @param commentText Text of the comment line. May not contain {@code (*} or {@code *)}.
     * @param value Value to return to the caller of the POU.
     */
    public void addReturnStatement(String commentText, PlcExpression value) {
        modelStatements.add(new PlcCommentLine(null));
        modelStatements.add(new PlcCommentLine(commentText));
        modelStatements.add(new PlcReturnStatement(value));
    }

    /**
     * Add a model statement to the POU.
     *
     * <p>
     * As a POU does not support model statements, the added statements are temporarily added to the builder.
     * </p>
     *
     * @param stat Model statement to add.
     */
    public void addStatement(PlcStatement stat) {
        modelStatements.add(stat);
    }

    /**
     * Add a model statements to the POU.
     *
     * <p>
     * As a POU does not support model statements, the added statements are temporarily added to the builder.
     * </p>
     *
     * @param stats Model statements to add.
     */
    public void addStatements(List<PlcStatement> stats) {
        modelStatements.addAll(stats);
    }

    /**
     * Retrieve the POU that is constructed. May only be used after creating a POU using {@link #createPou} and before
     * {@link #finishPou finishing a POU}.
     *
     * <p>
     * For obtaining the finished POU, use {@link #finishPou}.
     * </p>
     *
     * @return The created POU, except for the added model statements. Note that the returned POU remains connected to
     *     the builder, changes performed through the builder are visible in the returned POU except for changes in the
     *     model statements.
     * @see #finishPou
     */
    public PlcPou getPou() {
        Assert.notNull(pou);
        return pou;
    }

    /**
     * Retrieve the final POU. May only be used once after creating a POU using {@link #createPou}.
     *
     * <p>
     * After using this method, the POU is no longer attached to the builder. A new POU can be created after this call.
     * Only use this method after all code and data has been added.
     * </p>
     * <p>
     * For obtaining the POU under construction, use {@link #getPou}.
     * </p>
     *
     * @return The final POU, including the added model statements and the created temporary variables obtained through
     *     {@link #getScratchVariable} and similar methods of the {@link #exprGen expression generator} and the POU
     *     builder.
     * @see #getPou
     */
    public PlcPou finishPou() {
        Assert.notNull(pou);

        // Detach the POU from the builder.
        PlcPou finalPou = pou;
        pou = null;

        // Add the model statements.
        if (!modelStatements.isEmpty()) {
            target.getModelTextGenerator().toText(modelStatements, finalPou.body, finalPou.name, true);
        }

        // Add the created temporary variables.
        finalPou.tempVars.addAll(exprGen.getCreatedScratchVariables());

        // And return the result to the caller.
        return finalPou;
    }
}
