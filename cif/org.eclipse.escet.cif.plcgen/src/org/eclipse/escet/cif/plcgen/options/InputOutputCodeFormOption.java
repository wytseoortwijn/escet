//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.plcgen.options;

import static org.eclipse.escet.common.java.Strings.fmt;

import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.escet.common.app.framework.options.EnumOption;
import org.eclipse.escet.common.app.framework.options.Options;
import org.eclipse.escet.common.java.Strings;
import org.eclipse.escet.common.java.exceptions.InvalidOptionException;

/** Option class to define where the input and output code should be generated. */
public class InputOutputCodeFormOption extends EnumOption<InputOutputCodeForm> {
    /** The text of the long option. */
    private static final String LONG_OPTION_TEXT = "input-output-code-form";

    /** Phrase to explain the {@link InputOutputCodeForm#INPUT_MAIN_AND_OUTPUT_MAIN} value. */
    private static final String BOTH_MAIN_EXPLAIN = "both input and output code are in the main program";

    /** Phrase to explain the {@link InputOutputCodeForm#INPUT_MAIN_AND_OUTPUT_FUNC} value. */
    private static final String INPUT_MAIN_EXPLAIN = "input code in the main program and output code in a function";

    /** Phrase to explain the {@link InputOutputCodeForm#INPUT_FUNC_AND_OUTPUT_MAIN} value. */
    private static final String OUTPUT_MAIN_EXPLAIN = "input code in a function and output code in the main program";

    /** Phrase to explain the {@link InputOutputCodeForm#INPUT_FUNC_AND_OUTPUT_FUNC} value. */
    private static final String BOTH_FUNC_EXPLAIN = "both input and output code each in their own function";

    /** Mapping from option value text in lowercase to the corresponding option value. */
    private static final Map<String, InputOutputCodeForm> OPTION_VALUES = Map.of(
            "both-main", InputOutputCodeForm.INPUT_MAIN_AND_OUTPUT_MAIN,
            "input-main-output-func", InputOutputCodeForm.INPUT_MAIN_AND_OUTPUT_FUNC,
            "input-func-output-main", InputOutputCodeForm.INPUT_FUNC_AND_OUTPUT_MAIN,
            "both-func", InputOutputCodeForm.INPUT_FUNC_AND_OUTPUT_FUNC);

    /** Constructor of the {@link InputOutputCodeFormOption} class. */
    public InputOutputCodeFormOption() {
        super("Input/output code form",
                "Where to generate input and output code. Choose between: "
                        + BOTH_MAIN_EXPLAIN + " (FORM=both-main), or "
                        + INPUT_MAIN_EXPLAIN + " (FORM=input-main-output-func), or "
                        + OUTPUT_MAIN_EXPLAIN + " (FORM=input-func-output-main), or "
                        + BOTH_FUNC_EXPLAIN + " (FORM=both-func). "
                        + "[DEFAULT=both-main]",
                null, LONG_OPTION_TEXT, "FORM", InputOutputCodeForm.INPUT_MAIN_AND_OUTPUT_MAIN, true,
                "Where to generate input and output code.");
    }

    @Override
    public InputOutputCodeForm parseValue(String optName, String value) {
        InputOutputCodeForm enumValue = OPTION_VALUES.get(value.toLowerCase(Locale.US));
        if (enumValue == null) {
            throw new InvalidOptionException("Unknown option value.");
        }
        return enumValue;
    }

    @Override
    public String[] getCmdLine(Object value) {
        InputOutputCodeForm form = (InputOutputCodeForm)value;
        for (Entry<String, InputOutputCodeForm> entry: OPTION_VALUES.entrySet()) {
            if (entry.getValue() == form) {
                return new String[] {fmt("--%s=%s", LONG_OPTION_TEXT, entry.getKey())};
            }
        }
        throw new AssertionError("Command line conversion of \"" + value + "\" failed.");
    }

    @Override
    protected String getDialogText(InputOutputCodeForm value) {
        return switch (value) {
            case INPUT_MAIN_AND_OUTPUT_MAIN -> formatExplanation(BOTH_MAIN_EXPLAIN);
            case INPUT_MAIN_AND_OUTPUT_FUNC -> formatExplanation(INPUT_MAIN_EXPLAIN);
            case INPUT_FUNC_AND_OUTPUT_MAIN -> formatExplanation(OUTPUT_MAIN_EXPLAIN);
            case INPUT_FUNC_AND_OUTPUT_FUNC -> formatExplanation(BOTH_FUNC_EXPLAIN);
            default -> throw new AssertionError("Unexpected enumeration literal \"" + value + "\".");
        };
    }

    /**
     * Format the explanation text, so it becomes a sentence.
     *
     * @param explanation Explanation for format.
     * @return The constructed sentence.
     */
    private String formatExplanation(String explanation) {
        return Strings.makeInitialUppercase(explanation) + ".";
    }

    /**
     * Get the selected value of the {@link InputOutputCodeFormOption} option.
     *
     * @return The selected value of the option.
     */
    public static InputOutputCodeForm getValue() {
        return Options.get(InputOutputCodeFormOption.class);
    }
}
