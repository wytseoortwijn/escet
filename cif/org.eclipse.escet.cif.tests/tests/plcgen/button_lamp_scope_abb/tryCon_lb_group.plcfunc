FUNCTION tryCon_lb_group: BOOL
VAR_INPUT
    isProgress: BOOL;
END_VAR
VAR
    funcIsProgress: BOOL;
    current_hw_lamp: BOOL;
    current_hw_lamp_1: BOOL;
    current_sup: BYTE;
    current_sup_1: BYTE;
    edge_hw_lamp: BOOL;
    edge_sup: BYTE;
    eventEnabled: BOOL;
END_VAR

funcIsProgress := isProgress;
(*************************************************************
 * Try to perform controllable event "lb_group.off".
 *
 * - Automaton "hw_lamp" must always synchronize.
 * - Automaton "sup" must always synchronize.
 *************************************************************)
eventEnabled := TRUE;
(*******************************
 * Check each synchronizing automaton for having an edge with a true guard.
 *******************************)
(***********
 * Test edge of automaton "hw_lamp" to synchronize for event "lb_group.off".
 * This automaton must have an edge with a true guard to allow the event.
 *
 * Edge being tested:
 * - Location "On":
 *   - 1st edge in the location
 ***********)
IF hw_lamp = 1 THEN
    edge_hw_lamp := 0;
ELSE
    (* The automaton has no edge with a true guard. Skip to the next event. *)
    eventEnabled := FALSE;
END_IF;
IF eventEnabled THEN
    (***********
     * Test edges of automaton "sup" to synchronize for event "lb_group.off".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edges being tested:
     * - Location "s8":
     *   - 1st edge in the location
     * - Location "s9":
     *   - 1st edge in the location
     ***********)
    IF sup = 7 THEN
        edge_sup := 0;
    ELSIF sup = 8 THEN
        edge_sup := 1;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
END_IF;
(* All checks have been done. If variable "eventEnabled" still holds, event "lb_group.off" can occur. *)
IF eventEnabled THEN
    funcIsProgress := TRUE;
    (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
    current_hw_lamp := hw_lamp;
    current_sup := sup;
    (*******************************
     * Perform the assignments of each synchronizing automaton.
     *******************************)
    (* Perform assignments of automaton "hw_lamp". *)
    IF edge_hw_lamp = 0 THEN
        (* Perform assignments of the 1st edge in location "hw_lamp.On". *)
        (* Perform update of current-location variable for automaton "hw_lamp". *)
        hw_lamp := 0;
    END_IF;
    (* Perform assignments of automaton "sup". *)
    IF edge_sup = 0 THEN
        (* Perform assignments of the 1st edge in location "sup.s8". *)
        (* Perform update of current-location variable for automaton "sup". *)
        sup := 9;
    ELSIF edge_sup = 1 THEN
        (* Perform assignments of the 1st edge in location "sup.s9". *)
        (* Perform update of current-location variable for automaton "sup". *)
        sup := 0;
    END_IF;
END_IF;

(*************************************************************
 * Try to perform controllable event "lb_group.on".
 *
 * - Automaton "hw_lamp" must always synchronize.
 * - Automaton "sup" must always synchronize.
 *************************************************************)
eventEnabled := TRUE;
(*******************************
 * Check each synchronizing automaton for having an edge with a true guard.
 *******************************)
(***********
 * Test edge of automaton "hw_lamp" to synchronize for event "lb_group.on".
 * This automaton must have an edge with a true guard to allow the event.
 *
 * Edge being tested:
 * - Location "Off":
 *   - 1st edge in the location
 ***********)
IF hw_lamp = 0 THEN
    edge_hw_lamp := 0;
ELSE
    (* The automaton has no edge with a true guard. Skip to the next event. *)
    eventEnabled := FALSE;
END_IF;
IF eventEnabled THEN
    (***********
     * Test edges of automaton "sup" to synchronize for event "lb_group.on".
     * This automaton must have an edge with a true guard to allow the event.
     *
     * Edges being tested:
     * - Location "s2":
     *   - 1st edge in the location
     * - Location "s4":
     *   - 1st edge in the location
     ***********)
    IF sup = 1 THEN
        edge_sup := 0;
    ELSIF sup = 3 THEN
        edge_sup := 1;
    ELSE
        (* The automaton has no edge with a true guard. Skip to the next event. *)
        eventEnabled := FALSE;
    END_IF;
END_IF;
(* All checks have been done. If variable "eventEnabled" still holds, event "lb_group.on" can occur. *)
IF eventEnabled THEN
    funcIsProgress := TRUE;
    (* Make temporary copies of assigned variables to preserve the old values while assigning new values. *)
    current_hw_lamp_1 := hw_lamp;
    current_sup_1 := sup;
    (*******************************
     * Perform the assignments of each synchronizing automaton.
     *******************************)
    (* Perform assignments of automaton "hw_lamp". *)
    IF edge_hw_lamp = 0 THEN
        (* Perform assignments of the 1st edge in location "hw_lamp.Off". *)
        (* Perform update of current-location variable for automaton "hw_lamp". *)
        hw_lamp := 1;
    END_IF;
    (* Perform assignments of automaton "sup". *)
    IF edge_sup = 0 THEN
        (* Perform assignments of the 1st edge in location "sup.s2". *)
        (* Perform update of current-location variable for automaton "sup". *)
        sup := 2;
    ELSIF edge_sup = 1 THEN
        (* Perform assignments of the 1st edge in location "sup.s4". *)
        (* Perform update of current-location variable for automaton "sup". *)
        sup := 4;
    END_IF;
END_IF;

(* Return event execution progress. *)
tryCon_lb_group := funcIsProgress;
RETURN;
END_FUNCTION
