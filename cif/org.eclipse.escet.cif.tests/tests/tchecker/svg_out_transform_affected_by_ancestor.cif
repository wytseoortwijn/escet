//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

svgfile "svg_out_transform_affected_by_ancestor.svg";

// No warning for parent without "rotate", "scale*", "matrix", or "skew*".
svgout id "box1" attr "transform" value "translate(5, 6)";

// Warning for parent with "rotate".
svgout id "box2" attr "transform" value "translate(5, 6)";

// Warning for parent with "matrix".
svgout id "box3" attr "transform" value "translate(5, 6)";

// Warning for parent with "scale".
svgout id "box4" attr "transform" value "translate(5, 6)";

// Warning for parent with "scaleX".
svgout id "box5" attr "transform" value "translate(5, 6)";

// Warning for parent with "scaleY".
svgout id "box6" attr "transform" value "translate(5, 6)";

// Warning for parent with "skew".
svgout id "box7" attr "transform" value "translate(5, 6)";

// Warning for parent with "skewX".
svgout id "box8" attr "transform" value "translate(5, 6)";

// Warning for parent with "skewY".
svgout id "box9" attr "transform" value "translate(5, 6)";

// Warning for grandparent with "rotate", "scale*", "matrix", or "skew*".
svgout id "box10" attr "transform" value "translate(5, 6)";

// No warning for parent with "translate".
svgout id "box11" attr "transform" value "translate(5, 6)";

// Warning for nameless parent.
svgout id "box12" attr "transform" value "translate(5, 6)";
