/* CIF to C translation of ranged_int_in_type_decl.cif
 * Generated file, DO NOT EDIT
 */

#include <stdio.h>
#include <stdlib.h>
#include "ranged_int_in_type_decl_engine.h"

#ifndef MAX_NUM_ITERS
#define MAX_NUM_ITERS 1000
#endif

/* What to do if a range error is found in an assignment? */
#ifdef KEEP_RUNNING
static inline void RangeErrorDetected(void) { /* Do nothing, error is already reported. */ }
#else
static inline void RangeErrorDetected(void) { exit(1); }
#endif

/* Type support code. */
int EnumTypePrint(ranged_int_in_type_declEnum value, char *dest, int start, int end) {
    int last = end - 1;
    const char *lit_name = enum_names[value];
    while (start < last && *lit_name) {
        dest[start++] = *lit_name;
        lit_name++;
    }
    dest[start] = '\0';
    return start;
}


/** Event names. */
const char *ranged_int_in_type_decl_event_names[] = {
    "initial-step", /**< Initial step. */
    "delay-step",   /**< Delay step. */
    "c_decr",       /**< Event "c_decr". */
};

/** Enumeration names. */
const char *enum_names[] = {
    /** Literal "__some_dummy_enum_literal". */
    "__some_dummy_enum_literal",
};

/* Constants. */


/* Functions. */


/* Input variables. */


/* State variables. */

/** Discrete variable "int[0..10] Test.item". */
IntType Test_item_;

RealType model_time; /**< Current model time. */

/** Initialize constants. */
static void InitConstants(void) {

}

/** Print function. */
#if PRINT_OUTPUT
static void PrintOutput(ranged_int_in_type_decl_Event_ event, BoolType pre) {
}
#endif

/* Edge execution code. */

/**
 * Execute code for edge with index 0 and event "c_decr".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge0(void) {
    BoolType guard = (Test_item_) > (0);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        ranged_int_in_type_decl_InfoEvent(c_decr_, TRUE);
    #endif

    {
        IntType rhs2 = (Test_item_) - (1);
        #if CHECK_RANGES
        if ((rhs2) < 0) {
            fprintf(stderr, "RangeError: Writing %d into \"int[0..10]\"\n", rhs2);
            fprintf(stderr, "            at " "Test.item" "\n");
            RangeErrorDetected();
        }
        #endif
        Test_item_ = rhs2;
    }

    #if EVENT_OUTPUT
        ranged_int_in_type_decl_InfoEvent(c_decr_, FALSE);
    #endif
    return TRUE;
}

/**
 * Normalize and check the new value of a continuous variable after an update.
 * @param new_value Unnormalized new value of the continuous variable.
 * @param var_name Name of the continuous variable in the CIF model.
 * @return The normalized new value of the continuous variable.
 */
static inline RealType UpdateContValue(RealType new_value, const char *var_name) {
    if (isfinite(new_value)) {
        return (new_value == -0.0) ? 0.0 : new_value;
    }

    const char *err_type;
    if (isnan(new_value)) {
        err_type = "NaN";
    } else if (new_value > 0) {
        err_type = "+inf";
    } else {
        err_type = "-inf";
    }
    fprintf(stderr, "Continuous variable \"%s\" has become %s.\n", var_name, err_type);

#ifdef KEEP_RUNNING
    return 0.0;
#else
    exit(1);
#endif
}

/** Repeatedly perform discrete event steps, until no progress can be made any more. */
static void PerformEdges(void) {
    /* Uncontrollables. */
    int count = 0;
    for (;;) {
        count++;
        if (count > MAX_NUM_ITERS) { /* 'Infinite' loop detection. */
            fprintf(stderr, "Warning: Quitting after performing %d uncontrollable events, infinite loop?\n", count);
            break;
        }

        BoolType edgeExecuted = false;



        if (!edgeExecuted) {
            break; /* No edge fired, done with discrete steps. */
        }
    }

    /* Controllables. */
    count = 0;
    for (;;) {
        count++;
        if (count > MAX_NUM_ITERS) { /* 'Infinite' loop detection. */
            fprintf(stderr, "Warning: Quitting after performing %d controllable events, infinite loop?\n", count);
            break;
        }

        BoolType edgeExecuted = false;

        edgeExecuted |= execEdge0(); /* (Try to) perform edge with index 0 and event "c_decr". */

        if (!edgeExecuted) {
            break; /* No edge fired, done with discrete steps. */
        }
    }
}

/** First model call, initializing, and performing discrete events before the first time step. */
void ranged_int_in_type_decl_EngineFirstStep(void) {
    InitConstants();

    model_time = 0.0;

    Test_item_ = 0;

    #if PRINT_OUTPUT
        /* pre-initial and post-initial prints. */
        PrintOutput(EVT_INITIAL_, TRUE);
        PrintOutput(EVT_INITIAL_, FALSE);
    #endif

    PerformEdges();

    #if PRINT_OUTPUT
        /* pre-timestep print. */
        PrintOutput(EVT_DELAY_, TRUE);
    #endif
}

/**
 * Engine takes a time step of length \a delta.
 * @param delta Length of the time step.
 */
void ranged_int_in_type_decl_EngineTimeStep(double delta) {


    /* Update continuous variables. */
    if (delta > 0.0) {

        model_time += delta;
    }

    #if PRINT_OUTPUT
        /* post-timestep print. */
        PrintOutput(EVT_DELAY_, FALSE);
    #endif

    PerformEdges();

    #if PRINT_OUTPUT
        /* pre-timestep print. */
        PrintOutput(EVT_DELAY_, TRUE);
    #endif
}

