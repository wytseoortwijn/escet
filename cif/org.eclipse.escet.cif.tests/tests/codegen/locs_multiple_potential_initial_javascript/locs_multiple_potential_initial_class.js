/** Tuples. */


/** locs_multiple_potential_initial code generated from a CIF specification. */
class locs_multiple_potential_initial_class {
    /** locs_multiple_potential_initialEnum declaration. It contains the single merged enum from the CIF model. */
    locs_multiple_potential_initialEnum = Object.freeze({
        /** Literal "loc1". */
        _loc1: Symbol("loc1"),

        /** Literal "loc2". */
        _loc2: Symbol("loc2")
    })

    /** Should execution timing information be provided? */
    doInfoExec = true;

    /** Should executed event information be provided? */
    doInfoEvent = true;

    /** Should print output be provided? */
    doInfoPrintOutput = true;

    /** Should state output be provided? */
    doStateOutput = false;

    /** Should transition output be provided? */
    doTransitionOutput = true;

    /** Whether the constants have been initialized already. */
    constantsInitialized = false;

    /** Whether the SVG copy and move declarations have been applied already. */
    svgCopiesAndMovesApplied = false;

    /** Whether this is the first time the code is (to be) executed. */
    firstExec;

    /** The names of all the events. */
    EVENT_NAMES = [

    ];


    /** Variable 'time', tracks elapsed time for a session. */
    time;

    /**
     * The frequency in times per second, that the code should
     * be executed (if positive), or execute as fast as possible, that is
     * as many times per second as possible (if negative or zero).
     */
    frequency = 60;

    /**
     * Whether the next execution is the first execution of the session.
     * Used to initialize time-related variables for starting, pausing,
     * resuming or resetting each session.
     */
    first;

    /**
     * Whether the simulation is currently running, and should process
     * user input, or is paused.
     */
    playing;

    /** The start time of the current session. */
    startMilli;

    /**
     * The targeted end time of the current/next cycle, to ensure
     * that the duration of the cycle matches with the configured
     * frequency.
     */
    targetMilli;


    /** Discrete variable "a". */
    a_;


    /** SVG output elements. */


    /**
     * SVG input queue with functions for handling clicked SVG
     * input elements, from first clicked at the head of the array
     * to last clicked at the tail of the array.
     */
    svgInQueue;

    /**
     * The SVG input id corresponding to the SVG input element that
     * was clicked. Is 'null' if no SVG input element was clicked so
     * far, or all clicks have already been processed.
     */
    svgInId;

    /**
     * The 0-based index of the event corresponding to the SVG input
     * element that was clicked. Is '-1' if no SVG input element was
     * clicked so far, or all clicks have already been processed.
     */
    svgInEvent;

    /** SVG input click event handlers. */


    /** SVG input event setters. */


    /** Starts the simulation. */
    start() {
        if (!this.playing) {
            this.playing = true;
            this.exec();
        }
    }

    /** Stops the simulation. */
    stop() {
        if (this.playing) {
            this.playing = false;
        }
    }

    /** Resets the object to its initial state. */
    reset() {
        this.stop();
        this.firstExec = true;
        this.time = 0.0;
        this.first = true;
        this.timePaused = null;
        this.initState();
        this.initUI();
        this.updateUI();
    }

    /**
     * Execute the code once. Inputs are read, transitions are executed until
     * none are possible, outputs are written, etc.
     *
     * @param newTime The time in seconds, since the start of the first
     *      execution.
     * @throws {locs_multiple_potential_initialException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execOnce(newTime) {
        // Pre execution notification.
        this.preExec();

        // Update values of input variables.
        this.updateInputs();

        // Initialize the state.
        if (this.firstExec) {
            this.initState();
        }

        // Calculate time delta.
        var delta = newTime - this.time;

        // Update values of continuous variables.
        if (!this.firstExec) {
                        // No continuous variables, except variable 'time'.
        }

        // Update time.
        this.time = newTime;

        // Apply print declarations.
        if (this.firstExec) {
            // For 'initial' transition.
            if (this.doInfoPrintOutput) this.printOutput(-3, true);
            if (this.doInfoPrintOutput) this.printOutput(-3, false);
            if (this.doStateOutput) locs_multiple_potential_initial.log('Initial state: ' + locs_multiple_potential_initial.getStateText());
            if (this.doStateOutput || this.doTransitionOutput) this.log('');

        } else {
            // For 'post' of time transition.
            if (this.doInfoPrintOutput) this.printOutput(-2, false);
        }

        // Execute environment events and SVG input events as long as they are possible, emptying the SVG input queue.
        while (true) {
            // Handle next element from SVG input queue, if not already already processing one.
            if (this.svgInEvent == -1 && this.svgInQueue.length > 0) {
                var func = this.svgInQueue.shift(); // Remove head of the queue.
                func(); // Call function, to set the event to allow.
            }

            // Try to execute an SVG input mapping with updates for each environment event, and an edge for each SVG
            // input event.
            var anythingExecuted = false;



            // Stop if no SVG input mapping and no edge was executed, and no more SVG input clicks are to be processed.
            if (!anythingExecuted && this.svgInQueue.length == 0) {
                break;
            }
        }

        // Make sure all outstanding SVG input clicks have been processed.
        console.assert(this.svgInEvent == -1 && this.svgInQueue.length == 0);

        // Execute uncontrollable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Execute controllable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Apply print declarations for 'pre' of time transition.
        if (this.doInfoPrintOutput) this.printOutput(-2, true);

        // Post execution notification.
        this.postExec();

        // Done.
        this.firstExec = false;
    }

    /**
     * Calls {@link #execWhile}, which repeatedly {@link #execOnce executes the code}.
     *
     * @throws {locs_multiple_potential_initialException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    exec() {
        this.execWhile(1);
    }

    /**
     * Repeatedly {@link #execOnce executes the code}.
     *
     * @param delay The delay before executing, in milliseconds.
     *
     * @throws {locs_multiple_potential_initialException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execWhile(delay) {
        setTimeout(
            function () {
                // Pre execution timing.
                var now = Date.now();
                var preMilli = now;

                // On first execution, initialize variables for timing.
                if (locs_multiple_potential_initial.first) {
                    locs_multiple_potential_initial.first = false;
                    locs_multiple_potential_initial.startMilli = now;
                    locs_multiple_potential_initial.targetMilli = locs_multiple_potential_initial.startMilli;
                    preMilli = locs_multiple_potential_initial.startMilli;
                }

                // Handle pausing/playing.
                if (!locs_multiple_potential_initial.playing) {
                    locs_multiple_potential_initial.timePaused = now;
                    return;
                }

                if (locs_multiple_potential_initial.timePaused) {
                    locs_multiple_potential_initial.startMilli += (now - locs_multiple_potential_initial.timePaused);
                    locs_multiple_potential_initial.targetMilli += (now - locs_multiple_potential_initial.timePaused);
                    locs_multiple_potential_initial.timePaused = null;
                }

                // Get cycle time and current 'time'.
                var frequency = locs_multiple_potential_initial.frequency;
                var cycleMilli = (frequency <= 0) ? -1 : 1e3 / frequency;
                var timeMilli = preMilli - locs_multiple_potential_initial.startMilli;

                // Execute once.
                locs_multiple_potential_initial.execOnce(timeMilli / 1e3);

                // Post execution timing.
                var postMilli = Date.now();
                var duration = postMilli - preMilli;
                if (locs_multiple_potential_initial.doInfoExec) {
                    locs_multiple_potential_initial.infoExec(duration, cycleMilli);
                }

                // Ensure frequency.
                var remainderMilli = 0;
                if (frequency > 0) {
                    locs_multiple_potential_initial.targetMilli += cycleMilli;
                    remainderMilli = locs_multiple_potential_initial.targetMilli - postMilli;
                }

                // Execute again.
                locs_multiple_potential_initial.execWhile(remainderMilli > 0 ? remainderMilli : 0);
            },
        delay);
    }


    /**
     * Initializes the state.
     *
     * @throws {locs_multiple_potential_initialException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    initState() {
        // Initialize constants, if not yet done so.
        if (!this.constantsInitialized) {
            this.constantsInitialized = true;

        }

        // Initialize SVG input.
        this.svgInQueue = [];
        this.svgInId = null;
        this.svgInEvent = -1;


        // CIF model state variables.
        try {
            locs_multiple_potential_initial.a_ = locs_multiple_potential_initial.locs_multiple_potential_initialEnum._loc1;
        } catch (e) {
            if (e instanceof locs_multiple_potential_initialException) {
                e = new locs_multiple_potential_initialException("Failed to evaluate the initial value of variable \"a\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Initializes the user interface, either when loading the page
     * or when resetting the simulation.
     */
    initUI() {


        // Apply SVG copies and moves, if not done so before.
        if (!this.svgCopiesAndMovesApplied) {
            this.svgCopiesAndMovesApplied = true;

            // Apply SVG copy declarations.


            // Apply SVG move declarations.

        }

        // Prepare SVG output.


        // Prepare SVG input.

    }

    /**
     * Updates the user interface based on the latest state of
     * the model. Is called at the end of each cycle.
     *
     * @throws {locs_multiple_potential_initialException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    updateUI() {
        // Apply CIF/SVG output mappings.

    }

    /**
     * Updates the values of the input variables. Other variables from the
     * state may not be accessed or modified.
     */
    updateInputs() {
        // Nothing is done here by default.
    }

    /** Logs a normal message. */
    log(message) {
        console.log(message);

    }

    /** Logs an warning message. */
    warning(message) {
        console.log(message);

    }

    /** Logs an error message. */
    error(message) {
        console.log(message);

    }

    /** Logs a runtime error of type locs_multiple_potential_initialException. */
    runtimeError(e, isCause = false) {
        console.assert(e instanceof locs_multiple_potential_initialException);
        if (isCause) {
            this.error("CAUSE: " + e.message);
        } else {
            this.error("ERROR: " + e.message);
        }
        if (e.cause) {
            this.runtimeError(e.cause, true);
        }
    }

    /**
     * Informs about the duration of a single execution.
     *
     * @param duration The duration of the execution, in milliseconds.
     * @param cycleTime The desired maximum duration of the execution, in
     *      milliseconds, or '-1' if not available.
     */
    infoExec(duration, cycleTime) {
        // Nothing is done here by default.
    }

    /**
     * Informs that an event will be or has been executed.
     *
     * @param idx The 0-based index of the event.
     * @param pre Whether the event will be executed ('true') or has
     *      been executed ('false').
     */
    infoEvent(idx, pre) {
        if (pre) {
            if (this.doTransitionOutput) locs_multiple_potential_initial.log(locs_multiple_potential_initialUtils.fmt('Transition: event %s', locs_multiple_potential_initial.getEventName(idx)));
        } else {
            if (this.doStateOutput) locs_multiple_potential_initial.log('State: ' + locs_multiple_potential_initial.getStateText());
        }
    }

    /**
     * Informs that the code is about to be executed. For the
     * {@link #firstExec} the state has not yet been initialized, except for
     * {@link #time}.
     */
    preExec() {
        // Nothing is done here by default.
    }

    /** Informs that the code was just executed. */
    postExec() {
        this.updateUI();
    }

    /**
     * Returns the name of an event.
     *
     * @param idx The 0-based index of the event.
     * @return The name of the event.
     */
    getEventName(idx) {
        return this.EVENT_NAMES[idx];
    }

    /**
     * Returns a single-line textual representation of the model state.
     *
     * @return The single-line textual representation of the model state.
     */
    getStateText() {
        var state = locs_multiple_potential_initialUtils.fmt('time=%s', locs_multiple_potential_initial.time);
        state += locs_multiple_potential_initialUtils.fmt(', a=%s', locs_multiple_potential_initialUtils.valueToStr(locs_multiple_potential_initial.a_));
        return state;
    }


    /**
     * Evaluates algebraic variable "a.x".
     *
     * @return The evaluation result.
     * @throws {locs_multiple_potential_initialException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    a_x_() {
        try {
            return true;
        } catch (e) {
            if (e instanceof locs_multiple_potential_initialException) {
                e = new locs_multiple_potential_initialException("Failed to evaluate algebraic variable \"a.x\".", e);
            }
            throw e;
        }
    }



    /**
     * Print output for all relevant print declarations.
     *
     * @param idx The 0-based event index of the transition, or '-2' for
     *      time transitions, or '-3' for the 'initial' transition.
     * @param pre Whether to print output for the pre/source state of the
     *      transition ('true') or for the post/target state of the
     *      transition ('false').
     * @throws {locs_multiple_potential_initialException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    printOutput(idx, pre) {
        // No print declarations.
    }

    /**
     * Informs that new print output is available.
     *
     * @param text The text being printed.
     * @param target The file or special target to which text is to be printed.
     *      If printed to a file, an absolute or relative local file system
     *      path is given. Paths may contain both '/' and '\\'
     *      as file separators. Use {@link locs_multiple_potential_initialUtils#normalizePrintTarget}
     *      to normalize the path to use '/' file separators. There are two
     *      special targets: ':stdout' to print to the standard output stream,
     *      and ':stderr' to print to the standard error stream.
     */
    infoPrintOutput(text, target) {
        if (target == ':stdout') {
            locs_multiple_potential_initial.log(text);
        } else if (target == ':stderr') {
            locs_multiple_potential_initial.error(text);
        } else {
            var path = locs_multiple_potential_initialUtils.normalizePrintTarget(target);
            locs_multiple_potential_initial.infoPrintOutput(path + ': ' + text, ':stdout');
        }
    }
}
