/** Tuples. */


/** runtime_error code generated from a CIF specification. */
class runtime_error_class {
    /** runtime_errorEnum declaration. It contains the single merged enum from the CIF model. */
    runtime_errorEnum = Object.freeze({
        /** Literal "__some_dummy_enum_literal". */
        ___some_dummy_enum_literal: Symbol("__some_dummy_enum_literal")
    })

    /** Should execution timing information be provided? */
    doInfoExec = true;

    /** Should executed event information be provided? */
    doInfoEvent = true;

    /** Should print output be provided? */
    doInfoPrintOutput = true;

    /** Should state output be provided? */
    doStateOutput = false;

    /** Should transition output be provided? */
    doTransitionOutput = true;

    /** Whether the constants have been initialized already. */
    constantsInitialized = false;

    /** Whether the SVG copy and move declarations have been applied already. */
    svgCopiesAndMovesApplied = false;

    /** Whether this is the first time the code is (to be) executed. */
    firstExec;

    /** The names of all the events. */
    EVENT_NAMES = [
        "e"
    ];


    /** Variable 'time', tracks elapsed time for a session. */
    time;

    /**
     * The frequency in times per second, that the code should
     * be executed (if positive), or execute as fast as possible, that is
     * as many times per second as possible (if negative or zero).
     */
    frequency = 60;

    /**
     * Whether the next execution is the first execution of the session.
     * Used to initialize time-related variables for starting, pausing,
     * resuming or resetting each session.
     */
    first;

    /**
     * Whether the simulation is currently running, and should process
     * user input, or is paused.
     */
    playing;

    /** The start time of the current session. */
    startMilli;

    /**
     * The targeted end time of the current/next cycle, to ensure
     * that the duration of the cycle matches with the configured
     * frequency.
     */
    targetMilli;


    /** Continuous variable "g.cont_var1". */
    g_cont_var1_;

    /** Discrete variable "a.x". */
    a_x_;

    /** Discrete variable "a.y". */
    a_y_;

    /** Continuous variable "g.cont_var2". */
    g_cont_var2_;

    /** Input variable "svg.i". */
    svg_i_;

    /** SVG output elements. */


    /**
     * SVG input queue with functions for handling clicked SVG
     * input elements, from first clicked at the head of the array
     * to last clicked at the tail of the array.
     */
    svgInQueue;

    /**
     * The SVG input id corresponding to the SVG input element that
     * was clicked. Is 'null' if no SVG input element was clicked so
     * far, or all clicks have already been processed.
     */
    svgInId;

    /**
     * The 0-based index of the event corresponding to the SVG input
     * element that was clicked. Is '-1' if no SVG input element was
     * clicked so far, or all clicks have already been processed.
     */
    svgInEvent;

    /** SVG input click event handlers. */


    /** SVG input event setters. */


    /** Starts the simulation. */
    start() {
        if (!this.playing) {
            this.playing = true;
            this.exec();
        }
    }

    /** Stops the simulation. */
    stop() {
        if (this.playing) {
            this.playing = false;
        }
    }

    /** Resets the object to its initial state. */
    reset() {
        this.stop();
        this.firstExec = true;
        this.time = 0.0;
        this.first = true;
        this.timePaused = null;
        this.initState();
        this.initUI();
        this.updateUI();
    }

    /**
     * Execute the code once. Inputs are read, transitions are executed until
     * none are possible, outputs are written, etc.
     *
     * @param newTime The time in seconds, since the start of the first
     *      execution.
     * @throws {runtime_errorException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execOnce(newTime) {
        // Pre execution notification.
        this.preExec();

        // Update values of input variables.
        this.updateInputs();

        // Initialize the state.
        if (this.firstExec) {
            this.initState();
        }

        // Calculate time delta.
        var delta = newTime - this.time;

        // Update values of continuous variables.
        if (!this.firstExec) {
                        try {
                            var deriv0 = runtime_error.g_cont_var1_deriv();
                            var deriv1 = runtime_error.g_cont_var2_deriv();

                            runtime_error.g_cont_var1_ = runtime_error.g_cont_var1_ + delta * deriv0;
                            runtime_errorUtils.checkReal(runtime_error.g_cont_var1_, "g.cont_var1");
                            if (runtime_error.g_cont_var1_ == -0.0) runtime_error.g_cont_var1_ = 0.0;
                            runtime_error.g_cont_var2_ = runtime_error.g_cont_var2_ + delta * deriv1;
                            runtime_errorUtils.checkReal(runtime_error.g_cont_var2_, "g.cont_var2");
                            if (runtime_error.g_cont_var2_ == -0.0) runtime_error.g_cont_var2_ = 0.0;
                        } catch (e) {
                            if (e instanceof runtime_errorException) {
                                e = new runtime_errorException("Failed to update continuous variables after time passage.", e);
                                this.runtimeError(e);
                            }
                            throw e;
                        }
        }

        // Update time.
        this.time = newTime;

        // Apply print declarations.
        if (this.firstExec) {
            // For 'initial' transition.
            if (this.doInfoPrintOutput) this.printOutput(-3, true);
            if (this.doInfoPrintOutput) this.printOutput(-3, false);
            if (this.doStateOutput) runtime_error.log('Initial state: ' + runtime_error.getStateText());
            if (this.doStateOutput || this.doTransitionOutput) this.log('');

        } else {
            // For 'post' of time transition.
            if (this.doInfoPrintOutput) this.printOutput(-2, false);
        }

        // Execute environment events and SVG input events as long as they are possible, emptying the SVG input queue.
        while (true) {
            // Handle next element from SVG input queue, if not already already processing one.
            if (this.svgInEvent == -1 && this.svgInQueue.length > 0) {
                var func = this.svgInQueue.shift(); // Remove head of the queue.
                func(); // Call function, to set the event to allow.
            }

            // Try to execute an SVG input mapping with updates for each environment event, and an edge for each SVG
            // input event.
            var anythingExecuted = false;



            // Stop if no SVG input mapping and no edge was executed, and no more SVG input clicks are to be processed.
            if (!anythingExecuted && this.svgInQueue.length == 0) {
                break;
            }
        }

        // Make sure all outstanding SVG input clicks have been processed.
        console.assert(this.svgInEvent == -1 && this.svgInQueue.length == 0);

        // Execute uncontrollable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Execute controllable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;

            // Event "e".
            anythingExecuted |= this.execEdge0();

            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Apply print declarations for 'pre' of time transition.
        if (this.doInfoPrintOutput) this.printOutput(-2, true);

        // Post execution notification.
        this.postExec();

        // Done.
        this.firstExec = false;
    }

    /**
     * Calls {@link #execWhile}, which repeatedly {@link #execOnce executes the code}.
     *
     * @throws {runtime_errorException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    exec() {
        this.execWhile(1);
    }

    /**
     * Repeatedly {@link #execOnce executes the code}.
     *
     * @param delay The delay before executing, in milliseconds.
     *
     * @throws {runtime_errorException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execWhile(delay) {
        setTimeout(
            function () {
                // Pre execution timing.
                var now = Date.now();
                var preMilli = now;

                // On first execution, initialize variables for timing.
                if (runtime_error.first) {
                    runtime_error.first = false;
                    runtime_error.startMilli = now;
                    runtime_error.targetMilli = runtime_error.startMilli;
                    preMilli = runtime_error.startMilli;
                }

                // Handle pausing/playing.
                if (!runtime_error.playing) {
                    runtime_error.timePaused = now;
                    return;
                }

                if (runtime_error.timePaused) {
                    runtime_error.startMilli += (now - runtime_error.timePaused);
                    runtime_error.targetMilli += (now - runtime_error.timePaused);
                    runtime_error.timePaused = null;
                }

                // Get cycle time and current 'time'.
                var frequency = runtime_error.frequency;
                var cycleMilli = (frequency <= 0) ? -1 : 1e3 / frequency;
                var timeMilli = preMilli - runtime_error.startMilli;

                // Execute once.
                runtime_error.execOnce(timeMilli / 1e3);

                // Post execution timing.
                var postMilli = Date.now();
                var duration = postMilli - preMilli;
                if (runtime_error.doInfoExec) {
                    runtime_error.infoExec(duration, cycleMilli);
                }

                // Ensure frequency.
                var remainderMilli = 0;
                if (frequency > 0) {
                    runtime_error.targetMilli += cycleMilli;
                    remainderMilli = runtime_error.targetMilli - postMilli;
                }

                // Execute again.
                runtime_error.execWhile(remainderMilli > 0 ? remainderMilli : 0);
            },
        delay);
    }

    /**
     * Execute code for edge with index 0 and event "e".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {runtime_errorException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge0() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(0, true);
            if (this.doInfoEvent) this.infoEvent(0, true);

            try {
                if (true) {
                    runtime_error.a_x_ = runtime_error.a_x_;
                } else if (runtime_errorUtils.equalObjs(runtime_errorUtils.divide(runtime_error.a_x_, runtime_error.a_y_), 5.0)) {
                    var rhs1 = (runtime_error.a_x_) + (1);
                    if ((rhs1) > 3) {
                        runtime_errorUtils.rangeErrInt("a.x", runtime_errorUtils.valueToStr(rhs1), "int[0..3]");
                    }
                    runtime_error.a_x_ = rhs1;
                } else if (true) {
                    runtime_error.a_x_ = runtime_errorUtils.div(runtime_error.a_x_, runtime_error.a_y_);
                }
            } catch (e) {
                if (e instanceof runtime_errorException) {
                    e = new runtime_errorException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(0, false);
            if (this.doInfoPrintOutput) this.printOutput(0, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof runtime_errorException) {
                e = new runtime_errorException("Failed to (try to) execute event \"e\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Initializes the state.
     *
     * @throws {runtime_errorException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    initState() {
        // Initialize constants, if not yet done so.
        if (!this.constantsInitialized) {
            this.constantsInitialized = true;

        }

        // Initialize SVG input.
        this.svgInQueue = [];
        this.svgInId = null;
        this.svgInEvent = -1;


        // CIF model state variables.
        try {
            runtime_error.g_cont_var1_ = 0.0;
        } catch (e) {
            if (e instanceof runtime_errorException) {
                e = new runtime_errorException("Failed to evaluate the initial value of variable \"g.cont_var1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            runtime_error.a_x_ = 0;
        } catch (e) {
            if (e instanceof runtime_errorException) {
                e = new runtime_errorException("Failed to evaluate the initial value of variable \"a.x\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            runtime_error.a_y_ = 0;
        } catch (e) {
            if (e instanceof runtime_errorException) {
                e = new runtime_errorException("Failed to evaluate the initial value of variable \"a.y\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            runtime_error.g_cont_var2_ = runtime_errorUtils.divide(runtime_error.a_x_, runtime_error.a_y_);
        } catch (e) {
            if (e instanceof runtime_errorException) {
                e = new runtime_errorException("Failed to evaluate the initial value of variable \"g.cont_var2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Initializes the user interface, either when loading the page
     * or when resetting the simulation.
     */
    initUI() {


        // Apply SVG copies and moves, if not done so before.
        if (!this.svgCopiesAndMovesApplied) {
            this.svgCopiesAndMovesApplied = true;

            // Apply SVG copy declarations.


            // Apply SVG move declarations.

        }

        // Prepare SVG output.


        // Prepare SVG input.

    }

    /**
     * Updates the user interface based on the latest state of
     * the model. Is called at the end of each cycle.
     *
     * @throws {runtime_errorException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    updateUI() {
        // Apply CIF/SVG output mappings.

    }

    /**
     * Updates the values of the input variables. Other variables from the
     * state may not be accessed or modified.
     */
    updateInputs() {
        // Nothing is done here by default.
    }

    /** Logs a normal message. */
    log(message) {
        console.log(message);

    }

    /** Logs an warning message. */
    warning(message) {
        console.log(message);

    }

    /** Logs an error message. */
    error(message) {
        console.log(message);

    }

    /** Logs a runtime error of type runtime_errorException. */
    runtimeError(e, isCause = false) {
        console.assert(e instanceof runtime_errorException);
        if (isCause) {
            this.error("CAUSE: " + e.message);
        } else {
            this.error("ERROR: " + e.message);
        }
        if (e.cause) {
            this.runtimeError(e.cause, true);
        }
    }

    /**
     * Informs about the duration of a single execution.
     *
     * @param duration The duration of the execution, in milliseconds.
     * @param cycleTime The desired maximum duration of the execution, in
     *      milliseconds, or '-1' if not available.
     */
    infoExec(duration, cycleTime) {
        // Nothing is done here by default.
    }

    /**
     * Informs that an event will be or has been executed.
     *
     * @param idx The 0-based index of the event.
     * @param pre Whether the event will be executed ('true') or has
     *      been executed ('false').
     */
    infoEvent(idx, pre) {
        if (pre) {
            if (this.doTransitionOutput) runtime_error.log(runtime_errorUtils.fmt('Transition: event %s', runtime_error.getEventName(idx)));
        } else {
            if (this.doStateOutput) runtime_error.log('State: ' + runtime_error.getStateText());
        }
    }

    /**
     * Informs that the code is about to be executed. For the
     * {@link #firstExec} the state has not yet been initialized, except for
     * {@link #time}.
     */
    preExec() {
        // Nothing is done here by default.
    }

    /** Informs that the code was just executed. */
    postExec() {
        this.updateUI();
    }

    /**
     * Returns the name of an event.
     *
     * @param idx The 0-based index of the event.
     * @return The name of the event.
     */
    getEventName(idx) {
        return this.EVENT_NAMES[idx];
    }

    /**
     * Returns a single-line textual representation of the model state.
     *
     * @return The single-line textual representation of the model state.
     */
    getStateText() {
        var state = runtime_errorUtils.fmt('time=%s', runtime_error.time);
        state += runtime_errorUtils.fmt(', a.x=%s', runtime_errorUtils.valueToStr(runtime_error.a_x_));
        state += runtime_errorUtils.fmt(', a.y=%s', runtime_errorUtils.valueToStr(runtime_error.a_y_));
        state += runtime_errorUtils.fmt(', g.cont_var1=%s', runtime_errorUtils.valueToStr(runtime_error.g_cont_var1_));
        state += runtime_errorUtils.fmt(', g.cont_var1\'=%s', runtime_errorUtils.valueToStr(runtime_error.g_cont_var1_deriv()));
        state += runtime_errorUtils.fmt(', g.cont_var2=%s', runtime_errorUtils.valueToStr(runtime_error.g_cont_var2_));
        state += runtime_errorUtils.fmt(', g.cont_var2\'=%s', runtime_errorUtils.valueToStr(runtime_error.g_cont_var2_deriv()));
        return state;
    }


    /**
     * Evaluates algebraic variable "g.alg_var".
     *
     * @return The evaluation result.
     * @throws {runtime_errorException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    g_alg_var_() {
        try {
            return runtime_errorUtils.div(runtime_error.a_x_, runtime_error.a_y_);
        } catch (e) {
            if (e instanceof runtime_errorException) {
                e = new runtime_errorException("Failed to evaluate algebraic variable \"g.alg_var\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates derivative of continuous variable "g.cont_var1".
     *
     * @return The evaluation result.
     * @throws {runtime_errorException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    g_cont_var1_deriv() {
        try {
            return runtime_errorUtils.divide(runtime_error.a_x_, runtime_error.a_y_);
        } catch (e) {
            if (e instanceof runtime_errorException) {
                e = new runtime_errorException("Failed to evaluate the derivative of continuous variable \"g.cont_var1\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates derivative of continuous variable "g.cont_var2".
     *
     * @return The evaluation result.
     * @throws {runtime_errorException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    g_cont_var2_deriv() {
        try {
            return 1.0;
        } catch (e) {
            if (e instanceof runtime_errorException) {
                e = new runtime_errorException("Failed to evaluate the derivative of continuous variable \"g.cont_var2\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "g.fail".
     *
     * @param g_fail_x_ Function parameter "g.fail.x".
     * @return The return value of the function.
     * @throws {runtime_errorException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    g_fail_(g_fail_x_) {
        try {
            // Variable "g.fail.y".
            var g_fail_y_ = runtime_errorUtils.addInt(g_fail_x_, 1);

            // Execute statements in the function body.
            return runtime_errorUtils.div(g_fail_y_, g_fail_x_);
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof runtime_errorException) {
                e = new runtime_errorException("Failed to execute internal user-defined function \"g.fail\".", e);
            }
            throw e;
        }
    }

    /**
     * Print output for all relevant print declarations.
     *
     * @param idx The 0-based event index of the transition, or '-2' for
     *      time transitions, or '-3' for the 'initial' transition.
     * @param pre Whether to print output for the pre/source state of the
     *      transition ('true') or for the post/target state of the
     *      transition ('false').
     * @throws {runtime_errorException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    printOutput(idx, pre) {
        if (true) {
            if (pre) {
                var whenCond;
                try {
                    whenCond = (runtime_errorUtils.div(runtime_error.a_x_, runtime_error.a_y_)) > (0);
                } catch (e) {
                    if (e instanceof runtime_errorException) {
                        e = new runtime_errorException("Failed to evaluate print declaration \"when pre\" filter.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                if (whenCond) {
                    var text;
                    try {
                        text = "txt";
                    } catch (e) {
                        if (e instanceof runtime_errorException) {
                            e = new runtime_errorException("Failed to evaluate print declaration \"pre\" text.", e);
                            this.runtimeError(e);
                        }
                        throw e;
                    }
                    this.infoPrintOutput(text, ":stdout");
                }
            }
        }
        if (true) {
            if (!pre) {
                var whenCond;
                try {
                    whenCond = (runtime_errorUtils.div(runtime_error.a_x_, runtime_error.a_y_)) > (0);
                } catch (e) {
                    if (e instanceof runtime_errorException) {
                        e = new runtime_errorException("Failed to evaluate print declaration \"when post\" filter.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                if (whenCond) {
                    var text;
                    try {
                        text = "txt";
                    } catch (e) {
                        if (e instanceof runtime_errorException) {
                            e = new runtime_errorException("Failed to evaluate print declaration \"post\" text.", e);
                            this.runtimeError(e);
                        }
                        throw e;
                    }
                    this.infoPrintOutput(text, ":stdout");
                }
            }
        }
        if (true) {
            if (pre) {
                var text;
                try {
                    text = runtime_errorUtils.fmt("%d", runtime_errorUtils.div(runtime_error.a_x_, runtime_error.a_y_));
                } catch (e) {
                    if (e instanceof runtime_errorException) {
                        e = new runtime_errorException("Failed to evaluate print declaration \"pre\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = runtime_errorUtils.fmt("%d", runtime_errorUtils.div(runtime_error.a_x_, runtime_error.a_y_));
                } catch (e) {
                    if (e instanceof runtime_errorException) {
                        e = new runtime_errorException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    var value = runtime_error.g_alg_var_();
                    text = runtime_errorUtils.valueToStr(value);
                } catch (e) {
                    if (e instanceof runtime_errorException) {
                        e = new runtime_errorException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    var value = runtime_error.g_fail_(0);
                    text = runtime_errorUtils.valueToStr(value);
                } catch (e) {
                    if (e instanceof runtime_errorException) {
                        e = new runtime_errorException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
    }

    /**
     * Informs that new print output is available.
     *
     * @param text The text being printed.
     * @param target The file or special target to which text is to be printed.
     *      If printed to a file, an absolute or relative local file system
     *      path is given. Paths may contain both '/' and '\\'
     *      as file separators. Use {@link runtime_errorUtils#normalizePrintTarget}
     *      to normalize the path to use '/' file separators. There are two
     *      special targets: ':stdout' to print to the standard output stream,
     *      and ':stderr' to print to the standard error stream.
     */
    infoPrintOutput(text, target) {
        if (target == ':stdout') {
            runtime_error.log(text);
        } else if (target == ':stderr') {
            runtime_error.error(text);
        } else {
            var path = runtime_errorUtils.normalizePrintTarget(target);
            runtime_error.infoPrintOutput(path + ': ' + text, ':stdout');
        }
    }
}
