/** Tuples. */


            /** Tuple class for CIF tuple type representative "tuple(int[1..1]; int[2..2])". */
            class CifTuple_T2II {
                /** The 1st field. */
                _field0;

                /** The 2nd field. */
                _field1;

                /**
                 * Constructor for the {@link CifTuple_T2II} class.
                 *
                 * @param _field0 The 1st field.
                 * @param _field1 The 2nd field.
                 */
                constructor(_field0, _field1) {
                    this._field0 = _field0;
                    this._field1 = _field1;
                }

                copy() {
                    return new CifTuple_T2II(this._field0, this._field1);
                }

                toString() {
                    var rslt = '(';
                    rslt += exprsUtils.valueToStr(this._field0);
                    rslt += ', ';
                    rslt += exprsUtils.valueToStr(this._field1);
                    rslt += ')';
                    return rslt;
                }
            }

/** exprs code generated from a CIF specification. */
class exprs_class {
    /** exprsEnum declaration. It contains the single merged enum from the CIF model. */
    exprsEnum = Object.freeze({
        /** Literal "A". */
        _A: Symbol("A"),

        /** Literal "B". */
        _B: Symbol("B")
    })

    /** Should execution timing information be provided? */
    doInfoExec = true;

    /** Should executed event information be provided? */
    doInfoEvent = true;

    /** Should print output be provided? */
    doInfoPrintOutput = true;

    /** Should state output be provided? */
    doStateOutput = false;

    /** Should transition output be provided? */
    doTransitionOutput = true;

    /** Whether the constants have been initialized already. */
    constantsInitialized = false;

    /** Whether the SVG copy and move declarations have been applied already. */
    svgCopiesAndMovesApplied = false;

    /** Whether this is the first time the code is (to be) executed. */
    firstExec;

    /** The names of all the events. */
    EVENT_NAMES = [
        "a1.e"
    ];

    /** Constant "x1". */
    x1_;

    /** Variable 'time', tracks elapsed time for a session. */
    time;

    /**
     * The frequency in times per second, that the code should
     * be executed (if positive), or execute as fast as possible, that is
     * as many times per second as possible (if negative or zero).
     */
    frequency = 60;

    /**
     * Whether the next execution is the first execution of the session.
     * Used to initialize time-related variables for starting, pausing,
     * resuming or resetting each session.
     */
    first;

    /**
     * Whether the simulation is currently running, and should process
     * user input, or is paused.
     */
    playing;

    /** The start time of the current session. */
    startMilli;

    /**
     * The targeted end time of the current/next cycle, to ensure
     * that the duration of the cycle matches with the configured
     * frequency.
     */
    targetMilli;


    /** Continuous variable "x5". */
    x5_;

    /** Discrete variable "a1.x". */
    a1_x_;

    /** Discrete variable "AA.vb". */
    AA_vb_;

    /** Discrete variable "AA.vi". */
    AA_vi_;

    /** Discrete variable "AA.vp". */
    AA_vp_;

    /** Discrete variable "AA.vn". */
    AA_vn_;

    /** Discrete variable "AA.vz". */
    AA_vz_;

    /** Discrete variable "AA.vr". */
    AA_vr_;

    /** Discrete variable "AA.vs". */
    AA_vs_;

    /** Discrete variable "AA.ve". */
    AA_ve_;

    /** Discrete variable "AA.va". */
    AA_va_;

    /** Discrete variable "AA.v2". */
    AA_v2_;

    /** Discrete variable "AA.i2r". */
    AA_i2r_;

    /** Discrete variable "AA.b2s". */
    AA_b2s_;

    /** Discrete variable "AA.i2s". */
    AA_i2s_;

    /** Discrete variable "AA.r2s". */
    AA_r2s_;

    /** Discrete variable "AA.s2b". */
    AA_s2b_;

    /** Discrete variable "AA.s2i". */
    AA_s2i_;

    /** Discrete variable "AA.s2r". */
    AA_s2r_;

    /** Discrete variable "AA.self_cast1". */
    AA_self_cast1_;

    /** Discrete variable "AA.self_cast2". */
    AA_self_cast2_;

    /** Discrete variable "AA.inv1". */
    AA_inv1_;

    /** Discrete variable "AA.inv2". */
    AA_inv2_;

    /** Discrete variable "AA.neg1". */
    AA_neg1_;

    /** Discrete variable "AA.neg2". */
    AA_neg2_;

    /** Discrete variable "AA.neg3". */
    AA_neg3_;

    /** Discrete variable "AA.neg4". */
    AA_neg4_;

    /** Discrete variable "AA.pos1". */
    AA_pos1_;

    /** Discrete variable "AA.pos2". */
    AA_pos2_;

    /** Discrete variable "AA.posneg". */
    AA_posneg_;

    /** Discrete variable "AA.l3i". */
    AA_l3i_;

    /** Discrete variable "AA.idx1". */
    AA_idx1_;

    /** Discrete variable "AA.vt". */
    AA_vt_;

    /** Discrete variable "AA.vf". */
    AA_vf_;

    /** Discrete variable "AA.short_and". */
    AA_short_and_;

    /** Discrete variable "AA.short_or". */
    AA_short_or_;

    /** Discrete variable "AA.impl". */
    AA_impl_;

    /** Discrete variable "AA.biimpl". */
    AA_biimpl_;

    /** Discrete variable "AA.conj". */
    AA_conj_;

    /** Discrete variable "AA.disj". */
    AA_disj_;

    /** Discrete variable "AA.lt1". */
    AA_lt1_;

    /** Discrete variable "AA.le1". */
    AA_le1_;

    /** Discrete variable "AA.gt1". */
    AA_gt1_;

    /** Discrete variable "AA.ge1". */
    AA_ge1_;

    /** Discrete variable "AA.lt2". */
    AA_lt2_;

    /** Discrete variable "AA.le2". */
    AA_le2_;

    /** Discrete variable "AA.gt2". */
    AA_gt2_;

    /** Discrete variable "AA.ge2". */
    AA_ge2_;

    /** Discrete variable "AA.lt3". */
    AA_lt3_;

    /** Discrete variable "AA.le3". */
    AA_le3_;

    /** Discrete variable "AA.gt3". */
    AA_gt3_;

    /** Discrete variable "AA.ge3". */
    AA_ge3_;

    /** Discrete variable "AA.lt4". */
    AA_lt4_;

    /** Discrete variable "AA.le4". */
    AA_le4_;

    /** Discrete variable "AA.gt4". */
    AA_gt4_;

    /** Discrete variable "AA.ge4". */
    AA_ge4_;

    /** Discrete variable "AA.eq1". */
    AA_eq1_;

    /** Discrete variable "AA.eq2". */
    AA_eq2_;

    /** Discrete variable "AA.eq3". */
    AA_eq3_;

    /** Discrete variable "AA.eq4". */
    AA_eq4_;

    /** Discrete variable "AA.eq5". */
    AA_eq5_;

    /** Discrete variable "AA.ne1". */
    AA_ne1_;

    /** Discrete variable "AA.ne2". */
    AA_ne2_;

    /** Discrete variable "AA.ne3". */
    AA_ne3_;

    /** Discrete variable "AA.ne4". */
    AA_ne4_;

    /** Discrete variable "AA.ne5". */
    AA_ne5_;

    /** Discrete variable "AA.add1". */
    AA_add1_;

    /** Discrete variable "AA.add2". */
    AA_add2_;

    /** Discrete variable "AA.add3". */
    AA_add3_;

    /** Discrete variable "AA.add4". */
    AA_add4_;

    /** Discrete variable "AA.add5". */
    AA_add5_;

    /** Discrete variable "AA.add6". */
    AA_add6_;

    /** Discrete variable "AA.add7". */
    AA_add7_;

    /** Discrete variable "AA.add8". */
    AA_add8_;

    /** Discrete variable "AA.sub1". */
    AA_sub1_;

    /** Discrete variable "AA.sub2". */
    AA_sub2_;

    /** Discrete variable "AA.sub3". */
    AA_sub3_;

    /** Discrete variable "AA.sub4". */
    AA_sub4_;

    /** Discrete variable "AA.sub5". */
    AA_sub5_;

    /** Discrete variable "AA.sub6". */
    AA_sub6_;

    /** Discrete variable "AA.sub7". */
    AA_sub7_;

    /** Discrete variable "AA.mul1". */
    AA_mul1_;

    /** Discrete variable "AA.mul2". */
    AA_mul2_;

    /** Discrete variable "AA.mul3". */
    AA_mul3_;

    /** Discrete variable "AA.mul4". */
    AA_mul4_;

    /** Discrete variable "AA.mul5". */
    AA_mul5_;

    /** Discrete variable "AA.mul6". */
    AA_mul6_;

    /** Discrete variable "AA.mul7". */
    AA_mul7_;

    /** Discrete variable "AA.rdiv1". */
    AA_rdiv1_;

    /** Discrete variable "AA.rdiv2". */
    AA_rdiv2_;

    /** Discrete variable "AA.rdiv3". */
    AA_rdiv3_;

    /** Discrete variable "AA.rdiv4". */
    AA_rdiv4_;

    /** Discrete variable "AA.rdiv5". */
    AA_rdiv5_;

    /** Discrete variable "AA.rdiv6". */
    AA_rdiv6_;

    /** Discrete variable "AA.div1". */
    AA_div1_;

    /** Discrete variable "AA.div2". */
    AA_div2_;

    /** Discrete variable "AA.div3". */
    AA_div3_;

    /** Discrete variable "AA.div4". */
    AA_div4_;

    /** Discrete variable "AA.mod1". */
    AA_mod1_;

    /** Discrete variable "AA.mod2". */
    AA_mod2_;

    /** Discrete variable "AA.li". */
    AA_li_;

    /** Discrete variable "AA.tii". */
    AA_tii_;

    /** Discrete variable "AA.ss". */
    AA_ss_;

    /** Discrete variable "AA.proj1". */
    AA_proj1_;

    /** Discrete variable "AA.proj2". */
    AA_proj2_;

    /** Discrete variable "AA.proj3". */
    AA_proj3_;

    /** Discrete variable "AA.proj4". */
    AA_proj4_;

    /** Discrete variable "AA.proj5". */
    AA_proj5_;

    /** Discrete variable "AA.proj6". */
    AA_proj6_;

    /** Discrete variable "AA.f_acos". */
    AA_f_acos_;

    /** Discrete variable "AA.f_asin". */
    AA_f_asin_;

    /** Discrete variable "AA.f_atan". */
    AA_f_atan_;

    /** Discrete variable "AA.f_cos". */
    AA_f_cos_;

    /** Discrete variable "AA.f_sin". */
    AA_f_sin_;

    /** Discrete variable "AA.f_tan". */
    AA_f_tan_;

    /** Discrete variable "AA.f_abs1". */
    AA_f_abs1_;

    /** Discrete variable "AA.f_abs12". */
    AA_f_abs12_;

    /** Discrete variable "AA.f_abs2". */
    AA_f_abs2_;

    /** Discrete variable "AA.f_cbrt". */
    AA_f_cbrt_;

    /** Discrete variable "AA.f_ceil". */
    AA_f_ceil_;

    /** Discrete variable "AA.f_empty". */
    AA_f_empty_;

    /** Discrete variable "AA.f_exp". */
    AA_f_exp_;

    /** Discrete variable "AA.f_floor". */
    AA_f_floor_;

    /** Discrete variable "AA.f_ln". */
    AA_f_ln_;

    /** Discrete variable "AA.f_log". */
    AA_f_log_;

    /** Discrete variable "AA.f_max1". */
    AA_f_max1_;

    /** Discrete variable "AA.f_max2". */
    AA_f_max2_;

    /** Discrete variable "AA.f_max3". */
    AA_f_max3_;

    /** Discrete variable "AA.f_max4". */
    AA_f_max4_;

    /** Discrete variable "AA.f_min1". */
    AA_f_min1_;

    /** Discrete variable "AA.f_min2". */
    AA_f_min2_;

    /** Discrete variable "AA.f_min3". */
    AA_f_min3_;

    /** Discrete variable "AA.f_min4". */
    AA_f_min4_;

    /** Discrete variable "AA.f_pow1". */
    AA_f_pow1_;

    /** Discrete variable "AA.f_pow12". */
    AA_f_pow12_;

    /** Discrete variable "AA.f_pow2". */
    AA_f_pow2_;

    /** Discrete variable "AA.f_pow3". */
    AA_f_pow3_;

    /** Discrete variable "AA.f_pow4". */
    AA_f_pow4_;

    /** Discrete variable "AA.f_round". */
    AA_f_round_;

    /** Discrete variable "AA.f_scale". */
    AA_f_scale_;

    /** Discrete variable "AA.f_sign1". */
    AA_f_sign1_;

    /** Discrete variable "AA.f_sign2". */
    AA_f_sign2_;

    /** Discrete variable "AA.f_size1". */
    AA_f_size1_;

    /** Discrete variable "AA.f_size2". */
    AA_f_size2_;

    /** Discrete variable "AA.f_sqrt". */
    AA_f_sqrt_;

    /** Input variable "x8". */
    x8_;

    /** SVG output elements. */


    /**
     * SVG input queue with functions for handling clicked SVG
     * input elements, from first clicked at the head of the array
     * to last clicked at the tail of the array.
     */
    svgInQueue;

    /**
     * The SVG input id corresponding to the SVG input element that
     * was clicked. Is 'null' if no SVG input element was clicked so
     * far, or all clicks have already been processed.
     */
    svgInId;

    /**
     * The 0-based index of the event corresponding to the SVG input
     * element that was clicked. Is '-1' if no SVG input element was
     * clicked so far, or all clicks have already been processed.
     */
    svgInEvent;

    /** SVG input click event handlers. */


    /** SVG input event setters. */


    /** Starts the simulation. */
    start() {
        if (!this.playing) {
            this.playing = true;
            this.exec();
        }
    }

    /** Stops the simulation. */
    stop() {
        if (this.playing) {
            this.playing = false;
        }
    }

    /** Resets the object to its initial state. */
    reset() {
        this.stop();
        this.firstExec = true;
        this.time = 0.0;
        this.first = true;
        this.timePaused = null;
        this.initState();
        this.initUI();
        this.updateUI();
    }

    /**
     * Execute the code once. Inputs are read, transitions are executed until
     * none are possible, outputs are written, etc.
     *
     * @param newTime The time in seconds, since the start of the first
     *      execution.
     * @throws {exprsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execOnce(newTime) {
        // Pre execution notification.
        this.preExec();

        // Update values of input variables.
        this.updateInputs();

        // Initialize the state.
        if (this.firstExec) {
            this.initState();
        }

        // Calculate time delta.
        var delta = newTime - this.time;

        // Update values of continuous variables.
        if (!this.firstExec) {
                        try {
                            var deriv0 = exprs.x5_deriv();

                            exprs.x5_ = exprs.x5_ + delta * deriv0;
                            exprsUtils.checkReal(exprs.x5_, "x5");
                            if (exprs.x5_ == -0.0) exprs.x5_ = 0.0;
                        } catch (e) {
                            if (e instanceof exprsException) {
                                e = new exprsException("Failed to update continuous variables after time passage.", e);
                                this.runtimeError(e);
                            }
                            throw e;
                        }
        }

        // Update time.
        this.time = newTime;

        // Apply print declarations.
        if (this.firstExec) {
            // For 'initial' transition.
            if (this.doInfoPrintOutput) this.printOutput(-3, true);
            if (this.doInfoPrintOutput) this.printOutput(-3, false);
            if (this.doStateOutput) exprs.log('Initial state: ' + exprs.getStateText());
            if (this.doStateOutput || this.doTransitionOutput) this.log('');

        } else {
            // For 'post' of time transition.
            if (this.doInfoPrintOutput) this.printOutput(-2, false);
        }

        // Execute environment events and SVG input events as long as they are possible, emptying the SVG input queue.
        while (true) {
            // Handle next element from SVG input queue, if not already already processing one.
            if (this.svgInEvent == -1 && this.svgInQueue.length > 0) {
                var func = this.svgInQueue.shift(); // Remove head of the queue.
                func(); // Call function, to set the event to allow.
            }

            // Try to execute an SVG input mapping with updates for each environment event, and an edge for each SVG
            // input event.
            var anythingExecuted = false;



            // Stop if no SVG input mapping and no edge was executed, and no more SVG input clicks are to be processed.
            if (!anythingExecuted && this.svgInQueue.length == 0) {
                break;
            }
        }

        // Make sure all outstanding SVG input clicks have been processed.
        console.assert(this.svgInEvent == -1 && this.svgInQueue.length == 0);

        // Execute uncontrollable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Execute controllable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;

            // Event "a1.e".
            anythingExecuted |= this.execEdge0();

            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Apply print declarations for 'pre' of time transition.
        if (this.doInfoPrintOutput) this.printOutput(-2, true);

        // Post execution notification.
        this.postExec();

        // Done.
        this.firstExec = false;
    }

    /**
     * Calls {@link #execWhile}, which repeatedly {@link #execOnce executes the code}.
     *
     * @throws {exprsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    exec() {
        this.execWhile(1);
    }

    /**
     * Repeatedly {@link #execOnce executes the code}.
     *
     * @param delay The delay before executing, in milliseconds.
     *
     * @throws {exprsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execWhile(delay) {
        setTimeout(
            function () {
                // Pre execution timing.
                var now = Date.now();
                var preMilli = now;

                // On first execution, initialize variables for timing.
                if (exprs.first) {
                    exprs.first = false;
                    exprs.startMilli = now;
                    exprs.targetMilli = exprs.startMilli;
                    preMilli = exprs.startMilli;
                }

                // Handle pausing/playing.
                if (!exprs.playing) {
                    exprs.timePaused = now;
                    return;
                }

                if (exprs.timePaused) {
                    exprs.startMilli += (now - exprs.timePaused);
                    exprs.targetMilli += (now - exprs.timePaused);
                    exprs.timePaused = null;
                }

                // Get cycle time and current 'time'.
                var frequency = exprs.frequency;
                var cycleMilli = (frequency <= 0) ? -1 : 1e3 / frequency;
                var timeMilli = preMilli - exprs.startMilli;

                // Execute once.
                exprs.execOnce(timeMilli / 1e3);

                // Post execution timing.
                var postMilli = Date.now();
                var duration = postMilli - preMilli;
                if (exprs.doInfoExec) {
                    exprs.infoExec(duration, cycleMilli);
                }

                // Ensure frequency.
                var remainderMilli = 0;
                if (frequency > 0) {
                    exprs.targetMilli += cycleMilli;
                    remainderMilli = exprs.targetMilli - postMilli;
                }

                // Execute again.
                exprs.execWhile(remainderMilli > 0 ? remainderMilli : 0);
            },
        delay);
    }

    /**
     * Execute code for edge with index 0 and event "a1.e".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {exprsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge0() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(0, true);
            if (this.doInfoEvent) this.infoEvent(0, true);

            try {
                if ((!exprsUtils.equalObjs(exprs.a1_x_, 1)) && (!exprsUtils.equalObjs(exprs.a1_x_, 2))) {
                    exprs.a1_x_ = 3;
                } else if ((!exprsUtils.equalObjs(exprs.a1_x_, 2)) && (!exprsUtils.equalObjs(exprs.a1_x_, 3))) {
                    exprs.a1_x_ = 4;
                }
            } catch (e) {
                if (e instanceof exprsException) {
                    e = new exprsException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(0, false);
            if (this.doInfoPrintOutput) this.printOutput(0, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to (try to) execute event \"a1.e\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Initializes the state.
     *
     * @throws {exprsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    initState() {
        // Initialize constants, if not yet done so.
        if (!this.constantsInitialized) {
            this.constantsInitialized = true;
            exprs.x1_ = 5;
        }

        // Initialize SVG input.
        this.svgInQueue = [];
        this.svgInId = null;
        this.svgInEvent = -1;


        // CIF model state variables.
        try {
            exprs.x5_ = 0.0;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"x5\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.a1_x_ = 0;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"a1.x\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_vb_ = true;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.vb\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_vi_ = 5;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.vi\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_vp_ = 2;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.vp\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_vn_ = -(1);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.vn\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_vz_ = 1;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.vz\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_vr_ = 1.23;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.vr\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_vs_ = "a";
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.vs\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_ve_ = exprs.exprsEnum._A;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.ve\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_va_ = [1, 2];
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.va\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_v2_ = 5.0;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.v2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_i2r_ = exprsUtils.intToReal(exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.i2r\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_b2s_ = exprsUtils.boolToStr(exprs.AA_vb_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.b2s\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_i2s_ = exprsUtils.intToStr(exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.i2s\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_r2s_ = exprsUtils.realToStr(exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.r2s\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_s2b_ = exprsUtils.strToBool(exprs.AA_b2s_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.s2b\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_s2i_ = exprsUtils.strToInt(exprs.AA_i2s_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.s2i\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_s2r_ = exprsUtils.strToReal(exprs.AA_r2s_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.s2r\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_self_cast1_ = [1, 2, 3];
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.self_cast1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_self_cast2_ = exprs.AA_self_cast1_;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.self_cast2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_inv1_ = !(exprs.AA_vb_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.inv1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_inv2_ = exprs.AA_vb_;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.inv2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_neg1_ = exprsUtils.negateInt(exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.neg1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_neg2_ = exprsUtils.negateInt(exprsUtils.negateInt(exprs.AA_vi_));
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.neg2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_neg3_ = -(exprs.AA_vp_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.neg3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_neg4_ = -(-(exprs.AA_vp_));
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.neg4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_pos1_ = exprs.AA_vi_;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.pos1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_pos2_ = exprs.AA_vi_;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.pos2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_posneg_ = exprsUtils.negateInt(exprsUtils.negateInt(exprsUtils.negateInt(exprsUtils.negateInt(exprs.AA_vi_))));
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.posneg\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_l3i_ = [true];
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.l3i\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_idx1_ = 1;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.idx1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_vt_ = true;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.vt\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_vf_ = false;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.vf\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_short_and_ = (exprs.AA_vf_) && (exprsUtils.projectList(exprs.AA_l3i_, exprs.AA_idx1_));
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.short_and\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_short_or_ = (exprs.AA_vt_) || (exprsUtils.projectList(exprs.AA_l3i_, exprs.AA_idx1_));
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.short_or\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_impl_ = !(exprs.AA_vb_) || (exprs.AA_vb_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.impl\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_biimpl_ = exprsUtils.equalObjs(exprs.AA_vb_, exprs.AA_vb_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.biimpl\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_conj_ = (exprs.AA_vb_) && (exprs.AA_vb_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.conj\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_disj_ = (exprs.AA_vb_) || (exprs.AA_vb_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.disj\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_lt1_ = (exprs.AA_vi_) < (exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.lt1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_le1_ = (exprs.AA_vi_) <= (exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.le1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_gt1_ = (exprs.AA_vi_) > (exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.gt1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_ge1_ = (exprs.AA_vi_) >= (exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.ge1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_lt2_ = (exprs.AA_vi_) < (exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.lt2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_le2_ = (exprs.AA_vi_) <= (exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.le2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_gt2_ = (exprs.AA_vi_) > (exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.gt2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_ge2_ = (exprs.AA_vi_) >= (exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.ge2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_lt3_ = (exprs.AA_vr_) < (exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.lt3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_le3_ = (exprs.AA_vr_) <= (exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.le3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_gt3_ = (exprs.AA_vr_) > (exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.gt3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_ge3_ = (exprs.AA_vr_) >= (exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.ge3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_lt4_ = (exprs.AA_vr_) < (exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.lt4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_le4_ = (exprs.AA_vr_) <= (exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.le4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_gt4_ = (exprs.AA_vr_) > (exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.gt4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_ge4_ = (exprs.AA_vr_) >= (exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.ge4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_eq1_ = exprsUtils.equalObjs(exprs.AA_vb_, exprs.AA_vb_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.eq1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_eq2_ = exprsUtils.equalObjs(exprs.AA_vi_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.eq2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_eq3_ = exprsUtils.equalObjs(exprs.AA_vr_, exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.eq3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_eq4_ = exprsUtils.equalObjs(exprs.AA_vs_, exprs.AA_vs_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.eq4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_eq5_ = (exprs.AA_ve_) == (exprs.AA_ve_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.eq5\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_ne1_ = !exprsUtils.equalObjs(exprs.AA_vb_, exprs.AA_vb_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.ne1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_ne2_ = !exprsUtils.equalObjs(exprs.AA_vi_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.ne2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_ne3_ = !exprsUtils.equalObjs(exprs.AA_vr_, exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.ne3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_ne4_ = !exprsUtils.equalObjs(exprs.AA_vs_, exprs.AA_vs_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.ne4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_ne5_ = (exprs.AA_ve_) != (exprs.AA_ve_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.ne5\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_add1_ = exprsUtils.addInt(exprs.AA_vi_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.add1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_add2_ = exprsUtils.addReal(exprs.AA_vi_, exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.add2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_add3_ = exprsUtils.addReal(exprs.AA_vr_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.add3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_add4_ = exprsUtils.addReal(exprs.AA_vr_, exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.add4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_add5_ = exprsUtils.addString(exprs.AA_vs_, exprs.AA_vs_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.add5\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_add6_ = (exprs.AA_vp_) + (exprs.AA_vp_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.add6\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_add7_ = exprsUtils.addInt(exprs.AA_vi_, exprs.AA_vp_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.add7\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_add8_ = exprsUtils.addInt(exprs.AA_vp_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.add8\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_sub1_ = exprsUtils.subtractInt(exprs.AA_vi_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.sub1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_sub2_ = exprsUtils.subtractReal(exprs.AA_vi_, exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.sub2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_sub3_ = exprsUtils.subtractReal(exprs.AA_vr_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.sub3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_sub4_ = exprsUtils.subtractReal(exprs.AA_vr_, exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.sub4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_sub5_ = (exprs.AA_vp_) - (exprs.AA_vp_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.sub5\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_sub6_ = exprsUtils.subtractInt(exprs.AA_vi_, exprs.AA_vp_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.sub6\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_sub7_ = exprsUtils.subtractInt(exprs.AA_vp_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.sub7\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_mul1_ = exprsUtils.multiplyInt(exprs.AA_vi_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.mul1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_mul2_ = exprsUtils.multiplyReal(exprs.AA_vi_, exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.mul2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_mul3_ = exprsUtils.multiplyReal(exprs.AA_vr_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.mul3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_mul4_ = exprsUtils.multiplyReal(exprs.AA_vr_, exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.mul4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_mul5_ = (exprs.AA_vp_) * (exprs.AA_vp_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.mul5\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_mul6_ = exprsUtils.multiplyInt(exprs.AA_vi_, exprs.AA_vp_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.mul6\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_mul7_ = exprsUtils.multiplyInt(exprs.AA_vp_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.mul7\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_rdiv1_ = exprsUtils.divide(exprs.AA_vi_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.rdiv1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_rdiv2_ = exprsUtils.divide(exprs.AA_vi_, exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.rdiv2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_rdiv3_ = exprsUtils.divide(exprs.AA_vr_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.rdiv3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_rdiv4_ = exprsUtils.divide(exprs.AA_vr_, exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.rdiv4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_rdiv5_ = ((double)(exprs.AA_vi_)) / (exprs.AA_vp_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.rdiv5\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_rdiv6_ = ((double)(exprs.AA_vi_)) / (exprs.AA_vn_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.rdiv6\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_div1_ = exprsUtils.div(exprs.AA_vi_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.div1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_div2_ = (exprs.AA_vi_) / (exprs.AA_vp_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.div2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_div3_ = exprsUtils.div(exprs.AA_vi_, exprs.AA_vn_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.div3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_div4_ = exprsUtils.div(exprs.AA_vi_, exprs.AA_vz_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.div4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_mod1_ = exprsUtils.mod(exprs.AA_vi_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.mod1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_mod2_ = (exprs.AA_vi_) % (exprs.AA_vp_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.mod2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_li_ = [1, 2];
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.li\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_tii_ = new CifTuple_T2II(1, 2);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.tii\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_ss_ = "abc";
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.ss\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_proj1_ = exprsUtils.projectList(exprs.AA_li_, 0);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.proj1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_proj2_ = exprsUtils.projectList(exprs.AA_li_, -(1));
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.proj2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_proj3_ = (exprs.AA_tii_)._field0;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.proj3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_proj4_ = (exprs.AA_tii_)._field1;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.proj4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_proj5_ = exprsUtils.projectString(exprs.AA_ss_, 0);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.proj5\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_proj6_ = exprsUtils.projectString(exprs.AA_ss_, -(1));
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.proj6\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_acos_ = exprsUtils.acos(exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_acos\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_asin_ = exprsUtils.asin(exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_asin\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_atan_ = exprsUtils.atan(exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_atan\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_cos_ = exprsUtils.cos(exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_cos\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_sin_ = exprsUtils.sin(exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_sin\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_tan_ = exprsUtils.tan(exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_tan\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_abs1_ = exprsUtils.absInt(exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_abs1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_abs12_ = exprsUtils.absInt(exprs.AA_vp_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_abs12\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_abs2_ = exprsUtils.absReal(exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_abs2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_cbrt_ = exprsUtils.cbrt(exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_cbrt\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_ceil_ = exprsUtils.ceil(exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_ceil\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_empty_ = exprsUtils.empty(exprs.AA_va_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_empty\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_exp_ = exprsUtils.exp(exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_exp\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_floor_ = exprsUtils.floor(exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_floor\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_ln_ = exprsUtils.ln(exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_ln\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_log_ = exprsUtils.log(exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_log\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_max1_ = exprsUtils.maxInt(exprs.AA_vi_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_max1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_max2_ = exprsUtils.maxReal(exprs.AA_vi_, exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_max2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_max3_ = exprsUtils.maxReal(exprs.AA_vr_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_max3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_max4_ = exprsUtils.maxReal(exprs.AA_vr_, exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_max4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_min1_ = exprsUtils.minInt(exprs.AA_vi_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_min1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_min2_ = exprsUtils.minReal(exprs.AA_vi_, exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_min2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_min3_ = exprsUtils.minReal(exprs.AA_vr_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_min3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_min4_ = exprsUtils.minReal(exprs.AA_vr_, exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_min4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_pow1_ = exprsUtils.powReal(exprs.AA_vi_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_pow1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_pow12_ = exprsUtils.powInt(exprs.AA_vp_, exprs.AA_vp_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_pow12\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_pow2_ = exprsUtils.powReal(exprs.AA_vi_, exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_pow2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_pow3_ = exprsUtils.powReal(exprs.AA_vr_, exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_pow3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_pow4_ = exprsUtils.powReal(exprs.AA_vr_, exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_pow4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_round_ = exprsUtils.round(exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_round\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_scale_ = exprsUtils.scale(exprs.AA_vr_, 0, 10, 1, 11);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_scale\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_sign1_ = exprsUtils.signInt(exprs.AA_vi_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_sign1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_sign2_ = exprsUtils.signInt(exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_sign2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_size1_ = exprsUtils.sizeList(exprs.AA_va_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_size1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_size2_ = exprsUtils.sizeString(exprs.AA_vs_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_size2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            exprs.AA_f_sqrt_ = exprsUtils.sqrt(exprs.AA_vr_);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the initial value of variable \"AA.f_sqrt\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Initializes the user interface, either when loading the page
     * or when resetting the simulation.
     */
    initUI() {


        // Apply SVG copies and moves, if not done so before.
        if (!this.svgCopiesAndMovesApplied) {
            this.svgCopiesAndMovesApplied = true;

            // Apply SVG copy declarations.


            // Apply SVG move declarations.

        }

        // Prepare SVG output.


        // Prepare SVG input.

    }

    /**
     * Updates the user interface based on the latest state of
     * the model. Is called at the end of each cycle.
     *
     * @throws {exprsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    updateUI() {
        // Apply CIF/SVG output mappings.

    }

    /**
     * Updates the values of the input variables. Other variables from the
     * state may not be accessed or modified.
     */
    updateInputs() {
        // Nothing is done here by default.
    }

    /** Logs a normal message. */
    log(message) {
        console.log(message);

    }

    /** Logs an warning message. */
    warning(message) {
        console.log(message);

    }

    /** Logs an error message. */
    error(message) {
        console.log(message);

    }

    /** Logs a runtime error of type exprsException. */
    runtimeError(e, isCause = false) {
        console.assert(e instanceof exprsException);
        if (isCause) {
            this.error("CAUSE: " + e.message);
        } else {
            this.error("ERROR: " + e.message);
        }
        if (e.cause) {
            this.runtimeError(e.cause, true);
        }
    }

    /**
     * Informs about the duration of a single execution.
     *
     * @param duration The duration of the execution, in milliseconds.
     * @param cycleTime The desired maximum duration of the execution, in
     *      milliseconds, or '-1' if not available.
     */
    infoExec(duration, cycleTime) {
        // Nothing is done here by default.
    }

    /**
     * Informs that an event will be or has been executed.
     *
     * @param idx The 0-based index of the event.
     * @param pre Whether the event will be executed ('true') or has
     *      been executed ('false').
     */
    infoEvent(idx, pre) {
        if (pre) {
            if (this.doTransitionOutput) exprs.log(exprsUtils.fmt('Transition: event %s', exprs.getEventName(idx)));
        } else {
            if (this.doStateOutput) exprs.log('State: ' + exprs.getStateText());
        }
    }

    /**
     * Informs that the code is about to be executed. For the
     * {@link #firstExec} the state has not yet been initialized, except for
     * {@link #time}.
     */
    preExec() {
        // Nothing is done here by default.
    }

    /** Informs that the code was just executed. */
    postExec() {
        this.updateUI();
    }

    /**
     * Returns the name of an event.
     *
     * @param idx The 0-based index of the event.
     * @return The name of the event.
     */
    getEventName(idx) {
        return this.EVENT_NAMES[idx];
    }

    /**
     * Returns a single-line textual representation of the model state.
     *
     * @return The single-line textual representation of the model state.
     */
    getStateText() {
        var state = exprsUtils.fmt('time=%s', exprs.time);
        state += exprsUtils.fmt(', a1.x=%s', exprsUtils.valueToStr(exprs.a1_x_));
        state += exprsUtils.fmt(', AA.add1=%s', exprsUtils.valueToStr(exprs.AA_add1_));
        state += exprsUtils.fmt(', AA.add2=%s', exprsUtils.valueToStr(exprs.AA_add2_));
        state += exprsUtils.fmt(', AA.add3=%s', exprsUtils.valueToStr(exprs.AA_add3_));
        state += exprsUtils.fmt(', AA.add4=%s', exprsUtils.valueToStr(exprs.AA_add4_));
        state += exprsUtils.fmt(', AA.add5=%s', exprsUtils.valueToStr(exprs.AA_add5_));
        state += exprsUtils.fmt(', AA.add6=%s', exprsUtils.valueToStr(exprs.AA_add6_));
        state += exprsUtils.fmt(', AA.add7=%s', exprsUtils.valueToStr(exprs.AA_add7_));
        state += exprsUtils.fmt(', AA.add8=%s', exprsUtils.valueToStr(exprs.AA_add8_));
        state += exprsUtils.fmt(', AA.b2s=%s', exprsUtils.valueToStr(exprs.AA_b2s_));
        state += exprsUtils.fmt(', AA.biimpl=%s', exprsUtils.valueToStr(exprs.AA_biimpl_));
        state += exprsUtils.fmt(', AA.conj=%s', exprsUtils.valueToStr(exprs.AA_conj_));
        state += exprsUtils.fmt(', AA.disj=%s', exprsUtils.valueToStr(exprs.AA_disj_));
        state += exprsUtils.fmt(', AA.div1=%s', exprsUtils.valueToStr(exprs.AA_div1_));
        state += exprsUtils.fmt(', AA.div2=%s', exprsUtils.valueToStr(exprs.AA_div2_));
        state += exprsUtils.fmt(', AA.div3=%s', exprsUtils.valueToStr(exprs.AA_div3_));
        state += exprsUtils.fmt(', AA.div4=%s', exprsUtils.valueToStr(exprs.AA_div4_));
        state += exprsUtils.fmt(', AA.eq1=%s', exprsUtils.valueToStr(exprs.AA_eq1_));
        state += exprsUtils.fmt(', AA.eq2=%s', exprsUtils.valueToStr(exprs.AA_eq2_));
        state += exprsUtils.fmt(', AA.eq3=%s', exprsUtils.valueToStr(exprs.AA_eq3_));
        state += exprsUtils.fmt(', AA.eq4=%s', exprsUtils.valueToStr(exprs.AA_eq4_));
        state += exprsUtils.fmt(', AA.eq5=%s', exprsUtils.valueToStr(exprs.AA_eq5_));
        state += exprsUtils.fmt(', AA.f_abs1=%s', exprsUtils.valueToStr(exprs.AA_f_abs1_));
        state += exprsUtils.fmt(', AA.f_abs12=%s', exprsUtils.valueToStr(exprs.AA_f_abs12_));
        state += exprsUtils.fmt(', AA.f_abs2=%s', exprsUtils.valueToStr(exprs.AA_f_abs2_));
        state += exprsUtils.fmt(', AA.f_acos=%s', exprsUtils.valueToStr(exprs.AA_f_acos_));
        state += exprsUtils.fmt(', AA.f_asin=%s', exprsUtils.valueToStr(exprs.AA_f_asin_));
        state += exprsUtils.fmt(', AA.f_atan=%s', exprsUtils.valueToStr(exprs.AA_f_atan_));
        state += exprsUtils.fmt(', AA.f_cbrt=%s', exprsUtils.valueToStr(exprs.AA_f_cbrt_));
        state += exprsUtils.fmt(', AA.f_ceil=%s', exprsUtils.valueToStr(exprs.AA_f_ceil_));
        state += exprsUtils.fmt(', AA.f_cos=%s', exprsUtils.valueToStr(exprs.AA_f_cos_));
        state += exprsUtils.fmt(', AA.f_empty=%s', exprsUtils.valueToStr(exprs.AA_f_empty_));
        state += exprsUtils.fmt(', AA.f_exp=%s', exprsUtils.valueToStr(exprs.AA_f_exp_));
        state += exprsUtils.fmt(', AA.f_floor=%s', exprsUtils.valueToStr(exprs.AA_f_floor_));
        state += exprsUtils.fmt(', AA.f_ln=%s', exprsUtils.valueToStr(exprs.AA_f_ln_));
        state += exprsUtils.fmt(', AA.f_log=%s', exprsUtils.valueToStr(exprs.AA_f_log_));
        state += exprsUtils.fmt(', AA.f_max1=%s', exprsUtils.valueToStr(exprs.AA_f_max1_));
        state += exprsUtils.fmt(', AA.f_max2=%s', exprsUtils.valueToStr(exprs.AA_f_max2_));
        state += exprsUtils.fmt(', AA.f_max3=%s', exprsUtils.valueToStr(exprs.AA_f_max3_));
        state += exprsUtils.fmt(', AA.f_max4=%s', exprsUtils.valueToStr(exprs.AA_f_max4_));
        state += exprsUtils.fmt(', AA.f_min1=%s', exprsUtils.valueToStr(exprs.AA_f_min1_));
        state += exprsUtils.fmt(', AA.f_min2=%s', exprsUtils.valueToStr(exprs.AA_f_min2_));
        state += exprsUtils.fmt(', AA.f_min3=%s', exprsUtils.valueToStr(exprs.AA_f_min3_));
        state += exprsUtils.fmt(', AA.f_min4=%s', exprsUtils.valueToStr(exprs.AA_f_min4_));
        state += exprsUtils.fmt(', AA.f_pow1=%s', exprsUtils.valueToStr(exprs.AA_f_pow1_));
        state += exprsUtils.fmt(', AA.f_pow12=%s', exprsUtils.valueToStr(exprs.AA_f_pow12_));
        state += exprsUtils.fmt(', AA.f_pow2=%s', exprsUtils.valueToStr(exprs.AA_f_pow2_));
        state += exprsUtils.fmt(', AA.f_pow3=%s', exprsUtils.valueToStr(exprs.AA_f_pow3_));
        state += exprsUtils.fmt(', AA.f_pow4=%s', exprsUtils.valueToStr(exprs.AA_f_pow4_));
        state += exprsUtils.fmt(', AA.f_round=%s', exprsUtils.valueToStr(exprs.AA_f_round_));
        state += exprsUtils.fmt(', AA.f_scale=%s', exprsUtils.valueToStr(exprs.AA_f_scale_));
        state += exprsUtils.fmt(', AA.f_sign1=%s', exprsUtils.valueToStr(exprs.AA_f_sign1_));
        state += exprsUtils.fmt(', AA.f_sign2=%s', exprsUtils.valueToStr(exprs.AA_f_sign2_));
        state += exprsUtils.fmt(', AA.f_sin=%s', exprsUtils.valueToStr(exprs.AA_f_sin_));
        state += exprsUtils.fmt(', AA.f_size1=%s', exprsUtils.valueToStr(exprs.AA_f_size1_));
        state += exprsUtils.fmt(', AA.f_size2=%s', exprsUtils.valueToStr(exprs.AA_f_size2_));
        state += exprsUtils.fmt(', AA.f_sqrt=%s', exprsUtils.valueToStr(exprs.AA_f_sqrt_));
        state += exprsUtils.fmt(', AA.f_tan=%s', exprsUtils.valueToStr(exprs.AA_f_tan_));
        state += exprsUtils.fmt(', AA.ge1=%s', exprsUtils.valueToStr(exprs.AA_ge1_));
        state += exprsUtils.fmt(', AA.ge2=%s', exprsUtils.valueToStr(exprs.AA_ge2_));
        state += exprsUtils.fmt(', AA.ge3=%s', exprsUtils.valueToStr(exprs.AA_ge3_));
        state += exprsUtils.fmt(', AA.ge4=%s', exprsUtils.valueToStr(exprs.AA_ge4_));
        state += exprsUtils.fmt(', AA.gt1=%s', exprsUtils.valueToStr(exprs.AA_gt1_));
        state += exprsUtils.fmt(', AA.gt2=%s', exprsUtils.valueToStr(exprs.AA_gt2_));
        state += exprsUtils.fmt(', AA.gt3=%s', exprsUtils.valueToStr(exprs.AA_gt3_));
        state += exprsUtils.fmt(', AA.gt4=%s', exprsUtils.valueToStr(exprs.AA_gt4_));
        state += exprsUtils.fmt(', AA.i2r=%s', exprsUtils.valueToStr(exprs.AA_i2r_));
        state += exprsUtils.fmt(', AA.i2s=%s', exprsUtils.valueToStr(exprs.AA_i2s_));
        state += exprsUtils.fmt(', AA.idx1=%s', exprsUtils.valueToStr(exprs.AA_idx1_));
        state += exprsUtils.fmt(', AA.impl=%s', exprsUtils.valueToStr(exprs.AA_impl_));
        state += exprsUtils.fmt(', AA.inv1=%s', exprsUtils.valueToStr(exprs.AA_inv1_));
        state += exprsUtils.fmt(', AA.inv2=%s', exprsUtils.valueToStr(exprs.AA_inv2_));
        state += exprsUtils.fmt(', AA.l3i=%s', exprsUtils.valueToStr(exprs.AA_l3i_));
        state += exprsUtils.fmt(', AA.le1=%s', exprsUtils.valueToStr(exprs.AA_le1_));
        state += exprsUtils.fmt(', AA.le2=%s', exprsUtils.valueToStr(exprs.AA_le2_));
        state += exprsUtils.fmt(', AA.le3=%s', exprsUtils.valueToStr(exprs.AA_le3_));
        state += exprsUtils.fmt(', AA.le4=%s', exprsUtils.valueToStr(exprs.AA_le4_));
        state += exprsUtils.fmt(', AA.li=%s', exprsUtils.valueToStr(exprs.AA_li_));
        state += exprsUtils.fmt(', AA.lt1=%s', exprsUtils.valueToStr(exprs.AA_lt1_));
        state += exprsUtils.fmt(', AA.lt2=%s', exprsUtils.valueToStr(exprs.AA_lt2_));
        state += exprsUtils.fmt(', AA.lt3=%s', exprsUtils.valueToStr(exprs.AA_lt3_));
        state += exprsUtils.fmt(', AA.lt4=%s', exprsUtils.valueToStr(exprs.AA_lt4_));
        state += exprsUtils.fmt(', AA.mod1=%s', exprsUtils.valueToStr(exprs.AA_mod1_));
        state += exprsUtils.fmt(', AA.mod2=%s', exprsUtils.valueToStr(exprs.AA_mod2_));
        state += exprsUtils.fmt(', AA.mul1=%s', exprsUtils.valueToStr(exprs.AA_mul1_));
        state += exprsUtils.fmt(', AA.mul2=%s', exprsUtils.valueToStr(exprs.AA_mul2_));
        state += exprsUtils.fmt(', AA.mul3=%s', exprsUtils.valueToStr(exprs.AA_mul3_));
        state += exprsUtils.fmt(', AA.mul4=%s', exprsUtils.valueToStr(exprs.AA_mul4_));
        state += exprsUtils.fmt(', AA.mul5=%s', exprsUtils.valueToStr(exprs.AA_mul5_));
        state += exprsUtils.fmt(', AA.mul6=%s', exprsUtils.valueToStr(exprs.AA_mul6_));
        state += exprsUtils.fmt(', AA.mul7=%s', exprsUtils.valueToStr(exprs.AA_mul7_));
        state += exprsUtils.fmt(', AA.ne1=%s', exprsUtils.valueToStr(exprs.AA_ne1_));
        state += exprsUtils.fmt(', AA.ne2=%s', exprsUtils.valueToStr(exprs.AA_ne2_));
        state += exprsUtils.fmt(', AA.ne3=%s', exprsUtils.valueToStr(exprs.AA_ne3_));
        state += exprsUtils.fmt(', AA.ne4=%s', exprsUtils.valueToStr(exprs.AA_ne4_));
        state += exprsUtils.fmt(', AA.ne5=%s', exprsUtils.valueToStr(exprs.AA_ne5_));
        state += exprsUtils.fmt(', AA.neg1=%s', exprsUtils.valueToStr(exprs.AA_neg1_));
        state += exprsUtils.fmt(', AA.neg2=%s', exprsUtils.valueToStr(exprs.AA_neg2_));
        state += exprsUtils.fmt(', AA.neg3=%s', exprsUtils.valueToStr(exprs.AA_neg3_));
        state += exprsUtils.fmt(', AA.neg4=%s', exprsUtils.valueToStr(exprs.AA_neg4_));
        state += exprsUtils.fmt(', AA.pos1=%s', exprsUtils.valueToStr(exprs.AA_pos1_));
        state += exprsUtils.fmt(', AA.pos2=%s', exprsUtils.valueToStr(exprs.AA_pos2_));
        state += exprsUtils.fmt(', AA.posneg=%s', exprsUtils.valueToStr(exprs.AA_posneg_));
        state += exprsUtils.fmt(', AA.proj1=%s', exprsUtils.valueToStr(exprs.AA_proj1_));
        state += exprsUtils.fmt(', AA.proj2=%s', exprsUtils.valueToStr(exprs.AA_proj2_));
        state += exprsUtils.fmt(', AA.proj3=%s', exprsUtils.valueToStr(exprs.AA_proj3_));
        state += exprsUtils.fmt(', AA.proj4=%s', exprsUtils.valueToStr(exprs.AA_proj4_));
        state += exprsUtils.fmt(', AA.proj5=%s', exprsUtils.valueToStr(exprs.AA_proj5_));
        state += exprsUtils.fmt(', AA.proj6=%s', exprsUtils.valueToStr(exprs.AA_proj6_));
        state += exprsUtils.fmt(', AA.r2s=%s', exprsUtils.valueToStr(exprs.AA_r2s_));
        state += exprsUtils.fmt(', AA.rdiv1=%s', exprsUtils.valueToStr(exprs.AA_rdiv1_));
        state += exprsUtils.fmt(', AA.rdiv2=%s', exprsUtils.valueToStr(exprs.AA_rdiv2_));
        state += exprsUtils.fmt(', AA.rdiv3=%s', exprsUtils.valueToStr(exprs.AA_rdiv3_));
        state += exprsUtils.fmt(', AA.rdiv4=%s', exprsUtils.valueToStr(exprs.AA_rdiv4_));
        state += exprsUtils.fmt(', AA.rdiv5=%s', exprsUtils.valueToStr(exprs.AA_rdiv5_));
        state += exprsUtils.fmt(', AA.rdiv6=%s', exprsUtils.valueToStr(exprs.AA_rdiv6_));
        state += exprsUtils.fmt(', AA.s2b=%s', exprsUtils.valueToStr(exprs.AA_s2b_));
        state += exprsUtils.fmt(', AA.s2i=%s', exprsUtils.valueToStr(exprs.AA_s2i_));
        state += exprsUtils.fmt(', AA.s2r=%s', exprsUtils.valueToStr(exprs.AA_s2r_));
        state += exprsUtils.fmt(', AA.self_cast1=%s', exprsUtils.valueToStr(exprs.AA_self_cast1_));
        state += exprsUtils.fmt(', AA.self_cast2=%s', exprsUtils.valueToStr(exprs.AA_self_cast2_));
        state += exprsUtils.fmt(', AA.short_and=%s', exprsUtils.valueToStr(exprs.AA_short_and_));
        state += exprsUtils.fmt(', AA.short_or=%s', exprsUtils.valueToStr(exprs.AA_short_or_));
        state += exprsUtils.fmt(', AA.ss=%s', exprsUtils.valueToStr(exprs.AA_ss_));
        state += exprsUtils.fmt(', AA.sub1=%s', exprsUtils.valueToStr(exprs.AA_sub1_));
        state += exprsUtils.fmt(', AA.sub2=%s', exprsUtils.valueToStr(exprs.AA_sub2_));
        state += exprsUtils.fmt(', AA.sub3=%s', exprsUtils.valueToStr(exprs.AA_sub3_));
        state += exprsUtils.fmt(', AA.sub4=%s', exprsUtils.valueToStr(exprs.AA_sub4_));
        state += exprsUtils.fmt(', AA.sub5=%s', exprsUtils.valueToStr(exprs.AA_sub5_));
        state += exprsUtils.fmt(', AA.sub6=%s', exprsUtils.valueToStr(exprs.AA_sub6_));
        state += exprsUtils.fmt(', AA.sub7=%s', exprsUtils.valueToStr(exprs.AA_sub7_));
        state += exprsUtils.fmt(', AA.tii=%s', exprsUtils.valueToStr(exprs.AA_tii_));
        state += exprsUtils.fmt(', AA.v2=%s', exprsUtils.valueToStr(exprs.AA_v2_));
        state += exprsUtils.fmt(', AA.va=%s', exprsUtils.valueToStr(exprs.AA_va_));
        state += exprsUtils.fmt(', AA.vb=%s', exprsUtils.valueToStr(exprs.AA_vb_));
        state += exprsUtils.fmt(', AA.ve=%s', exprsUtils.valueToStr(exprs.AA_ve_));
        state += exprsUtils.fmt(', AA.vf=%s', exprsUtils.valueToStr(exprs.AA_vf_));
        state += exprsUtils.fmt(', AA.vi=%s', exprsUtils.valueToStr(exprs.AA_vi_));
        state += exprsUtils.fmt(', AA.vn=%s', exprsUtils.valueToStr(exprs.AA_vn_));
        state += exprsUtils.fmt(', AA.vp=%s', exprsUtils.valueToStr(exprs.AA_vp_));
        state += exprsUtils.fmt(', AA.vr=%s', exprsUtils.valueToStr(exprs.AA_vr_));
        state += exprsUtils.fmt(', AA.vs=%s', exprsUtils.valueToStr(exprs.AA_vs_));
        state += exprsUtils.fmt(', AA.vt=%s', exprsUtils.valueToStr(exprs.AA_vt_));
        state += exprsUtils.fmt(', AA.vz=%s', exprsUtils.valueToStr(exprs.AA_vz_));
        state += exprsUtils.fmt(', x5=%s', exprsUtils.valueToStr(exprs.x5_));
        state += exprsUtils.fmt(', x5\'=%s', exprsUtils.valueToStr(exprs.x5_deriv()));
        return state;
    }


    /**
     * Evaluates algebraic variable "v1".
     *
     * @return The evaluation result.
     * @throws {exprsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    v1_() {
        try {
            return (((exprs.a1_x_) > (0)) && ((exprs.a1_x_) < (5))) ? 0 : ((((exprs.a1_x_) > (6)) && ((exprs.a1_x_) < (9))) ? 1 : (2));
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate algebraic variable \"v1\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "if1".
     *
     * @return The evaluation result.
     * @throws {exprsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    if1_() {
        try {
            return ((exprs.time) > (1)) ? 1 : (0);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate algebraic variable \"if1\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "if2".
     *
     * @return The evaluation result.
     * @throws {exprsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    if2_() {
        try {
            return (((exprs.time) > (1)) ? 1 : (((exprs.time) > (0.5)) ? 2 : (0))) + (1);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate algebraic variable \"if2\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "if3".
     *
     * @return The evaluation result.
     * @throws {exprsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    if3_() {
        try {
            return (((exprs.time) > (1)) ? 1 : (((exprs.time) > (0.5)) ? 2 : (((exprs.time) > (0.25)) ? 3 : (0)))) + (2);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate algebraic variable \"if3\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "fcall1".
     *
     * @return The evaluation result.
     * @throws {exprsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    fcall1_() {
        try {
            return exprs.inc_(0);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate algebraic variable \"fcall1\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "fcall2".
     *
     * @return The evaluation result.
     * @throws {exprsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    fcall2_() {
        try {
            return exprs.inc_(exprs.inc_(0));
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate algebraic variable \"fcall2\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "vea".
     *
     * @return The evaluation result.
     * @throws {exprsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    vea_() {
        try {
            return exprs.exprsEnum._A;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate algebraic variable \"vea\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "x2".
     *
     * @return The evaluation result.
     * @throws {exprsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    x2_() {
        try {
            return exprs.x1_;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate algebraic variable \"x2\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "x3".
     *
     * @return The evaluation result.
     * @throws {exprsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    x3_() {
        try {
            return exprs.x2_();
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate algebraic variable \"x3\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "x4".
     *
     * @return The evaluation result.
     * @throws {exprsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    x4_() {
        try {
            return exprs.a1_x_;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate algebraic variable \"x4\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "x6".
     *
     * @return The evaluation result.
     * @throws {exprsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    x6_() {
        try {
            return exprsUtils.addReal(exprs.x5_, exprs.x5_deriv());
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate algebraic variable \"x6\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "x7".
     *
     * @return The evaluation result.
     * @throws {exprsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    x7_() {
        try {
            return (exprs.vea_()) == (exprs.exprsEnum._B);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate algebraic variable \"x7\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "x9".
     *
     * @return The evaluation result.
     * @throws {exprsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    x9_() {
        try {
            return exprsUtils.addInt(exprs.x8_, 1);
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate algebraic variable \"x9\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates derivative of continuous variable "x5".
     *
     * @return The evaluation result.
     * @throws {exprsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    x5_deriv() {
        try {
            return 1.0;
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to evaluate the derivative of continuous variable \"x5\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "f1".
     *
     * @param f1_x_ Function parameter "f1.x".
     * @return The return value of the function.
     * @throws {exprsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    f1_(f1_x_) {
        try {
            // Execute statements in the function body.
            while ((!exprsUtils.equalObjs(f1_x_, 0)) && (!exprsUtils.equalObjs(f1_x_, 4))) {
                f1_x_ = exprsUtils.subtractInt(f1_x_, 1);
            }

            if ((!exprsUtils.equalObjs(f1_x_, 1)) && (!exprsUtils.equalObjs(f1_x_, 2))) {
                f1_x_ = 3;
            } else if ((!exprsUtils.equalObjs(f1_x_, 2)) && (!exprsUtils.equalObjs(f1_x_, 3))) {
                f1_x_ = 4;
            }

            return f1_x_;
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to execute internal user-defined function \"f1\".", e);
            }
            throw e;
        }
    }

    /**
     * Function "inc".
     *
     * @param inc_x_ Function parameter "inc.x".
     * @return The return value of the function.
     * @throws {exprsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    inc_(inc_x_) {
        try {
            // Execute statements in the function body.
            return exprsUtils.addInt(inc_x_, 1);
            throw new Error('No return statement at end of function.');
        } catch (e) {
            if (e instanceof exprsException) {
                e = new exprsException("Failed to execute internal user-defined function \"inc\".", e);
            }
            throw e;
        }
    }

    /**
     * Print output for all relevant print declarations.
     *
     * @param idx The 0-based event index of the transition, or '-2' for
     *      time transitions, or '-3' for the 'initial' transition.
     * @param pre Whether to print output for the pre/source state of the
     *      transition ('true') or for the post/target state of the
     *      transition ('false').
     * @throws {exprsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    printOutput(idx, pre) {
        // No print declarations.
    }

    /**
     * Informs that new print output is available.
     *
     * @param text The text being printed.
     * @param target The file or special target to which text is to be printed.
     *      If printed to a file, an absolute or relative local file system
     *      path is given. Paths may contain both '/' and '\\'
     *      as file separators. Use {@link exprsUtils#normalizePrintTarget}
     *      to normalize the path to use '/' file separators. There are two
     *      special targets: ':stdout' to print to the standard output stream,
     *      and ':stderr' to print to the standard error stream.
     */
    infoPrintOutput(text, target) {
        if (target == ':stdout') {
            exprs.log(text);
        } else if (target == ':stderr') {
            exprs.error(text);
        } else {
            var path = exprsUtils.normalizePrintTarget(target);
            exprs.infoPrintOutput(path + ': ' + text, ':stdout');
        }
    }
}
