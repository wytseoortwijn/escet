/* CIF to C translation of svg_input_event.cif
 * Generated file, DO NOT EDIT
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "svg_input_event_engine.h"

#ifndef MAX_NUM_ITERS
#define MAX_NUM_ITERS 1000
#endif

/* What to do if a range error is found in an assignment? */
#ifdef KEEP_RUNNING
static void RangeErrorDetected(void) { /* Do nothing, error is already reported. */ }
#else
static void RangeErrorDetected(void) { exit(1); }
#endif

/* Type support code. */
int EnumTypePrint(svg_input_eventEnum value, char *dest, int start, int end) {
    int last = end - 1;
    const char *lit_name = enum_names[value];
    while (start < last && *lit_name) {
        dest[start++] = *lit_name;
        lit_name++;
    }
    dest[start] = '\0';
    return start;
}


/** Event names. */
const char *svg_input_event_event_names[] = {
    "initial-step", /**< Initial step. */
    "delay-step",   /**< Delay step. */
    "a",            /**< Event "a". */
    "b1",           /**< Event "b1". */
    "b2",           /**< Event "b2". */
    "c",            /**< Event "c". */
    "d",            /**< Event "d". */
    "e",            /**< Event "e". */
    "f",            /**< Event "f". */
    "g.z",          /**< Event "g.z". */
};

/** Enumeration names. */
const char *enum_names[] = {
    /** Literal "l1". */
    "l1",

    /** Literal "l2". */
    "l2",
};

/* Constants. */


/* Functions. */


/* Input variables. */


/* State variables. */

/** Discrete variable "E aut_b". */
svg_input_eventEnum aut_b_;

/** Discrete variable "int aut_d.vd". */
IntType aut_d_vd_;

/** Discrete variable "bool aut_f.b". */
BoolType aut_f_b_;

/** Discrete variable "bool g.a.b". */
BoolType g_a_b_;

/** Discrete variable "real x6.x". */
RealType x6_x_;

/* Derivative and algebraic variable functions. */



RealType model_time; /**< Current model time. */

/** Initialize constants. */
static void InitConstants(void) {

}

/** Print function. */
#if PRINT_OUTPUT
static void PrintOutput(svg_input_event_Event_ event, BoolType pre) {
}
#endif

/* Edge execution code. */

/**
 * Execute code for edge with index 0 and event "a".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge0(void) {
    #if EVENT_OUTPUT
        svg_input_event_InfoEvent(a_, TRUE);
    #endif

    #if EVENT_OUTPUT
        svg_input_event_InfoEvent(a_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 1 and event "b1".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge1(void) {
    BoolType guard = (aut_b_) == (_svg_input_event_l1);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        svg_input_event_InfoEvent(b1_, TRUE);
    #endif

    aut_b_ = _svg_input_event_l2;

    #if EVENT_OUTPUT
        svg_input_event_InfoEvent(b1_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 2 and event "b2".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge2(void) {
    BoolType guard = (aut_b_) == (_svg_input_event_l2);
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        svg_input_event_InfoEvent(b2_, TRUE);
    #endif

    aut_b_ = _svg_input_event_l1;

    #if EVENT_OUTPUT
        svg_input_event_InfoEvent(b2_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 3 and event "c".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge3(void) {
    #if EVENT_OUTPUT
        svg_input_event_InfoEvent(c_, TRUE);
    #endif

    #if EVENT_OUTPUT
        svg_input_event_InfoEvent(c_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 4 and event "d".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge4(void) {
    #if EVENT_OUTPUT
        svg_input_event_InfoEvent(d_, TRUE);
    #endif

    if (TRUE) {
        aut_d_vd_ = IntegerAdd(aut_d_vd_, 1);
    } else if (TRUE) {
        aut_d_vd_ = IntegerMultiply(aut_d_vd_, -(1));
    }

    #if EVENT_OUTPUT
        svg_input_event_InfoEvent(d_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 5 and event "e".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge5(void) {
    BoolType guard = FALSE;
    if (!guard) return FALSE;

    #if EVENT_OUTPUT
        svg_input_event_InfoEvent(e_, TRUE);
    #endif

    #if EVENT_OUTPUT
        svg_input_event_InfoEvent(e_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 6 and event "f".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge6(void) {
    #if EVENT_OUTPUT
        svg_input_event_InfoEvent(f_, TRUE);
    #endif

    aut_f_b_ = !(aut_f_b_);

    #if EVENT_OUTPUT
        svg_input_event_InfoEvent(f_, FALSE);
    #endif
    return TRUE;
}

/**
 * Execute code for edge with index 7 and event "g.z".
 *
 * @return Whether the edge was performed.
 */
static BoolType execEdge7(void) {
    #if EVENT_OUTPUT
        svg_input_event_InfoEvent(g_z_, TRUE);
    #endif

    g_a_b_ = !(g_a_b_);

    #if EVENT_OUTPUT
        svg_input_event_InfoEvent(g_z_, FALSE);
    #endif
    return TRUE;
}

/**
 * Normalize and check the new value of a continuous variable after an update.
 * @param new_value Unnormalized new value of the continuous variable.
 * @param var_name Name of the continuous variable in the CIF model.
 * @return The normalized new value of the continuous variable.
 */
static RealType UpdateContValue(RealType new_value, const char *var_name, BoolType ok) {
    if (ok) {
        return (new_value == -0.0) ? 0.0 : new_value;
    }
    fprintf(stderr, "Continuous variable \"%s\" has become %.1f.\n", var_name, new_value);

#ifdef KEEP_RUNNING
    return 0.0;
#else
    exit(1);
#endif
}

/** Repeatedly perform discrete event steps, until no progress can be made any more. */
static void PerformEdges(void) {
    /* Uncontrollables. */
    int count = 0;
    for (;;) {
        count++;
        if (count > MAX_NUM_ITERS) { /* 'Infinite' loop detection. */
            fprintf(stderr, "Warning: Quitting after performing %d uncontrollable events, infinite loop?\n", count);
            break;
        }

        BoolType edgeExecuted = false;



        if (!edgeExecuted) {
            break; /* No edge fired, done with discrete steps. */
        }
    }

    /* Controllables. */
    count = 0;
    for (;;) {
        count++;
        if (count > MAX_NUM_ITERS) { /* 'Infinite' loop detection. */
            fprintf(stderr, "Warning: Quitting after performing %d controllable events, infinite loop?\n", count);
            break;
        }

        BoolType edgeExecuted = false;

        edgeExecuted |= execEdge0(); /* (Try to) perform edge with index 0 and event "a". */
        edgeExecuted |= execEdge1(); /* (Try to) perform edge with index 1 and event "b1". */
        edgeExecuted |= execEdge2(); /* (Try to) perform edge with index 2 and event "b2". */
        edgeExecuted |= execEdge3(); /* (Try to) perform edge with index 3 and event "c". */
        edgeExecuted |= execEdge4(); /* (Try to) perform edge with index 4 and event "d". */
        edgeExecuted |= execEdge5(); /* (Try to) perform edge with index 5 and event "e". */
        edgeExecuted |= execEdge6(); /* (Try to) perform edge with index 6 and event "f". */
        edgeExecuted |= execEdge7(); /* (Try to) perform edge with index 7 and event "g.z". */

        if (!edgeExecuted) {
            break; /* No edge fired, done with discrete steps. */
        }
    }
}

/** First model call, initializing, and performing discrete events before the first time step. */
void svg_input_event_EngineFirstStep(void) {
    InitConstants();

    model_time = 0.0;

    aut_b_ = _svg_input_event_l1;
    aut_d_vd_ = 0;
    aut_f_b_ = TRUE;
    g_a_b_ = FALSE;
    x6_x_ = 0.0;

    #if PRINT_OUTPUT
        /* pre-initial and post-initial prints. */
        PrintOutput(EVT_INITIAL_, TRUE);
        PrintOutput(EVT_INITIAL_, FALSE);
    #endif

    PerformEdges();

    #if PRINT_OUTPUT
        /* pre-timestep print. */
        PrintOutput(EVT_DELAY_, TRUE);
    #endif
}

/**
 * Engine takes a time step of length \a delta.
 * @param delta Length of the time step.
 */
void svg_input_event_EngineTimeStep(double delta) {


    /* Update continuous variables. */
    if (delta > 0.0) {

        model_time += delta;
    }

    #if PRINT_OUTPUT
        /* post-timestep print. */
        PrintOutput(EVT_DELAY_, FALSE);
    #endif

    PerformEdges();

    #if PRINT_OUTPUT
        /* pre-timestep print. */
        PrintOutput(EVT_DELAY_, TRUE);
    #endif
}

