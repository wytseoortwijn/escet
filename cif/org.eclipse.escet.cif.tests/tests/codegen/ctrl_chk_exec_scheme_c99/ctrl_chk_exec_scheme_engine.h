/* Headers for the CIF to C translation of ctrl_chk_exec_scheme.cif
 * Generated file, DO NOT EDIT
 */

#ifndef CIF_C_CTRL_CHK_EXEC_SCHEME_ENGINE_H
#define CIF_C_CTRL_CHK_EXEC_SCHEME_ENGINE_H

#include "ctrl_chk_exec_scheme_library.h"

/* Types of the specification.
 * Note that integer ranges are ignored in C.
 */
enum Enumctrl_chk_exec_scheme_ {
    /** Literal "__some_dummy_enum_literal". */
    _ctrl_chk_exec_scheme___some_dummy_enum_literal,
};
typedef enum Enumctrl_chk_exec_scheme_ ctrl_chk_exec_schemeEnum;

extern const char *enum_names[];
extern int EnumTypePrint(ctrl_chk_exec_schemeEnum value, char *dest, int start, int end);


/* Event declarations. */
enum ctrl_chk_exec_schemeEventEnum_ {
    /** Initial step. */
    EVT_INITIAL_,

    /** Delay step. */
    EVT_DELAY_,

    /** Event "chan". */
    chan_,

    /** Event "edge_order.e". */
    edge_order_e_,

    /** Event "event_order_abs1.a". */
    event_order_abs1_a_,

    /** Event "event_order_abs2.a". */
    event_order_abs2_a_,

    /** Event "event_order_abs3.a". */
    event_order_abs3_a_,

    /** Event "event_order_casing.aAa". */
    event_order_casing_aAa_,

    /** Event "event_order_casing.aaA". */
    event_order_casing_aaA_,

    /** Event "event_order_casing.aaa". */
    event_order_casing_aaa_,

    /** Event "event_order_escaping.tim". */
    event_order_escaping_tim_,

    /** Event "event_order_escaping.time". */
    event_order_escaping_time_,

    /** Event "event_order_escaping.timer". */
    event_order_escaping_timer_,

    /** Event "event_order_prefix.x". */
    event_order_prefix_x_,

    /** Event "event_order_prefix.x2". */
    event_order_prefix_x2_,

    /** Event "event_order_prefix.x2b". */
    event_order_prefix_x2b_,

    /** Event "event_order_sort.a". */
    event_order_sort_a_,

    /** Event "event_order_sort.b". */
    event_order_sort_b_,

    /** Event "event_order_sort.c". */
    event_order_sort_c_,

    /** Event "one_trans_per_evt1.a". */
    one_trans_per_evt1_a_,

    /** Event "one_trans_per_evt1.b". */
    one_trans_per_evt1_b_,

    /** Event "one_trans_per_evt2.e". */
    one_trans_per_evt2_e_,

    /** Event "one_trans_per_evt3.a". */
    one_trans_per_evt3_a_,

    /** Event "one_trans_per_evt3.b". */
    one_trans_per_evt3_b_,

    /** Event "one_trans_per_evt3.c". */
    one_trans_per_evt3_c_,

    /** Event "one_trans_per_evt3.d". */
    one_trans_per_evt3_d_,

    /** Event "unctrl_vs_ctrl.c1". */
    unctrl_vs_ctrl_c1_,

    /** Event "unctrl_vs_ctrl.c2". */
    unctrl_vs_ctrl_c2_,

    /** Event "unctrl_vs_ctrl.u1". */
    unctrl_vs_ctrl_u1_,

    /** Event "unctrl_vs_ctrl.u2". */
    unctrl_vs_ctrl_u2_,
};
typedef enum ctrl_chk_exec_schemeEventEnum_ ctrl_chk_exec_scheme_Event_;

/** Names of all the events. */
extern const char *ctrl_chk_exec_scheme_event_names[];

/* Constants. */


/* Input variables. */




/* Declaration of internal functions. */


/* State variables (use for output only). */
extern RealType model_time; /**< Current model time. */

/** Discrete variable "int[0..5] chan_rcv1.x". */
extern IntType chan_rcv1_x_;

/** Discrete variable "int[0..5] chan_rcv2.x". */
extern IntType chan_rcv2_x_;

/** Discrete variable "int[0..2,000] chan_sync1.v". */
extern IntType chan_sync1_v_;

/** Discrete variable "int[0..50] edge_order.v". */
extern IntType edge_order_v_;

/** Discrete variable "int[0..50] event_order_abs2.v". */
extern IntType event_order_abs2_v_;

/** Discrete variable "int[0..50] event_order_casing.v". */
extern IntType event_order_casing_v_;

/** Discrete variable "int[0..50] event_order_escaping.v". */
extern IntType event_order_escaping_v_;

/** Discrete variable "int[0..50] event_order_prefix.v". */
extern IntType event_order_prefix_v_;

/** Discrete variable "int[0..50] event_order_sort.v". */
extern IntType event_order_sort_v_;

/** Discrete variable "int[0..50] one_trans_per_evt1.v". */
extern IntType one_trans_per_evt1_v_;

/** Discrete variable "int[0..5] one_trans_per_evt2a.v". */
extern IntType one_trans_per_evt2a_v_;

/** Discrete variable "int[0..50] one_trans_per_evt2a.w". */
extern IntType one_trans_per_evt2a_w_;

/** Discrete variable "int[0..50] one_trans_per_evt2b.w". */
extern IntType one_trans_per_evt2b_w_;

/** Discrete variable "int[0..5] one_trans_per_evt3.v". */
extern IntType one_trans_per_evt3_v_;

/** Discrete variable "int[0..5] one_trans_per_evt3.x". */
extern IntType one_trans_per_evt3_x_;

/** Discrete variable "int[0..40] one_trans_per_evt3.w". */
extern IntType one_trans_per_evt3_w_;

/** Discrete variable "int[0..2,000] unctrl_vs_ctrl.v". */
extern IntType unctrl_vs_ctrl_v_;

/* Algebraic and derivative functions (use for output only). */






/* Code entry points. */
void ctrl_chk_exec_scheme_EngineFirstStep(void);
void ctrl_chk_exec_scheme_EngineTimeStep(double delta);

#if EVENT_OUTPUT
/**
 * External callback function reporting about the execution of an event.
 * @param event Event being executed.
 * @param pre If \c TRUE, event is about to be executed. If \c FALSE, event has been executed.
 * @note Function must be implemented externally.
 */
extern void ctrl_chk_exec_scheme_InfoEvent(ctrl_chk_exec_scheme_Event_ event, BoolType pre);
#endif

#if PRINT_OUTPUT
/**
 * External callback function to output the given text-line to the given filename.
 * @param text Text to print (does not have a EOL character).
 * @param fname Name of the file to print to.
 */
extern void ctrl_chk_exec_scheme_PrintOutput(const char *text, const char *fname);
#endif

#endif

