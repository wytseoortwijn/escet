/* Additional code to allow compilation and test of the generated code.
 *
 * This file is generated, DO NOT EDIT
 */

#include <stdio.h>
#include "ranged_int_in_type_decl_engine.h"

/* Assign values to the input variables. */
void ranged_int_in_type_decl_AssignInputVariables(void) {

}

void ranged_int_in_type_decl_InfoEvent(ranged_int_in_type_decl_Event_ event, BoolType pre) {
    const char *prePostText = pre ? "pre" : "post";
    printf("Executing %s-event \"%s\"\n", prePostText, ranged_int_in_type_decl_event_names[event]);
}

void ranged_int_in_type_decl_PrintOutput(const char *text, const char *fname) {
    printf("Print @ %s: \"%s\"\n", fname, text);
}

int main(void) {
    ranged_int_in_type_decl_EngineFirstStep();

    ranged_int_in_type_decl_EngineTimeStep(1.0);
    return 0;
}

