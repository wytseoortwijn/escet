/* Additional code to allow compilation and test of the generated code.
 *
 * This file is generated, DO NOT EDIT
 */

#include <stdio.h>
#include "svg_input_update_engine.h"

/* Assign values to the input variables. */
void svg_input_update_AssignInputVariables(void) {
    /* Input variable "i". */
    i_ = FALSE;

    /* Input variable "j". */
    j_ = FALSE;

    /* Input variable "ia". */
    ia_ = 0;

    /* Input variable "ib". */
    ib_ = 0;

    /* Input variable "ic". */
    ic_ = 0;

    /* Input variable "g.i". */
    g_i_ = 0;
}

void svg_input_update_InfoEvent(svg_input_update_Event_ event, BoolType pre) {
    const char *prePostText = pre ? "pre" : "post";
    printf("Executing %s-event \"%s\"\n", prePostText, svg_input_update_event_names[event]);
}

void svg_input_update_PrintOutput(const char *text, const char *fname) {
    printf("Print @ %s: \"%s\"\n", fname, text);
}

int main(void) {
    svg_input_update_EngineFirstStep();

    svg_input_update_EngineTimeStep(1.0);
    return 0;
}

