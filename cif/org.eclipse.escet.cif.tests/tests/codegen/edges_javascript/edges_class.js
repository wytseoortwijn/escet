/** Tuples. */


            /** Tuple class for CIF tuple type representative "tuple(int a; int b)". */
            class CifTuple_T2II {
                /** The 1st field. */
                _field0;

                /** The 2nd field. */
                _field1;

                /**
                 * Constructor for the {@link CifTuple_T2II} class.
                 *
                 * @param _field0 The 1st field.
                 * @param _field1 The 2nd field.
                 */
                constructor(_field0, _field1) {
                    this._field0 = _field0;
                    this._field1 = _field1;
                }

                copy() {
                    return new CifTuple_T2II(this._field0, this._field1);
                }

                toString() {
                    var rslt = '(';
                    rslt += edgesUtils.valueToStr(this._field0);
                    rslt += ', ';
                    rslt += edgesUtils.valueToStr(this._field1);
                    rslt += ')';
                    return rslt;
                }
            }


            /** Tuple class for CIF tuple type representative "tuple(tuple(int a; int b) t; string c)". */
            class CifTuple_T2T2IIS {
                /** The 1st field. */
                _field0;

                /** The 2nd field. */
                _field1;

                /**
                 * Constructor for the {@link CifTuple_T2T2IIS} class.
                 *
                 * @param _field0 The 1st field.
                 * @param _field1 The 2nd field.
                 */
                constructor(_field0, _field1) {
                    this._field0 = _field0;
                    this._field1 = _field1;
                }

                copy() {
                    return new CifTuple_T2T2IIS(this._field0, this._field1);
                }

                toString() {
                    var rslt = '(';
                    rslt += edgesUtils.valueToStr(this._field0);
                    rslt += ', ';
                    rslt += edgesUtils.valueToStr(this._field1);
                    rslt += ')';
                    return rslt;
                }
            }


            /** Tuple class for CIF tuple type representative "tuple(list[1] int x; list[1] real y)". */
            class CifTuple_T2LILR {
                /** The 1st field. */
                _field0;

                /** The 2nd field. */
                _field1;

                /**
                 * Constructor for the {@link CifTuple_T2LILR} class.
                 *
                 * @param _field0 The 1st field.
                 * @param _field1 The 2nd field.
                 */
                constructor(_field0, _field1) {
                    this._field0 = _field0;
                    this._field1 = _field1;
                }

                copy() {
                    return new CifTuple_T2LILR(this._field0, this._field1);
                }

                toString() {
                    var rslt = '(';
                    rslt += edgesUtils.valueToStr(this._field0);
                    rslt += ', ';
                    rslt += edgesUtils.valueToStr(this._field1);
                    rslt += ')';
                    return rslt;
                }
            }


            /** Tuple class for CIF tuple type representative "tuple(string s; list[2] tuple(list[1] int x; list[1] real y) z)". */
            class CifTuple_T2SLT2LILR {
                /** The 1st field. */
                _field0;

                /** The 2nd field. */
                _field1;

                /**
                 * Constructor for the {@link CifTuple_T2SLT2LILR} class.
                 *
                 * @param _field0 The 1st field.
                 * @param _field1 The 2nd field.
                 */
                constructor(_field0, _field1) {
                    this._field0 = _field0;
                    this._field1 = _field1;
                }

                copy() {
                    return new CifTuple_T2SLT2LILR(this._field0, this._field1);
                }

                toString() {
                    var rslt = '(';
                    rslt += edgesUtils.valueToStr(this._field0);
                    rslt += ', ';
                    rslt += edgesUtils.valueToStr(this._field1);
                    rslt += ')';
                    return rslt;
                }
            }

/** edges code generated from a CIF specification. */
class edges_class {
    /** edgesEnum declaration. It contains the single merged enum from the CIF model. */
    edgesEnum = Object.freeze({
        /** Literal "loc1". */
        _loc1: Symbol("loc1"),

        /** Literal "loc2". */
        _loc2: Symbol("loc2"),

        /** Literal "loc3". */
        _loc3: Symbol("loc3")
    })

    /** Should execution timing information be provided? */
    doInfoExec = true;

    /** Should executed event information be provided? */
    doInfoEvent = true;

    /** Should print output be provided? */
    doInfoPrintOutput = true;

    /** Should state output be provided? */
    doStateOutput = false;

    /** Should transition output be provided? */
    doTransitionOutput = true;

    /** Whether the constants have been initialized already. */
    constantsInitialized = false;

    /** Whether the SVG copy and move declarations have been applied already. */
    svgCopiesAndMovesApplied = false;

    /** Whether this is the first time the code is (to be) executed. */
    firstExec;

    /** The names of all the events. */
    EVENT_NAMES = [
        "e02a",
        "e02b",
        "e03a",
        "e03b",
        "e04a",
        "e04b",
        "e04c",
        "e04d",
        "e04e",
        "e04f",
        "e05a",
        "e05b",
        "e05c",
        "e05d",
        "e05e",
        "e06a",
        "e06b",
        "e06c",
        "e06d",
        "e06e",
        "e07a",
        "e07b",
        "e08a",
        "e08b",
        "e08c",
        "e08d",
        "e08e",
        "e08f",
        "e08g",
        "e08h",
        "e09a",
        "e09b",
        "e09c",
        "e09d",
        "e09e",
        "e09f",
        "e09g",
        "e10a",
        "e10b",
        "e10c",
        "e10d",
        "e10e",
        "e10f",
        "e10g",
        "e10h",
        "e10i",
        "e11a",
        "e12a",
        "e12b",
        "e12c",
        "e12d",
        "e12e",
        "e13a",
        "e13b",
        "e13c",
        "e13d",
        "e13e",
        "e14a",
        "e14b",
        "e14c",
        "e14d",
        "e14e",
        "e14f",
        "e14g",
        "e14h"
    ];


    /** Variable 'time', tracks elapsed time for a session. */
    time;

    /**
     * The frequency in times per second, that the code should
     * be executed (if positive), or execute as fast as possible, that is
     * as many times per second as possible (if negative or zero).
     */
    frequency = 60;

    /**
     * Whether the next execution is the first execution of the session.
     * Used to initialize time-related variables for starting, pausing,
     * resuming or resetting each session.
     */
    first;

    /**
     * Whether the simulation is currently running, and should process
     * user input, or is paused.
     */
    playing;

    /** The start time of the current session. */
    startMilli;

    /**
     * The targeted end time of the current/next cycle, to ensure
     * that the duration of the cycle matches with the configured
     * frequency.
     */
    targetMilli;


    /** Discrete variable "aut02.x". */
    aut02_x_;

    /** Discrete variable "aut02". */
    aut02_;

    /** Continuous variable "aut03.c". */
    aut03_c_;

    /** Discrete variable "aut03.d". */
    aut03_d_;

    /** Discrete variable "aut04.a". */
    aut04_a_;

    /** Discrete variable "aut04.b". */
    aut04_b_;

    /** Discrete variable "aut04.c". */
    aut04_c_;

    /** Discrete variable "aut04.d". */
    aut04_d_;

    /** Discrete variable "aut05.v1". */
    aut05_v1_;

    /** Discrete variable "aut05.v2". */
    aut05_v2_;

    /** Discrete variable "aut06.v1". */
    aut06_v1_;

    /** Discrete variable "aut06.v2". */
    aut06_v2_;

    /** Discrete variable "aut06.x". */
    aut06_x_;

    /** Discrete variable "aut06.y". */
    aut06_y_;

    /** Continuous variable "aut07.x". */
    aut07_x_;

    /** Continuous variable "aut07.y". */
    aut07_y_;

    /** Discrete variable "aut08.tt1". */
    aut08_tt1_;

    /** Discrete variable "aut08.tt2". */
    aut08_tt2_;

    /** Discrete variable "aut08.t". */
    aut08_t_;

    /** Discrete variable "aut08.i". */
    aut08_i_;

    /** Discrete variable "aut08.j". */
    aut08_j_;

    /** Discrete variable "aut08.s". */
    aut08_s_;

    /** Discrete variable "aut09.ll1". */
    aut09_ll1_;

    /** Discrete variable "aut09.ll2". */
    aut09_ll2_;

    /** Discrete variable "aut09.l". */
    aut09_l_;

    /** Discrete variable "aut09.i". */
    aut09_i_;

    /** Discrete variable "aut09.j". */
    aut09_j_;

    /** Discrete variable "aut10.x1". */
    aut10_x1_;

    /** Discrete variable "aut10.x2". */
    aut10_x2_;

    /** Discrete variable "aut10.l". */
    aut10_l_;

    /** Discrete variable "aut10.li". */
    aut10_li_;

    /** Discrete variable "aut10.lr". */
    aut10_lr_;

    /** Discrete variable "aut10.i". */
    aut10_i_;

    /** Discrete variable "aut10.r". */
    aut10_r_;

    /** Discrete variable "aut11.v1". */
    aut11_v1_;

    /** Discrete variable "aut12.x". */
    aut12_x_;

    /** Discrete variable "aut12.y". */
    aut12_y_;

    /** Discrete variable "aut12.z". */
    aut12_z_;

    /** Discrete variable "aut12.td". */
    aut12_td_;

    /** Continuous variable "aut12.t". */
    aut12_t_;

    /** Continuous variable "aut12.u". */
    aut12_u_;

    /** Discrete variable "aut13.x". */
    aut13_x_;

    /** Discrete variable "aut13.y". */
    aut13_y_;

    /** Discrete variable "aut13.z". */
    aut13_z_;

    /** Input variable "aut14.b". */
    aut14_b_;

    /** Input variable "aut14.i". */
    aut14_i_;

    /** Input variable "aut14.r". */
    aut14_r_;

    /** SVG output elements. */


    /**
     * SVG input queue with functions for handling clicked SVG
     * input elements, from first clicked at the head of the array
     * to last clicked at the tail of the array.
     */
    svgInQueue;

    /**
     * The SVG input id corresponding to the SVG input element that
     * was clicked. Is 'null' if no SVG input element was clicked so
     * far, or all clicks have already been processed.
     */
    svgInId;

    /**
     * The 0-based index of the event corresponding to the SVG input
     * element that was clicked. Is '-1' if no SVG input element was
     * clicked so far, or all clicks have already been processed.
     */
    svgInEvent;

    /** SVG input click event handlers. */


    /** SVG input event setters. */


    /** Starts the simulation. */
    start() {
        if (!this.playing) {
            this.playing = true;
            this.exec();
        }
    }

    /** Stops the simulation. */
    stop() {
        if (this.playing) {
            this.playing = false;
        }
    }

    /** Resets the object to its initial state. */
    reset() {
        this.stop();
        this.firstExec = true;
        this.time = 0.0;
        this.first = true;
        this.timePaused = null;
        this.initState();
        this.initUI();
        this.updateUI();
    }

    /**
     * Execute the code once. Inputs are read, transitions are executed until
     * none are possible, outputs are written, etc.
     *
     * @param newTime The time in seconds, since the start of the first
     *      execution.
     * @throws {edgesException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execOnce(newTime) {
        // Pre execution notification.
        this.preExec();

        // Update values of input variables.
        this.updateInputs();

        // Initialize the state.
        if (this.firstExec) {
            this.initState();
        }

        // Calculate time delta.
        var delta = newTime - this.time;

        // Update values of continuous variables.
        if (!this.firstExec) {
                        try {
                            var deriv0 = edges.aut03_c_deriv();
                            var deriv1 = edges.aut07_x_deriv();
                            var deriv2 = edges.aut07_y_deriv();
                            var deriv3 = edges.aut12_t_deriv();
                            var deriv4 = edges.aut12_u_deriv();

                            edges.aut03_c_ = edges.aut03_c_ + delta * deriv0;
                            edgesUtils.checkReal(edges.aut03_c_, "aut03.c");
                            if (edges.aut03_c_ == -0.0) edges.aut03_c_ = 0.0;
                            edges.aut07_x_ = edges.aut07_x_ + delta * deriv1;
                            edgesUtils.checkReal(edges.aut07_x_, "aut07.x");
                            if (edges.aut07_x_ == -0.0) edges.aut07_x_ = 0.0;
                            edges.aut07_y_ = edges.aut07_y_ + delta * deriv2;
                            edgesUtils.checkReal(edges.aut07_y_, "aut07.y");
                            if (edges.aut07_y_ == -0.0) edges.aut07_y_ = 0.0;
                            edges.aut12_t_ = edges.aut12_t_ + delta * deriv3;
                            edgesUtils.checkReal(edges.aut12_t_, "aut12.t");
                            if (edges.aut12_t_ == -0.0) edges.aut12_t_ = 0.0;
                            edges.aut12_u_ = edges.aut12_u_ + delta * deriv4;
                            edgesUtils.checkReal(edges.aut12_u_, "aut12.u");
                            if (edges.aut12_u_ == -0.0) edges.aut12_u_ = 0.0;
                        } catch (e) {
                            if (e instanceof edgesException) {
                                e = new edgesException("Failed to update continuous variables after time passage.", e);
                                this.runtimeError(e);
                            }
                            throw e;
                        }
        }

        // Update time.
        this.time = newTime;

        // Apply print declarations.
        if (this.firstExec) {
            // For 'initial' transition.
            if (this.doInfoPrintOutput) this.printOutput(-3, true);
            if (this.doInfoPrintOutput) this.printOutput(-3, false);
            if (this.doStateOutput) edges.log('Initial state: ' + edges.getStateText());
            if (this.doStateOutput || this.doTransitionOutput) this.log('');

        } else {
            // For 'post' of time transition.
            if (this.doInfoPrintOutput) this.printOutput(-2, false);
        }

        // Execute environment events and SVG input events as long as they are possible, emptying the SVG input queue.
        while (true) {
            // Handle next element from SVG input queue, if not already already processing one.
            if (this.svgInEvent == -1 && this.svgInQueue.length > 0) {
                var func = this.svgInQueue.shift(); // Remove head of the queue.
                func(); // Call function, to set the event to allow.
            }

            // Try to execute an SVG input mapping with updates for each environment event, and an edge for each SVG
            // input event.
            var anythingExecuted = false;



            // Stop if no SVG input mapping and no edge was executed, and no more SVG input clicks are to be processed.
            if (!anythingExecuted && this.svgInQueue.length == 0) {
                break;
            }
        }

        // Make sure all outstanding SVG input clicks have been processed.
        console.assert(this.svgInEvent == -1 && this.svgInQueue.length == 0);

        // Execute uncontrollable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Execute controllable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;

            // Event "e02a".
            anythingExecuted |= this.execEdge0();

            // Event "e02b".
            anythingExecuted |= this.execEdge1();

            // Event "e03a".
            anythingExecuted |= this.execEdge2();

            // Event "e03b".
            anythingExecuted |= this.execEdge3();

            // Event "e04a".
            anythingExecuted |= this.execEdge4();

            // Event "e04b".
            anythingExecuted |= this.execEdge5();

            // Event "e04c".
            anythingExecuted |= this.execEdge6();

            // Event "e04d".
            anythingExecuted |= this.execEdge7();

            // Event "e04e".
            anythingExecuted |= this.execEdge8();

            // Event "e04f".
            anythingExecuted |= this.execEdge9();

            // Event "e05a".
            anythingExecuted |= this.execEdge10();

            // Event "e05b".
            anythingExecuted |= this.execEdge11();

            // Event "e05c".
            anythingExecuted |= this.execEdge12();

            // Event "e05d".
            anythingExecuted |= this.execEdge13();

            // Event "e05e".
            anythingExecuted |= this.execEdge14();

            // Event "e06a".
            anythingExecuted |= this.execEdge15();

            // Event "e06b".
            anythingExecuted |= this.execEdge16();

            // Event "e06c".
            anythingExecuted |= this.execEdge17();

            // Event "e06d".
            anythingExecuted |= this.execEdge18();

            // Event "e06e".
            anythingExecuted |= this.execEdge19();

            // Event "e07a".
            anythingExecuted |= this.execEdge20();

            // Event "e07b".
            anythingExecuted |= this.execEdge21();

            // Event "e08a".
            anythingExecuted |= this.execEdge22();

            // Event "e08b".
            anythingExecuted |= this.execEdge23();

            // Event "e08c".
            anythingExecuted |= this.execEdge24();

            // Event "e08d".
            anythingExecuted |= this.execEdge25();

            // Event "e08e".
            anythingExecuted |= this.execEdge26();

            // Event "e08f".
            anythingExecuted |= this.execEdge27();

            // Event "e08g".
            anythingExecuted |= this.execEdge28();

            // Event "e08h".
            anythingExecuted |= this.execEdge29();

            // Event "e09a".
            anythingExecuted |= this.execEdge30();

            // Event "e09b".
            anythingExecuted |= this.execEdge31();

            // Event "e09c".
            anythingExecuted |= this.execEdge32();

            // Event "e09d".
            anythingExecuted |= this.execEdge33();

            // Event "e09e".
            anythingExecuted |= this.execEdge34();

            // Event "e09f".
            anythingExecuted |= this.execEdge35();

            // Event "e09g".
            anythingExecuted |= this.execEdge36();

            // Event "e10a".
            anythingExecuted |= this.execEdge37();

            // Event "e10b".
            anythingExecuted |= this.execEdge38();

            // Event "e10c".
            anythingExecuted |= this.execEdge39();

            // Event "e10d".
            anythingExecuted |= this.execEdge40();

            // Event "e10e".
            anythingExecuted |= this.execEdge41();

            // Event "e10f".
            anythingExecuted |= this.execEdge42();

            // Event "e10g".
            anythingExecuted |= this.execEdge43();

            // Event "e10h".
            anythingExecuted |= this.execEdge44();

            // Event "e10i".
            anythingExecuted |= this.execEdge45();

            // Event "e11a".
            anythingExecuted |= this.execEdge46();

            // Event "e12a".
            anythingExecuted |= this.execEdge47();

            // Event "e12b".
            anythingExecuted |= this.execEdge48();

            // Event "e12c".
            anythingExecuted |= this.execEdge49();

            // Event "e12d".
            anythingExecuted |= this.execEdge50();

            // Event "e12e".
            anythingExecuted |= this.execEdge51();

            // Event "e13a".
            anythingExecuted |= this.execEdge52();

            // Event "e13b".
            anythingExecuted |= this.execEdge53();

            // Event "e13c".
            anythingExecuted |= this.execEdge54();

            // Event "e13d".
            anythingExecuted |= this.execEdge55();

            // Event "e13e".
            anythingExecuted |= this.execEdge56();

            // Event "e14a".
            anythingExecuted |= this.execEdge57();

            // Event "e14b".
            anythingExecuted |= this.execEdge58();

            // Event "e14c".
            anythingExecuted |= this.execEdge59();

            // Event "e14d".
            anythingExecuted |= this.execEdge60();

            // Event "e14e".
            anythingExecuted |= this.execEdge61();

            // Event "e14f".
            anythingExecuted |= this.execEdge62();

            // Event "e14g".
            anythingExecuted |= this.execEdge63();

            // Event "e14h".
            anythingExecuted |= this.execEdge64();

            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Apply print declarations for 'pre' of time transition.
        if (this.doInfoPrintOutput) this.printOutput(-2, true);

        // Post execution notification.
        this.postExec();

        // Done.
        this.firstExec = false;
    }

    /**
     * Calls {@link #execWhile}, which repeatedly {@link #execOnce executes the code}.
     *
     * @throws {edgesException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    exec() {
        this.execWhile(1);
    }

    /**
     * Repeatedly {@link #execOnce executes the code}.
     *
     * @param delay The delay before executing, in milliseconds.
     *
     * @throws {edgesException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execWhile(delay) {
        setTimeout(
            function () {
                // Pre execution timing.
                var now = Date.now();
                var preMilli = now;

                // On first execution, initialize variables for timing.
                if (edges.first) {
                    edges.first = false;
                    edges.startMilli = now;
                    edges.targetMilli = edges.startMilli;
                    preMilli = edges.startMilli;
                }

                // Handle pausing/playing.
                if (!edges.playing) {
                    edges.timePaused = now;
                    return;
                }

                if (edges.timePaused) {
                    edges.startMilli += (now - edges.timePaused);
                    edges.targetMilli += (now - edges.timePaused);
                    edges.timePaused = null;
                }

                // Get cycle time and current 'time'.
                var frequency = edges.frequency;
                var cycleMilli = (frequency <= 0) ? -1 : 1e3 / frequency;
                var timeMilli = preMilli - edges.startMilli;

                // Execute once.
                edges.execOnce(timeMilli / 1e3);

                // Post execution timing.
                var postMilli = Date.now();
                var duration = postMilli - preMilli;
                if (edges.doInfoExec) {
                    edges.infoExec(duration, cycleMilli);
                }

                // Ensure frequency.
                var remainderMilli = 0;
                if (frequency > 0) {
                    edges.targetMilli += cycleMilli;
                    remainderMilli = edges.targetMilli - postMilli;
                }

                // Execute again.
                edges.execWhile(remainderMilli > 0 ? remainderMilli : 0);
            },
        delay);
    }

    /**
     * Execute code for edge with index 0 and event "e02a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge0() {
        try {
            var guard;
            try {
                guard = ((edges.aut02_) == (edges.edgesEnum._loc1)) || (((edges.aut02_) == (edges.edgesEnum._loc2)) || ((edges.aut02_) == (edges.edgesEnum._loc3)));
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to evaluate guard.", e);
                }
                throw e;
            }

            if (!guard) {
                return false;
            }

            if (this.doInfoPrintOutput) this.printOutput(0, true);
            if (this.doInfoEvent) this.infoEvent(0, true);

            try {
                if ((edges.aut02_) == (edges.edgesEnum._loc1)) {
                    edges.aut02_ = edges.edgesEnum._loc2;
                } else if ((edges.aut02_) == (edges.edgesEnum._loc2)) {
                    edges.aut02_ = edges.edgesEnum._loc3;
                } else if ((edges.aut02_) == (edges.edgesEnum._loc3)) {
                    edges.aut02_ = edges.edgesEnum._loc1;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(0, false);
            if (this.doInfoPrintOutput) this.printOutput(0, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e02a\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 1 and event "e02b".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge1() {
        try {
            var guard;
            try {
                guard = (((edges.aut02_) == (edges.edgesEnum._loc1)) && (edgesUtils.equalObjs(edges.aut02_x_, 2))) || (((edges.aut02_) == (edges.edgesEnum._loc2)) || (((edges.aut02_) == (edges.edgesEnum._loc3)) && (edgesUtils.equalObjs(edges.aut02_x_, 3))));
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to evaluate guard.", e);
                }
                throw e;
            }

            if (!guard) {
                return false;
            }

            if (this.doInfoPrintOutput) this.printOutput(1, true);
            if (this.doInfoEvent) this.infoEvent(1, true);

            try {
                if (((edges.aut02_) == (edges.edgesEnum._loc1)) && (edgesUtils.equalObjs(edges.aut02_x_, 2))) {
                    edges.aut02_ = edges.edgesEnum._loc1;
                } else if ((edges.aut02_) == (edges.edgesEnum._loc2)) {
                    edges.aut02_x_ = 1;
                } else if (((edges.aut02_) == (edges.edgesEnum._loc3)) && (edgesUtils.equalObjs(edges.aut02_x_, 3))) {
                    edges.aut02_x_ = 1;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(1, false);
            if (this.doInfoPrintOutput) this.printOutput(1, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e02b\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 2 and event "e03a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge2() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(2, true);
            if (this.doInfoEvent) this.infoEvent(2, true);

            try {
                edges.aut03_c_ = 1.23;
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(2, false);
            if (this.doInfoPrintOutput) this.printOutput(2, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e03a\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 3 and event "e03b".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge3() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(3, true);
            if (this.doInfoEvent) this.infoEvent(3, true);

            try {
                edges.aut03_d_ = 2;
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(3, false);
            if (this.doInfoPrintOutput) this.printOutput(3, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e03b\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 4 and event "e04a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge4() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(4, true);
            if (this.doInfoEvent) this.infoEvent(4, true);

            try {
                if (edgesUtils.equalObjs(edges.aut04_a_, 1)) {
                    edges.aut04_b_ = 2;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(4, false);
            if (this.doInfoPrintOutput) this.printOutput(4, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e04a\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 5 and event "e04b".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge5() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(5, true);
            if (this.doInfoEvent) this.infoEvent(5, true);

            try {
                if (edgesUtils.equalObjs(edges.aut04_a_, 1)) {
                    edges.aut04_b_ = 2;
                } else if (edgesUtils.equalObjs(edges.aut04_a_, 2)) {
                    edges.aut04_b_ = 3;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(5, false);
            if (this.doInfoPrintOutput) this.printOutput(5, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e04b\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 6 and event "e04c".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge6() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(6, true);
            if (this.doInfoEvent) this.infoEvent(6, true);

            try {
                if (edgesUtils.equalObjs(edges.aut04_a_, 1)) {
                    edges.aut04_b_ = 2;
                } else if (edgesUtils.equalObjs(edges.aut04_a_, 2)) {
                    edges.aut04_b_ = 3;
                } else if (edgesUtils.equalObjs(edges.aut04_a_, 3)) {
                    edges.aut04_b_ = 4;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(6, false);
            if (this.doInfoPrintOutput) this.printOutput(6, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e04c\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 7 and event "e04d".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge7() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(7, true);
            if (this.doInfoEvent) this.infoEvent(7, true);

            try {
                if (edgesUtils.equalObjs(edges.aut04_a_, 1)) {
                    edges.aut04_b_ = 2;
                } else if (edgesUtils.equalObjs(edges.aut04_a_, 2)) {
                    edges.aut04_b_ = 3;
                } else if (edgesUtils.equalObjs(edges.aut04_a_, 3)) {
                    edges.aut04_b_ = 4;
                } else {
                    edges.aut04_b_ = 5;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(7, false);
            if (this.doInfoPrintOutput) this.printOutput(7, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e04d\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 8 and event "e04e".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge8() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(8, true);
            if (this.doInfoEvent) this.infoEvent(8, true);

            try {
                if (edgesUtils.equalObjs(edges.aut04_a_, 1)) {
                    edges.aut04_b_ = 2;
                } else {
                    edges.aut04_b_ = 5;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(8, false);
            if (this.doInfoPrintOutput) this.printOutput(8, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e04e\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 9 and event "e04f".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge9() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(9, true);
            if (this.doInfoEvent) this.infoEvent(9, true);

            try {
                if (edgesUtils.equalObjs(edges.aut04_a_, 1)) {
                    edges.aut04_b_ = 2;
                } else {
                    edges.aut04_b_ = 5;
                }
                if (edgesUtils.equalObjs(edges.aut04_a_, 1)) {
                    edges.aut04_c_ = 2;
                } else {
                    edges.aut04_d_ = 5;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(9, false);
            if (this.doInfoPrintOutput) this.printOutput(9, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e04f\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 10 and event "e05a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge10() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(10, true);
            if (this.doInfoEvent) this.infoEvent(10, true);

            try {
                {
                    var rhs1 = 3;
                    var index2 = 0;
                    edges.aut05_v1_ = edgesUtils.modify(edges.aut05_v1_, index2, rhs1);
                }
                {
                    var rhs1 = 4;
                    var index2 = 1;
                    edges.aut05_v1_ = edgesUtils.modify(edges.aut05_v1_, index2, rhs1);
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(10, false);
            if (this.doInfoPrintOutput) this.printOutput(10, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e05a\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 11 and event "e05b".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge11() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(11, true);
            if (this.doInfoEvent) this.infoEvent(11, true);

            try {
                {
                    var rhs1 = 3;
                    var index2 = 0;
                    edges.aut05_v1_ = edgesUtils.modify(edges.aut05_v1_, index2, rhs1);
                }
                {
                    var rhs1 = 4;
                    var index2 = 1;
                    edges.aut05_v1_ = edgesUtils.modify(edges.aut05_v1_, index2, rhs1);
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(11, false);
            if (this.doInfoPrintOutput) this.printOutput(11, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e05b\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 12 and event "e05c".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge12() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(12, true);
            if (this.doInfoEvent) this.infoEvent(12, true);

            try {
                edges.aut05_v1_ = edges.aut05_v2_;
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(12, false);
            if (this.doInfoPrintOutput) this.printOutput(12, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e05c\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 13 and event "e05d".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge13() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(13, true);
            if (this.doInfoEvent) this.infoEvent(13, true);

            try {
                {
                    var rhs1 = edgesUtils.projectList(edges.aut05_v2_, 0);
                    var index2 = 0;
                    edges.aut05_v1_ = edgesUtils.modify(edges.aut05_v1_, index2, rhs1);
                }
                {
                    var rhs1 = edgesUtils.projectList(edges.aut05_v2_, 1);
                    var index2 = 1;
                    edges.aut05_v1_ = edgesUtils.modify(edges.aut05_v1_, index2, rhs1);
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(13, false);
            if (this.doInfoPrintOutput) this.printOutput(13, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e05d\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 14 and event "e05e".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge14() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(14, true);
            if (this.doInfoEvent) this.infoEvent(14, true);

            try {
                {
                    var rhs1 = edgesUtils.projectList(edges.aut05_v2_, 1);
                    var index2 = 0;
                    edges.aut05_v1_ = edgesUtils.modify(edges.aut05_v1_, index2, rhs1);
                }
                {
                    var rhs1 = edgesUtils.projectList(edges.aut05_v2_, 0);
                    var index2 = 1;
                    edges.aut05_v1_ = edgesUtils.modify(edges.aut05_v1_, index2, rhs1);
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(14, false);
            if (this.doInfoPrintOutput) this.printOutput(14, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e05e\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 15 and event "e06a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge15() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(15, true);
            if (this.doInfoEvent) this.infoEvent(15, true);

            try {
                edges.aut06_v1_ = new CifTuple_T2II(3, 4);
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(15, false);
            if (this.doInfoPrintOutput) this.printOutput(15, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e06a\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 16 and event "e06b".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge16() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(16, true);
            if (this.doInfoEvent) this.infoEvent(16, true);

            try {
                {
                    var rhs1 = 5;
                    edges.aut06_v1_ = edges.aut06_v1_.copy();
                    edges.aut06_v1_._field0 = rhs1;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(16, false);
            if (this.doInfoPrintOutput) this.printOutput(16, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e06b\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 17 and event "e06c".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge17() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(17, true);
            if (this.doInfoEvent) this.infoEvent(17, true);

            try {
                {
                    var rhs1 = edges.aut06_v1_;
                    edges.aut06_x_ = (rhs1)._field0;
                    edges.aut06_y_ = (rhs1)._field1;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(17, false);
            if (this.doInfoPrintOutput) this.printOutput(17, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e06c\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 18 and event "e06d".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge18() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(18, true);
            if (this.doInfoEvent) this.infoEvent(18, true);

            try {
                edges.aut06_v1_ = new CifTuple_T2II(edgesUtils.addInt(edges.aut06_x_, 1), edgesUtils.multiplyInt(edges.aut06_y_, 2));
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(18, false);
            if (this.doInfoPrintOutput) this.printOutput(18, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e06d\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 19 and event "e06e".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge19() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(19, true);
            if (this.doInfoEvent) this.infoEvent(19, true);

            try {
                edges.aut06_v1_ = edges.aut06_v2_;
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(19, false);
            if (this.doInfoPrintOutput) this.printOutput(19, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e06e\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 20 and event "e07a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge20() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(20, true);
            if (this.doInfoEvent) this.infoEvent(20, true);

            try {
                edges.aut07_x_ = 5.0;
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(20, false);
            if (this.doInfoPrintOutput) this.printOutput(20, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e07a\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 21 and event "e07b".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge21() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(21, true);
            if (this.doInfoEvent) this.infoEvent(21, true);

            try {
                edges.aut07_y_ = edges.aut07_x_;
                edges.aut07_x_ = 5.0;
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(21, false);
            if (this.doInfoPrintOutput) this.printOutput(21, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e07b\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 22 and event "e08a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge22() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(22, true);
            if (this.doInfoEvent) this.infoEvent(22, true);

            try {
                edges.aut08_tt1_ = new CifTuple_T2T2IIS(new CifTuple_T2II(1, 2), "abc");
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(22, false);
            if (this.doInfoPrintOutput) this.printOutput(22, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e08a\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 23 and event "e08b".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge23() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(23, true);
            if (this.doInfoEvent) this.infoEvent(23, true);

            try {
                edges.aut08_tt1_ = edges.aut08_tt2_;
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(23, false);
            if (this.doInfoPrintOutput) this.printOutput(23, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e08b\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 24 and event "e08c".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge24() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(24, true);
            if (this.doInfoEvent) this.infoEvent(24, true);

            try {
                {
                    var rhs1 = edges.aut08_t_;
                    edges.aut08_tt1_ = edges.aut08_tt1_.copy();
                    edges.aut08_tt1_._field0 = rhs1;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(24, false);
            if (this.doInfoPrintOutput) this.printOutput(24, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e08c\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 25 and event "e08d".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge25() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(25, true);
            if (this.doInfoEvent) this.infoEvent(25, true);

            try {
                {
                    var rhs1 = 3;
                    var part2 = (edges.aut08_tt1_)._field0;
                    part2 = part2.copy();
                    part2._field1 = rhs1;
                    edges.aut08_tt1_ = edges.aut08_tt1_.copy();
                    edges.aut08_tt1_._field0 = part2;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(25, false);
            if (this.doInfoPrintOutput) this.printOutput(25, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e08d\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 26 and event "e08e".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge26() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(26, true);
            if (this.doInfoEvent) this.infoEvent(26, true);

            try {
                {
                    var rhs1 = 4;
                    var part2 = (edges.aut08_tt1_)._field0;
                    part2 = part2.copy();
                    part2._field0 = rhs1;
                    edges.aut08_tt1_ = edges.aut08_tt1_.copy();
                    edges.aut08_tt1_._field0 = part2;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(26, false);
            if (this.doInfoPrintOutput) this.printOutput(26, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e08e\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 27 and event "e08f".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge27() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(27, true);
            if (this.doInfoEvent) this.infoEvent(27, true);

            try {
                {
                    var rhs1 = "def";
                    edges.aut08_tt1_ = edges.aut08_tt1_.copy();
                    edges.aut08_tt1_._field1 = rhs1;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(27, false);
            if (this.doInfoPrintOutput) this.printOutput(27, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e08f\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 28 and event "e08g".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge28() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(28, true);
            if (this.doInfoEvent) this.infoEvent(28, true);

            try {
                {
                    var rhs1 = (edges.aut08_tt1_)._field0;
                    edges.aut08_i_ = (rhs1)._field0;
                    edges.aut08_j_ = (rhs1)._field1;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(28, false);
            if (this.doInfoPrintOutput) this.printOutput(28, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e08g\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 29 and event "e08h".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge29() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(29, true);
            if (this.doInfoEvent) this.infoEvent(29, true);

            try {
                {
                    var rhs1 = edges.aut08_tt1_;
                    edges.aut08_i_ = (rhs1)._field0._field0;
                    edges.aut08_j_ = (rhs1)._field0._field1;
                    edges.aut08_s_ = (rhs1)._field1;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(29, false);
            if (this.doInfoPrintOutput) this.printOutput(29, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e08h\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 30 and event "e09a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge30() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(30, true);
            if (this.doInfoEvent) this.infoEvent(30, true);

            try {
                edges.aut09_ll1_ = [[1, 2, 3], [4, 5, 6]];
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(30, false);
            if (this.doInfoPrintOutput) this.printOutput(30, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e09a\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 31 and event "e09b".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge31() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(31, true);
            if (this.doInfoEvent) this.infoEvent(31, true);

            try {
                edges.aut09_ll1_ = edges.aut09_ll2_;
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(31, false);
            if (this.doInfoPrintOutput) this.printOutput(31, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e09b\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 32 and event "e09c".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge32() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(32, true);
            if (this.doInfoEvent) this.infoEvent(32, true);

            try {
                {
                    var rhs1 = edges.aut09_l_;
                    var index2 = 0;
                    edges.aut09_ll1_ = edgesUtils.modify(edges.aut09_ll1_, index2, rhs1);
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(32, false);
            if (this.doInfoPrintOutput) this.printOutput(32, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e09c\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 33 and event "e09d".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge33() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(33, true);
            if (this.doInfoEvent) this.infoEvent(33, true);

            try {
                {
                    var rhs1 = 6;
                    var index2 = 0;
                    var index3 = 1;
                    var part4 = edgesUtils.projectList(edges.aut09_ll1_, index2);
                    part4 = edgesUtils.modify(part4, index3, rhs1);
                    edges.aut09_ll1_ = edgesUtils.modify(edges.aut09_ll1_, index2, part4);
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(33, false);
            if (this.doInfoPrintOutput) this.printOutput(33, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e09d\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 34 and event "e09e".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge34() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(34, true);
            if (this.doInfoEvent) this.infoEvent(34, true);

            try {
                edges.aut09_i_ = edgesUtils.projectList(edges.aut09_l_, 0);
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(34, false);
            if (this.doInfoPrintOutput) this.printOutput(34, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e09e\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 35 and event "e09f".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge35() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(35, true);
            if (this.doInfoEvent) this.infoEvent(35, true);

            try {
                edges.aut09_i_ = edgesUtils.projectList(edgesUtils.projectList(edges.aut09_ll1_, 0), 1);
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(35, false);
            if (this.doInfoPrintOutput) this.printOutput(35, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e09f\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 36 and event "e09g".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge36() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(36, true);
            if (this.doInfoEvent) this.infoEvent(36, true);

            try {
                edges.aut09_ll1_ = [[edges.aut09_i_, edges.aut09_j_, edgesUtils.addInt(edges.aut09_i_, edges.aut09_j_)], [edgesUtils.negateInt(edges.aut09_i_), edgesUtils.negateInt(edges.aut09_j_), edgesUtils.subtractInt(edgesUtils.negateInt(edges.aut09_i_), edges.aut09_j_)]];
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(36, false);
            if (this.doInfoPrintOutput) this.printOutput(36, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e09g\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 37 and event "e10a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge37() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(37, true);
            if (this.doInfoEvent) this.infoEvent(37, true);

            try {
                edges.aut10_x1_ = edges.aut10_x2_;
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(37, false);
            if (this.doInfoPrintOutput) this.printOutput(37, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e10a\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 38 and event "e10b".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge38() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(38, true);
            if (this.doInfoEvent) this.infoEvent(38, true);

            try {
                edges.aut10_x1_ = new CifTuple_T2SLT2LILR("abc", [new CifTuple_T2LILR([1], [2.0]), new CifTuple_T2LILR([edges.aut10_i_], [edges.aut10_r_])]);
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(38, false);
            if (this.doInfoPrintOutput) this.printOutput(38, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e10b\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 39 and event "e10c".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge39() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(39, true);
            if (this.doInfoEvent) this.infoEvent(39, true);

            try {
                {
                    var rhs1 = "def";
                    edges.aut10_x1_ = edges.aut10_x1_.copy();
                    edges.aut10_x1_._field0 = rhs1;
                }
                {
                    var rhs1 = [new CifTuple_T2LILR([1], [2.0]), new CifTuple_T2LILR([3], [4.0])];
                    edges.aut10_x1_ = edges.aut10_x1_.copy();
                    edges.aut10_x1_._field1 = rhs1;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(39, false);
            if (this.doInfoPrintOutput) this.printOutput(39, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e10c\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 40 and event "e10d".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge40() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(40, true);
            if (this.doInfoEvent) this.infoEvent(40, true);

            try {
                {
                    var rhs1 = new CifTuple_T2LILR([4], [5.0]);
                    var index2 = 0;
                    var part3 = (edges.aut10_x1_)._field1;
                    part3 = edgesUtils.modify(part3, index2, rhs1);
                    edges.aut10_x1_ = edges.aut10_x1_.copy();
                    edges.aut10_x1_._field1 = part3;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(40, false);
            if (this.doInfoPrintOutput) this.printOutput(40, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e10d\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 41 and event "e10e".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge41() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(41, true);
            if (this.doInfoEvent) this.infoEvent(41, true);

            try {
                {
                    var rhs1 = 5;
                    var index2 = 0;
                    var index3 = 0;
                    var part4 = (edges.aut10_x1_)._field1;
                    var part5 = edgesUtils.projectList(part4, index2);
                    var part6 = (part5)._field0;
                    part6 = edgesUtils.modify(part6, index3, rhs1);
                    part5 = part5.copy();
                    part5._field0 = part6;
                    part4 = edgesUtils.modify(part4, index2, part5);
                    edges.aut10_x1_ = edges.aut10_x1_.copy();
                    edges.aut10_x1_._field1 = part4;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(41, false);
            if (this.doInfoPrintOutput) this.printOutput(41, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e10e\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 42 and event "e10f".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge42() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(42, true);
            if (this.doInfoEvent) this.infoEvent(42, true);

            try {
                edges.aut10_l_ = (edges.aut10_x1_)._field1;
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(42, false);
            if (this.doInfoPrintOutput) this.printOutput(42, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e10f\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 43 and event "e10g".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge43() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(43, true);
            if (this.doInfoEvent) this.infoEvent(43, true);

            try {
                edges.aut10_li_ = (edgesUtils.projectList((edges.aut10_x1_)._field1, 0))._field0;
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(43, false);
            if (this.doInfoPrintOutput) this.printOutput(43, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e10g\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 44 and event "e10h".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge44() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(44, true);
            if (this.doInfoEvent) this.infoEvent(44, true);

            try {
                edges.aut10_lr_ = (edgesUtils.projectList((edges.aut10_x1_)._field1, 0))._field1;
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(44, false);
            if (this.doInfoPrintOutput) this.printOutput(44, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e10h\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 45 and event "e10i".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge45() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(45, true);
            if (this.doInfoEvent) this.infoEvent(45, true);

            try {
                edges.aut10_i_ = edgesUtils.projectList((edgesUtils.projectList((edges.aut10_x1_)._field1, 0))._field0, 0);
                edges.aut10_r_ = edgesUtils.projectList((edgesUtils.projectList((edges.aut10_x1_)._field1, 0))._field1, 0);
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(45, false);
            if (this.doInfoPrintOutput) this.printOutput(45, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e10i\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 46 and event "e11a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge46() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(46, true);
            if (this.doInfoEvent) this.infoEvent(46, true);

            try {
                if (edgesUtils.equalObjs((edgesUtils.projectList(edges.aut11_v1_, 0))._field0, 1)) {
                    var rhs1 = edgesUtils.addInt((edgesUtils.projectList(edges.aut11_v1_, 0))._field0, 1);
                    var index2 = 0;
                    var part3 = edgesUtils.projectList(edges.aut11_v1_, index2);
                    part3 = part3.copy();
                    part3._field0 = rhs1;
                    edges.aut11_v1_ = edgesUtils.modify(edges.aut11_v1_, index2, part3);
                } else if (edgesUtils.equalObjs((edgesUtils.projectList(edges.aut11_v1_, 0))._field0, 2)) {
                    var rhs1 = edgesUtils.subtractInt((edgesUtils.projectList(edges.aut11_v1_, 0))._field1, 1);
                    var index2 = 0;
                    var part3 = edgesUtils.projectList(edges.aut11_v1_, index2);
                    part3 = part3.copy();
                    part3._field1 = rhs1;
                    edges.aut11_v1_ = edgesUtils.modify(edges.aut11_v1_, index2, part3);
                } else {
                    var rhs1 = (edgesUtils.projectList(edges.aut11_v1_, 2))._field0;
                    var index2 = 1;
                    var part3 = edgesUtils.projectList(edges.aut11_v1_, index2);
                    part3 = part3.copy();
                    part3._field0 = rhs1;
                    edges.aut11_v1_ = edgesUtils.modify(edges.aut11_v1_, index2, part3);
                }
                {
                    var rhs1 = 3;
                    var index2 = 2;
                    var part3 = edgesUtils.projectList(edges.aut11_v1_, index2);
                    part3 = part3.copy();
                    part3._field0 = rhs1;
                    edges.aut11_v1_ = edgesUtils.modify(edges.aut11_v1_, index2, part3);
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(46, false);
            if (this.doInfoPrintOutput) this.printOutput(46, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e11a\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 47 and event "e12a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge47() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(47, true);
            if (this.doInfoEvent) this.infoEvent(47, true);

            try {
                edges.aut12_z_ = edges.aut12_v_();
                edges.aut12_x_ = 1.0;
                edges.aut12_y_ = 1.0;
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(47, false);
            if (this.doInfoPrintOutput) this.printOutput(47, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e12a\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 48 and event "e12b".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge48() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(48, true);
            if (this.doInfoEvent) this.infoEvent(48, true);

            try {
                {
                    var aut12_v_tmp1 = edges.aut12_v_();
                    edges.aut12_x_ = aut12_v_tmp1;
                    edges.aut12_y_ = aut12_v_tmp1;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(48, false);
            if (this.doInfoPrintOutput) this.printOutput(48, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e12b\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 49 and event "e12c".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge49() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(49, true);
            if (this.doInfoEvent) this.infoEvent(49, true);

            try {
                {
                    var aut12_v_tmp1 = edges.aut12_v_();
                    edges.aut12_td_ = edges.aut12_w_();
                    edges.aut12_x_ = aut12_v_tmp1;
                    edges.aut12_y_ = aut12_v_tmp1;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(49, false);
            if (this.doInfoPrintOutput) this.printOutput(49, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e12c\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 50 and event "e12d".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge50() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(50, true);
            if (this.doInfoEvent) this.infoEvent(50, true);

            try {
                {
                    var aut12_t_tmp1 = edges.aut12_t_deriv();
                    edges.aut12_x_ = aut12_t_tmp1;
                    edges.aut12_y_ = aut12_t_tmp1;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(50, false);
            if (this.doInfoPrintOutput) this.printOutput(50, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e12d\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 51 and event "e12e".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge51() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(51, true);
            if (this.doInfoEvent) this.infoEvent(51, true);

            try {
                edges.aut12_td_ = edges.aut12_u_deriv();
                edges.aut12_x_ = 1.0;
                edges.aut12_y_ = 1.0;
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(51, false);
            if (this.doInfoPrintOutput) this.printOutput(51, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e12e\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 52 and event "e13a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge52() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(52, true);
            if (this.doInfoEvent) this.infoEvent(52, true);

            try {
                edges.aut13_x_ = 1.0;
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(52, false);
            if (this.doInfoPrintOutput) this.printOutput(52, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e13a\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 53 and event "e13b".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge53() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(53, true);
            if (this.doInfoEvent) this.infoEvent(53, true);

            try {
                if (edgesUtils.equalObjs(edges.aut13_z_, 5.0)) {
                    edges.aut13_x_ = 2.0;
                } else {
                    edges.aut13_x_ = 3.0;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(53, false);
            if (this.doInfoPrintOutput) this.printOutput(53, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e13b\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 54 and event "e13c".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge54() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(54, true);
            if (this.doInfoEvent) this.infoEvent(54, true);

            try {
                if (edgesUtils.equalObjs(edges.aut13_z_, 5.0)) {
                    edges.aut13_x_ = 2.0;
                } else if (edgesUtils.equalObjs(edges.aut13_z_, 21.0)) {
                    edges.aut13_x_ = 3.0;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(54, false);
            if (this.doInfoPrintOutput) this.printOutput(54, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e13c\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 55 and event "e13d".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge55() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(55, true);
            if (this.doInfoEvent) this.infoEvent(55, true);

            try {
                if (edgesUtils.equalObjs(edges.aut13_z_, 5.0)) {
                    edges.aut13_x_ = 2.0;
                } else if (edgesUtils.equalObjs(edges.aut13_z_, 21.0)) {
                    edges.aut13_x_ = 3.0;
                } else {
                    edges.aut13_x_ = 4.0;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(55, false);
            if (this.doInfoPrintOutput) this.printOutput(55, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e13d\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 56 and event "e13e".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge56() {
        try {

            if (this.doInfoPrintOutput) this.printOutput(56, true);
            if (this.doInfoEvent) this.infoEvent(56, true);

            try {
                if (edgesUtils.equalObjs(edges.aut13_w_(), 4.0)) {
                    edges.aut13_x_ = 1.0;
                } else if (edgesUtils.equalObjs(edges.aut13_v_(), 5.0)) {
                    edges.aut13_x_ = 2.0;
                }
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to execute update(s).", e);
                }
                throw e;
            }

            if (this.doInfoEvent) this.infoEvent(56, false);
            if (this.doInfoPrintOutput) this.printOutput(56, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e13e\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 57 and event "e14a".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge57() {
        try {
            var guard;
            try {
                guard = edges.aut14_b_;
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to evaluate guard.", e);
                }
                throw e;
            }

            if (!guard) {
                return false;
            }

            if (this.doInfoPrintOutput) this.printOutput(57, true);
            if (this.doInfoEvent) this.infoEvent(57, true);

            if (this.doInfoEvent) this.infoEvent(57, false);
            if (this.doInfoPrintOutput) this.printOutput(57, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e14a\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 58 and event "e14b".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge58() {
        try {
            var guard;
            try {
                guard = (edges.aut14_i_) > (3);
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to evaluate guard.", e);
                }
                throw e;
            }

            if (!guard) {
                return false;
            }

            if (this.doInfoPrintOutput) this.printOutput(58, true);
            if (this.doInfoEvent) this.infoEvent(58, true);

            if (this.doInfoEvent) this.infoEvent(58, false);
            if (this.doInfoPrintOutput) this.printOutput(58, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e14b\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 59 and event "e14c".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge59() {
        try {
            var guard;
            try {
                guard = !edgesUtils.equalObjs(edgesUtils.addReal(edges.aut14_r_, edges.aut14_i_), 18.0);
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to evaluate guard.", e);
                }
                throw e;
            }

            if (!guard) {
                return false;
            }

            if (this.doInfoPrintOutput) this.printOutput(59, true);
            if (this.doInfoEvent) this.infoEvent(59, true);

            if (this.doInfoEvent) this.infoEvent(59, false);
            if (this.doInfoPrintOutput) this.printOutput(59, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e14c\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 60 and event "e14d".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge60() {
        try {
            var guard;
            try {
                guard = !(edges.aut14_b_);
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to evaluate guard.", e);
                }
                throw e;
            }

            if (!guard) {
                return false;
            }

            if (this.doInfoPrintOutput) this.printOutput(60, true);
            if (this.doInfoEvent) this.infoEvent(60, true);

            if (this.doInfoEvent) this.infoEvent(60, false);
            if (this.doInfoPrintOutput) this.printOutput(60, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e14d\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 61 and event "e14e".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge61() {
        try {
            var guard;
            try {
                guard = (edgesUtils.negateInt(edges.aut14_i_)) < (5);
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to evaluate guard.", e);
                }
                throw e;
            }

            if (!guard) {
                return false;
            }

            if (this.doInfoPrintOutput) this.printOutput(61, true);
            if (this.doInfoEvent) this.infoEvent(61, true);

            if (this.doInfoEvent) this.infoEvent(61, false);
            if (this.doInfoPrintOutput) this.printOutput(61, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e14e\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 62 and event "e14f".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge62() {
        try {
            var guard;
            try {
                guard = (edgesUtils.negateReal(edges.aut14_r_)) < (6);
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to evaluate guard.", e);
                }
                throw e;
            }

            if (!guard) {
                return false;
            }

            if (this.doInfoPrintOutput) this.printOutput(62, true);
            if (this.doInfoEvent) this.infoEvent(62, true);

            if (this.doInfoEvent) this.infoEvent(62, false);
            if (this.doInfoPrintOutput) this.printOutput(62, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e14f\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 63 and event "e14g".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge63() {
        try {
            var guard;
            try {
                guard = (edges.aut14_i_) < (7);
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to evaluate guard.", e);
                }
                throw e;
            }

            if (!guard) {
                return false;
            }

            if (this.doInfoPrintOutput) this.printOutput(63, true);
            if (this.doInfoEvent) this.infoEvent(63, true);

            if (this.doInfoEvent) this.infoEvent(63, false);
            if (this.doInfoPrintOutput) this.printOutput(63, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e14g\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Execute code for edge with index 64 and event "e14h".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge64() {
        try {
            var guard;
            try {
                guard = (edges.aut14_r_) < (8);
            } catch (e) {
                if (e instanceof edgesException) {
                    e = new edgesException("Failed to evaluate guard.", e);
                }
                throw e;
            }

            if (!guard) {
                return false;
            }

            if (this.doInfoPrintOutput) this.printOutput(64, true);
            if (this.doInfoEvent) this.infoEvent(64, true);

            if (this.doInfoEvent) this.infoEvent(64, false);
            if (this.doInfoPrintOutput) this.printOutput(64, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to (try to) execute event \"e14h\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Initializes the state.
     *
     * @throws {edgesException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    initState() {
        // Initialize constants, if not yet done so.
        if (!this.constantsInitialized) {
            this.constantsInitialized = true;

        }

        // Initialize SVG input.
        this.svgInQueue = [];
        this.svgInId = null;
        this.svgInEvent = -1;


        // CIF model state variables.
        try {
            edges.aut02_x_ = 0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut02.x\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut02_ = edges.edgesEnum._loc1;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut02\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut03_c_ = 0.0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut03.c\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut03_d_ = 0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut03.d\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut04_a_ = 0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut04.a\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut04_b_ = 0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut04.b\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut04_c_ = 0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut04.c\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut04_d_ = 0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut04.d\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut05_v1_ = [0, 0, 0, 0, 0];
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut05.v1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut05_v2_ = [0, 0, 0, 0, 0];
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut05.v2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut06_v1_ = new CifTuple_T2II(0, 0);
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut06.v1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut06_v2_ = new CifTuple_T2II(0, 0);
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut06.v2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut06_x_ = 0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut06.x\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut06_y_ = 0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut06.y\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut07_x_ = 0.0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut07.x\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut07_y_ = 0.0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut07.y\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut08_tt1_ = new CifTuple_T2T2IIS(new CifTuple_T2II(0, 0), "");
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut08.tt1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut08_tt2_ = new CifTuple_T2T2IIS(new CifTuple_T2II(0, 0), "");
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut08.tt2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut08_t_ = new CifTuple_T2II(0, 0);
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut08.t\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut08_i_ = 0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut08.i\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut08_j_ = 0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut08.j\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut08_s_ = "";
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut08.s\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut09_ll1_ = [[0, 0, 0], [0, 0, 0]];
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut09.ll1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut09_ll2_ = [[0, 0, 0], [0, 0, 0]];
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut09.ll2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut09_l_ = [0, 0, 0];
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut09.l\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut09_i_ = 0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut09.i\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut09_j_ = 0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut09.j\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut10_x1_ = new CifTuple_T2SLT2LILR("", [new CifTuple_T2LILR([0], [0.0]), new CifTuple_T2LILR([0], [0.0])]);
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut10.x1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut10_x2_ = new CifTuple_T2SLT2LILR("", [new CifTuple_T2LILR([0], [0.0]), new CifTuple_T2LILR([0], [0.0])]);
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut10.x2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut10_l_ = [new CifTuple_T2LILR([0], [0.0]), new CifTuple_T2LILR([0], [0.0])];
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut10.l\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut10_li_ = [0];
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut10.li\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut10_lr_ = [0.0];
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut10.lr\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut10_i_ = 0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut10.i\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut10_r_ = 0.0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut10.r\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut11_v1_ = [new CifTuple_T2II(0, 0), new CifTuple_T2II(0, 0), new CifTuple_T2II(0, 0)];
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut11.v1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut12_x_ = 0.0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut12.x\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut12_y_ = 0.0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut12.y\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut12_z_ = 0.0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut12.z\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut12_td_ = 0.0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut12.td\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut12_t_ = 0.0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut12.t\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut12_u_ = 0.0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut12.u\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut13_x_ = 0.0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut13.x\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut13_y_ = 0.0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut13.y\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            edges.aut13_z_ = 0.0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the initial value of variable \"aut13.z\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Initializes the user interface, either when loading the page
     * or when resetting the simulation.
     */
    initUI() {


        // Apply SVG copies and moves, if not done so before.
        if (!this.svgCopiesAndMovesApplied) {
            this.svgCopiesAndMovesApplied = true;

            // Apply SVG copy declarations.


            // Apply SVG move declarations.

        }

        // Prepare SVG output.


        // Prepare SVG input.

    }

    /**
     * Updates the user interface based on the latest state of
     * the model. Is called at the end of each cycle.
     *
     * @throws {edgesException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    updateUI() {
        // Apply CIF/SVG output mappings.

    }

    /**
     * Updates the values of the input variables. Other variables from the
     * state may not be accessed or modified.
     */
    updateInputs() {
        // Nothing is done here by default.
    }

    /** Logs a normal message. */
    log(message) {
        console.log(message);

    }

    /** Logs an warning message. */
    warning(message) {
        console.log(message);

    }

    /** Logs an error message. */
    error(message) {
        console.log(message);

    }

    /** Logs a runtime error of type edgesException. */
    runtimeError(e, isCause = false) {
        console.assert(e instanceof edgesException);
        if (isCause) {
            this.error("CAUSE: " + e.message);
        } else {
            this.error("ERROR: " + e.message);
        }
        if (e.cause) {
            this.runtimeError(e.cause, true);
        }
    }

    /**
     * Informs about the duration of a single execution.
     *
     * @param duration The duration of the execution, in milliseconds.
     * @param cycleTime The desired maximum duration of the execution, in
     *      milliseconds, or '-1' if not available.
     */
    infoExec(duration, cycleTime) {
        // Nothing is done here by default.
    }

    /**
     * Informs that an event will be or has been executed.
     *
     * @param idx The 0-based index of the event.
     * @param pre Whether the event will be executed ('true') or has
     *      been executed ('false').
     */
    infoEvent(idx, pre) {
        if (pre) {
            if (this.doTransitionOutput) edges.log(edgesUtils.fmt('Transition: event %s', edges.getEventName(idx)));
        } else {
            if (this.doStateOutput) edges.log('State: ' + edges.getStateText());
        }
    }

    /**
     * Informs that the code is about to be executed. For the
     * {@link #firstExec} the state has not yet been initialized, except for
     * {@link #time}.
     */
    preExec() {
        // Nothing is done here by default.
    }

    /** Informs that the code was just executed. */
    postExec() {
        this.updateUI();
    }

    /**
     * Returns the name of an event.
     *
     * @param idx The 0-based index of the event.
     * @return The name of the event.
     */
    getEventName(idx) {
        return this.EVENT_NAMES[idx];
    }

    /**
     * Returns a single-line textual representation of the model state.
     *
     * @return The single-line textual representation of the model state.
     */
    getStateText() {
        var state = edgesUtils.fmt('time=%s', edges.time);
        state += edgesUtils.fmt(', aut02=%s', edgesUtils.valueToStr(edges.aut02_));
        state += edgesUtils.fmt(', aut02.x=%s', edgesUtils.valueToStr(edges.aut02_x_));
        state += edgesUtils.fmt(', aut03.c=%s', edgesUtils.valueToStr(edges.aut03_c_));
        state += edgesUtils.fmt(', aut03.c\'=%s', edgesUtils.valueToStr(edges.aut03_c_deriv()));
        state += edgesUtils.fmt(', aut03.d=%s', edgesUtils.valueToStr(edges.aut03_d_));
        state += edgesUtils.fmt(', aut04.a=%s', edgesUtils.valueToStr(edges.aut04_a_));
        state += edgesUtils.fmt(', aut04.b=%s', edgesUtils.valueToStr(edges.aut04_b_));
        state += edgesUtils.fmt(', aut04.c=%s', edgesUtils.valueToStr(edges.aut04_c_));
        state += edgesUtils.fmt(', aut04.d=%s', edgesUtils.valueToStr(edges.aut04_d_));
        state += edgesUtils.fmt(', aut05.v1=%s', edgesUtils.valueToStr(edges.aut05_v1_));
        state += edgesUtils.fmt(', aut05.v2=%s', edgesUtils.valueToStr(edges.aut05_v2_));
        state += edgesUtils.fmt(', aut06.v1=%s', edgesUtils.valueToStr(edges.aut06_v1_));
        state += edgesUtils.fmt(', aut06.v2=%s', edgesUtils.valueToStr(edges.aut06_v2_));
        state += edgesUtils.fmt(', aut06.x=%s', edgesUtils.valueToStr(edges.aut06_x_));
        state += edgesUtils.fmt(', aut06.y=%s', edgesUtils.valueToStr(edges.aut06_y_));
        state += edgesUtils.fmt(', aut07.x=%s', edgesUtils.valueToStr(edges.aut07_x_));
        state += edgesUtils.fmt(', aut07.x\'=%s', edgesUtils.valueToStr(edges.aut07_x_deriv()));
        state += edgesUtils.fmt(', aut07.y=%s', edgesUtils.valueToStr(edges.aut07_y_));
        state += edgesUtils.fmt(', aut07.y\'=%s', edgesUtils.valueToStr(edges.aut07_y_deriv()));
        state += edgesUtils.fmt(', aut08.i=%s', edgesUtils.valueToStr(edges.aut08_i_));
        state += edgesUtils.fmt(', aut08.j=%s', edgesUtils.valueToStr(edges.aut08_j_));
        state += edgesUtils.fmt(', aut08.s=%s', edgesUtils.valueToStr(edges.aut08_s_));
        state += edgesUtils.fmt(', aut08.t=%s', edgesUtils.valueToStr(edges.aut08_t_));
        state += edgesUtils.fmt(', aut08.tt1=%s', edgesUtils.valueToStr(edges.aut08_tt1_));
        state += edgesUtils.fmt(', aut08.tt2=%s', edgesUtils.valueToStr(edges.aut08_tt2_));
        state += edgesUtils.fmt(', aut09.i=%s', edgesUtils.valueToStr(edges.aut09_i_));
        state += edgesUtils.fmt(', aut09.j=%s', edgesUtils.valueToStr(edges.aut09_j_));
        state += edgesUtils.fmt(', aut09.l=%s', edgesUtils.valueToStr(edges.aut09_l_));
        state += edgesUtils.fmt(', aut09.ll1=%s', edgesUtils.valueToStr(edges.aut09_ll1_));
        state += edgesUtils.fmt(', aut09.ll2=%s', edgesUtils.valueToStr(edges.aut09_ll2_));
        state += edgesUtils.fmt(', aut10.i=%s', edgesUtils.valueToStr(edges.aut10_i_));
        state += edgesUtils.fmt(', aut10.l=%s', edgesUtils.valueToStr(edges.aut10_l_));
        state += edgesUtils.fmt(', aut10.li=%s', edgesUtils.valueToStr(edges.aut10_li_));
        state += edgesUtils.fmt(', aut10.lr=%s', edgesUtils.valueToStr(edges.aut10_lr_));
        state += edgesUtils.fmt(', aut10.r=%s', edgesUtils.valueToStr(edges.aut10_r_));
        state += edgesUtils.fmt(', aut10.x1=%s', edgesUtils.valueToStr(edges.aut10_x1_));
        state += edgesUtils.fmt(', aut10.x2=%s', edgesUtils.valueToStr(edges.aut10_x2_));
        state += edgesUtils.fmt(', aut11.v1=%s', edgesUtils.valueToStr(edges.aut11_v1_));
        state += edgesUtils.fmt(', aut12.t=%s', edgesUtils.valueToStr(edges.aut12_t_));
        state += edgesUtils.fmt(', aut12.t\'=%s', edgesUtils.valueToStr(edges.aut12_t_deriv()));
        state += edgesUtils.fmt(', aut12.td=%s', edgesUtils.valueToStr(edges.aut12_td_));
        state += edgesUtils.fmt(', aut12.u=%s', edgesUtils.valueToStr(edges.aut12_u_));
        state += edgesUtils.fmt(', aut12.u\'=%s', edgesUtils.valueToStr(edges.aut12_u_deriv()));
        state += edgesUtils.fmt(', aut12.x=%s', edgesUtils.valueToStr(edges.aut12_x_));
        state += edgesUtils.fmt(', aut12.y=%s', edgesUtils.valueToStr(edges.aut12_y_));
        state += edgesUtils.fmt(', aut12.z=%s', edgesUtils.valueToStr(edges.aut12_z_));
        state += edgesUtils.fmt(', aut13.x=%s', edgesUtils.valueToStr(edges.aut13_x_));
        state += edgesUtils.fmt(', aut13.y=%s', edgesUtils.valueToStr(edges.aut13_y_));
        state += edgesUtils.fmt(', aut13.z=%s', edgesUtils.valueToStr(edges.aut13_z_));
        return state;
    }


    /**
     * Evaluates algebraic variable "aut12.v".
     *
     * @return The evaluation result.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    aut12_v_() {
        try {
            return edgesUtils.addReal(edges.aut12_x_, edges.aut12_y_);
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate algebraic variable \"aut12.v\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "aut12.w".
     *
     * @return The evaluation result.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    aut12_w_() {
        try {
            return edgesUtils.addReal(edges.aut12_v_(), edges.aut12_z_);
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate algebraic variable \"aut12.w\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "aut13.v".
     *
     * @return The evaluation result.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    aut13_v_() {
        try {
            return edgesUtils.addReal(edges.aut13_x_, edges.aut13_y_);
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate algebraic variable \"aut13.v\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "aut13.w".
     *
     * @return The evaluation result.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    aut13_w_() {
        try {
            return edgesUtils.addReal(edges.aut13_v_(), edges.aut13_z_);
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate algebraic variable \"aut13.w\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates derivative of continuous variable "aut03.c".
     *
     * @return The evaluation result.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    aut03_c_deriv() {
        try {
            return 1.0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the derivative of continuous variable \"aut03.c\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates derivative of continuous variable "aut07.x".
     *
     * @return The evaluation result.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    aut07_x_deriv() {
        try {
            return 1.0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the derivative of continuous variable \"aut07.x\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates derivative of continuous variable "aut07.y".
     *
     * @return The evaluation result.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    aut07_y_deriv() {
        try {
            return 2.0;
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the derivative of continuous variable \"aut07.y\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates derivative of continuous variable "aut12.t".
     *
     * @return The evaluation result.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    aut12_t_deriv() {
        try {
            return edgesUtils.addReal(edges.aut12_x_, edges.aut12_y_);
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the derivative of continuous variable \"aut12.t\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates derivative of continuous variable "aut12.u".
     *
     * @return The evaluation result.
     * @throws {edgesException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    aut12_u_deriv() {
        try {
            return edgesUtils.addReal(edges.aut12_t_deriv(), edges.aut12_z_);
        } catch (e) {
            if (e instanceof edgesException) {
                e = new edgesException("Failed to evaluate the derivative of continuous variable \"aut12.u\".", e);
            }
            throw e;
        }
    }


    /**
     * Print output for all relevant print declarations.
     *
     * @param idx The 0-based event index of the transition, or '-2' for
     *      time transitions, or '-3' for the 'initial' transition.
     * @param pre Whether to print output for the pre/source state of the
     *      transition ('true') or for the post/target state of the
     *      transition ('false').
     * @throws {edgesException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    printOutput(idx, pre) {
        // No print declarations.
    }

    /**
     * Informs that new print output is available.
     *
     * @param text The text being printed.
     * @param target The file or special target to which text is to be printed.
     *      If printed to a file, an absolute or relative local file system
     *      path is given. Paths may contain both '/' and '\\'
     *      as file separators. Use {@link edgesUtils#normalizePrintTarget}
     *      to normalize the path to use '/' file separators. There are two
     *      special targets: ':stdout' to print to the standard output stream,
     *      and ':stderr' to print to the standard error stream.
     */
    infoPrintOutput(text, target) {
        if (target == ':stdout') {
            edges.log(text);
        } else if (target == ':stderr') {
            edges.error(text);
        } else {
            var path = edgesUtils.normalizePrintTarget(target);
            edges.infoPrintOutput(path + ': ' + text, ':stdout');
        }
    }
}
