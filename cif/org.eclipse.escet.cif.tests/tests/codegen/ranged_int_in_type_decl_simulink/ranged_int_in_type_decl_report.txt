Input/output report of the ranged_int_in_type_decl.cif SFunction.

During code generation, CIF variables are made available in the Simulink vectors.
This report lists the variables in each vector, along with their index number.

Modes
-----
No variables are available here.

Continuous states
-----------------
time 1

Inputs
------
No variables are available here.

Outputs
-------
Test.item 1
