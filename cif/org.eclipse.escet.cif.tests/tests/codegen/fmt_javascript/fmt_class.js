/** Tuples. */


            /** Tuple class for CIF tuple type representative "tuple(int[1..1]; list[1] int[2..2])". */
            class CifTuple_T2ILI {
                /** The 1st field. */
                _field0;

                /** The 2nd field. */
                _field1;

                /**
                 * Constructor for the {@link CifTuple_T2ILI} class.
                 *
                 * @param _field0 The 1st field.
                 * @param _field1 The 2nd field.
                 */
                constructor(_field0, _field1) {
                    this._field0 = _field0;
                    this._field1 = _field1;
                }

                copy() {
                    return new CifTuple_T2ILI(this._field0, this._field1);
                }

                toString() {
                    var rslt = '(';
                    rslt += fmtUtils.valueToStr(this._field0);
                    rslt += ', ';
                    rslt += fmtUtils.valueToStr(this._field1);
                    rslt += ')';
                    return rslt;
                }
            }

/** fmt code generated from a CIF specification. */
class fmt_class {
    /** fmtEnum declaration. It contains the single merged enum from the CIF model. */
    fmtEnum = Object.freeze({
        /** Literal "A". */
        _A: Symbol("A"),

        /** Literal "B". */
        _B: Symbol("B")
    })

    /** Should execution timing information be provided? */
    doInfoExec = true;

    /** Should executed event information be provided? */
    doInfoEvent = true;

    /** Should print output be provided? */
    doInfoPrintOutput = true;

    /** Should state output be provided? */
    doStateOutput = false;

    /** Should transition output be provided? */
    doTransitionOutput = true;

    /** Whether the constants have been initialized already. */
    constantsInitialized = false;

    /** Whether the SVG copy and move declarations have been applied already. */
    svgCopiesAndMovesApplied = false;

    /** Whether this is the first time the code is (to be) executed. */
    firstExec;

    /** The names of all the events. */
    EVENT_NAMES = [

    ];


    /** Variable 'time', tracks elapsed time for a session. */
    time;

    /**
     * The frequency in times per second, that the code should
     * be executed (if positive), or execute as fast as possible, that is
     * as many times per second as possible (if negative or zero).
     */
    frequency = 60;

    /**
     * Whether the next execution is the first execution of the session.
     * Used to initialize time-related variables for starting, pausing,
     * resuming or resetting each session.
     */
    first;

    /**
     * Whether the simulation is currently running, and should process
     * user input, or is paused.
     */
    playing;

    /** The start time of the current session. */
    startMilli;

    /**
     * The targeted end time of the current/next cycle, to ensure
     * that the duration of the cycle matches with the configured
     * frequency.
     */
    targetMilli;



    /** Input variable "b". */
    b_;

    /** Input variable "i". */
    i_;

    /** Input variable "r". */
    r_;

    /** SVG output elements. */


    /**
     * SVG input queue with functions for handling clicked SVG
     * input elements, from first clicked at the head of the array
     * to last clicked at the tail of the array.
     */
    svgInQueue;

    /**
     * The SVG input id corresponding to the SVG input element that
     * was clicked. Is 'null' if no SVG input element was clicked so
     * far, or all clicks have already been processed.
     */
    svgInId;

    /**
     * The 0-based index of the event corresponding to the SVG input
     * element that was clicked. Is '-1' if no SVG input element was
     * clicked so far, or all clicks have already been processed.
     */
    svgInEvent;

    /** SVG input click event handlers. */


    /** SVG input event setters. */


    /** Starts the simulation. */
    start() {
        if (!this.playing) {
            this.playing = true;
            this.exec();
        }
    }

    /** Stops the simulation. */
    stop() {
        if (this.playing) {
            this.playing = false;
        }
    }

    /** Resets the object to its initial state. */
    reset() {
        this.stop();
        this.firstExec = true;
        this.time = 0.0;
        this.first = true;
        this.timePaused = null;
        this.initState();
        this.initUI();
        this.updateUI();
    }

    /**
     * Execute the code once. Inputs are read, transitions are executed until
     * none are possible, outputs are written, etc.
     *
     * @param newTime The time in seconds, since the start of the first
     *      execution.
     * @throws {fmtException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execOnce(newTime) {
        // Pre execution notification.
        this.preExec();

        // Update values of input variables.
        this.updateInputs();

        // Initialize the state.
        if (this.firstExec) {
            this.initState();
        }

        // Calculate time delta.
        var delta = newTime - this.time;

        // Update values of continuous variables.
        if (!this.firstExec) {
                        // No continuous variables, except variable 'time'.
        }

        // Update time.
        this.time = newTime;

        // Apply print declarations.
        if (this.firstExec) {
            // For 'initial' transition.
            if (this.doInfoPrintOutput) this.printOutput(-3, true);
            if (this.doInfoPrintOutput) this.printOutput(-3, false);
            if (this.doStateOutput) fmt.log('Initial state: ' + fmt.getStateText());
            if (this.doStateOutput || this.doTransitionOutput) this.log('');

        } else {
            // For 'post' of time transition.
            if (this.doInfoPrintOutput) this.printOutput(-2, false);
        }

        // Execute environment events and SVG input events as long as they are possible, emptying the SVG input queue.
        while (true) {
            // Handle next element from SVG input queue, if not already already processing one.
            if (this.svgInEvent == -1 && this.svgInQueue.length > 0) {
                var func = this.svgInQueue.shift(); // Remove head of the queue.
                func(); // Call function, to set the event to allow.
            }

            // Try to execute an SVG input mapping with updates for each environment event, and an edge for each SVG
            // input event.
            var anythingExecuted = false;



            // Stop if no SVG input mapping and no edge was executed, and no more SVG input clicks are to be processed.
            if (!anythingExecuted && this.svgInQueue.length == 0) {
                break;
            }
        }

        // Make sure all outstanding SVG input clicks have been processed.
        console.assert(this.svgInEvent == -1 && this.svgInQueue.length == 0);

        // Execute uncontrollable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Execute controllable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Apply print declarations for 'pre' of time transition.
        if (this.doInfoPrintOutput) this.printOutput(-2, true);

        // Post execution notification.
        this.postExec();

        // Done.
        this.firstExec = false;
    }

    /**
     * Calls {@link #execWhile}, which repeatedly {@link #execOnce executes the code}.
     *
     * @throws {fmtException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    exec() {
        this.execWhile(1);
    }

    /**
     * Repeatedly {@link #execOnce executes the code}.
     *
     * @param delay The delay before executing, in milliseconds.
     *
     * @throws {fmtException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execWhile(delay) {
        setTimeout(
            function () {
                // Pre execution timing.
                var now = Date.now();
                var preMilli = now;

                // On first execution, initialize variables for timing.
                if (fmt.first) {
                    fmt.first = false;
                    fmt.startMilli = now;
                    fmt.targetMilli = fmt.startMilli;
                    preMilli = fmt.startMilli;
                }

                // Handle pausing/playing.
                if (!fmt.playing) {
                    fmt.timePaused = now;
                    return;
                }

                if (fmt.timePaused) {
                    fmt.startMilli += (now - fmt.timePaused);
                    fmt.targetMilli += (now - fmt.timePaused);
                    fmt.timePaused = null;
                }

                // Get cycle time and current 'time'.
                var frequency = fmt.frequency;
                var cycleMilli = (frequency <= 0) ? -1 : 1e3 / frequency;
                var timeMilli = preMilli - fmt.startMilli;

                // Execute once.
                fmt.execOnce(timeMilli / 1e3);

                // Post execution timing.
                var postMilli = Date.now();
                var duration = postMilli - preMilli;
                if (fmt.doInfoExec) {
                    fmt.infoExec(duration, cycleMilli);
                }

                // Ensure frequency.
                var remainderMilli = 0;
                if (frequency > 0) {
                    fmt.targetMilli += cycleMilli;
                    remainderMilli = fmt.targetMilli - postMilli;
                }

                // Execute again.
                fmt.execWhile(remainderMilli > 0 ? remainderMilli : 0);
            },
        delay);
    }


    /**
     * Initializes the state.
     *
     * @throws {fmtException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    initState() {
        // Initialize constants, if not yet done so.
        if (!this.constantsInitialized) {
            this.constantsInitialized = true;

        }

        // Initialize SVG input.
        this.svgInQueue = [];
        this.svgInId = null;
        this.svgInEvent = -1;


        // CIF model state variables.
        // No state variables, except variable 'time'.
    }

    /**
     * Initializes the user interface, either when loading the page
     * or when resetting the simulation.
     */
    initUI() {


        // Apply SVG copies and moves, if not done so before.
        if (!this.svgCopiesAndMovesApplied) {
            this.svgCopiesAndMovesApplied = true;

            // Apply SVG copy declarations.


            // Apply SVG move declarations.

        }

        // Prepare SVG output.


        // Prepare SVG input.

    }

    /**
     * Updates the user interface based on the latest state of
     * the model. Is called at the end of each cycle.
     *
     * @throws {fmtException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    updateUI() {
        // Apply CIF/SVG output mappings.

    }

    /**
     * Updates the values of the input variables. Other variables from the
     * state may not be accessed or modified.
     */
    updateInputs() {
        // Nothing is done here by default.
    }

    /** Logs a normal message. */
    log(message) {
        console.log(message);

    }

    /** Logs an warning message. */
    warning(message) {
        console.log(message);

    }

    /** Logs an error message. */
    error(message) {
        console.log(message);

    }

    /** Logs a runtime error of type fmtException. */
    runtimeError(e, isCause = false) {
        console.assert(e instanceof fmtException);
        if (isCause) {
            this.error("CAUSE: " + e.message);
        } else {
            this.error("ERROR: " + e.message);
        }
        if (e.cause) {
            this.runtimeError(e.cause, true);
        }
    }

    /**
     * Informs about the duration of a single execution.
     *
     * @param duration The duration of the execution, in milliseconds.
     * @param cycleTime The desired maximum duration of the execution, in
     *      milliseconds, or '-1' if not available.
     */
    infoExec(duration, cycleTime) {
        // Nothing is done here by default.
    }

    /**
     * Informs that an event will be or has been executed.
     *
     * @param idx The 0-based index of the event.
     * @param pre Whether the event will be executed ('true') or has
     *      been executed ('false').
     */
    infoEvent(idx, pre) {
        if (pre) {
            if (this.doTransitionOutput) fmt.log(fmtUtils.fmt('Transition: event %s', fmt.getEventName(idx)));
        } else {
            if (this.doStateOutput) fmt.log('State: ' + fmt.getStateText());
        }
    }

    /**
     * Informs that the code is about to be executed. For the
     * {@link #firstExec} the state has not yet been initialized, except for
     * {@link #time}.
     */
    preExec() {
        // Nothing is done here by default.
    }

    /** Informs that the code was just executed. */
    postExec() {
        this.updateUI();
    }

    /**
     * Returns the name of an event.
     *
     * @param idx The 0-based index of the event.
     * @return The name of the event.
     */
    getEventName(idx) {
        return this.EVENT_NAMES[idx];
    }

    /**
     * Returns a single-line textual representation of the model state.
     *
     * @return The single-line textual representation of the model state.
     */
    getStateText() {
        var state = fmtUtils.fmt('time=%s', fmt.time);

        return state;
    }


    /**
     * Evaluates algebraic variable "s".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    s_() {
        try {
            return "a\nb\tc\\d\"e";
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"s\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "ls".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    ls_() {
        try {
            return [fmt.s_()];
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"ls\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "s1".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    s1_() {
        try {
            return "a";
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"s1\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "s2".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    s2_() {
        try {
            return "a\nb\tc\\d\"e f%g%%h%si%fj";
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"s2\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "s3".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    s3_() {
        try {
            return "a\nb\tc\\d\"e f%g%%h%si%fj";
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"s3\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "neg12345".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    neg12345_() {
        try {
            return -(12345);
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"neg12345\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "r456".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    r456_() {
        try {
            return 4.56;
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"r456\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "r_zero".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    r_zero_() {
        try {
            return 0.0;
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"r_zero\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "s0".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    s0_() {
        try {
            return "a";
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"s0\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "i0".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    i0_() {
        try {
            return 1;
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"i0\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "b0".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    b0_() {
        try {
            return true;
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"b0\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "t0".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    t0_() {
        try {
            return new CifTuple_T2ILI(1, [2]);
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"t0\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "r0".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    r0_() {
        try {
            return 1.23456E7;
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"r0\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "s00".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    s00_() {
        try {
            return "1.23456e7";
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"s00\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "l0".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    l0_() {
        try {
            return ["a", "b"];
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"l0\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "e0".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    e0_() {
        try {
            return fmt.fmtEnum._A;
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"e0\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "ii1".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    ii1_() {
        try {
            return 1;
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"ii1\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "ii2".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    ii2_() {
        try {
            return 2;
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"ii2\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "ii3".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    ii3_() {
        try {
            return 3;
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"ii3\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "ii4".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    ii4_() {
        try {
            return 4;
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"ii4\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "ii5".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    ii5_() {
        try {
            return 5;
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"ii5\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "rr6".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    rr6_() {
        try {
            return 6.0;
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"rr6\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "ii7".
     *
     * @return The evaluation result.
     * @throws {fmtException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    ii7_() {
        try {
            return 7;
        } catch (e) {
            if (e instanceof fmtException) {
                e = new fmtException("Failed to evaluate algebraic variable \"ii7\".", e);
            }
            throw e;
        }
    }



    /**
     * Print output for all relevant print declarations.
     *
     * @param idx The 0-based event index of the transition, or '-2' for
     *      time transitions, or '-3' for the 'initial' transition.
     * @param pre Whether to print output for the pre/source state of the
     *      transition ('true') or for the post/target state of the
     *      transition ('false').
     * @throws {fmtException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    printOutput(idx, pre) {
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%b", fmt.b_);
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%B", fmt.b_);
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%d", fmt.i_);
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%x", fmt.i_);
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%X", fmt.i_);
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%-15s", fmtUtils.valueToStr(fmt.b_));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%f", fmt.r_);
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%,.3f", fmt.r_);
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%+.3e", fmt.r_);
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%+.3E", fmt.r_);
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%,.3g", fmt.r_);
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%,.3G", fmt.r_);
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%s", fmtUtils.valueToStr(fmt.b_));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%s", fmtUtils.valueToStr(fmt.i_));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%s", fmtUtils.valueToStr(fmt.r_));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%s", fmt.s_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%s", fmtUtils.valueToStr(fmt.ls_()));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%,23.5f", fmt.r_);
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("% 25.3f", fmt.r_);
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%+0,25.3f", fmt.r_);
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("a%%b%sc", fmtUtils.valueToStr(fmt.r_));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("a%sb%sc%d", fmtUtils.valueToStr(fmt.i_), fmtUtils.valueToStr(fmt.b_), fmt.i_);
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%s", fmt.s1_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%s", fmt.s2_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%s # 1\n2\t3\\4\"5 6%%7", fmt.s3_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("_%-+,8d_", fmt.neg12345_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("_%-+,8d_", fmt.neg12345_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("_%-+,8d_", fmt.neg12345_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("_%-+,8d_", fmt.neg12345_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("_%-+,8d_", fmt.neg12345_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("_%-+,8d_", fmt.neg12345_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("#%.0f#", fmt.r456_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("#%.00f#", fmt.r456_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("#%.000f#", fmt.r456_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("#%.1f#", fmt.r456_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("#%.01f#", fmt.r456_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("#%.2f#", fmt.r456_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("@%.0e@", fmt.r456_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("@%.0E@", fmt.r456_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("@%.0f@", fmt.r456_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("@%.0g@", fmt.r456_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("@%.0G@", fmt.r456_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%.4g", fmtUtils.addReal(fmt.r_zero_(), 1.2345678E9));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%.4g", fmtUtils.addReal(fmt.r_zero_(), 1.2345678E8));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%.4g", fmtUtils.addReal(fmt.r_zero_(), 1.2345678E7));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%.4g", fmtUtils.addReal(fmt.r_zero_(), 1234567.8));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%.4g", fmtUtils.addReal(fmt.r_zero_(), 123456.78));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%.4g", fmtUtils.addReal(fmt.r_zero_(), 12345.678));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%.4g", fmtUtils.addReal(fmt.r_zero_(), 1234.5678));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%.4g", fmtUtils.addReal(fmt.r_zero_(), 123.45678));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%.4g", fmtUtils.addReal(fmt.r_zero_(), 12.345678));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%.4g", fmtUtils.addReal(fmt.r_zero_(), 1.2345678));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%.4g", fmtUtils.addReal(fmt.r_zero_(), 0.12345678));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%.4g", fmtUtils.addReal(fmt.r_zero_(), 0.012345678));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%.4g", fmtUtils.addReal(fmt.r_zero_(), 0.0012345678));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%.4g", fmtUtils.addReal(fmt.r_zero_(), 1.2345678E-4));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%.4g", fmtUtils.addReal(fmt.r_zero_(), 1.2345678E-5));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%s", fmt.s0_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%s", fmtUtils.valueToStr(fmt.i0_()));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%s", fmtUtils.valueToStr(fmt.b0_()));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%s", fmtUtils.valueToStr(fmt.t0_()));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%s", fmtUtils.valueToStr(fmt.r0_()));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%s", fmt.s00_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%s", fmtUtils.valueToStr(fmt.l0_()));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%s", fmtUtils.valueToStr(fmt.e0_()));
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
        if (true) {
            if (!pre) {
                var text;
                try {
                    text = fmtUtils.fmt("%s %s %s %s %s %f", fmtUtils.valueToStr(fmt.ii1_()), fmtUtils.valueToStr(fmt.ii4_()), fmtUtils.valueToStr(fmt.ii2_()), fmtUtils.valueToStr(fmt.rr6_()), fmtUtils.valueToStr(fmt.ii2_()), fmt.rr6_());
                } catch (e) {
                    if (e instanceof fmtException) {
                        e = new fmtException("Failed to evaluate print declaration \"post\" text.", e);
                        this.runtimeError(e);
                    }
                    throw e;
                }
                this.infoPrintOutput(text, ":stdout");
            }
        }
    }

    /**
     * Informs that new print output is available.
     *
     * @param text The text being printed.
     * @param target The file or special target to which text is to be printed.
     *      If printed to a file, an absolute or relative local file system
     *      path is given. Paths may contain both '/' and '\\'
     *      as file separators. Use {@link fmtUtils#normalizePrintTarget}
     *      to normalize the path to use '/' file separators. There are two
     *      special targets: ':stdout' to print to the standard output stream,
     *      and ':stderr' to print to the standard error stream.
     */
    infoPrintOutput(text, target) {
        if (target == ':stdout') {
            fmt.log(text);
        } else if (target == ':stderr') {
            fmt.error(text);
        } else {
            var path = fmtUtils.normalizePrintTarget(target);
            fmt.infoPrintOutput(path + ': ' + text, ':stdout');
        }
    }
}
