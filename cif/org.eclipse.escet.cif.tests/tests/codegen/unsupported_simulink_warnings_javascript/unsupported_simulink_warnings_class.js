/** Tuples. */


            /** Tuple class for CIF tuple type representative "tuple(int x; int y)". */
            class CifTuple_T2II {
                /** The 1st field. */
                _field0;

                /** The 2nd field. */
                _field1;

                /**
                 * Constructor for the {@link CifTuple_T2II} class.
                 *
                 * @param _field0 The 1st field.
                 * @param _field1 The 2nd field.
                 */
                constructor(_field0, _field1) {
                    this._field0 = _field0;
                    this._field1 = _field1;
                }

                copy() {
                    return new CifTuple_T2II(this._field0, this._field1);
                }

                toString() {
                    var rslt = '(';
                    rslt += unsupported_simulink_warningsUtils.valueToStr(this._field0);
                    rslt += ', ';
                    rslt += unsupported_simulink_warningsUtils.valueToStr(this._field1);
                    rslt += ')';
                    return rslt;
                }
            }

/** unsupported_simulink_warnings code generated from a CIF specification. */
class unsupported_simulink_warnings_class {
    /** unsupported_simulink_warningsEnum declaration. It contains the single merged enum from the CIF model. */
    unsupported_simulink_warningsEnum = Object.freeze({
        /** Literal "__some_dummy_enum_literal". */
        ___some_dummy_enum_literal: Symbol("__some_dummy_enum_literal")
    })

    /** Should execution timing information be provided? */
    doInfoExec = true;

    /** Should executed event information be provided? */
    doInfoEvent = true;

    /** Should print output be provided? */
    doInfoPrintOutput = true;

    /** Should state output be provided? */
    doStateOutput = false;

    /** Should transition output be provided? */
    doTransitionOutput = true;

    /** Whether the constants have been initialized already. */
    constantsInitialized = false;

    /** Whether the SVG copy and move declarations have been applied already. */
    svgCopiesAndMovesApplied = false;

    /** Whether this is the first time the code is (to be) executed. */
    firstExec;

    /** The names of all the events. */
    EVENT_NAMES = [
        "a.e"
    ];


    /** Variable 'time', tracks elapsed time for a session. */
    time;

    /**
     * The frequency in times per second, that the code should
     * be executed (if positive), or execute as fast as possible, that is
     * as many times per second as possible (if negative or zero).
     */
    frequency = 60;

    /**
     * Whether the next execution is the first execution of the session.
     * Used to initialize time-related variables for starting, pausing,
     * resuming or resetting each session.
     */
    first;

    /**
     * Whether the simulation is currently running, and should process
     * user input, or is paused.
     */
    playing;

    /** The start time of the current session. */
    startMilli;

    /**
     * The targeted end time of the current/next cycle, to ensure
     * that the duration of the cycle matches with the configured
     * frequency.
     */
    targetMilli;


    /** Discrete variable "a.d1". */
    a_d1_;

    /** Discrete variable "a.d2". */
    a_d2_;

    /** Discrete variable "a.d3". */
    a_d3_;

    /** Discrete variable "a.d4". */
    a_d4_;

    /** Discrete variable "a.d5". */
    a_d5_;

    /** Discrete variable "a.d6". */
    a_d6_;

    /** Discrete variable "a.d7". */
    a_d7_;


    /** SVG output elements. */


    /**
     * SVG input queue with functions for handling clicked SVG
     * input elements, from first clicked at the head of the array
     * to last clicked at the tail of the array.
     */
    svgInQueue;

    /**
     * The SVG input id corresponding to the SVG input element that
     * was clicked. Is 'null' if no SVG input element was clicked so
     * far, or all clicks have already been processed.
     */
    svgInId;

    /**
     * The 0-based index of the event corresponding to the SVG input
     * element that was clicked. Is '-1' if no SVG input element was
     * clicked so far, or all clicks have already been processed.
     */
    svgInEvent;

    /** SVG input click event handlers. */


    /** SVG input event setters. */


    /** Starts the simulation. */
    start() {
        if (!this.playing) {
            this.playing = true;
            this.exec();
        }
    }

    /** Stops the simulation. */
    stop() {
        if (this.playing) {
            this.playing = false;
        }
    }

    /** Resets the object to its initial state. */
    reset() {
        this.stop();
        this.firstExec = true;
        this.time = 0.0;
        this.first = true;
        this.timePaused = null;
        this.initState();
        this.initUI();
        this.updateUI();
    }

    /**
     * Execute the code once. Inputs are read, transitions are executed until
     * none are possible, outputs are written, etc.
     *
     * @param newTime The time in seconds, since the start of the first
     *      execution.
     * @throws {unsupported_simulink_warningsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execOnce(newTime) {
        // Pre execution notification.
        this.preExec();

        // Update values of input variables.
        this.updateInputs();

        // Initialize the state.
        if (this.firstExec) {
            this.initState();
        }

        // Calculate time delta.
        var delta = newTime - this.time;

        // Update values of continuous variables.
        if (!this.firstExec) {
                        // No continuous variables, except variable 'time'.
        }

        // Update time.
        this.time = newTime;

        // Apply print declarations.
        if (this.firstExec) {
            // For 'initial' transition.
            if (this.doInfoPrintOutput) this.printOutput(-3, true);
            if (this.doInfoPrintOutput) this.printOutput(-3, false);
            if (this.doStateOutput) unsupported_simulink_warnings.log('Initial state: ' + unsupported_simulink_warnings.getStateText());
            if (this.doStateOutput || this.doTransitionOutput) this.log('');

        } else {
            // For 'post' of time transition.
            if (this.doInfoPrintOutput) this.printOutput(-2, false);
        }

        // Execute environment events and SVG input events as long as they are possible, emptying the SVG input queue.
        while (true) {
            // Handle next element from SVG input queue, if not already already processing one.
            if (this.svgInEvent == -1 && this.svgInQueue.length > 0) {
                var func = this.svgInQueue.shift(); // Remove head of the queue.
                func(); // Call function, to set the event to allow.
            }

            // Try to execute an SVG input mapping with updates for each environment event, and an edge for each SVG
            // input event.
            var anythingExecuted = false;



            // Stop if no SVG input mapping and no edge was executed, and no more SVG input clicks are to be processed.
            if (!anythingExecuted && this.svgInQueue.length == 0) {
                break;
            }
        }

        // Make sure all outstanding SVG input clicks have been processed.
        console.assert(this.svgInEvent == -1 && this.svgInQueue.length == 0);

        // Execute uncontrollable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;


            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Execute controllable edges as long as they are possible.
        while (true) {
            // Try to execute an edge for each event.
            var edgeExecuted = false;

            // Event "a.e".
            anythingExecuted |= this.execEdge0();

            // Stop if no edge was executed.
            if (!edgeExecuted) {
                break;
            }
        }

        // Apply print declarations for 'pre' of time transition.
        if (this.doInfoPrintOutput) this.printOutput(-2, true);

        // Post execution notification.
        this.postExec();

        // Done.
        this.firstExec = false;
    }

    /**
     * Calls {@link #execWhile}, which repeatedly {@link #execOnce executes the code}.
     *
     * @throws {unsupported_simulink_warningsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    exec() {
        this.execWhile(1);
    }

    /**
     * Repeatedly {@link #execOnce executes the code}.
     *
     * @param delay The delay before executing, in milliseconds.
     *
     * @throws {unsupported_simulink_warningsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    execWhile(delay) {
        setTimeout(
            function () {
                // Pre execution timing.
                var now = Date.now();
                var preMilli = now;

                // On first execution, initialize variables for timing.
                if (unsupported_simulink_warnings.first) {
                    unsupported_simulink_warnings.first = false;
                    unsupported_simulink_warnings.startMilli = now;
                    unsupported_simulink_warnings.targetMilli = unsupported_simulink_warnings.startMilli;
                    preMilli = unsupported_simulink_warnings.startMilli;
                }

                // Handle pausing/playing.
                if (!unsupported_simulink_warnings.playing) {
                    unsupported_simulink_warnings.timePaused = now;
                    return;
                }

                if (unsupported_simulink_warnings.timePaused) {
                    unsupported_simulink_warnings.startMilli += (now - unsupported_simulink_warnings.timePaused);
                    unsupported_simulink_warnings.targetMilli += (now - unsupported_simulink_warnings.timePaused);
                    unsupported_simulink_warnings.timePaused = null;
                }

                // Get cycle time and current 'time'.
                var frequency = unsupported_simulink_warnings.frequency;
                var cycleMilli = (frequency <= 0) ? -1 : 1e3 / frequency;
                var timeMilli = preMilli - unsupported_simulink_warnings.startMilli;

                // Execute once.
                unsupported_simulink_warnings.execOnce(timeMilli / 1e3);

                // Post execution timing.
                var postMilli = Date.now();
                var duration = postMilli - preMilli;
                if (unsupported_simulink_warnings.doInfoExec) {
                    unsupported_simulink_warnings.infoExec(duration, cycleMilli);
                }

                // Ensure frequency.
                var remainderMilli = 0;
                if (frequency > 0) {
                    unsupported_simulink_warnings.targetMilli += cycleMilli;
                    remainderMilli = unsupported_simulink_warnings.targetMilli - postMilli;
                }

                // Execute again.
                unsupported_simulink_warnings.execWhile(remainderMilli > 0 ? remainderMilli : 0);
            },
        delay);
    }

    /**
     * Execute code for edge with index 0 and event "a.e".
     *
     * @return 'true' if the edge was executed, 'false' otherwise.
     * @throws {unsupported_simulink_warningsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    execEdge0() {
        try {
            var guard;
            try {
                guard = ((((unsupported_simulink_warnings.a_d1_)._field0) > (0)) || (((unsupported_simulink_warningsUtils.sizeString(unsupported_simulink_warnings.a_d2_)) > (0)) || (((unsupported_simulink_warningsUtils.projectList(unsupported_simulink_warnings.a_d3_, 0))._field0) > (0)))) || ((((unsupported_simulink_warningsUtils.sizeString(unsupported_simulink_warningsUtils.projectList(unsupported_simulink_warnings.a_d4_, 0))) > (0)) || (((unsupported_simulink_warningsUtils.projectList(unsupported_simulink_warningsUtils.projectList(unsupported_simulink_warnings.a_d5_, 0), 0))._field0) > (0))) || (((unsupported_simulink_warningsUtils.sizeString(unsupported_simulink_warningsUtils.projectList(unsupported_simulink_warningsUtils.projectList(unsupported_simulink_warnings.a_d6_, 0), 0))) > (0)) || (unsupported_simulink_warningsUtils.projectList(unsupported_simulink_warningsUtils.projectList(unsupported_simulink_warningsUtils.projectList(unsupported_simulink_warnings.a_d7_, 0), 0), 0))));
            } catch (e) {
                if (e instanceof unsupported_simulink_warningsException) {
                    e = new unsupported_simulink_warningsException("Failed to evaluate guard.", e);
                }
                throw e;
            }

            if (!guard) {
                return false;
            }

            if (this.doInfoPrintOutput) this.printOutput(0, true);
            if (this.doInfoEvent) this.infoEvent(0, true);

            if (this.doInfoEvent) this.infoEvent(0, false);
            if (this.doInfoPrintOutput) this.printOutput(0, false);
            if (this.doStateOutput || this.doTransitionOutput) this.log('');
            return true;
        } catch (e) {
            if (e instanceof unsupported_simulink_warningsException) {
                e = new unsupported_simulink_warningsException("Failed to (try to) execute event \"a.e\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Initializes the state.
     *
     * @throws {unsupported_simulink_warningsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    initState() {
        // Initialize constants, if not yet done so.
        if (!this.constantsInitialized) {
            this.constantsInitialized = true;

        }

        // Initialize SVG input.
        this.svgInQueue = [];
        this.svgInId = null;
        this.svgInEvent = -1;


        // CIF model state variables.
        try {
            unsupported_simulink_warnings.a_d1_ = new CifTuple_T2II(0, 0);
        } catch (e) {
            if (e instanceof unsupported_simulink_warningsException) {
                e = new unsupported_simulink_warningsException("Failed to evaluate the initial value of variable \"a.d1\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            unsupported_simulink_warnings.a_d2_ = "";
        } catch (e) {
            if (e instanceof unsupported_simulink_warningsException) {
                e = new unsupported_simulink_warningsException("Failed to evaluate the initial value of variable \"a.d2\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            unsupported_simulink_warnings.a_d3_ = [new CifTuple_T2II(0, 0)];
        } catch (e) {
            if (e instanceof unsupported_simulink_warningsException) {
                e = new unsupported_simulink_warningsException("Failed to evaluate the initial value of variable \"a.d3\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            unsupported_simulink_warnings.a_d4_ = [""];
        } catch (e) {
            if (e instanceof unsupported_simulink_warningsException) {
                e = new unsupported_simulink_warningsException("Failed to evaluate the initial value of variable \"a.d4\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            unsupported_simulink_warnings.a_d5_ = [[new CifTuple_T2II(0, 0)]];
        } catch (e) {
            if (e instanceof unsupported_simulink_warningsException) {
                e = new unsupported_simulink_warningsException("Failed to evaluate the initial value of variable \"a.d5\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            unsupported_simulink_warnings.a_d6_ = [[""]];
        } catch (e) {
            if (e instanceof unsupported_simulink_warningsException) {
                e = new unsupported_simulink_warningsException("Failed to evaluate the initial value of variable \"a.d6\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
        try {
            unsupported_simulink_warnings.a_d7_ = [[[false]]];
        } catch (e) {
            if (e instanceof unsupported_simulink_warningsException) {
                e = new unsupported_simulink_warningsException("Failed to evaluate the initial value of variable \"a.d7\".", e);
                this.runtimeError(e);
            }
            throw e;
        }
    }

    /**
     * Initializes the user interface, either when loading the page
     * or when resetting the simulation.
     */
    initUI() {


        // Apply SVG copies and moves, if not done so before.
        if (!this.svgCopiesAndMovesApplied) {
            this.svgCopiesAndMovesApplied = true;

            // Apply SVG copy declarations.


            // Apply SVG move declarations.

        }

        // Prepare SVG output.


        // Prepare SVG input.

    }

    /**
     * Updates the user interface based on the latest state of
     * the model. Is called at the end of each cycle.
     *
     * @throws {unsupported_simulink_warningsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    updateUI() {
        // Apply CIF/SVG output mappings.

    }

    /**
     * Updates the values of the input variables. Other variables from the
     * state may not be accessed or modified.
     */
    updateInputs() {
        // Nothing is done here by default.
    }

    /** Logs a normal message. */
    log(message) {
        console.log(message);

    }

    /** Logs an warning message. */
    warning(message) {
        console.log(message);

    }

    /** Logs an error message. */
    error(message) {
        console.log(message);

    }

    /** Logs a runtime error of type unsupported_simulink_warningsException. */
    runtimeError(e, isCause = false) {
        console.assert(e instanceof unsupported_simulink_warningsException);
        if (isCause) {
            this.error("CAUSE: " + e.message);
        } else {
            this.error("ERROR: " + e.message);
        }
        if (e.cause) {
            this.runtimeError(e.cause, true);
        }
    }

    /**
     * Informs about the duration of a single execution.
     *
     * @param duration The duration of the execution, in milliseconds.
     * @param cycleTime The desired maximum duration of the execution, in
     *      milliseconds, or '-1' if not available.
     */
    infoExec(duration, cycleTime) {
        // Nothing is done here by default.
    }

    /**
     * Informs that an event will be or has been executed.
     *
     * @param idx The 0-based index of the event.
     * @param pre Whether the event will be executed ('true') or has
     *      been executed ('false').
     */
    infoEvent(idx, pre) {
        if (pre) {
            if (this.doTransitionOutput) unsupported_simulink_warnings.log(unsupported_simulink_warningsUtils.fmt('Transition: event %s', unsupported_simulink_warnings.getEventName(idx)));
        } else {
            if (this.doStateOutput) unsupported_simulink_warnings.log('State: ' + unsupported_simulink_warnings.getStateText());
        }
    }

    /**
     * Informs that the code is about to be executed. For the
     * {@link #firstExec} the state has not yet been initialized, except for
     * {@link #time}.
     */
    preExec() {
        // Nothing is done here by default.
    }

    /** Informs that the code was just executed. */
    postExec() {
        this.updateUI();
    }

    /**
     * Returns the name of an event.
     *
     * @param idx The 0-based index of the event.
     * @return The name of the event.
     */
    getEventName(idx) {
        return this.EVENT_NAMES[idx];
    }

    /**
     * Returns a single-line textual representation of the model state.
     *
     * @return The single-line textual representation of the model state.
     */
    getStateText() {
        var state = unsupported_simulink_warningsUtils.fmt('time=%s', unsupported_simulink_warnings.time);
        state += unsupported_simulink_warningsUtils.fmt(', a.d1=%s', unsupported_simulink_warningsUtils.valueToStr(unsupported_simulink_warnings.a_d1_));
        state += unsupported_simulink_warningsUtils.fmt(', a.d2=%s', unsupported_simulink_warningsUtils.valueToStr(unsupported_simulink_warnings.a_d2_));
        state += unsupported_simulink_warningsUtils.fmt(', a.d3=%s', unsupported_simulink_warningsUtils.valueToStr(unsupported_simulink_warnings.a_d3_));
        state += unsupported_simulink_warningsUtils.fmt(', a.d4=%s', unsupported_simulink_warningsUtils.valueToStr(unsupported_simulink_warnings.a_d4_));
        state += unsupported_simulink_warningsUtils.fmt(', a.d5=%s', unsupported_simulink_warningsUtils.valueToStr(unsupported_simulink_warnings.a_d5_));
        state += unsupported_simulink_warningsUtils.fmt(', a.d6=%s', unsupported_simulink_warningsUtils.valueToStr(unsupported_simulink_warnings.a_d6_));
        state += unsupported_simulink_warningsUtils.fmt(', a.d7=%s', unsupported_simulink_warningsUtils.valueToStr(unsupported_simulink_warnings.a_d7_));
        return state;
    }


    /**
     * Evaluates algebraic variable "a1".
     *
     * @return The evaluation result.
     * @throws {unsupported_simulink_warningsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    a1_() {
        try {
            return new CifTuple_T2II(1, 2);
        } catch (e) {
            if (e instanceof unsupported_simulink_warningsException) {
                e = new unsupported_simulink_warningsException("Failed to evaluate algebraic variable \"a1\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "a2".
     *
     * @return The evaluation result.
     * @throws {unsupported_simulink_warningsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    a2_() {
        try {
            return "a";
        } catch (e) {
            if (e instanceof unsupported_simulink_warningsException) {
                e = new unsupported_simulink_warningsException("Failed to evaluate algebraic variable \"a2\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "a3".
     *
     * @return The evaluation result.
     * @throws {unsupported_simulink_warningsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    a3_() {
        try {
            return [new CifTuple_T2II(1, 2)];
        } catch (e) {
            if (e instanceof unsupported_simulink_warningsException) {
                e = new unsupported_simulink_warningsException("Failed to evaluate algebraic variable \"a3\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "a4".
     *
     * @return The evaluation result.
     * @throws {unsupported_simulink_warningsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    a4_() {
        try {
            return ["a"];
        } catch (e) {
            if (e instanceof unsupported_simulink_warningsException) {
                e = new unsupported_simulink_warningsException("Failed to evaluate algebraic variable \"a4\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "a5".
     *
     * @return The evaluation result.
     * @throws {unsupported_simulink_warningsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    a5_() {
        try {
            return [[new CifTuple_T2II(1, 2)]];
        } catch (e) {
            if (e instanceof unsupported_simulink_warningsException) {
                e = new unsupported_simulink_warningsException("Failed to evaluate algebraic variable \"a5\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "a6".
     *
     * @return The evaluation result.
     * @throws {unsupported_simulink_warningsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    a6_() {
        try {
            return [["a"]];
        } catch (e) {
            if (e instanceof unsupported_simulink_warningsException) {
                e = new unsupported_simulink_warningsException("Failed to evaluate algebraic variable \"a6\".", e);
            }
            throw e;
        }
    }

    /**
     * Evaluates algebraic variable "a7".
     *
     * @return The evaluation result.
     * @throws {unsupported_simulink_warningsException} In case of a runtime error caused by code
    *       generated from the CIF model.
     */
    a7_() {
        try {
            return [[[true]]];
        } catch (e) {
            if (e instanceof unsupported_simulink_warningsException) {
                e = new unsupported_simulink_warningsException("Failed to evaluate algebraic variable \"a7\".", e);
            }
            throw e;
        }
    }



    /**
     * Print output for all relevant print declarations.
     *
     * @param idx The 0-based event index of the transition, or '-2' for
     *      time transitions, or '-3' for the 'initial' transition.
     * @param pre Whether to print output for the pre/source state of the
     *      transition ('true') or for the post/target state of the
     *      transition ('false').
     * @throws {unsupported_simulink_warningsException} In case of a runtime error caused by code
     *      generated from the CIF model.
     */
    printOutput(idx, pre) {
        // No print declarations.
    }

    /**
     * Informs that new print output is available.
     *
     * @param text The text being printed.
     * @param target The file or special target to which text is to be printed.
     *      If printed to a file, an absolute or relative local file system
     *      path is given. Paths may contain both '/' and '\\'
     *      as file separators. Use {@link unsupported_simulink_warningsUtils#normalizePrintTarget}
     *      to normalize the path to use '/' file separators. There are two
     *      special targets: ':stdout' to print to the standard output stream,
     *      and ':stderr' to print to the standard error stream.
     */
    infoPrintOutput(text, target) {
        if (target == ':stdout') {
            unsupported_simulink_warnings.log(text);
        } else if (target == ':stderr') {
            unsupported_simulink_warnings.error(text);
        } else {
            var path = unsupported_simulink_warningsUtils.normalizePrintTarget(target);
            unsupported_simulink_warnings.infoPrintOutput(path + ': ' + text, ':stdout');
        }
    }
}
