//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.controllercheck.checks.boundedresponse;

import static org.eclipse.escet.common.java.Lists.last;
import static org.eclipse.escet.common.java.Strings.fmt;

import java.util.List;

import org.eclipse.escet.cif.controllercheck.checks.CheckConclusion;
import org.eclipse.escet.common.java.Assert;
import org.eclipse.escet.common.java.output.DebugNormalOutput;
import org.eclipse.escet.common.java.output.WarnOutput;

/** Conclusion of the bounded response check. */
public class BoundedResponseCheckConclusion implements CheckConclusion {
    /** The bound on the number of transitions that can be executed for uncontrollable events. */
    public final Bound uncontrollablesBound;

    /** The bound on the number of transitions that can be executed for controllable events. */
    public final Bound controllablesBound;

    /**
     * The maximum number of 'printed cycle states' to print. Value is non-negative and at most
     * {@code Integer.MAX_VALUE - 10}.
     */
    private final int maxPrintedCycleStatesCount;

    /**
     * Constructor for the {@link BoundedResponseCheckConclusion} class.
     *
     * @param uncontrollablesBound The bound on the number of transitions that can be executed for uncontrollable
     *     events.
     * @param controllablesBound The bound on the number of transitions that can be executed for controllable events.
     * @param maxPrintedCycleStatesCount The maximum number of 'printed cycle states' to print. Value must be
     *     non-negative and at most {@code Integer.MAX_VALUE - 10}.
     */
    public BoundedResponseCheckConclusion(Bound uncontrollablesBound, Bound controllablesBound,
            int maxPrintedCycleStatesCount)
    {
        this.uncontrollablesBound = uncontrollablesBound;
        this.controllablesBound = controllablesBound;
        this.maxPrintedCycleStatesCount = maxPrintedCycleStatesCount;

        if (!uncontrollablesBound.isBounded()) {
            List<String> states = uncontrollablesBound.getCycleStates();
            Assert.notNull(states);
            Assert.check(states.size() <= maxPrintedCycleStatesCount
                    || (states.size() == maxPrintedCycleStatesCount + 1 && last(states).equals("...")));
        }
        if (!controllablesBound.isBounded()) {
            List<String> states = controllablesBound.getCycleStates();
            Assert.notNull(states);
            Assert.check(states.size() <= maxPrintedCycleStatesCount
                    || (states.size() == maxPrintedCycleStatesCount + 1 && last(states).equals("...")));
        }
    }

    @Override
    public boolean propertyHolds() {
        return uncontrollablesBound.isBounded() && controllablesBound.isBounded();
    }

    @Override
    public boolean hasDetails() {
        return true;
    }

    @Override
    public void printResult(DebugNormalOutput out, WarnOutput warn) {
        if (!uncontrollablesBound.hasInitialState() || !controllablesBound.hasInitialState()) {
            warn.line("The specification cannot be initialized.");
        }

        if (propertyHolds()) {
            out.line("[OK] The specification has bounded response:");
        } else {
            out.line("[ERROR] The specification does NOT have bounded response:");
        }

        out.line();
        out.inc();

        printDetailsForBound(out, uncontrollablesBound, "uncontrollable");

        if (!uncontrollablesBound.isBounded()) {
            out.line();
        }

        printDetailsForBound(out, controllablesBound, "controllable");

        out.dec();
    }

    /**
     * Print details for the given bound.
     *
     * @param out Callback to send normal output to the user.
     * @param bound The bound.
     * @param eventKind The kind of events for which the bound is computed.
     */
    private void printDetailsForBound(DebugNormalOutput out, Bound bound, String eventKind) {
        if (bound.isBounded()) {
            int boundNr = bound.getBound();
            if (boundNr == 0) {
                out.line("- No transitions are possible for %s events.", eventKind);
            } else {
                out.line("- At most %,d iteration%s needed for the event loop for %s events.",
                        boundNr, (boundNr == 1) ? " is" : "s are", eventKind);
            }
        } else {
            out.line("- An infinite sequence of transitions is possible for %s events.", eventKind);

            out.line();
            out.line("  Edge%s enabled in cycle(s):", (bound.getCycleEdges().size() == 1) ? "" : "s");
            out.inc();
            for (String cycleEdge: bound.getCycleEdges().stream().sorted().toList()) {
                out.line(cycleEdge);
            }
            out.dec();

            out.line();
            if (maxPrintedCycleStatesCount > 0) {
                List<String> printedCycleStates = bound.getCycleStates();
                boolean complete = printedCycleStates.size() <= maxPrintedCycleStatesCount;
                String msg = complete ? "  States in cycle(s) at end of event loop executions:"
                        : fmt("  States in cycle(s) at end of event loop executions, limited to %,d 'printed cycle " +
                                "state%s':", maxPrintedCycleStatesCount, (maxPrintedCycleStatesCount == 1) ? "" : "s");
                out.line(msg);
                out.inc();
                for (String printedCycleState: printedCycleStates) {
                    out.line(printedCycleState);
                }
                out.dec();
            }
        }
    }
}
