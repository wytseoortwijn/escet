//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.controllercheck;

import static org.eclipse.escet.common.java.Lists.list;
import static org.eclipse.escet.common.java.Lists.listc;
import static org.eclipse.escet.common.java.Pair.pair;

import java.util.List;
import java.util.Set;

import org.eclipse.escet.cif.cif2cif.ElimComponentDefInst;
import org.eclipse.escet.cif.cif2cif.RemoveIoDecls;
import org.eclipse.escet.cif.common.CifCollectUtils;
import org.eclipse.escet.cif.common.CifEventUtils;
import org.eclipse.escet.cif.controllercheck.checks.CheckConclusion;
import org.eclipse.escet.cif.controllercheck.checks.ControllerCheckerCheck;
import org.eclipse.escet.cif.controllercheck.checks.boundedresponse.BoundedResponseCheck;
import org.eclipse.escet.cif.controllercheck.checks.confluence.ConfluenceCheck;
import org.eclipse.escet.cif.controllercheck.checks.finiteresponse.FiniteResponseCheck;
import org.eclipse.escet.cif.controllercheck.checks.nonblockingundercontrol.NonBlockingUnderControlCheck;
import org.eclipse.escet.cif.metamodel.cif.Specification;
import org.eclipse.escet.cif.metamodel.cif.declarations.Event;
import org.eclipse.escet.common.emf.EMFHelper;
import org.eclipse.escet.common.java.Pair;
import org.eclipse.escet.common.java.Strings;
import org.eclipse.escet.common.java.Termination;
import org.eclipse.escet.common.java.output.DebugNormalOutput;
import org.eclipse.escet.common.java.output.WarnOutput;

/** Controller properties checker. */
public class ControllerChecker {
    /** Constructor for the {@link ControllerChecker} class. */
    private ControllerChecker() {
        // Static class.
    }

    /**
     * Perform controller properties checks. At least one check must be enabled.
     *
     * @param spec The CIF specification to check.
     * @param specAbsPath The absolute local file system path to the CIF specification to check.
     * @param settings The settings to use.
     * @return The controller properties checker result, or {@code null} if termination was requested.
     */
    public static ControllerCheckerResult performChecks(Specification spec, String specAbsPath,
            ControllerCheckerSettings settings)
    {
        // Check settings.
        settings.check();

        // Get some settings.
        Termination termination = settings.getTermination();
        DebugNormalOutput normalOutput = settings.getNormalOutput();
        DebugNormalOutput debugOutput = settings.getDebugOutput();
        WarnOutput warnOutput = settings.getWarnOutput();

        // Preprocess and check the specification.
        spec = preprocessAndCheck(spec, specAbsPath, termination, warnOutput);

        // Get checks to perform.
        List<ControllerCheckerCheck<?>> checksToPerform = listc(4);
        if (settings.getCheckBoundedResponse()) {
            checksToPerform.add(new BoundedResponseCheck(settings.getBoundedResponseMaxPrintedCycleStates()));
        }
        if (settings.getCheckNonBlockingUnderControl()) {
            checksToPerform.add(new NonBlockingUnderControlCheck());
        }
        if (settings.getCheckFiniteResponse()) {
            checksToPerform.add(new FiniteResponseCheck(settings.getPrintFiniteResponseControlLoops()));
        }
        if (settings.getCheckConfluence()) {
            checksToPerform.add(new ConfluenceCheck());
        }

        // Perform the checks.
        int checksPerformed = 0;
        List<CheckConclusion> conclusions = listc(checksToPerform.size());
        for (int i = 0; i < checksToPerform.size(); i++) {
            // Get check to perform.
            ControllerCheckerCheck<?> check = checksToPerform.get(i);

            // Output check being performed.
            if (checksPerformed > 0) {
                normalOutput.line();
            }
            normalOutput.line("Checking for %s:", check.getPropertyName());

            // Perform check.
            debugOutput.inc();

            CheckConclusion conclusion = check.performCheck(spec, specAbsPath, settings);

            debugOutput.dec();

            if (conclusion == null || termination.isRequested()) {
                return null;
            }

            // Performed one more check.
            checksPerformed++;
            conclusions.add(conclusion);
        }

        // Construct the result.
        ControllerCheckerResult result = new ControllerCheckerResult(conclusions);

        // Output the checker conclusions.
        normalOutput.line();
        normalOutput.line("CONCLUSION:");

        List<Pair<String, CheckConclusion>> namedConclusions = list();
        namedConclusions.add(pair(BoundedResponseCheck.PROPERTY_NAME, result.boundedResponseConclusion));
        namedConclusions.add(
                pair(NonBlockingUnderControlCheck.PROPERTY_NAME, result.nonBlockingUnderControlConclusion));
        namedConclusions.add(pair(FiniteResponseCheck.PROPERTY_NAME, result.finiteResponseConclusion));
        namedConclusions.add(pair(ConfluenceCheck.PROPERTY_NAME, result.confluenceConclusion));

        for (int i = 0; i < namedConclusions.size(); i++) {
            // Get information on current and next conclusion.
            boolean isFirst = (i == 0);

            Pair<String, CheckConclusion> current = namedConclusions.get(i);
            Pair<String, CheckConclusion> previous = isFirst ? null : namedConclusions.get(i - 1);

            String name = current.left;
            CheckConclusion conclusion = current.right;
            CheckConclusion previousConclusion = (previous == null) ? null : previous.right;
            boolean conclusionHasDetails = conclusion != null && conclusion.hasDetails();
            boolean previousConclusionHasDetails = previousConclusion != null && previousConclusion.hasDetails();

            // Empty line between this conclusions and the previous one, if either of them prints details.
            if (!isFirst && (previousConclusionHasDetails || conclusionHasDetails)) {
                normalOutput.line();
            }

            // Output current conclusion.
            normalOutput.inc();
            if (conclusion != null) {
                conclusion.printResult(normalOutput, warnOutput);
            } else {
                normalOutput.line("[UNKNOWN] %s checking was disabled, %s property is unknown.",
                        Strings.makeInitialUppercase(name), name);
            }
            normalOutput.dec();
        }

        // Return the result.
        return result;
    }

    /**
     * Preprocess and check the input specification of the controller properties checker.
     *
     * @param spec The specification to preprocess and check. Is modified in-place, but not as much as the result of
     *     this method.
     * @param specAbsPath The absolute local file system path to the CIF specification to check.
     * @param termination Cooperative termination query function.
     * @param warnOutput Callback to send warnings to the user.
     * @return The preprocessed and checked specification, or {@code null} if termination was requested.
     */
    private static Specification preprocessAndCheck(Specification spec, String specAbsPath,
            Termination termination, WarnOutput warnOutput)
    {
        // Eliminate component definition/instantiation. This allows to perform precondition checks, as well as perform
        // annotation post checking.
        new ElimComponentDefInst().transform(spec);

        // Get the output specification, and the internal specification on which to perform the checks.
        // Copy the internal specification, to preserve the output specification.
        spec = EMFHelper.deepclone(spec);

        // Remove/ignore I/O declarations, to increase the supported subset.
        RemoveIoDecls removeIoDecls = new RemoveIoDecls();
        removeIoDecls.transform(spec);
        if (removeIoDecls.haveAnySvgInputDeclarationsBeenRemoved()) {
            warnOutput.line("The specification contains CIF/SVG input declarations. These will be ignored.");
        }

        // Check preconditions that apply to all checks.
        ControllerCheckerPreChecker checker = new ControllerCheckerPreChecker(termination);
        checker.reportPreconditionViolations(spec, specAbsPath, "CIF controller properties checker");
        if (termination.isRequested()) {
            return null;
        }

        // Warn if specification doesn't look very useful:
        // - Due to preconditions, all events have controllability, but check for none of them being (un)controllable.
        Set<Event> specAlphabet = CifEventUtils.getAlphabet(spec);
        if (specAlphabet.stream().allMatch(e -> !e.getControllable())) {
            warnOutput.line("The alphabet of the specification contains no controllable events.");
        }
        if (specAlphabet.stream().allMatch(e -> e.getControllable())) {
            warnOutput.line("The alphabet of the specification contains no uncontrollable events.");
        }

        // Warn if specification doesn't look very useful:
        // - Check for no input variables being present, to detect absence of a hardware mapping connecting
        //   uncontrollable events to input variables.
        if (CifCollectUtils.collectInputVariables(spec, list()).isEmpty()) {
            warnOutput.line("The specification contains no input variables.");
        }

        // Return the preprocessed and checked specification.
        return spec;
    }
}
