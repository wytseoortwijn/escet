//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.controllercheck.options;

import org.eclipse.escet.common.app.framework.options.IntegerOption;
import org.eclipse.escet.common.app.framework.options.Options;

/** Option to specify the maximum number of 'printed cycle states' to print for bounded response. */
public class MaxPrintedCycleStatesOption extends IntegerOption {
    /** Constructor for the {@link MaxPrintedCycleStatesOption} class. */
    public MaxPrintedCycleStatesOption() {
        super(
                // name
                "Maximum number of printed cycle states",

                // description
                "The maximum number of 'printed cycle states' to print to the console, " +
                        "in case bounded response doesn't hold due to cycles. "
                        + "The integer number must be non-negative and at most 2,147,483,637. [DEFAULT=10]",

                // cmdShort
                null,

                // cmdLong
                "max-cycle-states",

                // cmdValue
                "COUNT",

                // defaultValue
                10,

                // minimumValue
                0,

                // maximumValue
                Integer.MAX_VALUE - 10,

                // pageIncrementValue
                100,

                // showInDialog
                true,

                // optDialogDescr
                "The maximum number of 'printed cycle states' to print to the console, " +
                        "in case bounded response doesn't hold due to cycles.",

                // optDialogLabelText
                "Maximum number of 'printed cycle states':");
    }

    /**
     * Returns the maximum number of 'printed cycle states' to print, in case bounded response doesn't hold due to
     * cycles.
     *
     * @return The maximum number of 'printed cycle states' to print.
     */
    public static int getMaxPrintedCycleStatesCount() {
        return Options.get(MaxPrintedCycleStatesOption.class);
    }
}
