//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.multilevel.options;

import org.eclipse.escet.common.app.framework.Paths;
import org.eclipse.escet.common.app.framework.options.InputFileOption;
import org.eclipse.escet.common.app.framework.options.Options;
import org.eclipse.escet.common.app.framework.options.StringOption;
import org.eclipse.escet.common.java.PathPair;

/** Output directory option for the partial specifications. */
public class PartialSpecsOutputDirectoryOption extends StringOption {
    /** Description of the option. */
    private static final String DESCRIPTION = "The path to the output directory for all output files. "
            + "If no path is specified, a path is constructed from the name of the input specification by removing the "
            + "\".cif\" extension if available, and appending \"_multilevel\".";

    /**
     * Constructor for the {@link PartialSpecsOutputDirectoryOption} class. Don't directly create instances of this
     * class. Use the {@link Options#getInstance} method instead.
     */
    public PartialSpecsOutputDirectoryOption() {
        super("Partial specifications output directory", DESCRIPTION, null, "specs-dir", "PATH", null, true, true,
                DESCRIPTION, "Directory path:");
    }

    /**
     * Returns the path pair of the partial specifications output directory.
     *
     * @return The path pair of the partial specifications output directory.
     */
    public static PathPair getPath() {
        String path = Options.get(PartialSpecsOutputDirectoryOption.class);
        if (path == null) {
            path = InputFileOption.getDerivedPath(".cif", "_multilevel");
        }
        return new PathPair(path, Paths.resolve(path));
    }
}
