//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2023, 2024 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

package org.eclipse.escet.cif.multilevel.clustering;

import static org.eclipse.escet.common.java.Lists.list;

import java.util.BitSet;
import java.util.List;

import org.eclipse.escet.cif.metamodel.cif.Specification;
import org.eclipse.escet.cif.multilevel.ciftodmm.CifRelations;
import org.eclipse.escet.common.box.CodeBox;
import org.eclipse.escet.common.box.MemoryCodeBox;

/** Node in a multi-level synthesis tree. */
public class TreeNode {
    /** The plant groups that are included in the tree node. */
    public final BitSet plantGroups;

    /** The requirement groups that are included in the tree node. */
    public final BitSet requirementGroups;

    /** The child nodes of the tree node. Creates a node that is initially empty. */
    public final List<TreeNode> childNodes = list();

    /** Name of the node in the hierarchy. Is {@code null} until it is set in  {@link #linearizeTree}. */
    public String hierarchicalName = null;

    /** CIF specification of the node. Is {@code null} until it is set. */
    public Specification partialSpec = null;

    /** Constructor of the {@link TreeNode} class. */
    public TreeNode() {
        this(new BitSet(), new BitSet());
    }

    /**
     * Constructor of the {@link TreeNode} class. Creates a node that initially has no children.
     *
     * @param plantGroups The plant groups that are included in the tree node.
     * @param requirementGroups The requirement groups that are included in the tree node.
     */
    public TreeNode(BitSet plantGroups, BitSet requirementGroups) {
        this.plantGroups = plantGroups;
        this.requirementGroups = requirementGroups;
    }

    /**
     * Walk recursively depth-first left-to-right through the node tree rooted at this node, collect all encountered
     * nodes, and return them as a list of nodes. Also sets {@link #hierarchicalName} for each node in the tree.
     *
     * @return All encountered nodes in the tree rooted at this node.
     */
    public List<TreeNode> linearizeTree() {
        return linearizeTree(this, "partialSpec", list());
    }

    /**
     * Walk recursively depth-first left-to-right through the node tree, and add all encountered nodes to the result
     * list. Also set the {@link #hierarchicalName} for each node in the tree.
     *
     * @param node The node to start the walk.
     * @param hierarchicalName Name of {@code node} in the tree.
     * @param nodes Storage for encountered nodes, {@code node} is assumed not to be in the storage yet. Is extended
     *     in-place.
     * @return The encountered nodes including the (sub)tree rooted at {@code node}.
     */
    private static List<TreeNode> linearizeTree(TreeNode node, String hierarchicalName, List<TreeNode> nodes) {
        // Store the hierarchical name.
        node.hierarchicalName = hierarchicalName;
        nodes.add(node);

        // Explore child nodes for further naming.
        int index = 1;
        for (TreeNode child: node.childNodes) {
            linearizeTree(child, hierarchicalName + "_" + index, nodes);
            index++;
        }
        return nodes;
    }

    /**
     * Create a pretty-printed description of the content of the node.
     *
     * @param cifRelations Converter from group numbers to CIF plant and requirement names.
     * @return The produced description.
     */
    public CodeBox toBox(CifRelations cifRelations) {
        // Create output storage and add the CIF name of the node.
        CodeBox box = new MemoryCodeBox();
        box.add("Partial specification \"%s.cif\":", hierarchicalName);
        box.indent();

        // List plants of the node.
        box.add("Plants:");
        box.indent();
        if (plantGroups.isEmpty()) {
            box.add("None.");
        } else {
            for (String plantName: cifRelations.getSortedPlantNames(plantGroups)) {
                box.add("- Plant \"%s\".", plantName);
            }
        }
        box.dedent();
        box.add();

        // List requirements of the node.
        box.add("Requirements:");
        box.indent();
        if (requirementGroups.isEmpty()) {
            box.add("None.");
        } else {
            for (String reqName: cifRelations.getSortedRequirementNames(requirementGroups)) {
                box.add("- Requirement \"%s\".", reqName);
            }
        }
        box.dedent();
        box.add();

        // List child node specification names.
        box.add("Children of the partial specification:");
        box.indent();
        if (childNodes.isEmpty()) {
            box.add("None.");
        } else {
            for (TreeNode child: childNodes) {
                box.add("- Partial specification \"%s.cif\".", child.hierarchicalName);
            }
        }
        box.dedent();

        // And finish, returning the produced text.
        return box;
    }
}
